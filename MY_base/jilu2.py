#索引
create unique index "wgmf_place_order_line_unique" on wgmf_place_order_line (
   "place_id" DESC,
   "product_id" ASC
)
create unique index "wgmf_sale_shop_name" on sale_shop ("name")
#价格不一致
select   a.id,b.id,b.name,f.default_code,g.name,a.price,e.price,b.sale_date 
from pos_superorder_line a
 left join pos_superorder b on b.id=a.superorder_id
 left join sale_shop c on c.id=b.shop_id
 left join wgmf_product_directory d on d.id=c.product_directory
 left join wgmf_product_directory_line e on e.directory_id=d.id 
 left join product_product f on f.id=a.product_id
 left join product_template g on g.id=f.product_tmpl_id
where a.product_id =e.product_id  and a.price!=e.price and a.price!=0 and e.price=0 and f.default_code!='9.8888' order by f.id ,b.sale_date 


#产品目录没有的
select   a.id,b.id,b.name,f.default_code,g.name,a.price,b.sale_date 
from pos_superorder_line a
 left join pos_superorder b on b.id=a.superorder_id
 left join sale_shop c on c.id=b.shop_id
 left join wgmf_product_directory d on d.id=c.product_directory

 left join product_product f on f.id=a.product_id
 left join product_template g on g.id=f.product_tmpl_id
where not exists (select 1 from wgmf_product_directory_line where product_id=a.product_id)  
 and a.price!=0 and f.default_code!='9.8888' and f.default_code!='2.40091'
 and not exists (select 1 from mrp_bom where product_id=a.product_id and bom_id is null) 
 
 
 
 #外购入库
select b.default_code,c.name,sum(a.product_qty) from stock_move a 
left join product_product b on b.id=a.product_id
left join product_template c on c.id=b.product_tmpl_id
where a.location_id=89 and a.location_dest_id=12 and a.state='done' and a.date>='2015-04-30 16:00:00' and a.date<'2015-05-31 16:00:00'
group by b.id,c.id order by b.default_code
#字符串分割

select substr('zhang$lisheng@',1,position('a' in 'zhangalisheng$')-1)


#外购入库
with date_in as 
(select b.id,c.standard_price,b.default_code,-sum(a.product_qty) as qty from stock_move a 
left join product_product b on b.id=a.product_id
left join product_template c on c.id=b.product_tmpl_id
where a.location_id=89 and a.location_dest_id=12 and a.state='done' 
and a.date>='2015-04-30 16:00:00' and a.date<'2015-05-02 18:00:00' and  b.id=171
group by b.id,c.id 
union all
select b.id,c.standard_price,b.default_code,sum(a.product_qty) as qty from stock_move a 
left join product_product b on b.id=a.product_id
left join product_template c on c.id=b.product_tmpl_id
where a.location_id=12 and a.location_dest_id=89 and a.state='done' 
and a.date>='2015-04-30 16:00:00' and a.date<'2015-05-01 09:00:00' and b.id=171
group by b.id,c.id ) 

select id ,standard_price, sum(qty) from date_in group by id,standard_price



