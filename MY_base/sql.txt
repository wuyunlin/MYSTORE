with myshop as(
 select to_char(saledate,'yyyymmdd') as saledate,shop_id,xtnet,billnum from wgmf_sale_check where yn_hd='1'
),
mysale as(
select pos_superorder.shop_id,
       myshop.saledate,
       myshop.billnum,
       count(pos_superorder.id) as order_count,
       sum(coalesce(pos_superorder.amount_total, 0)) as pos_net,
       myshop.xtnet as hd_net
  from pos_superorder
 inner join myshop
    on (pos_superorder.shop_id = myshop.shop_id and
       to_char(pos_superorder.sale_date + interval '8 hours', 'yyyymmdd') =
       myshop.saledate)
 group by pos_superorder.shop_id, myshop.saledate, myshop.xtnet, myshop.billnum
)
select mysale.shop_id,
       sale_shop.stores_no,
       sale_shop.name,
       mysale.saledate,
       mysale.order_count as "销售汇总单数",
       mysale.pos_net as "销售汇总金额",
       mysale.hd_net as "日销售已核对金额",
       mysale.billnum as "已审核单数"
  from mysale
  left outer join sale_shop
    on (mysale.shop_id = sale_shop.id)
 where mysale.pos_net <> mysale.hd_net order by mysale.saledate


--select * from pos_superorder limit 10
