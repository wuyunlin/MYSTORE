#办事处借款 办事处押金的职员部门有问题的
select am.name as 凭证号,rp1.code as  职员编码,rp1.name as 职员名称,rp2.code as 部门编码,rp2.name as 部门名称 from account_move_line line 
left join res_partner rp1 on rp1.id=line.partner_id
left join res_partner rp2 on rp2.id=rp1.parent_id
left join account_move am on am.id=line.move_id
where (rp2.code is null or rp1.code is null) and rp1.id is not null and line.account_id in (536,537)

#删除销售单日志
select * from pos_superorder_log

#库位错误查询sql
select sm.id,sm.date,sm.origin,sm.*  from stock_move sm 
left join stock_location sl on sl.id=sm.location_id 
left join stock_location sl2 on sl2.id=sm.location_dest_id 
where sm.date>='2015-04-30 16:00:00' and sl.name='出库' ---and sl2.name!='库存' and state='done'
297+11
#查询数量*价格!=小计
select id from pos_superorder_line 
where price_subtotal!=price*qty and create_date>='2015-05-01 00:00:00'
#
select min(id) as 最小值,max(id) as 最大值,max(id)+1-min(id) as 差值,count(*) as 销售单数, max(id)+1-min(id)-count(*) from pos_superorder 
where create_date>='2015-06-04 16:00:00' and create_date<='2015-06-06 16:00:00'

#查询日审核与销售是否一致
with myshop as(
 select to_char(saledate,'yyyymmdd') as saledate,shop_id,xtnet,billnum from wgmf_sale_check where yn_hd='1' and saledate>='2015-05-21 16:00:00'  
),
mysale as(
select pos_superorder.shop_id,
       myshop.saledate,
       myshop.billnum,
       count(pos_superorder.id) as order_count,
       sum(coalesce(pos_superorder.amount_total, 0)) as pos_net,
       myshop.xtnet as hd_net
  from pos_superorder
 inner join myshop
    on (pos_superorder.shop_id = myshop.shop_id and
       to_char(pos_superorder.sale_date + interval '8 hours', 'yyyymmdd') =
       myshop.saledate) where approve_stage='done'
 group by pos_superorder.shop_id, myshop.saledate, myshop.xtnet, myshop.billnum
)
select mysale.shop_id,
       sale_shop.stores_no,
       sale_shop.name,
       mysale.saledate,
       mysale.order_count ,
       mysale.pos_net,
       mysale.hd_net,
       mysale.billnum 
  from mysale
  left outer join sale_shop
    on (mysale.shop_id = sale_shop.id)
 where mysale.pos_net <> mysale.hd_net order by mysale.saledate
 
 
 
 #销售单成本
with all_res as
(
---套装合计
with res as (select b.id,b.default_code,sum(a.qty) as qty from pos_superorder_line a 
left join product_product b on b.id=a.product_id
---left join sale_shop c on c.id=a.shop_id
left join pos_superorder d on d.id=a.superorder_id
where d.sale_date>='2015-04-30 16:00:00' 
and d.sale_date<'2015-05-31 16:00:00' and a.price=0 ---and c.state='approved'
and a.product_id  in (select product_id from mrp_bom where bom_id is null)
group by b.id )

select c.default_code,sum(res.qty*a.product_qty ) as qty
from mrp_bom a
left join mrp_bom b on b.id =a.bom_id
left join product_product c on c.id =a.product_id
inner join res on b.product_id=res.id
group by c.id---order by c.default_code
union all
--单品销售
select b.default_code,sum(a.qty) as qty from pos_superorder_line a 
left join product_product b on b.id=a.product_id
---left join sale_shop c on a.shop_id=c.id
inner join pos_superorder d on d.id=a.superorder_id
where d.sale_date>='2015-04-30 16:00:00' 
 and d.sale_date<'2015-05-31 16:00:00'  ---and a.price=0  ---and c.state='approved'
 and a.product_id not in (select product_id from mrp_bom where bom_id is null)
group by b.id,d.id) 


select b.default_code,sum(all_res.qty),a.price_tzq,sum(a.price_tzq*all_res.qty) from standard_months_price a 
left join product_product b on a.product_id=b.id
inner join all_res on all_res.default_code=b.default_code
where a.months=32 
group by b.id,a.id  order by b.default_code

 #核对按店的单品差异
 with all_res as
(
---套装合计
with res as (select c.stores_no,b.id,b.default_code,sum(a.qty) as qty from pos_superorder_line a 
left join product_product b on b.id=a.product_id
left join sale_shop c on c.id=a.shop_id
left join pos_superorder d on d.id=a.superorder_id
where d.sale_date>='2015-04-30 16:00:00' 
and d.sale_date<'2015-05-31 16:00:00' and a.price=0 ---and c.state='approved'
and a.product_id  in (select product_id from mrp_bom where bom_id is null)
group by b.id,c.id )

select res.stores_no,c.default_code,sum(res.qty*a.product_qty ) as qty
from mrp_bom a
left join mrp_bom b on b.id =a.bom_id
left join product_product c on c.id =a.product_id
inner join res on b.product_id=res.id
group by c.id,res.stores_no---order by c.default_code
union all
--单品销售
select c.stores_no,b.default_code,sum(a.qty) as qty from pos_superorder_line a 
left join product_product b on b.id=a.product_id
left join sale_shop c on a.shop_id=c.id
inner join pos_superorder d on d.id=a.superorder_id
where d.sale_date>='2015-04-30 16:00:00' 
 and d.sale_date<'2015-05-31 16:00:00'  ---and a.price=0  ---and c.state='approved'
 and a.product_id not in (select product_id from mrp_bom where bom_id is null)
group by b.id,c.stores_no) 


select all_res.stores_no,b.default_code,sum(all_res.qty),a.price_tzq,sum(a.price_tzq*all_res.qty) from standard_months_price a 
left join product_product b on a.product_id=b.id
inner join all_res on all_res.default_code=b.default_code
where a.months=32 and all_res.default_code='2.40010'
group by all_res.stores_no,b.id,a.id  order by all_res.stores_no
 
 
 #重复出库单
 select id from stock_picking where id in 

(
with res as (
select min(id) as id,origin,count(*) as cc ,date  from stock_picking where id in (
select distinct picking_id 
from stock_move 
where date>='2015-05-31 16:00:00' and date<'2015-06-30 16:00:00' and state='done' 
and (location_dest_id=9 or location_id=9)
) group by origin,date having(count(*)>1) 
)
---select count(*),sum(cc) from res 

select a.id from stock_picking a
inner join res on res.origin=a.origin 
where res.id!=a.id
order by a.date desc)
 
 
 
 