3.3.1	安装命令
1）	安装OpenERP必要的Python libraries
sudo apt-get install python-dateutil python-docutils python-feedparser python-gdata  python-jinja2 python-ldap python-libxslt1 python-lxml python-mako python-mock python-openid  python-psycopg2 python-psutil python-pybabel python-pychart python-pydot python-pyparsing  python-reportlab python-simplejson python-tz python-unittest2 python-vatnumber python-vobject  python-webdav python-werkzeug python-xlwt python-yaml python-zsi python-pyPdf





sudo apt-get install python-lxml python-mako python-egenix-mxdatetime python-dateutil python-psycopg2 python-pychart python-pydot python-tz python-reportlab python-yaml python-ldap libldap2-dev libsasl2-dev python-vobject python-dev –y

apt-get install python-uno
apt-get install openoffice.org-calc
apt-get install openoffice.org-core	
apt-get install openoffice.org-headless
apt-get install openoffice.org-writer

//***** Ubuntu 14.04 不能用以上命令安装，用apt-get install libreoffice-script-provider-python

//add by sai.he 2014.6.27 
apt-get install python-xlwt


2）创建用户 openerp 用于执行OpenERP及Postgres数据库访问
adduser --system --home=/opt/openerp --group openerp --shell /bin/bash
passwd openerp
输入两遍密码：openerppwd@88oscg

3) 增加OpenERP数据库用户
sudo -u postgres /usr/lib/postgresql/9.2/bin/createuser --createdb --username postgres --no-createrole --no-superuser --pwprompt openerp
  Enter password for new role: postgres
  Enter it again: postgres

4) 创建LOG目录并给予openerp访问权限
mkdir /var/log/openerp
chown openerp:root /var/log/openerp

5) 创建新增packagings存放目录
mkdir /opt/packagings
cd /opt/packagings

6) 安装Web框架包Werkzeug 0.8.1：
sudo wget http://pypi.python.org/packages/source/W/Werkzeug/Werkzeug-0.8.3.tar.gz
sudo tar -xzf Werkzeug-0.8.3.tar.gz
cd Werkzeug-0.8.3
sudo python setup.py install
cd ..

7）安装python-openid 2.2.5：
sudo wget http://pypi.python.org/packages/source/p/python-openid/python-openid-2.2.5.tar.gz
sudo tar -xzf python-openid-2.2.5.tar.gz
cd python-openid-2.2.5
sudo python setup.py install
cd ..

8）安装Babel 0.9.6：
sudo wget http://pypi.python.org/packages/source/B/Babel/Babel-0.9.6.tar.gz
sudo tar -xzf Babel-0.9.6.tar.gz
cd Babel-0.9.6
sudo python setup.py install
cd ..

9）安装Jinja
sudo wget http://pypi.python.org/packages/source/s/setuptools/setuptools-0.6c11.tar.gz
sudo tar zxvf setuptools-0.6c11.tar.gz	
cd setuptools-0.6c11
sudo python setup.py build
sudo python setup.py install
cd ..

sudo wget http://pypi.python.org/packages/source/J/Jinja2/Jinja2-2.6.tar.gz#md5=1c49a8825c993bfdcf55bb36897d28a2
sudo tar -xzf Jinja2-2.6.tar.gz
cd Jinja2-2.6
sudo python setup.py install
cd ..

//新增 unittest2的安装

sudo wget https://pypi.python.org/packages/source/u/unittest2/unittest2-0.5.1.tar.gz#md5=a0af5cac92bbbfa0c3b0e99571390e0f
sudo tar -xzf unittest2-0.5.1.tar.gz
cd unittest2-0.5.1
sudo python setup.py install
cd ..

//新增mock
sudo wget https://pypi.python.org/packages/source/m/mock/mock-1.0.1.tar.gz#md5=c3971991738caa55ec7c356bbc154ee2
sudo tar -xzf  mock-1.0.1.tar.gz   
cd mock-1.0.1
sudo python setup.py install
cd ..

//新增
sudo wget https://pypi.python.org/packages/source/d/docutils/docutils-0.11.tar.gz#md5=20ac380a18b369824276864d98ec0ad6

sudo tar -xzf  docutils-0.11.tar.gz 

cd  docutils-0.11

sudo python setup.py install
cd ..
/////
sudo wget  https://pypi.python.org/packages/source/s/simplejson/simplejson-3.4.0.tar.gz
sudo tar -xzf simplejson-3.4.0.tar.gz
cd simplejson-3.4.0/
sudo python setup.py install
cd ..

13) 安装aeroo报表引擎  //先安装完，再安装oe
cd /opt/packagings/
sudo wget http://ftp.edgewall.com/pub/genshi/Genshi-0.6.tar.gz
sudo tar -xzf Genshi-0.6.tar.gz
cd Genshi-0.6
sudo python setup.py install
cd ..
//新增
sudo wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py
sudo python ez_setup.py install

sudo apt-get install bzr
sudo bzr branch https://launchpad.net/aeroolib
cd aeroolib/aeroolib
sudo python setup.py install
cd ../..

//////8.0包
sudo wget  https://pypi.python.org/packages/source/d/decorator/decorator-3.4.0.tar.gz
sudo tar -xzf decorator-3.4.0.tar.gz
cd decorator-3.4.0/
sudo python setup.py install
cd ..
//////安装requests的包
apt-get install python-pip

sudo pip install requests


wget https://pypi.python.org/packages/source/g/gevent/gevent-1.0.1.tar.gz
tar zxvf gevent-1.0.1.tar.gz
cd gevent-1.0.1
sudo python setup.py install
cd ..


wget https://pypi.python.org/packages/source/p/psutil/psutil-2.1.1.tar.gz#md5=72a6b15d589fab11f6ca245b775bc3c6
sudo tar -xzf psutil-2.1.1.tar.gz
cd psutil-2.1.1
sudo python setup.py install
cd ..

安装wkhtmltopdf.org
wget http://downloads.sourceforge.net/project/wkhtmltopdf/0.12.1/wkhtmltox-0.12.1_linux-precise-amd64.deb
dpkg -i wkhtmltox-0.12.1_linux-precise-amd64.deb
检测： wkhtmltopdf http://www.baidu.com baidu.pdf


10）下载配置安装OpenERP

cd /opt/openerp
wget http://nightly.openerp.com/8.0/nightly/src/odoo_8.0-latest.tar.gz
tar zxvf odoo_8.0rc1-20140806-192536.tar.gz
mv openerp-8.0rc1 server
chown -R openerp: *
chmod -R u+x *

11）在server目录下增加OpenERP配置文件
vi /opt/openerp/server/openerp-server.conf，注意要增加一个addons的目录指定。

[options]
; This is the password that allows database operations:
admin_passwd = v8_oscg
db_host = False
db_port = False
db_user = openerp
db_password = False
log_level = debug
logfile = /var/log/openerp/openerp-server.log
addons_path = /opt/openerp/server/openerp/addons

12) 启动OpenERP
python /opt/openerp/server/openerp-server -c /opt/openerp/server/openerp-server.conf&
