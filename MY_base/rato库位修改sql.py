update stock_move sm set product_qty = 3516 from product_product pp where sm.product_id=pp.id and sm.picking_id = 1252 and product_id = 200

update mrp_production set location_dest_id = 25 where origin like 'SO059%';
update mrp_production set work_location = 25 where origin like 'SO059%';
update mrp_production set work_location = location_dest_id where origin like 'SO059%';
update mrp_production set routing_id = 


update stock_move set location_dest_id = 25 where location_dest_id = 13
update stock_move set location_id = 25 where location_id = 13

修改工艺路线
update mrp_production set routing_id = 752 where id = 2487
update mrp_production set routing_id = 753 where id = 2484
update mrp_production set routing_id = 757 where id = 1992
update mrp_production set routing_id = 756 where id = 965
update mrp_production set routing_id = 782 where id = 963
update mrp_production set routing_id = 755 where id = 964
update mrp_production set routing_id = 758 where id = 958
update mrp_production set routing_id = 754 where id = 957
update mrp_production set routing_id = 759 where id = 952

修改SO059上错误的库位
update stock_move sm set location_id = 12 from stock_picking sp where sm.picking_id = sp.id and sp.id = 3293
update stock_move sm set location_id = 12 from stock_picking sp where sm.picking_id = sp.id and sp.id = 3281


修改产品的单位及其bom上的单位与stock_move上的单位(裸铜线）
1、update product_template pt set uom_id =8 from product_product pp where pp.product_tmpl_id = pt.id and pp.id = 252;
2、update product_template pt set uom_po_id =8 from product_product pp where pp.product_tmpl_id = pt.id and pp.id = 252;
3、update mrp_bom m set product_uom = pt.uom_id from product_product pp,product_template pt where m.product_id = pp.id and pp.product_tmpl_id = pt.id and pp.id = 252
4、update stock_move m set product_uom=uom from (select p.id as id, t.uom_id as uom, t.uos_id as uos from product_product p left join product_template t on (p.product_tmpl_id = t.id)) p where m.product_id = p.id and p.id =252

修改产品的单位及其bom上的单位与stock_move上的单位(MA300198）
1、update product_template pt set uom_id =1 from product_product pp where pp.product_tmpl_id = pt.id and pp.id = 329;
2、update product_template pt set uom_po_id =1 from product_product pp where pp.product_tmpl_id = pt.id and pp.id = 329;
3、update mrp_bom m set product_uom = pt.uom_id from product_product pp,product_template pt where m.product_id = pp.id and pp.product_tmpl_id = pt.id and pp.id = 329

查找uom_id和uom_po_id不一样的产品数量
select count(*) from product_template where uom_id != uom_po_id

查找uom_id和uom_po_id不一样的产品编码
select pp.default_code from product_product pp,product_template pt where pp.product_tmpl_id = pt.id and pt.uom_id != pt.uom_po_id;

查找bom上单位与产品上单位不一致的数量
select count(*) from mrp_bom m, product_product pp,product_template pt where m.product_id = pp.id and pp.product_tmpl_id = pt.id and m.product_uom != pt.uom_id

修改BOM上的单位与产品上默认单位一致
update mrp_bom m set product_uom = pt.uom_id from product_product pp,product_template pt where m.product_id = pp.id and pp.product_tmpl_id = pt.id

修改取消的MO单变为“Done”
update mrp_production set state = 'done' where id = 3952  （改mo单状态）
update stock_move set state = 'done' where production_id = 3952（改产成品状态）
update stock_move set state = 'done' where name = 'MO/03319' and state = 'cancel'  （改原料的stock_move状态）

重新安排产线
update mrp_production set routing_id = 335 where id = 6527   (修改工艺路线）
update mrp_production set work_location = 19 where id = 6527 (修改作业库位）
update mrp_production set location_dest_id = 19 where id = 6527 (修改成品库位）
update stock_move set location_dest_id = 19 where production_id = 6527 and location_id = 7 (修改产成品对应stock_move对应的目的库位）
update stock_move set location_id = 19 where name = 'MO/05898' and location_dest_id = 7 (修改原料对应stock_move对应的源库位）
update stock_move st set location_dest_id = 19 from stock_picking sp where st.picking_id = sp.id and sp.origin = 'RO682/05898:MO/05898' and st.location_dest_id = 17  （修改投料单以及计划单上的stock_move对应的目的库位）
update stock_move st set location_id = 14 from stock_picking sp where st.picking_id = sp.id and sp.origin = 'RO676/05667:MO/05667' and st.location_id = 13 （修改投料单以及计划单上的stock_move对应的源库位）
update stock_move set location_dest_id = 22 where name = 'MO/03510' and location_id = 7

删除一条下错了的so单
删除所有stock_move
delete from stock_move sm where sm.name in (select name from mrp_production mp where sm.name = mp.name and mp.origin like '%RO366%')
delete from stock_move where origin like '%RO366%'   
（出入库单的name字段为产品，上一条sql删除不掉）

删除所有mrp_production
delete from mrp_production where origin like '%RO366%'

删除所有stock_picking
delete from stock_picking where origin like '%RO366%'

删除所有采购单
delete from purchase_order where origin like '%RO366%'

删除补货单
delete from procurement_order where origin like '%RO366%'

删除销售单
delete from sale_order where name = 'RO366'


修改1pr 1ps试用语句

投料单
delete from stock_move sm where sm.picking_id in (select id from stock_picking sp where sp.parent_id=xxx)
 and sm.product_id=281;
 
已投料数量
delete from stock_move where name='MO/xxx' and location_dest_id=7 and product_id=281;

 计划单
delete from stock_move where picking_id =xxx  and product_id=281;




修改错误数量以及计划单 

 update stock_move set product_qty=xxx where picking_id=yyy and product_id=zzz;
 [xxx为需要更新为的实际数  yyy为对应计划单的id  zzz为对应明细行产品的id]
 
 #注意当更新的实际数大于当前实际数时 下面这条语句无需执行
 update stock_move set product_qty=aaa where picking_id=yyy and product_id=zzz;
 [aaa为计划单计划数减去上述xxx ，yyy,zzz和上条语句一样]
 
 #执行完上述语句需要去做投料单


{'disable_create':1, 'disable_delete':1, 'disable_edit':0,'groups_disable_edit':'stock.group_stock_manager'}
{'disable_create':1,'disable_edit':0,'disable_delete':0,'groups_disable_edit':'stock.group_stock_manager'}

	<field name="product_id" domain="[('categ_id','in',(112,129))]" required="1"/>