with myshop as(
 select to_char(saledate,'yyyymmdd') as saledate,shop_id,xtnet,billnum from wgmf_sale_check where yn_hd='1' and saledate>='2015-05-21 16:00:00'
),
mysale as(
select pos_superorder.shop_id,
       myshop.saledate,
       myshop.billnum,
       count(pos_superorder.id) as order_count,
       sum(coalesce(pos_superorder.amount_total, 0)) as pos_net,
       myshop.xtnet as hd_net
  from pos_superorder
 inner join myshop
    on (pos_superorder.shop_id = myshop.shop_id and
       to_char(pos_superorder.sale_date + interval '8 hours', 'yyyymmdd') =
       myshop.saledate)
 group by pos_superorder.shop_id, myshop.saledate, myshop.xtnet, myshop.billnum
)
select mysale.shop_id,
       sale_shop.stores_no,
       sale_shop.name,
       mysale.saledate,
       mysale.order_count ,
       mysale.pos_net,
       mysale.hd_net,
       mysale.billnum 
  from mysale
  left outer join sale_shop
    on (mysale.shop_id = sale_shop.id)
 where mysale.pos_net <> mysale.hd_net order by mysale.saledate


select pp.default_code,t.name,sum(m.product_qty),sum(m.product_qty*t.standard_price) from wgmf_shouhuo_order sh,product_product pp,stock_move m left join product_template t
                           on m.product_id = t.id 
                           where m.location_dest_id = 12
                           and sh.wgmf_ps_bill = m.origin
                           and sh.shop_id  in (3,4,5,6,8,9,10,12,13,15)
                           and m.state='done'
                           and m.date>='2014-11-30 16:00:00'
                           and m.date<='2014-12-31 15:59:59'
                           and m.location_id = 89
                           and pp.product_tmpl_id=t.id
                           group by pp.default_code,t.name
                           

select pp.default_code, sum(m.product_qty)  from stock_move m, product_product pp   
                                      left join product_template t on (t.id = pp.product_tmpl_id  ) 
                                      where m.location_dest_id = 12 
                                      and pp.id = m.product_id 
                                      and m.state='done' and m.date>='2014-11-30 16:00:00' and m.date<='2014-12-31 15:59:59'
                                      and m.location_id = 89 
                                      and m.origin in (select wgmf_ps_bill from wgmf_shouhuo_order where shop_id  in (3,4,5,6,8,9,10,12,13,15) ) 
                                      group by pp.default_code                            
                           
select sum(t.standard_price*m.product_qty) from stock_move m, product_product pp   
                                      left join product_template t on (t.id = pp.product_tmpl_id  ) 
                                      where m.location_dest_id = 89
                                      and pp.id = m.product_id 
                                      and m.state='done' and m.date>='2014-11-30 16:00:00' and m.date<='2014-12-31 15:59:59'
                                      and m.location_id = 12
                                      and m.origin in (select wgmf_ps_bill from wgmf_shouhuo_order where shop_id  in (3,4,5,6,8,9,10,12,13,15) )                            
                                      
select line.id as 明细账ID,mm.name as 凭证,  move.name as 明细科目,rp.name as 制单人 from account_move_line line 
left join account_account move on move.id=line.account_id 
left join account_move mm on mm.id=line.move_id
left join res_users zd on zd.id=mm.create_uid
left join res_partner rp on rp.id=zd.partner_id

where move.wg_fzpz=true and line.analytic_account_id is null   order by mm.id                                      
                                      
                           