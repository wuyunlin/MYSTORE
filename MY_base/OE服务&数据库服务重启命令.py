OE服务启动/停止命令：
        1. 使用openerp/openerppwd@88oscg登录69 Server服务器外网ip 113.108.103.238 端口 3214
        2.1 OE服务查看命令 ps -ef |grep openerp-server
                下列为OE进程已经启动的显示：
                openerp@oe-app:~$ ps aux | grep openerp-server
                openerp  30057  0.1  0.1 196804 35392 ?        S    16:21   0:14 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30058  1.8  0.5 831924 164952 ?       Sl   16:21   2:40 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30061  1.8  0.5 911540 180872 ?       Sl   16:21   2:40 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30062  1.8  0.5 844152 176612 ?       Sl   16:21   2:38 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30069  2.0  0.5 838648 171372 ?       Sl   16:21   2:53 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30070  1.8  0.6 871672 204516 ?       Sl   16:21   2:34 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30075  1.8  0.6 866356 198968 ?       Sl   16:21   2:33 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30076  1.7  0.5 850476 183264 ?       Sl   16:21   2:25 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30077  1.4  0.5 839552 172064 ?       Sl   16:21   2:05 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30604  1.6  0.5 838788 171472 ?       Sl   16:43   2:00 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30767  1.8  0.5 841708 174464 ?       Sl   16:54   2:00 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30768  1.7  0.5 835900 168544 ?       Sl   16:54   1:55 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  30942  1.8  0.5 857252 190008 ?       Sl   17:08   1:45 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  31101  1.4  0.5 859068 191684 ?       Sl   17:18   1:15 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  31165  1.6  0.5 848304 180964 ?       Sl   17:20   1:21 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  31314  1.9  0.5 831764 164416 ?       Sl   17:33   1:20 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  31554  1.6  0.5 859620 192244 ?       Sl   17:53   0:47 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  31793  6.7  0.4 754252 161476 ?       RNl  18:14   1:52 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  32020  1.7  0.4 739380 146264 ?       SNl  18:33   0:09 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  32063  2.3  0.4 721736 137416 ?       Sl   18:37   0:07 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  32064  3.2  0.4 722080 137748 ?       Sl   18:37   0:09 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  32084  2.3  0.4 721704 137312 ?       Sl   18:37   0:06 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  32085  1.9  0.4 721720 137296 ?       Sl   18:37   0:05 python /opt/openerp/server/openerp-server -c /etc/openerp-server.conf   OE服务
                openerp  32423  0.0  0.0  11728   964 pts/1    S+   18:42   0:00 grep openerp-server
                openerp@oe-app:~$

        2.2 OE服务启动命令  /opt/openerp/openerp-ctrl start 使用2.1命令查看OE进程是否启动
        2.3 OE服务停止命令  /opt/openerp/openerp-ctrl stop 使用2.1命令查看OE进程是否关闭
postgresql数据库启动/停止命令：
        1. 先使用OE服务器停止命令停止openerp服务
        2. 使用openerp/openerppwd@88oscg登录69 DB服务器外网ip 113.108.103.238 端口 3215
        3. 停止postgresql数据库命令
        3.1 切换为postgres用户命令： su postgres  密码：postgres
        3.2 停止postgresql数据库命令：
                /usr/lib/postgresql/9.2/bin/pg_ctl -D /opt/pgdata/9.2/main stop
        3.2.1 查看postgresql服务命令：ps -ef |grep postgres
                openerp@oe-data:~$ ps aux | grep postgres
                postgres 11386  0.0  0.2 1802884 67088 ?       S    Jan25   0:24 /usr/lib/postgresql/9.2/bin/postgres -D /opt/pgdata/9.2/main                   postgresql主服务
                postgres 11474  0.0  0.0 1805956 9328 ?        Ss   13:47   0:00 postgres: openerp postgres 192.168.1.181(1826) idle                        链接postgresql的服务
                postgres 11475  0.0  0.0 1807628 25920 ?       Ss   13:47   0:00 postgres: openerp WGMF_Pro 192.168.1.181(1827) idle                        链接postgresql的服务
                postgres 11476  0.0  0.0 1805592 10072 ?       Ss   13:47   0:00 postgres: openerp WGMF_Pro 192.168.1.181(1828) idle                        链接postgresql的服务
                postgres 14513  2.7  4.2 1833400 1397564 ?     Ss   16:21   4:10 postgres: openerp WGMF_Pro 192.168.5.18(45675) idle                        链接postgresql的服务
                postgres 14514  2.1  4.3 1825768 1425452 ?     Ss   16:21   3:15 postgres: openerp WGMF_Pro 192.168.5.18(45677) idle                        链接postgresql的服务
                postgres 14516  1.5  3.8 1825124 1267332 ?     Ss   16:21   2:25 postgres: openerp WGMF_Pro 192.168.5.18(45679) idle                        链接postgresql的服务
                postgres 14517  5.1  4.3 1895860 1438800 ?     Ss   16:21   7:54 postgres: openerp WGMF_Pro 192.168.5.18(45683) idle                        链接postgresql的服务
                postgres 14529  2.3  3.5 1852912 1170980 ?     Ss   16:21   3:36 postgres: openerp WGMF_Pro 192.168.5.18(45705) idle                        链接postgresql的服务
                postgres 14531  2.4  4.1 1822020 1350848 ?     Ss   16:21   3:40 postgres: openerp WGMF_Pro 192.168.5.18(45709) idle                        链接postgresql的服务
                postgres 14536  3.7  3.9 1857992 1294724 ?     Ss   16:21   5:44 postgres: openerp WGMF_Pro 192.168.5.18(45715) idle                        链接postgresql的服务
                postgres 14537  2.7  4.4 1844584 1459896 ?     Ss   16:21   4:13 postgres: openerp WGMF_Pro 192.168.5.18(45716) idle                        链接postgresql的服务
                postgres 14541  2.8  4.4 1862048 1462624 ?     Ss   16:21   4:23 postgres: openerp WGMF_Pro 192.168.5.18(45728) idle                        链接postgresql的服务
                postgres 14542  2.5  3.8 1834680 1251488 ?     Ss   16:21   3:50 postgres: openerp WGMF_Pro 192.168.5.18(45731) idle                        链接postgresql的服务
                postgres 14544  4.7  4.2 1872864 1388992 ?     Ss   16:21   7:17 postgres: openerp WGMF_Pro 192.168.5.18(45734) idle                        链接postgresql的服务
                postgres 14552  3.0  4.4 1840044 1451704 ?     Ss   16:21   4:40 postgres: openerp WGMF_Pro 192.168.5.18(45741) idle                        链接postgresql的服务
                postgres 14555  4.2  4.1 1920336 1360428 ?     Ss   16:21   6:30 postgres: openerp WGMF_Pro 192.168.5.18(45749) idle                        链接postgresql的服务
                postgres 14556  2.2  3.9 1832332 1311352 ?     Ss   16:21   3:21 postgres: openerp WGMF_Pro 192.168.5.18(45751) idle                        链接postgresql的服务
                postgres 14560  4.8  4.6 1932848 1523788 ?     Ss   16:21   7:23 postgres: openerp WGMF_Pro 192.168.5.18(45757) idle                        链接postgresql的服务
                postgres 14561  2.1  3.8 1838344 1274636 ?     Ss   16:21   3:12 postgres: openerp WGMF_Pro 192.168.5.18(45759) idle                        链接postgresql的服务
                postgres 14878  2.3  3.2 1830984 1085472 ?     Ss   16:43   3:05 postgres: openerp WGMF_Pro 192.168.5.18(45974) idle                        链接postgresql的服务
                postgres 14879  2.4  3.9 1825124 1291996 ?     Ss   16:43   3:10 postgres: openerp WGMF_Pro 192.168.5.18(45976) idle                        链接postgresql的服务
                postgres 15039  3.4  3.3 1841856 1106328 ?     Ss   16:54   4:04 postgres: openerp WGMF_Pro 192.168.5.18(46074) idle                        链接postgresql的服务
                postgres 15040  2.5  3.5 1844332 1183416 ?     Ss   16:54   3:05 postgres: openerp WGMF_Pro 192.168.5.18(46075) idle                        链接postgresql的服务
                postgres 15041  1.8  3.7 1837272 1227348 ?     Ss   16:54   2:15 postgres: openerp WGMF_Pro 192.168.5.18(46077) idle                        链接postgresql的服务
                postgres 15042  3.5  5.1 1955852 1685196 ?     Ss   16:54   4:11 postgres: openerp WGMF_Pro 192.168.5.18(46080) idle                        链接postgresql的服务
                postgres 15420  6.4  3.3 1856968 1105032 ?     Ss   17:08   6:49 postgres: openerp WGMF_Pro 192.168.5.18(46193) idle                        链接postgresql的服务
                postgres 15421  1.7  3.7 1824360 1222720 ?     Ss   17:08   1:51 postgres: openerp WGMF_Pro 192.168.5.18(46195) idle                        链接postgresql的服务
                postgres 15567  1.5  2.8 1824840 947260 ?      Ss   17:18   1:30 postgres: openerp WGMF_Pro 192.168.5.18(46290) idle                        链接postgresql的服务
                postgres 15568  4.7  3.2 1930740 1072312 ?     Ss   17:18   4:32 postgres: openerp WGMF_Pro 192.168.5.18(46292) idle                        链接postgresql的服务
                postgres 15599  3.0  3.1 1905648 1038528 ?     Ss   17:21   2:51 postgres: openerp WGMF_Pro 192.168.5.18(46322) idle                        链接postgresql的服务
                postgres 15601  1.9  3.6 1832528 1205124 ?     Ss   17:21   1:46 postgres: openerp WGMF_Pro 192.168.5.18(46326) idle                        链接postgresql的服务
                postgres 15773  3.5  3.4 1918456 1124068 ?     Ss   17:34   2:51 postgres: openerp WGMF_Pro 192.168.5.18(46434) idle                        链接postgresql的服务
                postgres 15774  2.4  2.6 1819652 881208 ?      Ss   17:34   1:56 postgres: openerp WGMF_Pro 192.168.5.18(46436) idle                        链接postgresql的服务
                postgres 16001  1.8  2.6 1815272 864896 ?      Rs   17:54   1:07 postgres: openerp WGMF_Pro 192.168.5.18(46617) SELECT                        链接postgresql的服务
                postgres 16003  6.4  2.8 1839776 952636 ?      Ss   17:54   3:51 postgres: openerp WGMF_Pro 192.168.5.18(46620) idle                        链接postgresql的服务
                postgres 16543  3.1  2.4 1819468 803992 ?      Ss   18:37   0:31 postgres: openerp WGMF_Pro 192.168.5.18(46973) idle                        链接postgresql的服务
                postgres 16544  4.7  2.2 1815028 727136 ?      Ss   18:37   0:46 postgres: openerp WGMF_Pro 192.168.5.18(46975) idle                        链接postgresql的服务
                postgres 16547  4.6  2.1 1814984 723400 ?      Ss   18:37   0:45 postgres: openerp WGMF_Pro 192.168.5.18(46977) idle                        链接postgresql的服务
                postgres 16548  2.7  2.1 1815100 693572 ?      Ss   18:37   0:27 postgres: openerp WGMF_Pro 192.168.5.18(46979) idle                        链接postgresql的服务
                postgres 16551  1.1  2.3 1819512 777704 ?      Ss   18:38   0:11 postgres: openerp WGMF_Pro 192.168.5.18(46983) idle                        链接postgresql的服务
                postgres 16552  6.1  2.1 1814836 719136 ?      Ss   18:38   1:00 postgres: openerp WGMF_Pro 192.168.5.18(46985) idle                        链接postgresql的服务
                postgres 16557  0.5  2.0 1814368 686460 ?      Ss   18:38   0:05 postgres: openerp WGMF_Pro 192.168.5.18(46989) idle                        链接postgresql的服务
                postgres 16558  2.4  2.1 1819560 711496 ?      Ss   18:38   0:23 postgres: openerp WGMF_Pro 192.168.5.18(46991) idle                        链接postgresql的服务
                postgres 16620  5.5  2.2 1814696 726116 ?      Rs   18:44   0:31 postgres: openerp WGMF_Pro 192.168.5.18(47030) UPDATE                        链接postgresql的服务
                postgres 16621  0.8  0.1 1807228 53972 ?       Ss   18:44   0:04 postgres: openerp WGMF_Pro 192.168.5.18(47031) idle                        链接postgresql的服务
                postgres 16622  0.0  0.1 1807344 51528 ?       Ss   18:44   0:00 postgres: openerp WGMF_Pro 192.168.5.18(47032) idle                        链接postgresql的服务
                postgres 16626  0.8  0.1 1807744 54224 ?       Ss   18:44   0:04 postgres: openerp WGMF_Pro 192.168.5.18(47035) idle in transaction                        链接postgresql的服务
                postgres 16982 16.0  2.2 1806632 753232 ?      Rs   18:53   0:06 postgres: openerp WGMF_Pro 192.168.5.20(4670) SELECT                        链接postgresql的服务
                postgres 16988 75.0  2.3 1809004 779400 ?      Ss   18:54   0:09 postgres: openerp WGMF_Pro 192.168.5.18(47096) idle in transaction                        链接postgresql的服务
                postgres 16990  1.0  0.0 1805900 14632 ?       Ss   18:54   0:00 postgres: openerp WGMF_Pro 192.168.5.20(4696) idle                        链接postgresql的服务
                postgres 16992  0.0  0.0 1805612 9632 ?        Ss   18:54   0:00 postgres: openerp WGMF_Pro 192.168.5.18(47099) idle in transaction                        链接postgresql的服务
                postgres 16993  0.0  0.0 1804944 4516 ?        Ss   18:54   0:00 postgres: openerp WGMF_Pro 192.168.5.18(47100) idle                        链接postgresql的服务
                postgres 16994  0.0  0.0 1805700 15840 ?       Ss   18:54   0:00 postgres: openerp WGMF_Pro 192.168.5.18(47101) idle in transaction                        链接postgresql的服务
                openerp  16997  0.0  0.0  10000   652 pts/1    S+   18:54   0:00 grep postgres
                postgres 28092  0.0  4.5 1803972 1490008 ?     Ss   Jan25   1:00 postgres: checkpointer process
                postgres 28093  0.0  1.7 1803840 591500 ?      Ss   Jan25   0:16 postgres: writer process
                postgres 28094  0.0  0.0 1803688 18284 ?       Ss   Jan25   0:52 postgres: wal writer process
                postgres 28095  0.0  0.0 1805044 4060 ?        Ss   Jan25   0:02 postgres: autovacuum launcher process
                postgres 28096  0.0  0.0  97608  1968 ?        Ss   Jan25   0:47 postgres: stats collector process
                openerp@oe-data:~$
        3.2.2 如果停止命令失效或未成功停止postgresql服务，使用如下命令后在停止
                 停止链接postgresql的服务
                 /opt/openerp/kill-postgres-ctrl stop
                 停止postgresql主服务
                 /usr/lib/postgresql/9.2/bin/pg_ctl -D /opt/pgdata/9.2/main stop
        3.3 启动postgresql主服务
                /usr/lib/postgresql/9.2/bin/pg_ctl -D /opt/pgdata/9.2/main -l /var/log/postgresql/postgresql-9.2-main.log start
        3.4 查看postgresql 运行日志
                vi /var/log/postgresql/postgresql-9.2-main.log
        3.5 查看postgresql CPU占用情况
                htop
        3.6 启动时报错/运行日志中报错：FATAL:  lock file "postmaster.pid" already exists
                解决命令   
                /usr/local/pgsql/bin/pg_ctl stop –o SIGTERM
                /usr/lib/postgresql/9.2/bin/pg_ctl -D /opt/pgdata/9.2/main -l /var/log/postgresql/postgresql-9.2-main.log start
        3.7 启动时报错/运行日志中报错：FATAL:  could not create lock file ‘var/run/postgresql/.s.PGSQL.5432.lock’： No such file or directory 
                解决办法
                mkdir /var/run/postgresql
                /usr/lib/postgresql/9.2/bin/pg_ctl -D /opt/pgdata/9.2/main -l /var/log/postgresql/postgresql-9.2-main.log start
        3.8 如果postgresql CPU使用过高或是链接postgresql的服务大部分都在异常占用

                htop 后发现postgresql 的8个cpu大部分都是达到了100% 说明postgresql数据库线程被异常占用
                

           查看postgresql服务是否异常占用命令：ps -ef |grep postgres
                idle 代表空闲    SELECT 代表查询   UPDATE 代表更新  idle in transaction 代表空闲事务处理  DROP 代表删除  等等
                如果发现 idle 的进程很少很少几乎没有的情况说明postgresql服务被异常占用了。
                openerp@oe-data:~$ ps aux | grep postgres
                postgres 11386  0.0  0.2 1802884 67088 ?       S    Jan25   0:24 /usr/lib/postgresql/9.2/bin/postgres -D /opt/pgdata/9.2/main                   postgresql主服务
                postgres 11474  0.0  0.0 1805956 9328 ?        Ss   13:47   0:00 postgres: openerp postgres 192.168.1.181(1826) idle                        链接postgresql的服务
                postgres 11475  0.0  0.0 1807628 25920 ?       Ss   13:47   0:00 postgres: openerp WGMF_Pro 192.168.1.181(1827) idle                        链接postgresql的服务
                postgres 11476  0.0  0.0 1805592 10072 ?       Ss   13:47   0:00 postgres: openerp WGMF_Pro 192.168.1.181(1828) SELECT                        链接postgresql的服务
                postgres 14513  2.7  4.2 1833400 1397564 ?     Ss   16:21   4:10 postgres: openerp WGMF_Pro 192.168.5.18(45675) SELECT                        链接postgresql的服务
                postgres 14514  2.1  4.3 1825768 1425452 ?     Ss   16:21   3:15 postgres: openerp WGMF_Pro 192.168.5.18(45677) SELECT                        链接postgresql的服务
                postgres 14516  1.5  3.8 1825124 1267332 ?     Ss   16:21   2:25 postgres: openerp WGMF_Pro 192.168.5.18(45679) SELECT                        链接postgresql的服务
                postgres 14517  5.1  4.3 1895860 1438800 ?     Ss   16:21   7:54 postgres: openerp WGMF_Pro 192.168.5.18(45683) SELECT                        链接postgresql的服务
                postgres 14529  2.3  3.5 1852912 1170980 ?     Ss   16:21   3:36 postgres: openerp WGMF_Pro 192.168.5.18(45705) SELECT                        链接postgresql的服务
                postgres 14531  2.4  4.1 1822020 1350848 ?     Ss   16:21   3:40 postgres: openerp WGMF_Pro 192.168.5.18(45709) SELECT                        链接postgresql的服务
                postgres 14536  3.7  3.9 1857992 1294724 ?     Ss   16:21   5:44 postgres: openerp WGMF_Pro 192.168.5.18(45715) SELECT                        链接postgresql的服务
                postgres 14537  2.7  4.4 1844584 1459896 ?     Ss   16:21   4:13 postgres: openerp WGMF_Pro 192.168.5.18(45716) SELECT                        链接postgresql的服务
                postgres 14541  2.8  4.4 1862048 1462624 ?     Ss   16:21   4:23 postgres: openerp WGMF_Pro 192.168.5.18(45728) SELECT                        链接postgresql的服务
                postgres 14542  2.5  3.8 1834680 1251488 ?     Ss   16:21   3:50 postgres: openerp WGMF_Pro 192.168.5.18(45731) SELECT                        链接postgresql的服务
                postgres 14544  4.7  4.2 1872864 1388992 ?     Ss   16:21   7:17 postgres: openerp WGMF_Pro 192.168.5.18(45734) SELECT                        链接postgresql的服务
                postgres 14552  3.0  4.4 1840044 1451704 ?     Ss   16:21   4:40 postgres: openerp WGMF_Pro 192.168.5.18(45741) SELECT                        链接postgresql的服务
                postgres 14555  4.2  4.1 1920336 1360428 ?     Ss   16:21   6:30 postgres: openerp WGMF_Pro 192.168.5.18(45749) SELECT                        链接postgresql的服务
                postgres 14556  2.2  3.9 1832332 1311352 ?     Ss   16:21   3:21 postgres: openerp WGMF_Pro 192.168.5.18(45751) SELECT                        链接postgresql的服务
                postgres 14560  4.8  4.6 1932848 1523788 ?     Ss   16:21   7:23 postgres: openerp WGMF_Pro 192.168.5.18(45757) SELECT                        链接postgresql的服务
                postgres 14561  2.1  3.8 1838344 1274636 ?     Ss   16:21   3:12 postgres: openerp WGMF_Pro 192.168.5.18(45759) SELECT                        链接postgresql的服务
                postgres 14878  2.3  3.2 1830984 1085472 ?     Ss   16:43   3:05 postgres: openerp WGMF_Pro 192.168.5.18(45974) SELECT                        链接postgresql的服务
                postgres 14879  2.4  3.9 1825124 1291996 ?     Ss   16:43   3:10 postgres: openerp WGMF_Pro 192.168.5.18(45976) SELECT                        链接postgresql的服务
                postgres 15039  3.4  3.3 1841856 1106328 ?     Ss   16:54   4:04 postgres: openerp WGMF_Pro 192.168.5.18(46074) SELECT                        链接postgresql的服务
                postgres 15040  2.5  3.5 1844332 1183416 ?     Ss   16:54   3:05 postgres: openerp WGMF_Pro 192.168.5.18(46075) SELECT                        链接postgresql的服务
                postgres 15041  1.8  3.7 1837272 1227348 ?     Ss   16:54   2:15 postgres: openerp WGMF_Pro 192.168.5.18(46077) SELECT                        链接postgresql的服务
                postgres 15042  3.5  5.1 1955852 1685196 ?     Ss   16:54   4:11 postgres: openerp WGMF_Pro 192.168.5.18(46080) SELECT                        链接postgresql的服务
                postgres 15420  6.4  3.3 1856968 1105032 ?     Ss   17:08   6:49 postgres: openerp WGMF_Pro 192.168.5.18(46193) SELECT                        链接postgresql的服务
                postgres 15421  1.7  3.7 1824360 1222720 ?     Ss   17:08   1:51 postgres: openerp WGMF_Pro 192.168.5.18(46195) SELECT                        链接postgresql的服务
                postgres 15567  1.5  2.8 1824840 947260 ?      Ss   17:18   1:30 postgres: openerp WGMF_Pro 192.168.5.18(46290) SELECT                        链接postgresql的服务
                postgres 15568  4.7  3.2 1930740 1072312 ?     Ss   17:18   4:32 postgres: openerp WGMF_Pro 192.168.5.18(46292) SELECT                        链接postgresql的服务
                postgres 15599  3.0  3.1 1905648 1038528 ?     Ss   17:21   2:51 postgres: openerp WGMF_Pro 192.168.5.18(46322) SELECT                        链接postgresql的服务
                postgres 15601  1.9  3.6 1832528 1205124 ?     Ss   17:21   1:46 postgres: openerp WGMF_Pro 192.168.5.18(46326) SELECT                        链接postgresql的服务
                postgres 15773  3.5  3.4 1918456 1124068 ?     Ss   17:34   2:51 postgres: openerp WGMF_Pro 192.168.5.18(46434) SELECT                        链接postgresql的服务
                postgres 15774  2.4  2.6 1819652 881208 ?      Ss   17:34   1:56 postgres: openerp WGMF_Pro 192.168.5.18(46436) SELECT                        链接postgresql的服务
                postgres 16001  1.8  2.6 1815272 864896 ?      Rs   17:54   1:07 postgres: openerp WGMF_Pro 192.168.5.18(46617) SELECT                        链接postgresql的服务
                postgres 16003  6.4  2.8 1839776 952636 ?      Ss   17:54   3:51 postgres: openerp WGMF_Pro 192.168.5.18(46620) UPDATE                        链接postgresql的服务
                postgres 16543  3.1  2.4 1819468 803992 ?      Ss   18:37   0:31 postgres: openerp WGMF_Pro 192.168.5.18(46973) UPDATE                        链接postgresql的服务
                postgres 16544  4.7  2.2 1815028 727136 ?      Ss   18:37   0:46 postgres: openerp WGMF_Pro 192.168.5.18(46975) UPDATE                        链接postgresql的服务
                postgres 16547  4.6  2.1 1814984 723400 ?      Ss   18:37   0:45 postgres: openerp WGMF_Pro 192.168.5.18(46977) UPDATE                        链接postgresql的服务
                postgres 16548  2.7  2.1 1815100 693572 ?      Ss   18:37   0:27 postgres: openerp WGMF_Pro 192.168.5.18(46979) UPDATE                        链接postgresql的服务
                postgres 16551  1.1  2.3 1819512 777704 ?      Ss   18:38   0:11 postgres: openerp WGMF_Pro 192.168.5.18(46983) UPDATE                        链接postgresql的服务
                postgres 16552  6.1  2.1 1814836 719136 ?      Ss   18:38   1:00 postgres: openerp WGMF_Pro 192.168.5.18(46985) UPDATE                        链接postgresql的服务
                postgres 16557  0.5  2.0 1814368 686460 ?      Ss   18:38   0:05 postgres: openerp WGMF_Pro 192.168.5.18(46989) UPDATE                        链接postgresql的服务
                postgres 16558  2.4  2.1 1819560 711496 ?      Ss   18:38   0:23 postgres: openerp WGMF_Pro 192.168.5.18(46991) UPDATE                        链接postgresql的服务
                postgres 16620  5.5  2.2 1814696 726116 ?      Rs   18:44   0:31 postgres: openerp WGMF_Pro 192.168.5.18(47030) UPDATE                        链接postgresql的服务
                postgres 16621  0.8  0.1 1807228 53972 ?       Ss   18:44   0:04 postgres: openerp WGMF_Pro 192.168.5.18(47031) UPDATE                        链接postgresql的服务
                postgres 16622  0.0  0.1 1807344 51528 ?       Ss   18:44   0:00 postgres: openerp WGMF_Pro 192.168.5.18(47032) UPDATE                        链接postgresql的服务
                postgres 16626  0.8  0.1 1807744 54224 ?       Ss   18:44   0:04 postgres: openerp WGMF_Pro 192.168.5.18(47035) idle in transaction                        链接postgresql的服务
                postgres 16982 16.0  2.2 1806632 753232 ?      Rs   18:53   0:06 postgres: openerp WGMF_Pro 192.168.5.20(4670) SELECT                        链接postgresql的服务
                postgres 16988 75.0  2.3 1809004 779400 ?      Ss   18:54   0:09 postgres: openerp WGMF_Pro 192.168.5.18(47096) idle in transaction                        链接postgresql的服务
                postgres 16990  1.0  0.0 1805900 14632 ?       Ss   18:54   0:00 postgres: openerp WGMF_Pro 192.168.5.20(4696) idle                        链接postgresql的服务
                postgres 16992  0.0  0.0 1805612 9632 ?        Ss   18:54   0:00 postgres: openerp WGMF_Pro 192.168.5.18(47099) idle in transaction                        链接postgresql的服务
                postgres 16993  0.0  0.0 1804944 4516 ?        Ss   18:54   0:00 postgres: openerp WGMF_Pro 192.168.5.18(47100) idle                        链接postgresql的服务
                postgres 16994  0.0  0.0 1805700 15840 ?       Ss   18:54   0:00 postgres: openerp WGMF_Pro 192.168.5.18(47101) idle in transaction                        链接postgresql的服务
                openerp  16997  0.0  0.0  10000   652 pts/1    S+   18:54   0:00 grep postgres
                postgres 28092  0.0  4.5 1803972 1490008 ?     Ss   Jan25   1:00 postgres: checkpointer process
                postgres 28093  0.0  1.7 1803840 591500 ?      Ss   Jan25   0:16 postgres: writer process
                postgres 28094  0.0  0.0 1803688 18284 ?       Ss   Jan25   0:52 postgres: wal writer process
                postgres 28095  0.0  0.0 1805044 4060 ?        Ss   Jan25   0:02 postgres: autovacuum launcher process
                postgres 28096  0.0  0.0  97608  1968 ?        Ss   Jan25   0:47 postgres: stats collector process
                openerp@oe-data:~$

                解决办法：
                        1. 利用上面的OE服务停止命令去OE Server服务器停止OE服务
                        2. 停止链接postgresql的服务
                           /opt/openerp/kill-postgres-ctrl stop
                        3. 利用上面的OE服务启动命令去OE Server服务器启动OE服务


