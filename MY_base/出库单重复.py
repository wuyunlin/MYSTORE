
 #1重复出库单
 select id from stock_picking where id in 
(
with res as (
select min(id) as id,origin,count(*) as cc ,date  from stock_picking where id in (
select distinct picking_id 
from stock_move 
where date>='2015-05-31 16:00:00' and date<'2015-06-30 16:00:00' and state='done' 
and (location_dest_id=9 or location_id=9)
) group by origin,date having(count(*)>1) 
)
---select count(*),sum(cc) from res 

select a.id from stock_picking a
inner join res on res.origin=a.origin 
where res.id!=a.id
order by a.date desc)
#2销售明细汇总
with all_res as
(
---套装合计
with res as (select c.stores_no,b.id,b.default_code,sum(a.qty) as qty from pos_superorder_line a 
left join product_product b on b.id=a.product_id
left join sale_shop c on c.id=a.shop_id
left join pos_superorder d on d.id=a.superorder_id
where d.sale_date>='2015-05-31 16:00:00' 
and d.sale_date<'2015-06-30 16:00:00' and a.price!=0 and d.state='approved'
and a.product_id  in (select product_id from mrp_bom where bom_id is null)
group by b.id,c.id )

select res.stores_no, c.default_code,sum(res.qty*a.product_qty ) as qty
from mrp_bom a
left join mrp_bom b on b.id =a.bom_id
left join product_product c on c.id =a.product_id
inner join res on b.product_id=res.id where c.id=0
group by c.id,res.stores_no---order by c.default_code
union all
--单品销售
select c.stores_no,b.default_code,sum(a.qty) as qty from pos_superorder_line a 
left join product_product b on b.id=a.product_id
left join sale_shop c on a.shop_id=c.id
inner join pos_superorder d on d.id=a.superorder_id
where d.sale_date>='2015-05-31 16:00:00' 
 and d.sale_date<'2015-06-30 16:00:00'  and a.price!=0 ---and d.state='approved'
 and a.product_id not in (select product_id from mrp_bom where bom_id is null)
group by b.id,c.id) 


select b.default_code,sum(all_res.qty) from 
 product_product b 
inner join all_res on all_res.default_code=b.default_code group by b.default_code order by b.default_code


#3销出库明细汇总
with res as

(
with md as 
(select a.id,a.stores_no,b.lot_stock_id as location from sale_shop a
left join stock_warehouse b on b.id=a.warehouse_id where a.yymode='DZD') 

---出库
select c.id,c.default_code,md.stores_no,sum(a.product_qty) as qty from stock_move a
left join product_product b on b.id=a.product_id
inner join md on md.location=a.location_id
inner join product_product c on c.id=a.product_id
where a.state='done' and a.date>='2015-05-31 16:00:00' and a.date<'2015-06-30 16:00:00'
and a.location_dest_id=9 and a.product_id not in (select product_id from mrp_bom where bom_id is null)
group by c.id,md.stores_no
union all
---退货
select c.id,c.default_code,md.stores_no,-sum(a.product_qty) as qty from stock_move a
left join product_product b on b.id=a.product_id
inner join md on md.location=a.location_dest_id
inner join product_product c on c.id=a.product_id
where a.state='done' and a.date>='2015-05-31 16:00:00' and a.date<'2015-06-30 16:00:00'
and a.location_id=9 and a.product_id not in (select product_id from mrp_bom where bom_id is null)
group by c.id,md.stores_no

)

select res.default_code,sum(qty) from res where res.default_code='2.40010'
 group by res.default_code order by res.default_code 

 ---order by res.stores_no