# -*- coding:utf-8 -*-
from openerp.osv import osv, fields
from datetime import datetime, timedelta, date
from random import random

deng_is_exists_of_report = {}

class report_crocus_quantity_of_order(osv.osv_memory):
    _name = "report.crocus.quantity.of.order"
    _description = u"磨粉客单数统计表"
    def _get_organization_id(self, cr, uid, ids, fields_name, arg, context=None):
        """获取门店的上级组织"""
        res = {}.fromkeys(ids, '')
        for record in self.browse(cr, uid, ids, context=context):
            one_res = {'area_id': '', 'office_id': '', 'manager_id': '', 'sale_organization':False}
            shop_id = record.shop_id
            if shop_id:
                manager_id = shop_id.sale_organization
                if manager_id:
                    one_res['sale_organization'] = manager_id.id
                    one_res['manager_id'] = manager_id.name
                    office_id = manager_id.parent_id
                    if office_id:
                        one_res['office_id'] = office_id.name
                        area_id = office_id.parent_id and office_id.parent_id.name or False
                        one_res['area_id'] = area_id

            res[record.id] = one_res
        return res
    _columns = {
        'manager_id': fields.char(u"主管"),
        'office_id': fields.char(u"办事处"),
        'area_id': fields.char(u"大区"),
        "shop_id": fields.many2one("sale.shop", u"门店"),
        "date_order": fields.char(u"起止日期"),
        "no_drink_count": fields.integer(u"磨粉客单数"),
        "p_m_no_drink_count": fields.float(u"环比增长（%）", digits=(4, 2)),
        "p_y_no_drink_count": fields.float(u"同比增长（%）", digits=(4, 2)),
        "no_drink_order_price": fields.float(u"磨粉客单价（￥）", digits=(4, 2)),
        "drink_qty": fields.integer(u"饮品杯数"),
        "p_m_drink_count": fields.float(u"环比增长（%）", digits=(4, 2)),
        "p_y_drink_count": fields.float(u"同比增长（%）", digits=(4, 2)),
        "drink_order_price": fields.float(u"饮品客单价（￥）", digits=(4, 2)),
    }


class report_crocus_quantity_of_order_condition(osv.osv_memory):
    _name = "report.crocus.quantity.of.order.condition"
    _description = u"磨粉客单数统计表查询条件"
    _columns = {
        "shop_id": fields.many2many("sale.shop", string=u"门店"),
        "search_date_0": fields.date(u"开始时间"),
        "search_date_1": fields.date(u"结束时间"),
    }

    def name_get(self, cr, uid, ids, context=None):
        return {ids[0]: u"报表"}

    def btn_search(self, cr, uid, ids, context=None):
        d_search_condit = self.browse(cr, uid, ids[0])
        t_d0 = None; t_m_d0 = None; t_y_d0 = None
        t_d1 = None; t_m_d1 = None; t_y_d1 = None
        param_ids = None
        t = "t" + str(int(random()*10000))
        t_m = "t" + str(int(random()*10000))
        t_y = "t" + str(int(random()*10000))
        t_r = "t" + str(int(random()*10000))
        if d_search_condit.shop_id:
            param_ids = ",".join([str(x.id) for x in d_search_condit.shop_id])
        if d_search_condit.search_date_0:
            t_d0 = datetime.strptime(d_search_condit.search_date_0, '%Y-%m-%d')# + timedelta(days=1)
            t_m_d0 = ""
            t_m_d0 += str(t_d0.year if t_d0.month > 1 else t_d0.year - 1) + "-" + str(t_d0.month - 1 or 12) + "-" + str(t_d0.day)
            t_y_d0 = str(t_d0.year - 1) + "-" + str(t_d0.month) + "-" + str(t_d0.day)
        if d_search_condit.search_date_1:
            t_d1 = datetime.strptime(d_search_condit.search_date_1, '%Y-%m-%d')# + timedelta(days=1)
            t_m_d1 = ""
            t_m_d1 += str(t_d1.year if t_d1.month > 1 else t_d1.year - 1) + "-" + str(t_d1.month - 1 or 12) + "-" + str(t_d1.day)
            t_y_d1 = str(t_d1.year - 1) + "-" + str(t_d1.month) + "-" + str(t_d1.day)

        sql = ""
        sql_template = """
select a.*,b.no_drink_qty,b.no_drink_amount,b.no_drink_count,(a.qty-b.no_drink_qty)drink_qty,(amount-no_drink_amount)drink_amount,(count-no_drink_count)drink_count from (
	select shop_id,date_order date_order,sum(qty)qty,sum(price_subtotal)amount,count(1)count from (
		select a.id,a.shop_id,to_char(a.date_order,'yyyy-mm-01') date_order,sum(b.qty)qty,sum(b.price_subtotal)price_subtotal from pos_order a
		inner join pos_order_line b on a.id = b.order_id
		where 1=1 %(0)s
		group by a.id,a.shop_id,date_order
		union all
        select a.id,a.shop_id,to_char(a.sale_date,'yyyy-mm-01') date_order,sum(b.qty)qty,sum(a.amount_total)price_subtotal from pos_superorder a
        inner join pos_superorder_line b on a.id = b.superorder_id
        where 1=1 %(2)s
        group by a.id,a.shop_id,date_order
		) a
	group by shop_id,a.date_order) a
left join (
	select shop_id,date_order,sum(qty)no_drink_qty,sum(price_subtotal)no_drink_amount,count(1)no_drink_count from (
		select a.id,a.shop_id,to_char(a.date_order,'yyyy-mm-01') date_order,sum(c.qty)qty,sum(c.price_subtotal)price_subtotal from pos_order a
		inner join (
			select a.id from pos_order a
			inner join pos_order_line b on a.id = b.order_id
			inner join product_template c on b.product_id = c.id
			inner join product_category d on c.categ_id = d.id
			inner join product_category e on d.parent_id = e.id
			inner join product_category f on e.parent_id = f.id
			where e.id != 46 and f.id = 33 %(1)s
			group by a.id) b on a.id = b.id
		left join pos_order_line c on a.id = c.order_id
		group by a.id,a.shop_id,date_order
		union all
		select a.id,a.shop_id,to_char(a.sale_date,'yyyy-mm-01') date_order,sum(c.qty)qty,sum(a.amount_total)price_subtotal from pos_superorder a
		inner join (
			select a.id from pos_superorder a
			inner join pos_superorder_line b on a.id = b.superorder_id
			inner join product_template c on b.product_id = c.id
			inner join product_category d on c.categ_id = d.id
			inner join product_category e on d.parent_id = e.id
			inner join product_category f on e.parent_id = f.id
			where e.id != 46 and f.id = 33 %(3)s
			group by a.id) b on a.id = b.id
		left join pos_superorder_line c on a.id = c.superorder_id
		group by a.id,a.shop_id,date_order
		) a
	group by shop_id,a.date_order) b on a.shop_id = b.shop_id and a.date_order = b.date_order"""

        sql_template = "create temporary table %(n)s as " + self.reset_sql_for_Organization(sql_template)

        param0 = "and a.date_order >= '%s'"
        param1 = " and a.date_order < '%s'"
        param = param0 % t_d0 if t_d0 else ""
        param += param1 % t_d1 if t_d1 else ""
        param += param_ids and "and a.shop_id in (" + param_ids + ")" or ""
        sql += sql_template % {"n": t, "0": param, "1": param, "2": param.replace("date_order", "sale_date"), "3": param.replace("date_order", "sale_date")}
        param = param0 % t_m_d0 if t_m_d0 else ""
        param += param1 % t_m_d1 if t_m_d1 else ""
        param += param_ids and "and a.shop_id in (" + param_ids + ")" or ""
        sql += sql_template % {"n": t_m, "0": param, "1": param, "2": param.replace("date_order", "sale_date"), "3": param.replace("date_order", "sale_date")}
        param = param0 % t_y_d0 if t_y_d0 else ""
        param += param1 % t_y_d1 if t_y_d1 else ""
        param += param_ids and "and a.shop_id in (" + param_ids + ")" or ""
        sql += sql_template % {"n": t_y, "0": param, "1": param, "2": param.replace("date_order", "sale_date"), "3": param.replace("date_order", "sale_date")}


        sql_1 = """
select
t.shop_id,t.date_order,t.no_drink_count no_drink_count,
case t_m.no_drink_count when 0 then 0 else (t.no_drink_count-t_m.no_drink_count)*100.0/t_m.no_drink_count end p_m_no_drink_count,
case t_y.no_drink_count when 0 then 0 else (t.no_drink_count-t_y.no_drink_count)*100.0/t_y.no_drink_count end p_y_no_drink_count,
case t.no_drink_count when 0 then 0 else t.no_drink_amount*1.0/t.no_drink_count end no_drink_order_price,

t.drink_qty drink_qty,
case t_m.drink_count when 0 then 0 else (t.drink_count-t_m.drink_count)*100.0/t_m.drink_count end p_m_drink_count,
case t_y.drink_count when 0 then 0 else (t.drink_count-t_y.drink_count)*100.0/t_y.drink_count end p_y_drink_count,
case t.drink_count when 0 then 0 else t.drink_amount*1.0/t.drink_count end drink_order_price
from %(t)s t
left join %(t_m)s t_m on t.shop_id = t_m.shop_id  and to_date(t.date_order,'yyyy-mm-dd') = to_date(t_m.date_order,'yyyy-mm') + interval '1 month'
left join %(t_y)s t_y on t.shop_id = t_y.shop_id  and to_date(t.date_order,'yyyy-mm-dd') = to_date(t_y.date_order,'yyyy-mm') + interval '1 year'
""" % {"t": t, "t_m": t_m, "t_y": t_y, "n": t_r}

        sql_1 = "create temporary table %(n)s as " % {"n": t_r} + self.reset_sql_for_Organization(sql_1)

        sql_2 = "delete from report_crocus_quantity_of_order;update "+t_r+" set date_order = date_order || ' - ' || to_char(to_date(date_order, 'yyyy-mm-dd') + interval '1 month' - interval '1 day', 'yyyy-mm-dd');"

        sql_2 += "update "+t_r+" set date_order = '"+t_d0.strftime("%Y-%m-%d")+"' || ' - ' || substring(date_order from 14 for 10) where date_order like '" + t_d0.strftime("%Y-%m") + "%';" if t_d0 else ""
        sql_2 += "update "+t_r+" set date_order = substring(date_order from 1 for 10) || ' - ' || '"+t_d1.strftime("%Y-%m-%d")+"' where date_order like '" + t_d1.strftime("%Y-%m") + "%';" if t_d1 else ""
        sql_2 += "insert into report_crocus_quantity_of_order(manager_id,office_id,area_id,shop_id,date_order,no_drink_count,p_m_no_drink_count,p_y_no_drink_count,no_drink_order_price,drink_qty,p_m_drink_count,p_y_drink_count,drink_order_price) select * from %(n)s order by shop_id,date_order;" % {"n": t_r}
        sql_2 += "update report_crocus_quantity_of_order set create_uid = %(uid)s,write_uid = %(uid)s;" % {"uid": uid}

        cr.execute(sql + sql_1 + sql_2)
        t_ir_model_date = self.pool.get("ir.model.data")
        view_id = t_ir_model_date.get_object_reference(cr, uid, "wgmf3_report", "tree_crocus_quantity_of_order")[1]
        v_tree = {
            "name": "磨粉客单数统计",
            "type": "ir.actions.act_window",
            "res_model": "report.crocus.quantity.of.order",
            "view_type": "form",
            "view_mode": "tree",
            "view_id": view_id
        }
        return v_tree

        # mod_obj = self.pool.get('ir.model.data')
        # act_obj = self.pool.get('ir.actions.act_window')
        # result = mod_obj.get_object_reference(cr, uid, 'wgmf3_report', 'act_tree_crocus_quantity_of_order')
        # id = result and result[1] or False
        # result = act_obj.read(cr, uid, [id])[0]
        # return result

    def btn_search_X(self, cr, uid, ids, context=None):
        global deng_is_exists_of_report
        d_search_condit = self.browse(cr, uid, ids[0])
        d_param = []
        if d_search_condit.shop_id.id:
            d_param.append(("shop_id", "=", d_search_condit.shop_id.id))
        if d_search_condit.search_date_0:
            d = d_search_condit.search_date_0[:7]
            d = datetime.strptime(d, "%Y-%m")
            d = str(d)
            d_param.append(("create_date", ">=", d))
        if d_search_condit.search_date_1:
            d = d_search_condit.search_date_1[:7]
            d = datetime.strptime((date(int(d[:4]), int(d[5:7])+1, 1)).strftime("%Y-%m"), "%Y-%m")
            d = str(d)
            d_param.append(("create_date", "<", d))
        # raise osv.except_osv("ttt", str(d_param))
        if not deng_is_exists_of_report.get(str(d_param), False) or deng_is_exists_of_report["date"] + timedelta(minutes=5) < datetime.now():
            cr.execute("delete from report_crocus_quantity_of_order")
            t_pos_order = self.pool.get("pos.order")
            id_new_pos_order = []
            t_pos_order_line = self.pool.get("pos.order.line")
            id_pos_order_line = t_pos_order_line.search(cr, uid, d_param)
            m_pos_order_line = self.pool.get("pos.order.line").browse(cr, uid, id_pos_order_line)
            for m in m_pos_order_line:
                if m.product_id.categ_id.parent_id.id != 46 and m.product_id.categ_id.parent_id.parent_id.id == 33:
                    id_new_pos_order.append(m.order_id.id)
            m_pos_order = t_pos_order.browse(cr, uid, id_new_pos_order)
            list_pos_order_rows = {}
            for m in m_pos_order:
                search_date = m.date_order[:7]
                _tmp_m = {'shop_id': m.shop_id.id, 'search_date': search_date, 'quantity_total': 1}
                if search_date + str(m.shop_id.id) in list_pos_order_rows:
                    _tmp_m['quantity_total'] += list_pos_order_rows[search_date + str(m.shop_id.id)][
                        'quantity_total']
                list_pos_order_rows[search_date + str(m.shop_id.id)] = _tmp_m
            #
            t_pos_superorder = self.pool.get("pos.superorder")
            id_new_pos_superorder = []
            t_pos_superorder_line = self.pool.get("pos.superorder.line")
            id_pos_superorder_line = t_pos_superorder_line.search(cr, uid, d_param)
            m_pos_superorder_line = self.pool.get("pos.superorder.line").browse(cr, uid, id_pos_superorder_line)
            for m in m_pos_superorder_line:
                if m.product_id.categ_id.parent_id.id != 46 and m.product_id.categ_id.parent_id.parent_id.id == 33:
                    id_new_pos_superorder.append(m.superorder_id.id)
            m_pos_superorder = t_pos_superorder.browse(cr, uid, id_new_pos_superorder)
            list_pos_superorder_rows = {}
            for m in m_pos_superorder:
                search_date = m.sale_date[:7]
                _tmp_m = {'shop_id': m.shop_id.id, 'search_date': search_date, 'quantity_total': 1}
                if search_date + str(m.shop_id.id) in list_pos_superorder_rows:
                    _tmp_m['quantity_total'] += list_pos_superorder_rows[search_date + str(m.shop_id.id)][
                        'quantity_total']
                list_pos_order_rows[search_date + str(m.shop_id.id)] = _tmp_m
            #
            t_table = self.pool.get("report.crocus.quantity.of.order")
            for v, k in list_pos_order_rows.iteritems():
                val = {}
                val["shop_id"] = k["shop_id"]
                val["search_date"] = k["search_date"]
                val["quantity_total"] = k["quantity_total"]
                val["last_month_percent"] = 0
                last_month = (
                date(int(k["search_date"][:4]), int(k["search_date"][5:7]), 1) - timedelta(days=1)).strftime("%Y-%m")
                try:
                    last_month_qty = list_pos_order_rows[last_month + str(k["shop_id"])]["quantity_total"]
                    val["last_month_percent"] = (k["quantity_total"] - last_month_qty) / (last_month_qty * 1.0)
                except KeyError:
                    val["last_month_percent"] = -10000
                val["last_year_percent"] = 0
                last_year = str(int(k["search_date"][:4]) - 1) + k["search_date"][4:7]
                try:
                    last_year_qty = list_pos_order_rows[last_year + str(k["shop_id"])]["quantity_total"]
                    val["last_year_percent"] = (k["quantity_total"] - last_year_qty) / (last_year_qty * 1.0)
                except KeyError:
                    val["last_year_percent"] = -10000
                t_table.create(cr, uid, val, context)
            deng_is_exists_of_report[str(d_param)] = True
            deng_is_exists_of_report["date"] = datetime.now()

        t_ir_model_date = self.pool.get("ir.model.data")
        view_id = t_ir_model_date.get_object_reference(cr, uid, "wgmf3_report", "tree_crocus_quantity_of_order")[1]
        v_tree = {
            "name": "磨粉客单数统计",
            "type": "ir.actions.act_window",
            "res_model": "report.crocus.quantity.of.order",
            "view_type": "form",
            "view_mode": "tree",
            "view_id": view_id
        }
        return v_tree

    def action_financial_organization(self, cr, uid, ids, context=None):
        context.update({
            'res_model':'wgmf.sale.organization',
            'src_model':'sale.shop',
            'relation_field': 'sale_organization',
            'update_field': 'shop_id',
            'target': 'new'
        })
        return {
            'type' : 'ir.actions.client',
            'name' : u'门店组织',
            'tag' : 'wgmf_customize_query.choose_shop_template',
            'target': 'new',
            'context' : context,
        }

    def reset_sql_for_Organization(self, sql):
        sql = '''select manager.name manager_id,office.name office_id,area.name area_id,a.* from (
%(sql_organization)s
) a
left join sale_shop shop on shop.id = a.shop_id
left join wgmf_sale_organization manager on shop.sale_organization = manager.id
left join wgmf_sale_organization office on office.id = manager.parent_id
left join wgmf_sale_organization area on area.id = office.parent_id;''' % {"sql_organization": sql}
        return sql


report_crocus_quantity_of_order()
report_crocus_quantity_of_order_condition()