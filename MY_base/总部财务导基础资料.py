#会计科目
select code as 编码,name as 名称,case type when 'other' then '常规科目' when 'view' then '视图' when 'liquidity' then '流动资金' 
when 'receivable' then '应收的' when 'payable' then '应付的'
else '其他' end
as 类型,
case wg_fzpz when 't' then '必填' else '未设置' end
as 必填辅助,
case wg_btywhb when 't' then '必填' else '未设置' end
as 必填业务伙伴 
from  account_account where active


#辅助核算项
select aa.code as 编码,aa.name as 名称,case aa.type when 'normal' then '常规科目' when 'view' then '视图' 
else '其他' end
as 类型,
ab.name as 上级科目名称,
ab.code as 上级编码
from  account_analytic_account aa
left join account_analytic_account ab on ab.id=aa.parent_id where aa.create_date>='2014-12-31 16:00:00' order by ab.code desc