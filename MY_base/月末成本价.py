﻿insert into standard_months_price (product_id,months,price_tzq,price,qty_md,qty_zt,price_in,qty_in)

(
with data_in as 
(---外购入库退货(zt-gc)
select id,standard_price,sum(qty) as qty from 
(
select b.id,c.standard_price,-sum(a.product_qty) as qty from stock_move a 
left join product_product b on b.id=a.product_id
left join product_template c on c.id=b.product_tmpl_id
where a.location_id=89 and a.location_dest_id=12 and a.state='done' 
and a.date>='2015-05-31 16:00:00' and a.date<'2015-06-30 16:00:00' 
group by b.id,c.id 
union all
----外购入库(gc-zt)
select b.id,c.standard_price,sum(a.product_qty) as qty from stock_move a 
left join product_product b on b.id=a.product_id
left join product_template c on c.id=b.product_tmpl_id
where a.location_id=12 and a.location_dest_id=89 and a.state='done' 
and a.date>='2015-05-31 16:00:00' and a.date<'2015-06-30 16:00:00' 
group by b.id,c.id ) data_in_ori group by id,standard_price
),
-------------------------------------------
---在途仓新增量(zt add)
data_zt_add as (
select product_id,sum(qty) as qty from 
(
select product_id,-sum(product_qty) as qty from stock_move
where state='done' and date>='2015-05-31 16:00:00' and date<'2015-06-30 16:00:00' 
and location_id=89 and location_dest_id!=89  
group by product_id
union all
select product_id,sum(product_qty) as qty from stock_move 
where state='done' and date>='2015-05-31 16:00:00' and date<'2015-06-30 16:00:00' 
and location_id!=89 and location_dest_id=89 
 group by product_id) data_add_ori group by product_id
),
---------------------------------------------
----门店增量
data_md_add as (

select product_id ,sum(qty) as qty from 

(--门店本月发出
select product_id,-sum(product_qty) as qty  from stock_move 
where state='done' and date>='2015-05-31 16:00:00' and date<'2015-06-30 16:00:00' 
and location_id in (--门店仓库位
select b.lot_stock_id as id from sale_shop a 
left join stock_warehouse b on a.warehouse_id=b.id
where a.yymode  in ('DZD','ZMD') and b.lot_stock_id not in (12,47)
)
and location_dest_id not in (--门店仓库位
select b.lot_stock_id as id from sale_shop a 
left join stock_warehouse b on a.warehouse_id=b.id
where a.yymode  in ('DZD','ZMD') and b.lot_stock_id not in (12,47)
) group by product_id
union all
--门店本月入库
select product_id,sum(product_qty) as qty from stock_move 
where state='done' and date>='2015-05-31 16:00:00' and date<'2015-06-30 16:00:00' 
and location_id not in (--门店仓库位
select b.lot_stock_id as id from sale_shop a 
left join stock_warehouse b on a.warehouse_id=b.id
where a.yymode  in ('DZD','ZMD') and b.lot_stock_id not in (12,47)
)
and location_dest_id  in (--门店仓库位
select b.lot_stock_id as id from sale_shop a 
left join stock_warehouse b on a.warehouse_id=b.id
where a.yymode  in ('DZD','ZMD') and b.lot_stock_id not in (12,47)
)  group by product_id
) date_md_add_ori group by product_id

)

---------------------
---主查询
select 
data_in.id as 入库id,
a.product_id as 上月月末价产品id,
data_zt_add.product_id as 在途新增id,
data_md_add.product_id as 门店新增id,


coalesce(data_in.id,a.product_id,data_zt_add.product_id,data_md_add.product_id) as 产品id ,---b.price_in as 本月入库价,
data_in.standard_price as 本月入库价, 
coalesce(data_in.qty,0) as 本月入库数,
coalesce(a.qty_md,0) as 上月门店数量,
coalesce(a.qty_zt,0) as 上月在途数量,
coalesce(data_zt_add.qty,0) as 在途新增量,
coalesce(a.qty_zt,0)+coalesce(data_zt_add.qty,0) as 月末在途数量,
coalesce(data_md_add.qty,0) as 门店新增量,
data_md_add.qty as 门店新增量2,
coalesce(data_md_add.qty,0)+coalesce(a.qty_md,0) as 月末门店数量,
  
coalesce(data_in.standard_price,0)*coalesce(data_in.qty,0)+coalesce(a.price,0)*coalesce(a.qty_md+a.qty_zt,0) as 总金额,
coalesce(a.qty_md+a.qty_zt,0)+coalesce(data_in.qty,0) as 总数量,

case when coalesce(a.qty_md+a.qty_zt,0) +coalesce( data_in.qty,0)!=0
then (coalesce(data_in.standard_price,0)*coalesce(data_in.qty,0)+coalesce(a.price,0)*coalesce(a.qty_md+a.qty_zt,0)) /  (coalesce(a.qty_md+a.qty_zt,0) +coalesce( data_in.qty,0))
else coalesce(data_in.standard_price,a.price,0) end as 本月月末价格
from  product_product pp 
join product_template pt on pt.id=pp.product_tmpl_id 
full join data_in on data_in.id=pp.id
full join standard_months_price a on (a.product_id=pp.id and a.months=32 )
---left join standard_months_price b on b.product_id=data_in.id
full join data_zt_add on data_zt_add.product_id=pp.id
full join data_md_add on data_md_add.product_id=pp.id

where  data_md_add.product_id=933  and pt.sale_ok='t'---and b.months=32 
order by 产品id)