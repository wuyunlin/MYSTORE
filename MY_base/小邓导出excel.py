from ...wgmf_public.ExportData import ExportData2Excel
class ExportTest(ExportData2Excel):
    # 报表名（唯一）且和导出方法中name参数值一样，为了唯一性可采用”模块名_报表名“方式命名
    _name = "wmgf_report_user_test"
    # 报表SQL，参数用%s表示
    _command = "select login uid,password as pwd from res_users where login = '%s' or login = '%s'"
    # 字段名，用作表头
    _fields = [u'用户ID', u'密码']
    # 导出Excel文件名
    _file_name = u"用户表"

ExportTest()


class report_crocus_quantity_of_order_condition(osv.osv_memory):
    def btn_download_data(self, cr, uid, ids, context=None):
        name = "wmgf_report_user_test"
        # 报表参数，来自于用户数据条件赛选
        args = "112233,00060"
        return {"type": "ir.actions.act_su",
                "url": "/ExportData2Excel?name=%s&args=%s" % (name, args)}