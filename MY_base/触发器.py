-- Function: fun_update_shop_stock()

-- DROP FUNCTION fun_update_shop_stock();

CREATE OR REPLACE FUNCTION fun_update_shop_stock()
  RETURNS trigger AS
$BODY$
declare
  mgin_id	wgmf_shop_stock_manage.id%type;
  mgout_id	wgmf_shop_stock_manage.id%type;
  ins_mgin_id	wgmf_shop_stock_manage.id%type;
  ins_mgout_id	wgmf_shop_stock_manage.id%type;
  shopout_id	sale_shop.id%type;
  shopin_id	sale_shop.id%type;
  company_id 	stock_move.company_id%type;
  location_id   stock_move.location_id%type;
  mv_qty	stock_move.product_qty%type;
begin
	--select shop stock out(shopout_id)
	select sp.id into shopout_id from sale_shop sp left join stock_warehouse sw on sw.id=sp.warehouse_id
		where sw.lot_stock_id=NEW.location_id;
	select id into mgout_id from wgmf_shop_stock_manage where loc_id=NEW.location_id and product_id=NEW.product_id limit 1;
	--select shop stock in(shopin_id)
	select sp.id into shopin_id from sale_shop sp left join stock_warehouse sw on sw.id=sp.warehouse_id
		where sw.lot_stock_id=NEW.location_dest_id;
	select id into mgin_id from wgmf_shop_stock_manage where loc_id=NEW.location_dest_id and product_id=NEW.product_id limit 1;
	if tg_op='INSERT' and NEW.state='done' then
		--process shop stock out(mgout_id)
		if mgout_id > 0 then
			update wgmf_shop_stock_manage set qty_stock=qty_stock - NEW.product_qty where id=mgout_id;
		else
			insert into wgmf_shop_stock_manage(shop_id,product_id,product_uom,qty_stock,loc_id,company_id,create_uid,create_date,write_uid,write_date)
				values (shopout_id,NEW.product_id,NEW.product_uom,-NEW.product_qty,NEW.location_id,NEW.company_id,NEW.create_uid,NEW.create_date,NEW.write_uid,NEW.write_date)
				returning id into ins_mgout_id;
			insert into shop_stock_outcoming_rel(mg_id,mvout_id) values (ins_mgout_id, NEW.id);
		end if;
		--prodess shop stock in(mgin_id)
		if mgin_id > 0 then
			update wgmf_shop_stock_manage set qty_stock=qty_stock + NEW.product_qty where id=mgin_id;
		else
			insert into wgmf_shop_stock_manage(shop_id,product_id,product_uom,qty_stock,loc_id,company_id,create_uid,create_date,write_uid,write_date)
				values (shopin_id,NEW.product_id,NEW.product_uom,NEW.product_qty,NEW.location_dest_id,NEW.company_id,NEW.create_uid,NEW.create_date,NEW.write_uid,NEW.write_date)
				returning id into ins_mgin_id;
			insert into shop_stock_incoming_rel(mg_id,mvin_id) values (ins_mgin_id, NEW.id);
		end if;

	elseif  tg_op='UPDATE' then
		--raise exception 'new qty:%', NEW.product_qty;
		--process shop stock out(mgout_id)
		if mgout_id > 0 then
			if exists(select 1 from shop_stock_outcoming_rel where mg_id=mgout_id and mvout_id=NEW.id limit 1) then
				if NEW.state='done' then 
					update wgmf_shop_stock_manage set qty_stock=qty_stock + OLD.product_qty - NEW.product_qty where id=mgout_id;
				elseif NEW.state <> 'done' then
					update wgmf_shop_stock_manage set qty_stock=qty_stock + OLD.product_qty where id=mgout_id;
				end if;
			else
				if NEW.state='done' then
					update wgmf_shop_stock_manage set qty_stock=qty_stock - NEW.product_qty where id=mgout_id;
					insert into shop_stock_outcoming_rel(mg_id,mvout_id) values (mgout_id, NEW.id);
				end if;
			end if;
		else
			if NEW.state='done' then 
				insert into wgmf_shop_stock_manage(shop_id,product_id,product_uom,qty_stock,loc_id,company_id,create_uid,create_date,write_uid,write_date)
					values (shopout_id,NEW.product_id,NEW.product_uom,-NEW.product_qty,NEW.location_id,NEW.company_id,NEW.create_uid,NEW.create_date,NEW.write_uid,NEW.write_date)
					returning id into ins_mgout_id;
				insert into shop_stock_outcoming_rel(mg_id,mvout_id) values (ins_mgout_id, NEW.id);
			end if;
		end if;
		--prodess shop stock in(mgin_id)
		if mgin_id > 0 then
			if exists(select 1 from shop_stock_incoming_rel where mg_id=mgin_id and mvin_id=NEW.id limit 1) then
				if NEW.state='done' then
					update wgmf_shop_stock_manage set qty_stock=qty_stock - OLD.product_qty + NEW.product_qty where id=mgin_id;
				elseif NEW.state <> 'done' then
					update wgmf_shop_stock_manage set qty_stock=qty_stock - OLD.product_qty  where id=mgin_id;
				end if;
			else
				if NEW.state='done' then 
					update wgmf_shop_stock_manage set qty_stock=qty_stock + NEW.product_qty where id=mgin_id;
					insert into shop_stock_incoming_rel(mg_id,mvin_id) values (mgin_id, NEW.id);
				end if;
			end if;
		else
			if NEW.state='done' then
				insert into wgmf_shop_stock_manage(shop_id,product_id,product_uom,qty_stock,loc_id,company_id,create_uid,create_date,write_uid,write_date)
					values (shopin_id,NEW.product_id,NEW.product_uom,NEW.product_qty,NEW.location_dest_id,NEW.company_id,NEW.create_uid,NEW.create_date,NEW.write_uid,NEW.write_date)
					returning id into ins_mgin_id;
				insert into shop_stock_incoming_rel(mg_id,mvin_id) values (ins_mgin_id, NEW.id);
			end if;
		end if;
		
	elseif  tg_op='DELETE' then
		--process shop stock out(mgout_id)
		if mgout_id > 0 then
			if exists(select 1 from shop_stock_outcoming_rel where mg_id=mgout_id and mvout_id=OLD.id) then
				if OLD.state='done' then
					update wgmf_shop_stock_manage set qty_stock=qty_stock + OLD.product_qty where id=mgout_id;
				end if;
				delete from shop_stock_outcoming_rel where mg_id=mgout_id and mvout_id=OLD.id;
			end if;
		end if;
		--prodess shop stock in(mgin_id)
		if mgin_id > 0 then
			if exists(select 1 from shop_stock_incoming_rel where mg_id=mgin_id and mvin_id=OLD.id) then
				if OLD.state='done' then
					update wgmf_shop_stock_manage set qty_stock=qty_stock - OLD.product_qty where id=mgin_id;
				end if;
				delete from shop_stock_incoming_rel where mg_id=mgin_id and mvin_id=OLD.id;
			end if;
		end if;
	end if;  
	return NEW;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fun_update_shop_stock()
  OWNER TO openerp;
