# -*- coding: utf-8 -*-
import time
from datetime import datetime
from openerp.osv import osv, fields, expression
from openerp.tools.translate import _
import openerp
import re
from openerp.addons.mm.controllers.main import TOTAL_SYS_BILLTYPE_ID, TOTAL_STATE_ID, V_BIGCAT_ID, MAX_ATTEND_TIME, WASH_AIDTYPE_ID, REWORK_AIDTYPE_ID, WASH_STD_QTY

class bs_piecegroup(osv.osv):
    _name = 'bs.piecegroup'
    _description = u'计件组'
    _order = 'code'
    _rec_name = "name"

    _columns = {
        'code': fields.char(u'编码', size=64, required=True),
        'name': fields.char(u'名称', size=64, required=True),
        'isvalid': fields.boolean( u"启用", help=u'1:启用，0：未启用'),
        'res_company_id': fields.many2one('res.company', u'所属公司', required=True),
        'bs_wcenter_id': fields.many2one('bs.wcenter', u'工作中心', required=True),
        'bs_devicegroup_id': fields.many2one('bs.devicegroup', u'所属机群', required=True),
        'std_qty':fields.float(u'标准件基数',digits=(8, 2)),
        'remark': fields.char(u'备注', size=500),
        'create_date': fields.datetime(u'创建时间', readonly=True, help=u"当前系统时间"),
        'write_date': fields.datetime(u'修改时间', readonly=True, help=u"当前系统时间"),
        "create_uid": fields.many2one("res.users", u"创建人", readonly=True),
        'write_uid': fields.many2one("res.users", u"修改人", readonly=True),
    }

    _sql_constraints = [
        ('company_name_unique', 'unique(res_company_id, name)', u'计件组名称重复。'),
    ]

    _defaults = {
        'res_company_id': lambda s, cr, uid, c: s.pool.get('res.company')._company_default_get(cr, uid, 'bs.piecegroup', context=c),
        'isvalid': True,
        'std_qty': 1.00,
    }

    def _check_std_qty(self, cr, uid, ids, context=None):
        for piecegroup in self.browse(cr, uid, ids, context=context):
            if piecegroup.std_qty < 0.00:
                raise osv.except_osv(_('警告!'), _('标准件基数必须大于等于0。'))
        return True

    _constraints = [
        (_check_std_qty, '', []),
    ]

    def onchange_res_company_id(self, cr, uid, ids, res_company_id=False, context=None):
        res = {}
        if res_company_id:
            return {'value': {
                'bs_wcenter_id': False,
                'bs_devicegroup_id': False,
            }}
        return res

    def onchange_bs_wcenter_id(self, cr, uid, ids, bs_wcenter_id=False, context=None):
        res = {}
        if bs_wcenter_id:
            return {'value': {'bs_devicegroup_id': False,}}
        return res

    def unlink(self, cr, uid, ids, context=None):
        for each in self.browse(cr, uid, ids, context=context):
            if each.isvalid:
                raise osv.except_osv(_("错误!"), _("不需要的计件组可置为“不启用”。"))
            else:
                raise osv.except_osv(_("错误!"), _("计件组已置为“不启用”，不能删除。"))
        res = super(bs_piecegroup, self).unlink(cr, uid, ids, context)
        return res

    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        data = self.browse(cr, uid, id, context=context)
        default.update(name = data.name + _('(副本)'))
        return super(bs_piecegroup, self).copy(cr, uid, id, default, context=context)

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        context = context or {}
        if context.get('is_bs_piece_stdqty_product_id',False) == '1':
            ids = []
            vals = {
                'res_company_id': context.get('res_company_id',False),
                'bs_wcenter_id': context.get('bs_wcenter_id',False),
            }
            if vals['res_company_id'] and vals['bs_wcenter_id']:
                cr.execute(
                    '''
                    SELECT DISTINCT a.id
                      FROM bs_piecegroup a
                    LEFT JOIN bs_wcenter b ON b.id = a.bs_wcenter_id
                    WHERE b.bs_wcenter_id = %(bs_wcenter_id)s;
                    ''' % vals
                )
                ids = [r[0] for r in cr.fetchall()]
            args.append((('id', 'in', ids)))
        res = super(bs_piecegroup, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)
        return res

    def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
        args = args or []
        if name:
            positive_operators = ['=', 'ilike', '=ilike', 'like', '=like']
            ids = []
            if operator in positive_operators:
                ids = self.search(cr, user, [('name','=',name)]+ args, limit=limit, context=context)
            if not ids and operator not in expression.NEGATIVE_TERM_OPERATORS:
                ids = set()
                ids.update(self.search(cr, user, args + [('name',operator,name)], limit=limit, context=context))
                if not limit or len(ids) < limit:
                    ids.update(self.search(cr, user, args + [('name',operator,name)], limit=(limit and (limit-len(ids)) or False) , context=context))
                ids = list(ids)
            if not ids and operator in positive_operators:
                ptrn = re.compile('(\[(.*?)\])')
                res = ptrn.search(name)
                if res:
                    ids = self.search(cr, user, [('name','=', res.group(2))] + args, limit=limit, context=context)
        else:
            ids = self.search(cr, user, args, limit=limit, context=context)
        result = self.name_get(cr, user, ids, context=context)
        return result

bs_piecegroup()

class bs_piece_stdqty(osv.osv):
    _name = 'bs.piece.stdqty'
    _description = u'标准件基数'
    _order = 'id desc'

    _columns = {
        'res_company_id': fields.many2one('res.company', u'所属公司', required=True),
        'type': fields.selection([('pstep','按工序与产品计件'), ('ppiece','按计件组与产品计件')], u'计件方式', size=16, required=True, readonly=True),
        'bs_wprocess_id': fields.many2one('bs.wprocess', u'工序'),
        'product_id': fields.many2one('product.product', u'产品'),
        'bs_piecegroup_id': fields.many2one('bs.piecegroup', u'计件组'),
        'std_qty':fields.float(u'标准件基数',digits=(8, 2)),
        'isvalid': fields.boolean( u"有效", help=u'1:有效，0：无效'),
        'remark': fields.char(u'备注', size=512),
        'create_date': fields.datetime(u'创建时间', readonly=True, help=u"当前系统时间"),
        'write_date': fields.datetime(u'修改时间', readonly=True, help=u"当前系统时间"),
        "create_uid": fields.many2one("res.users", u"创建人", readonly=True),
        'write_uid': fields.many2one("res.users", u"修改人", readonly=True),
        'bs_wcenter_id': fields.many2one('bs.wcenter', u'工作中心', required=True),
    }

    _defaults = {
        'res_company_id': lambda s, cr, uid, c: s.pool.get('res.company')._company_default_get(cr, uid, 'bs.piecegroup', context=c),
        'isvalid': True,
        'std_qty': 1.00,
        'type': 'pstep',
    }

    def _check_wprocess_product_id(self, cr, uid, ids, context=None):
        for piece in self.browse(cr, uid, ids, context=context):
            if piece.bs_wprocess_id and piece.product_id:
                cr.execute("SELECT id FROM bs_piece_stdqty WHERE bs_wprocess_id=%d AND product_id=%d AND res_company_id=%d;" % (piece.bs_wprocess_id.id, piece.product_id.id, piece.res_company_id.id))
                data =  cr.fetchall()
                if len(data) > 1 or data[0][0] != ids[0]:
                    raise osv.except_osv(_('警告!'), _('%s 已经存在按工序+产品计件（%s + %s）的标准件基数。' % (piece.res_company_id.name, piece.bs_wprocess_id.name, piece.product_id.name) ))
        return True

    def _check_piecegroup_product_id(self, cr, uid, ids, context=None):
        for piece in self.browse(cr, uid, ids, context=context):
            if piece.bs_piecegroup_id and piece.product_id:
                cr.execute("SELECT id FROM bs_piece_stdqty WHERE bs_piecegroup_id=%d AND product_id=%d AND res_company_id=%d;" % (piece.bs_piecegroup_id.id, piece.product_id.id, piece.res_company_id.id))
                data =  cr.fetchall()
                if len(data) > 1 or data[0][0] != ids[0]:
                    raise osv.except_osv(_('警告!'), _('%s 已经存在按计件组+产品计件（%s + %s）的标准件基数。' % (piece.res_company_id.name, piece.bs_piecegroup_id.name, piece.product_id.name) ))
        return True

    def _check_std_qty(self, cr, uid, ids, context=None):
        for piece in self.browse(cr, uid, ids, context=context):
            if piece.std_qty < 0.00:
                raise osv.except_osv(_('警告!'), _('标准件基数必须大于等于0。'))
        return True

    _constraints = [
        (_check_wprocess_product_id, '', []),
        (_check_piecegroup_product_id, '', []),
        (_check_std_qty, '', []),
    ]

    def onchange_res_company_id(self, cr, uid, ids, res_company_id=False, context=None):
        res = {}
        if res_company_id:
            return {'value': {
                'bs_wprocess_id': False,
                'product_id': False,
                'bs_piecegroup_id': False,
                'std_qty': 1.00,
                'bs_wcenter_id': False,
            }}
        return res

    def onchange_bs_wcenter_id(self, cr, uid, ids, bs_wcenter_id=False, context=None):
        res = {}
        if bs_wcenter_id:
            return {'value': {
                'bs_wprocess_id': False,
                'product_id': False,
            }}
        return res


    def onchange_type(self, cr, uid, ids, type='', context=None):
        res = {}
        if type:
            return {'value': {
                'bs_wprocess_id': False,
                'product_id': False,
                'bs_piecegroup_id': False,
                'std_qty': 1.00,
            }}
        return res

    def name_get(self, cr, uid, ids, context=None):
        res = []
        context = context or {}
        if not ids:
            return []
        if isinstance(ids, (int, long)):
            ids = [ids]
        for record in self.read(cr, uid, ids, ['product_id', 'bs_wprocess_id', 'bs_piecegroup_id'], context=context):
            product_id = record['product_id'] or False
            bs_wprocess_id = record['bs_wprocess_id'] or False
            bs_piecegroup_id = record['bs_piecegroup_id'] or False
            product = product_id and product_id[1] or ''
            wprocess = bs_wprocess_id and bs_wprocess_id[1] or ''
            piecegroup = bs_piecegroup_id and bs_piecegroup_id[1] or ''
            name = wprocess + '--' + product if wprocess else piecegroup + '--' + product
            res.append((record['id'], name))
        return res

    def unlink(self, cr, uid, ids, context=None):
        for each in self.browse(cr, uid, ids, context=context):
            if each.isvalid:
                raise osv.except_osv(_("错误!"), _("不需要的标准件基数可置为“无效”。"))
            else:
                raise osv.except_osv(_("错误!"), _("标准件基数已置为“无效”，不能删除。"))
        res = super(bs_piece_stdqty, self).unlink(cr, uid, ids, context)
        return res

    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        default.update(bs_wprocess_id = False)
        default.update(product_id = False)
        default.update(bs_piecegroup_id = False)
        default.update(std_qty = 1.00)
        return super(bs_piece_stdqty, self).copy(cr, uid, id, default, context=context)

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        ''' 按工序和产品计件录入选择产品 '''
        context = context or {}
        if context.get('is_mm_productpiece_line_pstep',False) == '1':
            args.append((('bs_wcenter_id', '=', context.get('bs_wcenter_id',False))))
        res = super(bs_piece_stdqty, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)
        # print '===========',args
        return res

    def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
        args = args or []
        context = context or {}
        if name:
            sql = '''
            SELECT a.id
              FROM bs_piece_stdqty a
            LEFT JOIN product_product b ON b.id = a.product_id
            LEFT JOIN bs_wprocess c ON c.id = a.bs_wprocess_id
            LEFT JOIN bs_piecegroup d ON d.id = a.bs_piecegroup_id
            WHERE 1=1
              AND a.isvalid='t'
              AND (b.default_code LIKE '%%%s%%'
              OR b.name_template LIKE '%%%s%%'
              OR c.name LIKE '%%%s%%'
              OR d.name LIKE '%%%s%%')
            ''' % (name.strip(),name.strip(),name.strip(),name.strip())
            if context.get('is_mm_productpiece_line_pstep',False) == '1' and context.get('res_company_id',False) and context.get('bs_piecegroup_id',False):
                rec = self.pool.get('bs.piecegroup').browse(cr, user, context.get('bs_piecegroup_id',False))
                product_ids = [int(line.product_id.id) for line in rec.bs_devicegroup_id.ability_line] + [0,0]
                bs_wprocess_ids = [int(line.id) for line in rec.bs_wcenter_id.bs_wprocess_line] + [0,0]
                add_sql = ''' AND a.res_company_id = %d
                AND a.product_id IN %s
                and a.bs_wprocess_id IN %s
              ''' % (context.get('res_company_id',False), str(tuple(product_ids)), str(tuple(bs_wprocess_ids)))
                # sql = sql + add_sql
                # print '-------sql-------',sql
                cr.execute(sql)
                ids = map(lambda x: x[0], cr.fetchall())
            else:
                ids = []
        else:
            ids = self.search(cr, user, args, limit=limit, context=context)
        result = self.name_get(cr, user, ids, context=context)
        return result

bs_piece_stdqty()

class mm_aidtime_update(osv.osv_memory):
    _name = 'mm.aidtime.update'
    _description = u'批量刷新'

    _columns = {
        'v_aidtype_id': fields.many2one('bs.publictype.d', u'辅助类别', domain=[('bs_publictype_id.viewname','=','v_aidtype'), ('id','not in',(WASH_AIDTYPE_ID, REWORK_AIDTYPE_ID))], widget='seletion',  select=True),
        'attend_time': fields.float(u'出勤工时', digits=(8, 2)),
        'done_qty': fields.float(u'洗罐子数量', digits=(16, 2)),
    }

    _defaults = {
        'attend_time': 11.00,
        'done_qty': 0.00,
    }

    def onchange_attend_time(self, cr, uid, ids, attend_time=0.00, context=None):
        res = {'value':{}}
        if attend_time < 0:
            res['warning'] = {'title': _('警告'), 'message': _('出勤工时不能小于0，请重新设置。')}
            res['value'].update({'attend_time': 0.00})
        if attend_time > 24:
            res['warning'] = {'title': _('警告'), 'message': _('出勤工时不能大于24, 请重新设置。')}
            res['value'].update({'attend_time': 0.00})
        elif attend_time > MAX_ATTEND_TIME:
            res['warning'] = {'title': _('警告'), 'message': _('请注意，出勤工时已经超过%d。' % MAX_ATTEND_TIME)}
            res['value'].update({'attend_time': attend_time})
        return res

    def onchange_done_qty(self, cr, uid, ids, done_qty=0.00, context=None):
        res = {'value':{}}
        if done_qty < 0:
            res['warning'] = {'title': _('警告'), 'message': _('洗罐子数量不能小于0，请重新设置。')}
            res['value'].update({'done_qty': 0.00})
        return res

    def mass_update(self, cr, uid, ids, context=None):
        ''' 异常工时批量刷新出勤工时 '''
        context = context or {}
        ids = [ids] if isinstance(ids, (int, long)) else ids
        mm_aidtime_id = context.get('active_id', False)
        for rec in self.browse(cr, uid, ids, context):
            v_aidtype_id = rec.v_aidtype_id.id
            vals = {
                'mm_aidtime_id': mm_aidtime_id,
                'attend_time': rec.attend_time,
                'v_aidtype_id': v_aidtype_id,
            }
            if v_aidtype_id:
                cr.execute("UPDATE mm_aidtime_d SET attend_time=%(attend_time)s,v_aidtype_id=%(v_aidtype_id)s WHERE mm_aidtime_id=%(mm_aidtime_id)s;" % vals)
            else:
                cr.execute("UPDATE mm_aidtime_d SET attend_time=%(attend_time)s WHERE mm_aidtime_id=%(mm_aidtime_id)s;" % vals)
            self.pool.get('mm.aidtime').write(cr, uid, [mm_aidtime_id], vals={}, context=context)
        return True

    def mass_update_wash(self, cr, uid, ids, context=None):
        ''' 异常工时批量刷新数量 '''
        context = context or {}
        ids = [ids] if isinstance(ids, (int, long)) else ids
        mm_aidtime_id = context.get('active_id', False)
        for rec in self.browse(cr, uid, ids, context):
            vals = {
                'mm_aidtime_id': mm_aidtime_id,
                'done_qty': rec.done_qty,
            }
            cr.execute("UPDATE mm_aidtime_d SET done_qty=%(done_qty)s WHERE mm_aidtime_id=%(mm_aidtime_id)s;" % vals)
            self.pool.get('mm.aidtime').write(cr, uid, [mm_aidtime_id], vals={}, context=context)
        return True

    def mass_update_timepiece(self, cr, uid, ids, context=None):
        ''' 计时计件批量刷新出勤工时 '''
        context = context or {}
        ids = [ids] if isinstance(ids, (int, long)) else ids
        mm_timepiece_id = context.get('active_id', False)
        for rec in self.browse(cr, uid, ids, context):
            vals = {
                'mm_timepiece_id': mm_timepiece_id,
                'attend_time': rec.attend_time,
            }
            cr.execute("UPDATE mm_timepiece_d SET attend_time=%(attend_time)s WHERE mm_timepiece_id=%(mm_timepiece_id)s;" % vals)
            self.pool.get('mm.timepiece').write(cr, uid, [mm_timepiece_id], vals={}, context=context)
        return True

mm_aidtime_update()

class mm_aidtime(osv.osv):
    _name = 'mm.aidtime'
    _inherit = ['mail.thread']
    _description = u'异常工时'
    _order = 'date desc, bs_class_id, type'
    _rec_name = "billcode"

    _columns = {
        'billcode': fields.char(u'计件单号', size=64, required=True, readonly=True),
        'res_company_id': fields.many2one('res.company', u'所属公司', required=True),
        'bs_wcenter_id': fields.many2one('bs.wcenter', u'工作中心', required=True),
        'bs_class_id': fields.many2one('bs.class', u'班次', required=True),
        'date': fields.date(u'出勤日期', required=True),
        'type': fields.selection([('normal','正常'), ('wash','洗罐子'), ('rework','返工'), ('timely','计时产品')], u'异常类型', size=16, required=True),
        'v_aidtype_id': fields.many2one('bs.publictype.d', u'辅助类别', domain="[('bs_publictype_id.viewname','=','v_aidtype')]", widget='seletion',  select=True, readonly=True),
        'product_id': fields.many2one('product.product', u'成本归集产品', domain="[('supply_method', '=', 'produce')]"),
        'done_qty': fields.float(u'数量', digits=(16, 2)),
        'remark': fields.char(u'备注', size=512),
        'v_wfstate_id': fields.many2one('bs.publictype.d', u'状态', domain="[('bs_publictype_id.viewname','=','v_wfstate')]", widget='seletion',  select=True, required=True),
        'sys_billtype_id': fields.many2one('sys.billtype', u'单据类别', required=True, readonly=True),
        'res_users_idcheck': fields.many2one('res.users', u'审核人', readonly=True, select=True),
        'check_date': fields.datetime(u'审核时间', readonly=True, select=True),
        'create_date': fields.datetime(u'创建时间', readonly=True, help=u"当前系统时间"),
        'write_date': fields.datetime(u'修改时间', readonly=True, help=u"当前系统时间"),
        'create_uid': fields.many2one("res.users", u"创建人", readonly=True),
        'write_uid': fields.many2one("res.users", u"修改人", readonly=True),
        'aidtime_line': fields.one2many('mm.aidtime.d', 'mm_aidtime_id', u'异常工时明细(正常)'),
        'aidtime_line1': fields.one2many('mm.aidtime.d', 'mm_aidtime_id', u'异常工时明细(返工)'),
        'aidtime_line2': fields.one2many('mm.aidtime.d', 'mm_aidtime_id', u'异常工时明细(洗罐子)'),
        'aidtime_line3': fields.one2many('mm.aidtime.d', 'mm_aidtime_id', u'异常工时明细(计时产品)'),
    }

    _sql_constraints = [
        ('billcode_unique', 'unique(billcode)', u'单号重复!'),
    ]

    _defaults = {
        'done_qty': 0.00,
        'type': 'normal',
    }
    def _check_write_date(self, cr, uid, ids, context=None):
        ''' 校验公司+日期+工作中心 '''
        ids = [ids] if isinstance(ids, (int, long)) else ids
        for check in self.browse(cr, uid, ids, context=context):
            vals = {
                'date': check.date,
                'res_company_id': check.res_company_id.id,
                'v_wfstate_id': TOTAL_STATE_ID['cancel'],
                'bs_wcenter_id': check.bs_wcenter_id.id,
                'bs_class_id':check.bs_class_id.id,
                'type':check.type,
                'id': check.id,
                'product_id':check.product_id.id or 0,
            }
            cr.execute('''SELECT id, billcode 
                FROM mm_aidtime 
                WHERE res_company_id = %(res_company_id)s 
                AND bs_wcenter_id = %(bs_wcenter_id)s 
                AND date = '%(date)s' 
                AND type = '%(type)s'
                AND v_wfstate_id != %(v_wfstate_id)s 
                AND bs_class_id = %(bs_class_id)s
                AND product_id = %(product_id)s
                AND id != %(id)s;''' % vals)
            data = cr.fetchall()
            if len(data) >= 1:
                raise osv.except_osv(_('警告!'), _('%s %s 的异常工时录入单已存在, 请找到单号‘%s’进行操作。'% (check.bs_wcenter_id.name, check.date, data[0][1]) ))
        return True

    def _check_timepiece_daycheck(self, cr, uid, ids, context=None):
        ''' 日审之后不能创建录入单据 '''
        ids = [ids] if isinstance(ids, (int, long)) else ids
        for timepiece in self.browse(cr, uid, ids, context=context):
            res_company_id = timepiece.res_company_id.id
            date = timepiece.date
            if res_company_id and date:
                cr.execute("SELECT * FROM mm_timepiece_daycheck WHERE res_company_id = %d AND date = '%s' AND v_wfstate_id = %d;" % (res_company_id, date, TOTAL_STATE_ID['done']))
                if len(cr.fetchall()) >= 1:
                    raise osv.except_osv(_('提示!'), _('%s的计时计件已日审核, 不能再进行操作。' % date ))
        return True

    def _check_done_qty(self, cr, uid, ids, context=None):
        for aidtime in self.browse(cr, uid, ids, context=context):
            if aidtime.done_qty < 0.00:
                raise osv.except_osv(_('错误!'), _('数量必须大于等于0。'))
        return True

    def _check_write(self, cr, uid, ids, context=None):
        for aidtime in self.browse(cr, uid, ids, context=context):
            if aidtime.type == 'normal':
                for line in aidtime.aidtime_line:
                    if not line.v_aidtype_id.id and line.attend_time:
                        raise osv.except_osv(_('错误!'), _('%s 的辅助类别不能为空。'% line.bs_employee_id.res_users_id.name))

    _constraints = [
        (_check_timepiece_daycheck, '', []),
        (_check_done_qty, '', []),
        (_check_write_date, '', []),
        # (_check_write, '', []),
    ]

    def onchange_type(self, cr, uid, ids, type='', bs_wcenter_id=False, context=None):
        res = {}
        if type:
            aidtime_line, aidtime_line1 = False, False
            if type == 'wash':
                v_aidtype_id = WASH_AIDTYPE_ID
                #经典固元膏
                cr.execute("SELECT id FROM product_product WHERE default_code='2.40013';")
                product_id = cr.fetchall()[0][0]
                if bs_wcenter_id:
                    cr.execute('SELECT id AS bs_employee_id FROM bs_employee WHERE bs_wcenter_id=%d;' % bs_wcenter_id)
                    aidtime_line1 = cr.dictfetchall()
            elif type == 'rework':
                v_aidtype_id = REWORK_AIDTYPE_ID
                product_id = False
                if bs_wcenter_id:
                    cr.execute(' SELECT id AS bs_employee_id FROM bs_employee WHERE bs_wcenter_id=%d;' % bs_wcenter_id)
                    aidtime_line1 = cr.dictfetchall()
            elif type == 'normal':
                v_aidtype_id, product_id = False, False
                if bs_wcenter_id:
                    cr.execute(' SELECT id AS bs_employee_id FROM bs_employee WHERE bs_wcenter_id=%d;' % bs_wcenter_id)
                    aidtime_line = cr.dictfetchall()
            elif type == 'timely':
                v_aidtype_id, product_id = False, False
            return {'value': {
                'done_qty': 0.00,
                'v_aidtype_id': v_aidtype_id,
                'product_id': product_id,
                'aidtime_line': aidtime_line or False,
                'aidtime_line1': aidtime_line1 or False,
            }}
        return res

    def onchange_bs_wcenter_id(self, cr, uid, ids, bs_wcenter_id=False, type='', context=None):
        res = {'value': {'aidtime_line': False, 'aidtime_line1': False}}
        if bs_wcenter_id:
            res = {'value': {
                    'aidtime_line': False,
                    'aidtime_line1': False,
                    'aidtime_line2': False,
                    'aidtime_line3': False,
                }}
            return res
            cr.execute(' SELECT id AS bs_employee_id, 0.00 attend_time, 0.00 done_qty FROM bs_employee WHERE bs_wcenter_id=%d;' % bs_wcenter_id)
            aidtime_line = cr.dictfetchall()
            if type == 'normal':
                res = {'value': {
                    'aidtime_line': aidtime_line,
                    'aidtime_line1': False,
                    'aidtime_line2': False,
                }}
            elif type == 'rework':
                res = {'value': {
                    'aidtime_line': False,
                    'aidtime_line1': aidtime_line,
                    'aidtime_line2': False,
                }}
            elif type == 'wash':
                res = {'value': {
                    'aidtime_line': False,
                    'aidtime_line1': False,
                    'aidtime_line2': aidtime_line,
                }}
            return res
        return res

    def onchange_res_company_id(self, cr, uid, ids, res_company_id=False, context=None):
        res = {}
        if res_company_id:
            args = []
            args.append(('res_company_id', '=', res_company_id))
            args.append(('name', '=',  u'早班'))
            bs_class_ids = self.pool.get('bs.class').search(cr, uid, args, context=context)
            return {'value': {
                'bs_class_id': bs_class_ids and bs_class_ids[0] or False,
                'bs_wcenter_id': False,
                'aidtime_line': False,
                'aidtime_line1': False,
                'aidtime_line2': False,
                'aidtime_line3': False,
            }}
        return res

    _SYS_BILLTYPE_ID = TOTAL_SYS_BILLTYPE_ID['mm_aidtime']

    def default_get(self, cr, uid, fields, context=None):
        res = super(mm_aidtime, self).default_get(cr, uid, fields, context=context)
        res_company_id = self.pool.get('res.users').browse(cr, uid, uid).company_id.id
        date = openerp.osv.fields.date.context_today(self, cr, uid, context=context)
        value = {
            'billcode': u'系统自动生成',
            'date': date,
            'sys_billtype_id': self._SYS_BILLTYPE_ID,
            'v_wfstate_id': TOTAL_STATE_ID['draft'],
            'res_company_id': res_company_id,
        }
        res.update(value)
        return res

    def create(self, cr, uid, vals, context=None):
        vals = vals or {}
        now = fields.date.context_today(self, cr, uid, context=context)
        dt = vals.get('date', now)
        sys_billtype_id = self._SYS_BILLTYPE_ID
        if vals.get('billcode', u'系统自动生成') == u'系统自动生成':
            billcode = self.pool.get('sys.billtype').get(cr, uid, sys_billtype_id, dt, context=context)
            vals.update(billcode=billcode)
        value ={
            'res_company_id': vals.get('res_company_id',0),
            'billtype': sys_billtype_id,
            'billdate': dt,
        }
        data = self.pool.get('sys.fnclose.filter').check_close(cr, uid, value)
        if data.get('isclosed', False):
            raise osv.except_osv(_('警告！'), _('计划生产模块已关账，不能创建单据。'))
        # print '--------------------vals------------',vals
        aidtime_line = vals.get('aidtime_line', False) or vals.get('aidtime_line1', False) or vals.get('aidtime_line2', False) or vals.get('aidtime_line3', False) or [] 
        print '--------------------vals------------',aidtime_line
        #按产品计件录入
        total_done_qty = 0.00
        if vals.get('type', False) and vals.get('type', False) == 'wash' and vals.get('aidtime_line2', False):
            for line in vals.get('aidtime_line2', False):
                if line[2] and type(line[2]) is not list:
                    done_qty = line[2].get('done_qty', 0.00)
                    total_done_qty += done_qty
            vals.update(done_qty=total_done_qty)
            vals.update(aidtime_line=[])
            vals.update(aidtime_line1=[])
            vals.update(aidtime_line3=[])
        if vals.get('type', False) and vals.get('type', False) == 'normal':
            vals.update(aidtime_line=aidtime_line)
            vals.update(aidtime_line1=[])
            vals.update(aidtime_line2=[])
            vals.update(aidtime_line3=[])
        if vals.get('type', False) and vals.get('type', False) == 'rework':
            vals.update(aidtime_line=[])
            vals.update(aidtime_line2=[])
            vals.update(aidtime_line3=[])
        if vals.get('type', False) and vals.get('type', False) == 'timely':
            vals.update(aidtime_line=[])
            vals.update(aidtime_line1=[])
            vals.update(aidtime_line2=[])
        return super(mm_aidtime, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        print "----wriet----",vals
        write_flg = super(mm_aidtime, self).write(cr, uid, ids, vals, context=context)
        ids = [ids] if isinstance(ids, (int, long)) else ids
        for timepiece in self.browse(cr, uid, ids):
            if timepiece.type == 'wash': #洗罐子
                done_qty = sum([line.done_qty for line in timepiece.aidtime_line2])
                if done_qty != timepiece.done_qty:
                    super(mm_aidtime,self).write(cr, uid, timepiece.id, {'done_qty': done_qty})
        return write_flg

    def _check_line(self, cr, uid, check, context=None):
        todo = []
        self._check_timepiece_daycheck(cr, uid, [check.id], context=context)
        if check.type == 'normal' and check.aidtime_line:
            for line in check.aidtime_line:
                if line.attend_time == 0.00:
                    self.pool.get('mm.aidtime.d').unlink(cr, uid, [line.id])
                if line.attend_time != 0.00:
                    todo.append(line.id)
        elif check.type == 'rework' and check.aidtime_line1:
            if not check.done_qty:
                raise osv.except_osv(_('错误!'),_('数量必须大于0。'))
            for line in check.aidtime_line1:
                if line.attend_time == 0.00:
                    self.pool.get('mm.aidtime.d').unlink(cr, uid, [line.id])
                if line.attend_time != 0.00:
                    todo.append(line.id)
        elif check.type == 'wash' and check.aidtime_line2:
            for line in check.aidtime_line2:
                if line.done_qty == 0.00:
                    self.pool.get('mm.aidtime.d').unlink(cr, uid, [line.id])
                if line.done_qty != 0.00:
                    todo.append(line.id)
        elif check.type == 'timely' and check.aidtime_line3:
            for line in check.aidtime_line3:
                if line.attend_time == 0.00:
                    self.pool.get('mm.aidtime.d').unlink(cr, uid, [line.id])
                if line.attend_time != 0.00:
                    todo.append(line.id)
        return todo

    def check_done(self, cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids, context=context):
            self._check_write(cr, uid, [check.id], context=context)
            todo = self._check_line(cr, uid, check, context=context)
            if todo:
                vals = {
                    'v_wfstate_id': TOTAL_STATE_ID['done'],
                    'check_date':  time.strftime('%Y-%m-%d %H:%M:%S'),
                    'res_users_idcheck': uid,
                }
                self.write(cr, uid, [check.id], vals, context=context)
            else:
                raise osv.except_osv(_('错误!'),_('不能审核没有明细行（工时或数量都为0）的异常工时，请核查。'))

            context.update({
                'v_approve_id': 268,
                "res_company_id": check.res_company_id.id,
                "sys_billtype_id": self._SYS_BILLTYPE_ID,
                "res_id": check.id,
                "subject": u"异常工时单",
                "body_html": u""" 异常工时单：%s 已生效； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">This link</a>""" % (check.billcode,check.id,self._name),
                "auto_delete":True,
            })
            self.pool.get('bs.approve').func_send_mail(cr, uid, context)
        return True

    def check_order(self,cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids, context=context):
            self._check_write(cr, uid, [check.id], context=context)
            todo = self._check_line(cr, uid, check, context=context)
            if todo:
                vals = {
                    'v_wfstate_id': TOTAL_STATE_ID['approve'],
                    'check_date': False,
                    'res_users_idcheck': False,
                }
                self.write(cr, uid, [check.id], vals, context=context)
            else:
                raise osv.except_osv(_('错误!'),_('不能提交没有明细行（工时或数量都为0）的异常工时，请核查。'))

            context.update({
                'v_approve_id': 267,
                "res_company_id": check.res_company_id.id,
                "sys_billtype_id": self._SYS_BILLTYPE_ID,
                "res_id": check.id,
                "subject": u"异常工时单",
                "body_html": u""" 异常工时单：%s 已提交，请审核； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">This link</a>""" % (check.billcode,check.id,self._name),
                "auto_delete":True,
            })
            self.pool.get('bs.approve').func_send_mail(cr, uid, context)
        return True

    def check_draft(self,cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids, context=context):
            self._check_timepiece_daycheck(cr, uid, [check.id], context=context)
            vals = {
                'v_wfstate_id': TOTAL_STATE_ID['draft'],
                'check_date': False,
                'res_users_idcheck': False
            }
            self.write(cr, uid, [check.id],vals)

            context.update({
                'v_approve_id': 2002,
                "res_company_id": check.res_company_id.id,
                "sys_billtype_id": self._SYS_BILLTYPE_ID,
                "res_id": check.id,
                "subject": u"异常工时单",
                "body_html": u""" 异常工时单：%s 已反审，请处理； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">This link</a>""" % (check.billcode,check.id,self._name),
                "auto_delete":True,
            })
            self.pool.get('bs.approve').func_send_mail(cr, uid, context)
        return True

    def check_cancel(self,cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids, context=context):
            vals = {
                'v_wfstate_id': TOTAL_STATE_ID['cancel'],
                'check_date': False,
                'res_users_idcheck': False,
            }
            self.write(cr, uid, [check.id], vals, context=context)
        return True

    def unlink(self, cr, uid, ids, context=None):
        for aidtime in self.browse(cr, uid, ids, context=context):
            if aidtime.v_wfstate_id.id == TOTAL_STATE_ID['done']:
                raise osv.except_osv(_('错误!'), _('生效状态不能删除。'))
            if aidtime.v_wfstate_id.id == TOTAL_STATE_ID['cancel']:
                raise osv.except_osv(_('错误!'), _('单据已作废，不能删除。'))
            if aidtime.v_wfstate_id.id == TOTAL_STATE_ID['approve']:
                raise osv.except_osv(_('错误!'), _('审批中的单据，不能删除但可作废。'))
            cr.execute('DELETE FROM mm_aidtime_d WHERE mm_aidtime_id=%d;' % aidtime.id)
        return super(mm_aidtime, self).unlink(cr, uid, ids, context=context)

    def copy(self, cr, uid, id, default=None, context=None):
        context = context or {}
        default = default or {}
        now = fields.date.context_today(self, cr, uid, context=context)
        default.update(billcode = u'系统自动生成')
        default.update(date = now)
        default.update(v_wfstate_id = TOTAL_STATE_ID['draft'])
        return super(mm_aidtime, self).copy(cr, uid, id, default, context=context)


mm_aidtime()

class mm_aidtime_d(osv.osv):
    _name = 'mm.aidtime.d'
    _description = u'异常工时明细'
    _order = 'id'

    _columns = {
        'mm_aidtime_id': fields.many2one('mm.aidtime', u'异常工时', required=True),
        'bs_employee_id': fields.many2one('bs.employee', u'出勤人员', required=True),
        'v_aidtype_id': fields.many2one('bs.publictype.d', u'辅助类别', domain=[('bs_publictype_id.viewname','=','v_aidtype'), ('id','not in',(WASH_AIDTYPE_ID, REWORK_AIDTYPE_ID))], widget='seletion',  select=True),
        'attend_time': fields.float(u'出勤工时', digits=(8, 2), required=True),
        'done_qty': fields.float(u'洗罐子数量', digits=(16, 2)),
        'remark': fields.char(u'备注', size=512),
        'create_date': fields.datetime(u'创建时间', readonly=True, help=u"当前系统时间"),
        'write_date': fields.datetime(u'修改时间', readonly=True, help=u"当前系统时间"),
        "create_uid": fields.many2one("res.users", u"创建人", readonly=True),
        'write_uid': fields.many2one("res.users", u"修改人", readonly=True),
    }

    _sql_constraints = [
        ('aidtime_employee_unique', 'unique(mm_aidtime_id, bs_employee_id, v_aidtype_id)', u'人员重复，请检查。'),
    ]

    _defaults = {
        'attend_time': 0.00,
        'done_qty': 0.00,
        'v_aidtype_id':180  # 辅助类别默认为“辅助人工—清洁卫生”
    }

    def create(self, cr, uid, vals, context=None):
        return super(mm_aidtime_d, self).create(cr, uid, vals, context=context)

    def _check_attend_time(self, cr, uid, ids, context=None):
        for aidtime in self.browse(cr, uid, ids, context=context):
            if aidtime.attend_time < 0.00:
                raise osv.except_osv(_('警告!'), _('人员“%s”的出勤工时必须大于等于0。' % aidtime.bs_employee_id.res_users_id.name))
        return True

    _constraints = [
        (_check_attend_time, '', []),
    ]

    def onchange_attend_time(self, cr, uid, ids, attend_time=0, context=None):
        res = {'value':{}}
        if attend_time < 0:
            res['warning'] = {'title': _('警告'), 'message': _('出勤工时不能小于0，请重新设置。')}
            res['value'].update({'attend_time': 0.00})
        if attend_time > 24:
            res['warning'] = {'title': _('警告'), 'message': _('出勤工时不能大于24, 请重新设置。')}
            res['value'].update({'attend_time': 0.00})
        elif attend_time > MAX_ATTEND_TIME:
            res['warning'] = {'title': _('警告'), 'message': _('请注意，出勤工时已经超过%d。' % MAX_ATTEND_TIME)}
            res['value'].update({'attend_time': attend_time})
        return res

    def onchange_done_qty(self, cr, uid, ids, done_qty=0, context=None):
        res = {'value':{}}
        if done_qty < 0:
            res['warning'] = {'title': _('警告'), 'message': _('洗罐子数量不能小于0，请重新设置。')}
            res['value'].update({'done_qty': 0.00})
        return res

mm_aidtime_d()

class mm_timepiece_update(osv.osv_memory):
    _name = 'mm.aidtime.update'
    _description = u'批量刷新'

    _columns = {
        'std_qty': fields.float(u'标准件系数', digits=(8, 2), required=True),
        'product_id': fields.many2one('product.product', u'产品', required=True),
        'month': fields.many2one('account.period', u'月份', required=True),
        'bs_wprocess_id': fields.many2one('bs.wprocess', u'工序', ),
    }

mm_timepiece_update()

class mm_timepiece(osv.osv):
    _name = 'mm.timepiece'
    _inherit = ['mail.thread']
    _description = u'计时计件'
    _order = 'date desc, bs_wcenter_id, bs_class_id, bs_piecegroup_id'
    _rec_name = "billcode"

    _columns = {
        'billcode': fields.char(u'计件单号', size=64, required=True, readonly=True),
        'type': fields.selection([('product','按产品计件'),('step','按工序计件'),('piece','按计件组计件'),('pstep','按工序+产品计件'),('ppiece','按计件组+产品计件')], u'计件方式', size=16, required=True),
        'res_company_id': fields.many2one('res.company', u'所属公司', required=True),
        'date': fields.date(u'计件日期', required=True),
        'bs_wcenter_id': fields.many2one('bs.wcenter', u'工作中心', required=True),
        'bs_class_id': fields.many2one('bs.class', u'班次', required=True),
        'bs_piecegroup_id': fields.many2one('bs.piecegroup', u'计件组'),
        'product_qty': fields.float(u'产量', digits=(16, 2)),
        'std_qty': fields.float(u'标准件基数', digits=(8, 2), readonly=True),
        'done_qty': fields.float(u'总标准产量', digits=(16, 2), help=u'标准产量总和。'),
        'total_time': fields.float(u'标准件分配份额', digits=(16, 2), readonly=True, help=u'出勤时间总和。'),
        'remark': fields.char(u'备注', size=512),
        'v_wfstate_id': fields.many2one('bs.publictype.d', u'状态',domain="[('bs_publictype_id.viewname','=','v_wfstate')]",widget='seletion',  select=True, required=True),
        'sys_billtype_id': fields.many2one('sys.billtype', u'单据类别', required=True, readonly=True),
        'res_users_idcheck': fields.many2one('res.users', u'审核人', readonly=True, select=True),
        'check_date': fields.datetime(u'审核时间', readonly=True, select=True),
        'create_date': fields.datetime(u'创建时间', readonly=True, help=u"当前系统时间"),
        'write_date': fields.datetime(u'修改时间', readonly=True, help=u"当前系统时间"),
        "create_uid": fields.many2one("res.users", u"创建人", readonly=True),
        'write_uid': fields.many2one("res.users", u"修改人", readonly=True),
        'time_line': fields.one2many('mm.timepiece.d', 'mm_timepiece_id', u'工时计件明细'),
        'product_line': fields.one2many('mm.productpiece.d', 'mm_timepiece_id', u'产量计件明细'),
    }

    _sql_constraints = [
        ('billcode_unique', 'unique(billcode)', u'单号重复!'),
    ]

    def _check_date_and_bs_piecegroup_id(self, cr, uid, ids, context=None):
        ''' 校验主表的日期+班次+计件组唯一 '''
        for timepiece in self.browse(cr, uid, ids, context=context):
            date = timepiece.date or ''
            bs_piecegroup_id = timepiece.bs_piecegroup_id.id or False
            bs_class_id = timepiece.bs_class_id.id or False
            vals = {
                'date': date,
                'bs_piecegroup_id': bs_piecegroup_id,
                'bs_class_id': bs_class_id,
                'v_wfstate_id': TOTAL_STATE_ID['cancel'],
                'id': timepiece.id,
            }
            if date and bs_piecegroup_id:
                cr.execute('''SELECT id, billcode FROM mm_timepiece WHERE date = '%(date)s' AND bs_piecegroup_id = %(bs_piecegroup_id)s AND bs_class_id=%(bs_class_id)s AND v_wfstate_id != %(v_wfstate_id)s AND id != %(id)s;''' % vals)
                data = cr.fetchall()
                if len(data) >= 1:
                    raise osv.except_osv(_('警告!'), _('%s %s 计件组(%s)的工人计件录入单已存在,请找到单号“%s”进行处理。' % (date, timepiece.bs_class_id.name, timepiece.bs_piecegroup_id.name, data[0][1]) ))
        return True

    def _check_attend_time(self, cr, uid, ids, context=None):
        ''' 校验工时计件明细出勤人员的出勤工时 '''
        for timepiece in self.browse(cr, uid, ids, context=context):
            jijian_date = timepiece.date
            res_company_id = timepiece.res_company_id.id
            mm_timepiece_id = timepiece.id
            for line in timepiece.time_line:
                if line.attend_time:
                    bs_employee_id = line.bs_employee_id.id
                    attend_time = line.attend_time
                    vals = {
                        'bs_employee_id': bs_employee_id,
                        'res_company_id': res_company_id,
                        'date': jijian_date,
                        'mm_timepiece_id': mm_timepiece_id,
                    }
                    cr.execute(''' SELECT SUM(COALESCE(a.attend_time, 0.00)) AS attend_time
                                    FROM mm_timepiece_d a
                                    LEFT JOIN mm_timepiece b ON b.id = a.mm_timepiece_id
                                    WHERE a.bs_employee_id = %(bs_employee_id)s
                                      AND b.res_company_id = %(res_company_id)s
                                      AND b.date = '%(date)s';
                                ''' % vals)
                    data = cr.fetchall()
                    cr.execute(''' SELECT b.billcode
                                    FROM mm_timepiece_d a
                                    LEFT JOIN mm_timepiece b ON b.id = a.mm_timepiece_id
                                    WHERE a.bs_employee_id = %(bs_employee_id)s
                                      AND b.res_company_id = %(res_company_id)s
                                      AND b.date = '%(date)s'
                                      AND b.id != %(mm_timepiece_id)s;
                                ''' % vals)
                    data1 = cr.fetchall()
                    billcodes = ','.join([i[0] for i in data1])
                    if data and data[0][0] > MAX_ATTEND_TIME:
                        raise osv.except_osv(_('警告!'), _('%s的出勤工时之和必须小于等于%d,本单据出勤工时为%d,请结合以下单据核对后填写：%s' % (line.bs_employee_id.res_users_id.name, MAX_ATTEND_TIME, attend_time, billcodes) ))
        return True

    def _check_product_id(self, cr, uid, ids, context=None):
        ''' 按产品计件录入 '''
        ids = [ids] if isinstance(ids, (int, long)) else ids
        for timepiece in self.browse(cr, uid, ids, context=context):
            if timepiece.type == 'product':
                all_product = []
                for line in timepiece.product_line:
                    if line.product_id:
                        all_product.append(line.product_id.id)
                set_product = list(set(all_product))
                if len(all_product) != len(set_product):
                    raise osv.except_osv(_('警告!'), _('计件明细的产品重复，请检查。'))
        return True

    def _check_bs_wprocess_id(self, cr, uid, ids, context=None):
        ''' 按产品计件录入 '''
        ids = [ids] if isinstance(ids, (int, long)) else ids
        for timepiece in self.browse(cr, uid, ids, context=context):
            if timepiece.type == 'step':
                all_wprocess = []
                for line in timepiece.product_line:
                    if line.bs_wprocess_id:
                        all_wprocess.append(line.bs_wprocess_id.id)
                set_wprocess = list(set(all_wprocess))
                if len(all_wprocess) != len(set_wprocess):
                    raise osv.except_osv(_('警告!'), _('计件明细的工序重复，请检查。'))
        return True

    def _check_bs_piece_stdqty_id(self, cr, uid, ids, context=None):
        ''' 按工序与产品计件录入 '''
        ids = [ids] if isinstance(ids, (int, long)) else ids
        for timepiece in self.browse(cr, uid, ids, context=context):
            if timepiece.type == 'pstep':
                all_wprocess = []
                for line in timepiece.product_line:
                    if line.bs_piece_stdqty_id:
                        all_wprocess.append(line.bs_piece_stdqty_id.id)
                set_wprocess = list(set(all_wprocess))
                if len(all_wprocess) != len(set_wprocess):
                    raise osv.except_osv(_('警告!'), _('{}计件明细的“工序与产品”重复，请检查。').format(timepiece.billcode))
        return True

    def _check_timepiece_daycheck(self, cr, uid, ids, context=None):
        ''' 日审之后不能创建录入单据 '''
        ids = [ids] if isinstance(ids, (int, long)) else ids
        for timepiece in self.browse(cr, uid, ids, context=context):
            res_company_id = timepiece.res_company_id.id
            date = timepiece.date
            if res_company_id and date:
                cr.execute("""SELECT * FROM mm_timepiece_daycheck WHERE res_company_id = %d AND date = '%s' AND v_wfstate_id = %d;""" % (res_company_id, date, TOTAL_STATE_ID['done']))
                if len(cr.fetchall()) >= 1:
                    raise osv.except_osv(_('警告!'), _('%s的计时计件已日审核, 不能再进行操作。' % date ))
        return True

    def _check_write_date(self, cr, uid, ids, context=None):
        ''' 校验公司+日期+工作中心+班次'''
        ids = [ids] if isinstance(ids, (int, long)) else ids
        for check in self.browse(cr, uid, ids, context=context):
            vals = {
                'date': check.date,
                'res_company_id': check.res_company_id.id,
                'v_wfstate_id': TOTAL_STATE_ID['cancel'],
                'bs_wcenter_id': check.bs_wcenter_id.id,
                'bs_class_id':check.bs_class_id.id,
                'id': check.id,
            }
            cr.execute('''SELECT id, billcode 
                FROM mm_timepiece 
                WHERE res_company_id = %(res_company_id)s 
                AND bs_wcenter_id = %(bs_wcenter_id)s 
                AND date = '%(date)s' 
                AND v_wfstate_id != %(v_wfstate_id)s 
                AND bs_class_id = %(bs_class_id)s
                AND id != %(id)s;''' % vals)
            data = cr.fetchall()
            if len(data) >= 1:
                raise osv.except_osv(_('警告!'), _('%s %s 的计件录入单已存在, 请找到单号‘%s’进行操作。'% (check.bs_wcenter_id.name, check.date, data[0][1]) ))
        return True

    _constraints = [
        # (_check_date_and_bs_piecegroup_id, '', []),
        (_check_timepiece_daycheck, '', []),
        # (_check_attend_time, '', []),
        (_check_product_id, '', []),
        (_check_bs_wprocess_id, '', []),
        (_check_bs_piece_stdqty_id, '', []),
        # (_check_write_date, '', [])
    ]

    _defaults = {
        'product_qty': 0.00,
        'done_qty': 0.00,
        'std_qty': 0.00,
        'total_time': 0.00,
    }

    def onchange_res_company_id(self, cr, uid, ids, res_company_id=False, context=None):
        res = {}
        context = context or {}
        if res_company_id:
            args = []
            args.append(('res_company_id', '=', res_company_id))
            args.append(('name', '=',  u'早班'))
            bs_class_ids = self.pool.get('bs.class').search(cr, uid, args, context=context)
            bs_wcenter_id = False
            if context.get('type', False) == 'product' and res_company_id:
                cr.execute("""SELECT a.id FROM bs_wcenter a LEFT JOIN bs_pshop b ON b.id = a.bs_pshop_id
                               WHERE a.name IN ('烘烤工作中心','外包工作中心') AND b.res_company_id = %d """ % res_company_id)
                bs_wcenter_ids = [line[0] for line in cr.fetchall()]
                bs_wcenter_id = bs_wcenter_ids and bs_wcenter_ids[0] or False
            if context.get('type', False) == 'pstep' and res_company_id:
                cr.execute("""SELECT a.id FROM bs_wcenter a LEFT JOIN bs_pshop b ON b.id = a.bs_pshop_id
                               WHERE a.name IN ('内包工作中心','磨粉工作中心','伴侣工作中心','糕点工作中心') AND b.res_company_id = %d """ % res_company_id)
                bs_wcenter_ids = [line[0] for line in cr.fetchall()]
                bs_wcenter_id = bs_wcenter_ids and bs_wcenter_ids[0] or False
            if context.get('type', False) == 'piece' and res_company_id:
                cr.execute("""SELECT a.id FROM bs_wcenter a LEFT JOIN bs_pshop b ON b.id = a.bs_pshop_id
                               WHERE a.name = '物流工作中心' AND b.res_company_id = %d """ % res_company_id)
                bs_wcenter_ids = [line[0] for line in cr.fetchall()]
                bs_wcenter_id = bs_wcenter_ids and bs_wcenter_ids[0] or False
            res = {'value': {
                'bs_piecegroup_id': False,
                'bs_class_id': bs_class_ids and bs_class_ids[0] or False,
                'time_line': False,
                'product_line': False,
                'bs_wcenter_id': bs_wcenter_id,
            }}
            return res
        else:
            res = {'value': {
                'bs_piecegroup_id': False,
                'bs_class_id': False,
                'time_line': False,
                'product_line': False,
                'bs_wcenter_id': False,
            }}
            return res
        return res

    def onchange_bs_wcenter_id(self, cr, uid, ids, bs_wcenter_id=False, context=None):
        res = {}
        if bs_wcenter_id:
            res = {'value': {
                'bs_piecegroup_id': False,
                'time_line': False,
                'product_line': False,
            }}
            return res
        else:
            res = {'value': {
                'bs_piecegroup_id': False,
                'time_line': False,
                'product_line': False,
            }}
            return res
        return res

    def onchange_bs_piecegroup_id_pstep(self, cr, uid, ids, res_company_id=False, bs_wcenter_id=False, bs_class_id=False, bs_piecegroup_id=False, context=None):
        ''' 产品与工序计件 联想出明细 '''
        context = context or {}
        res = {'value': {'time_line': False, 'product_line': False}}
        return res # 直接返回，不联想出人员明细和产品明细 2016-06-27 工厂人员提出
        jjtype = context.get('type', False)
        if not res_company_id or not bs_wcenter_id or not bs_class_id or not bs_piecegroup_id:
            return {}
        if res_company_id and bs_wcenter_id and bs_class_id and bs_piecegroup_id and jjtype == 'pstep':
            bs_wcenter = self.pool.get('bs.wcenter').browse(cr, uid, bs_wcenter_id)
            vals = { 'res_company_id': res_company_id,
                     'bs_class_id': bs_class_id,
                     'bs_piecegroup_id': bs_piecegroup_id,
                     'type': jjtype,
                     'v_wfstate_id': TOTAL_STATE_ID['done'],
                     'bs_wcenter_id': bs_wcenter_id,}
            if bs_wcenter.name.strip() == u'伴侣工作中心':
                res = self._get_users_line_pre(cr, uid, ids, res=res, vals=vals, context=context)
                res = self._get_piece_stdqty_line_piecegroup(cr, uid, ids, res=res, vals=vals, context=context)
                return res
            if bs_wcenter.name.strip() == u'糕点工作中心':
                res = self._get_users_line_pre(cr, uid, ids, res=res, vals=vals, context=context)
                res = self._get_piece_stdqty_line_piecegroup(cr, uid, ids, res=res, vals=vals, context=context)
                return res
            if bs_wcenter.name.strip() == u'磨粉工作中心':
                res = self._get_users_line_pre(cr, uid, ids, res=res, vals=vals, context=context)
                return res
            if bs_wcenter.name.strip() == u'内包工作中心':
                return res
        return res

    def _get_piece_stdqty_line_piecegroup(self, cr, uid, ids, res={}, vals={}, context=None):
        ''' 获取 工序与产品计件 的产品计件明细'''
        sql = '''
        SELECT id bs_piece_stdqty_id,
                0.00 AS product_qty,
                std_qty,
                0.00 AS done_qty,
                product_id product_id2
          FROM bs_piece_stdqty
        WHERE bs_wcenter_id = %(bs_wcenter_id)s AND isvalid = 't'
          AND product_id IN (
              SELECT c.product_id
                FROM bs_devicegroup a
              INNER JOIN bs_piecegroup b ON b.bs_devicegroup_id = a.id
              INNER JOIN bs_ability c ON c.bs_devicegroup_id = a.id
              WHERE b.id = %(bs_piecegroup_id)s AND b.isvalid = 't')
        ''' % vals
        cr.execute(sql)
        product_line = cr.dictfetchall()
        res['value'].update(product_line=product_line)
        return res

    def onchange_bs_piecegroup_id_product(self, cr, uid, ids, res_company_id=False, bs_wcenter_id=False, bs_class_id=False, bs_piecegroup_id=False, context=None):
        ''' 产品计件 联想出明细 '''
        context = context or {}
        res = {'value': {'time_line': False, 'product_line': False}}
        return res # 直接返回，不联想出人员明细和产品明细 2016-06-27
        jjtype = context.get('type', False)
        if not res_company_id or not bs_wcenter_id or not bs_class_id or not bs_piecegroup_id:
            return {}
        if res_company_id and bs_wcenter_id and bs_class_id and bs_piecegroup_id and jjtype == 'product':
            vals = { 'res_company_id': res_company_id,
                     'bs_class_id': bs_class_id,
                     'bs_piecegroup_id': bs_piecegroup_id,
                     'type': jjtype,
                     'v_wfstate_id': TOTAL_STATE_ID['done'],
                     'bs_wcenter_id': bs_wcenter_id,}
            bs_wcenter = self.pool.get('bs.wcenter').browse(cr, uid, bs_wcenter_id)
            if bs_wcenter.name.strip() == u'烘烤工作中心' or bs_wcenter.name.strip() == u'外包工作中心':
                res = self._get_users_line_pre(cr, uid, ids, res=res, vals=vals, context=context)
                sql = '''SELECT c.product_id,
                        c.product_id product_id2,
                        0.00 AS product_qty,
                        c.std_qty AS std_qty,
                        0.00 AS done_qty
                    FROM bs_piecegroup a
                    INNER JOIN bs_devicegroup b ON b.id = a.bs_devicegroup_id
                    INNER JOIN bs_ability c ON c.bs_devicegroup_id = b.id
                    WHERE a.id = %(bs_piecegroup_id)s;''' % vals
                cr.execute(sql)
                product_line = cr.dictfetchall()
                res['value'].update(product_line=product_line)
                # for line in product_line:
                #     value = {'res_company_id': res_company_id, 'product_id': line['product_id'],}
                #     cr.execute('''SELECT DISTINCT std_qty FROM bs_material_corp WHERE res_company_id = %(res_company_id)s AND product_id = %(product_id)s;''' % value)
                #     std_qtys = cr.fetchall()
                #     if std_qtys and std_qtys[0][0]:
                #         line.update(std_qty=std_qtys[0][0])
                # print '----res',res
                return res

            # if bs_wcenter.name.strip() == u'外包工作中心':
            #     res = self._get_users_line_pre(cr, uid, ids, res=res, vals=vals, context=context)

            #     sql = '''
            #     SELECT a.product_id,
            #             a.product_id product_id2,
            #             0.00 AS product_qty,
            #             1.00 AS std_qty,
            #             0.00 AS done_qty
            #       FROM bs_ability a
            #     INNER JOIN bs_devicegroup b ON b.id = a.bs_devicegroup_id
            #     WHERE b.bs_wcenter_id = %(bs_wcenter_id)s;''' % vals
            #     cr.execute(sql)
            #     product_line = cr.dictfetchall()
            #     res['value'].update(product_line=product_line)
            #     for line in product_line:
            #         value = {'res_company_id': res_company_id, 'product_id': line['product_id'],}
            #         cr.execute('''SELECT DISTINCT std_qty FROM bs_material_corp WHERE res_company_id = %(res_company_id)s AND product_id = %(product_id)s;''' % value)
            #         std_qtys = cr.fetchall()
            #         if std_qtys and std_qtys[0][0]:
            #             line.update(std_qty=std_qtys[0][0])
            #     return res
        return res

    def _get_users_line_pre(self, cr, uid, ids, res={}, vals={}, context=None):
        ''' 获取前一天生效单据的人员明细 '''
        cr.execute('''SELECT a.bs_employee_id, a.wgmf_rolestatus, 0.00 AS attend_time, 0.00 AS done_qty
            FROM mm_timepiece_d a
            INNER JOIN mm_timepiece b on b.id = a.mm_timepiece_id
            WHERE b.id = (SELECT max(id) FROM mm_timepiece
                WHERE res_company_id = %(res_company_id)s AND bs_class_id = %(bs_class_id)s
                AND bs_piecegroup_id = %(bs_piecegroup_id)s AND type = '%(type)s' AND v_wfstate_id = %(v_wfstate_id)s);
        ''' % vals)
        time_line = cr.dictfetchall()
        res['value'].update(time_line=time_line)
        # print '------line--------#',res
        return res

    def _get_users_line_all(self, cr, uid, ids, res={}, vals={}, context=None):
        ''' 获取工作中心下 所有人员 '''
        cr.execute( '''
        SELECT a.id bs_employee_id, b.wgmf_rolestatus, 0.00 AS attend_time, 0.00 AS done_qty
          FROM bs_employee a
        LEFT JOIN res_users b ON b.id = a.res_users_id
        WHERE a.bs_wcenter_id = %(bs_wcenter_id)s;''' % vals)
        time_line = cr.dictfetchall()
        res['value'].update(time_line=time_line)
        return res

    def onchange_bs_piecegroup_id_other(self, cr, uid, ids, res_company_id=False, bs_class_id=False, bs_piecegroup_id=False, context=None):
        ''' 产品计件联想出明细 '''
        context = context or {}
        res = {'value': {'time_line': False, 'product_line': False}}
        jjtype = context.get('type', False)
        if not res_company_id or not bs_class_id or not bs_piecegroup_id:
            return {}
        vals = {
            'res_company_id': res_company_id,
            'bs_class_id': bs_class_id,
            'bs_piecegroup_id': bs_piecegroup_id,
            'type': jjtype,
            'v_wfstate_id': TOTAL_STATE_ID['done'],
        }
        if bs_class_id and bs_piecegroup_id:
            if jjtype == 'product':
                sql = '''
                SELECT a.bs_employee_id,
                        a.wgmf_rolestatus,
                        0.00 AS attend_time,
                        0.00 AS done_qty
                FROM mm_timepiece_d a
                LEFT JOIN mm_timepiece b on b.id = a.mm_timepiece_id
                WHERE b.id = (
                  SELECT max(id) FROM mm_timepiece
                  WHERE res_company_id = %(res_company_id)s
                    AND bs_class_id = %(bs_class_id)s
                    AND bs_piecegroup_id = %(bs_piecegroup_id)s
                    AND type = '%(type)s'
                    AND v_wfstate_id = %(v_wfstate_id)s
                );''' % vals
                cr.execute(sql)
                time_line = cr.dictfetchall()
                res['value'].update(time_line=time_line)

                sql = '''
                SELECT a.product_id,
                      --a.bs_wprocess_id,
                      a.product_id2,
                      0.00 AS product_qty,
                      --a.std_qty,
                      1.00 AS std_qty,
                      0.00 AS done_qty
                FROM mm_productpiece_d a
                LEFT JOIN mm_timepiece b on b.id = a.mm_timepiece_id
                WHERE b.id = (
                  SELECT max(id) FROM mm_timepiece
                  WHERE res_company_id = %(res_company_id)s
                    AND bs_class_id = %(bs_class_id)s
                    AND bs_piecegroup_id = %(bs_piecegroup_id)s
                    AND type = '%(type)s'
                    AND v_wfstate_id = %(v_wfstate_id)s
                );''' % vals
                cr.execute(sql)
                product_line = cr.dictfetchall()
                res['value'].update(product_line=product_line)
                for line in product_line:
                    value = {
                        'res_company_id': res_company_id,
                        'product_id': line['product_id'],
                    }
                    cr.execute('''SELECT DISTINCT std_qty FROM bs_material_corp WHERE res_company_id = %(res_company_id)s AND product_id = %(product_id)s;''' % value)
                    std_qtys = cr.fetchall()
                    if std_qtys and std_qtys[0][0]:
                        line.update(std_qty=std_qtys[0][0])
                    else:
                        line.update(std_qty=1.00)
                return res
            elif jjtype == 'step':
                sql = '''
                SELECT a.bs_employee_id,
                      a.wgmf_rolestatus,
                      0.00 AS attend_time,
                      0.00 AS done_qty
                FROM mm_timepiece_d a
                LEFT JOIN mm_timepiece b on b.id = a.mm_timepiece_id
                WHERE b.id = (
                  SELECT max(id) FROM mm_timepiece
                  WHERE res_company_id = %(res_company_id)s
                    AND bs_class_id = %(bs_class_id)s
                    AND bs_piecegroup_id = %(bs_piecegroup_id)s
                    AND type = '%(type)s'
                    AND v_wfstate_id = %(v_wfstate_id)s
                );''' % vals
                cr.execute(sql)
                time_line = cr.dictfetchall()
                res['value'].update(time_line=time_line)

                sql = '''
                SELECT a.bs_wprocess_id,
                      0.00 AS product_qty,
                      --a.std_qty,
                      1.00 AS std_qty,
                      0.00 AS done_qty
                FROM mm_productpiece_d a
                LEFT JOIN mm_timepiece b on b.id = a.mm_timepiece_id
                WHERE b.id = (
                  SELECT max(id) FROM mm_timepiece
                  WHERE res_company_id = %(res_company_id)s
                    AND bs_class_id = %(bs_class_id)s
                    AND bs_piecegroup_id = %(bs_piecegroup_id)s
                    AND type = '%(type)s'
                    AND v_wfstate_id = %(v_wfstate_id)s
                );''' % vals
                cr.execute(sql)
                product_line = cr.dictfetchall()
                res['value'].update(product_line=product_line)
                for line in product_line:
                    value = {
                        'res_company_id': res_company_id,
                        'bs_wprocess_id': line['bs_wprocess_id'],
                    }
                    sql = '''
                    SELECT DISTINCT a.std_qty
                    FROM bs_wprocess a
                    LEFT JOIN bs_wcenter b ON b.id = a.bs_wcenter_id
                    --LEFT JOIN bs_piecegroup c ON c.bs_wcenter_id = b.id
                    LEFT JOIN bs_pshop d ON d.id = b.bs_pshop_id
                    WHERE a.id = %(bs_wprocess_id)s AND d.res_company_id = %(res_company_id)s;
                    ''' % value
                    cr.execute(sql)
                    std_qtys = cr.fetchall()
                    if std_qtys and std_qtys[0][0]:
                        line.update(std_qty=std_qtys[0][0])
                    else:
                        line.update(std_qty=1.00)
                return res
            elif jjtype == 'pstep':
                sql = '''
                SELECT a.bs_employee_id,
                      a.wgmf_rolestatus,
                      0.00 AS attend_time,
                      0.00 AS done_qty
                FROM mm_timepiece_d a
                LEFT JOIN mm_timepiece b on b.id = a.mm_timepiece_id
                WHERE b.id = (
                  SELECT max(id) FROM mm_timepiece
                  WHERE res_company_id = %(res_company_id)s
                    AND bs_class_id = %(bs_class_id)s
                    AND bs_piecegroup_id = %(bs_piecegroup_id)s
                    AND type = '%(type)s'
                    AND v_wfstate_id = %(v_wfstate_id)s
                );''' % vals
                cr.execute(sql)
                time_line = cr.dictfetchall()
                res['value'].update(time_line=time_line)

                sql = '''
                SELECT a.bs_piece_stdqty_id,
                      0.00 AS product_qty,
                      --a.std_qty,
                      1.00 AS std_qty,
                      0.00 AS done_qty
                FROM mm_productpiece_d a
                LEFT JOIN mm_timepiece b on b.id = a.mm_timepiece_id
                WHERE b.id = (
                  SELECT max(id) FROM mm_timepiece
                  WHERE res_company_id = %(res_company_id)s
                    AND bs_class_id = %(bs_class_id)s
                    AND bs_piecegroup_id = %(bs_piecegroup_id)s
                    AND type = '%(type)s'
                    AND v_wfstate_id = %(v_wfstate_id)s
                );''' % vals
                cr.execute(sql)
                product_line = cr.dictfetchall()
                res['value'].update(product_line=product_line)
                for line in product_line:
                    piece_stdqty = self.pool.get('bs.piece.stdqty').browse(cr, uid, line['bs_piece_stdqty_id'])
                    line.update(product_id2=piece_stdqty.product_id.id)
                    if piece_stdqty.std_qty:
                        line.update(std_qty=piece_stdqty.std_qty)
                    else:
                        line.update(std_qty=1.00)
                return res
        return res

    def onchange_bs_piecegroup_id(self, cr, uid, ids, res_company_id=False, bs_class_id=False, bs_piecegroup_id=False, product_qty=0.00, context=None):
        ''' 按计件组计件录入 '''
        context = context or {}
        jjtype = context.get('type', False)
        res = {'value':{
            'std_qty': 1.00,
            'done_qty': product_qty,
            'time_line': False,
            'product_line': False,
        }}
        if not res_company_id or not bs_class_id or not bs_piecegroup_id:
            return res
        vals = {
            'res_company_id': res_company_id,
            'bs_class_id': bs_class_id,
            'bs_piecegroup_id': bs_piecegroup_id,
            'v_wfstate_id': TOTAL_STATE_ID['done'],
            'type': jjtype,
        }
        if res_company_id and bs_class_id and bs_piecegroup_id:
            sql = '''
            SELECT a.bs_employee_id,
                    a.wgmf_rolestatus,
                    0.00 AS attend_time,
                    0.00 AS done_qty
            FROM mm_timepiece_d a
            LEFT JOIN mm_timepiece b on b.id = a.mm_timepiece_id
            WHERE b.id = (
              SELECT max(id) FROM mm_timepiece
              WHERE res_company_id = %(res_company_id)s
                AND bs_class_id = %(bs_class_id)s
                AND bs_piecegroup_id = %(bs_piecegroup_id)s
                AND type = '%(type)s'
                AND v_wfstate_id = %(v_wfstate_id)s
            );''' % vals
            cr.execute(sql)
            time_line = cr.dictfetchall()
            res['value'].update(time_line=time_line)
            cr.execute(''' SELECT DISTINCT std_qty FROM bs_piecegroup WHERE id = %(bs_piecegroup_id)s AND res_company_id = %(res_company_id)s;''' % vals)
            std_qtys = cr.fetchall()
            if std_qtys and std_qtys[0][0]:
                std_qty = std_qtys[0][0]
                res['value'].update(std_qty=std_qty)
                res['value'].update(done_qty=product_qty * std_qty)
                return res
        res['value'].update(std_qty=1.00)
        res['value'].update(done_qty=product_qty)
        return res

    def onchange_product_qty_piece(self, cr, uid, ids, product_qty=0.00, std_qty=0.00, context=None):
        ''' 按计件组计件录入 '''
        res = {'value':{}}
        if product_qty  < 0.00:
            res['warning'] = {'title': _('警告'), 'message': _('产量必须大于等于0。')}
            res['value'].update({'product_qty': 0.00})
            res['value'].update({'done_qty': 0.00})
            return res
        res = {'value':{'done_qty': product_qty * std_qty,}}
        return res

    def _get_res_company_id(self, cr, uid, context=None):
        ''' 公司默认值 '''
        return self.pool.get('res.company')._company_default_get(cr, uid, 'mm.timepiece', context=context)

    def _get_type(self, cr, uid, context=None):
        '''计件方式默认值'''
        context = context or {}
        if 'type' in context and context['type']:
            type = context['type']
        return type

    _SYS_BILLTYPE_ID = TOTAL_SYS_BILLTYPE_ID['mm_timepiece']

    def default_get(self, cr, uid, fields, context=None):
        context = context or {}
        res = super(mm_timepiece, self).default_get(cr, uid, fields, context=context)
        date = openerp.osv.fields.date.context_today(self, cr, uid, context=context)
        ywType = self._get_type(cr, uid, context=context)
        res_company_id = self._get_res_company_id(cr, uid, context=context)
        value = {
            'billcode': u'系统自动生成',
            'date': date,
            'sys_billtype_id': self._SYS_BILLTYPE_ID,
            'v_wfstate_id': TOTAL_STATE_ID['draft'],
            'res_company_id': res_company_id,
            'type': ywType,
        }
        res.update(value)
        return res

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        context = context or {}
        if args and args[0] and 'type' in args[0]:
            args = []
        if context and context.get('type', False):
            args.append(( ('type', '=', context.get('type', False)) ))
        res = super(mm_timepiece, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=False)
        return res

    def create(self, cr, uid, vals, context=None):
        vals = vals or {}
        now = fields.date.context_today(self, cr, uid, context=context)
        dt = vals.get('date', now)
        sys_billtype_id = self._SYS_BILLTYPE_ID
        if vals.get('billcode', u'系统自动生成') == u'系统自动生成':
            billcode = self.pool.get('sys.billtype').get(cr, uid, sys_billtype_id, dt, context=context)
            vals.update(billcode=billcode)
        value ={
            'res_company_id': vals.get('res_company_id',0),
            'billtype': sys_billtype_id,
            'billdate': dt,
        }
        data = self.pool.get('sys.fnclose.filter').check_close(cr, uid, value)
        if data.get('isclosed', False):
            raise osv.except_osv(_('警告！'), _('计划生产模块已关账，不能创建单据。'))
        #按产品计件录入
        total_time_product = 0.00
        done_qty_product = 0.00
        if vals.get('type', False) and vals.get('type', False) == 'product':
            if vals.get('time_line', False):
                for line in vals.get('time_line', False):
                    if line[2] and type(line[2]) is not list:
                        employee = self.pool.get('bs.employee').browse(cr, uid, line[2].get('bs_employee_id',False))
                        # user = self.pool.get('res.users').browse(cr, uid, employee.res_users_id.id)
                        wgmf_rolestatus = employee.res_types.id
                        line[2].update({'wgmf_rolestatus': wgmf_rolestatus})
                        attend_time = line[2].get('attend_time', 0.00)
                        total_time_product += attend_time
            vals.update(total_time=total_time_product)
            if vals.get('product_line', False):
                for line in vals.get('product_line', False):
                    if line[2] and type(line[2]) is not list:
                        product_qty = line[2].get('product_qty', 0.00)
                        # cr.execute(''' SELECT DISTINCT std_qty
                        #                 FROM bs_material_corp
                        #                 WHERE res_company_id = %d
                        #                   AND product_id = %d;
                        #                 ''' % (vals['res_company_id'], line[2].get('product_id', False)))
                        # std_qtys = cr.fetchall()
                        # if std_qtys and std_qtys[0][0]:
                        #     std_qty = std_qtys[0][0]
                        # else:
                        #     std_qty = 1.00
                        std_qty = line[2].get('std_qty', 0.00)
                        line_done_qty = product_qty * std_qty
                        # line[2].update({'std_qty': std_qty, 'done_qty': line_done_qty, 'product_id2': line[2].get('product_id', False)})
                        line[2].update({'done_qty': line_done_qty, 'product_id2': line[2].get('product_id', False)})
                        done_qty_product += line_done_qty
            vals.update(done_qty=done_qty_product)
            # 每小时标准产量
            avg_qty = (done_qty_product*1.00)/total_time_product if total_time_product else 0.00
            if vals.get('time_line', False):
                for line in vals.get('time_line', False):
                    if line[2] and type(line[2]) is not list:
                        time_line_qty = round(avg_qty * line[2].get('attend_time', 0.00), 2)
                        line[2].update({'done_qty': time_line_qty})
        #按工序计件录入
        total_time_step = 0.00
        done_qty_step = 0.00
        if vals.get('type', False) and vals.get('type', False) == 'step':
            if vals.get('time_line', False):
                for line in vals.get('time_line', False):
                    if line[2] and type(line[2]) is not list:
                        employee = self.pool.get('bs.employee').browse(cr, uid, line[2].get('bs_employee_id',False))
                        # user = self.pool.get('res.users').browse(cr, uid, employee.res_users_id.id)
                        wgmf_rolestatus = employee.res_types.id
                        line[2].update({'wgmf_rolestatus': wgmf_rolestatus})
                        attend_time = line[2].get('attend_time', 0.00)
                        total_time_step += attend_time
            vals.update(total_time=total_time_step)
            if vals.get('product_line', False):
                for line in vals.get('product_line', False):
                    if line[2] and type(line[2]) is not list:
                        product_qty = line[2].get('product_qty', 0.00)
                        std_qty = line[2].get('std_qty', 0.00)
                        # cr.execute(''' SELECT DISTINCT a.std_qty
                        #                 FROM bs_wprocess a
                        #                 LEFT JOIN bs_wcenter b ON b.id = a.bs_wcenter_id
                        #                 LEFT JOIN bs_pshop c ON c.id = b.bs_pshop_id
                        #                 WHERE a.id = %d
                        #                   AND c.res_company_id = %d;
                        #                 ''' % (line[2].get('bs_wprocess_id', False), vals['res_company_id']))
                        # std_qtys = cr.fetchall()
                        # if std_qtys and std_qtys[0][0]:
                        #     std_qty = std_qtys[0][0]
                        # else:
                        #     std_qty = 1.00
                        line_done_qty = product_qty * std_qty
                        line[2].update({'std_qty': std_qty, 'done_qty': line_done_qty})
                        done_qty_step += line_done_qty
            vals.update(done_qty=done_qty_step)
            # 每小时标准产量
            avg_qty = (done_qty_step*1.00)/total_time_step if total_time_step else 0.00
            if vals.get('time_line', False):
                for line in vals.get('time_line', False):
                    if line[2] and type(line[2]) is not list:
                        time_line_qty = round(avg_qty * line[2].get('attend_time', 0.00), 2)
                        line[2].update({'done_qty': time_line_qty})
        #按计件组计件录入
        total_time_piece = 0.00
        done_qty_piece = 0.00
        if vals.get('type', False) and vals.get('type', False) == 'piece':
            if vals.get('time_line', False):
                for line in vals.get('time_line', False):
                    if line[2] and type(line[2]) is not list:
                        employee = self.pool.get('bs.employee').browse(cr, uid, line[2].get('bs_employee_id',False))
                        # user = self.pool.get('res.users').browse(cr, uid, employee.res_users_id.id)
                        wgmf_rolestatus = employee.res_types.id
                        line[2].update({'wgmf_rolestatus': wgmf_rolestatus})
                        attend_time = line[2].get('attend_time', 0.00)
                        total_time_piece += attend_time
            vals.update(total_time=total_time_piece)
            if vals.get('res_company_id', False) and vals.get('bs_piecegroup_id', False):
                cr.execute(''' SELECT DISTINCT std_qty
                                FROM bs_piecegroup
                                WHERE id = %(bs_piecegroup_id)s
                                  AND res_company_id = %(res_company_id)s;
                                ''' % vals)
                std_qtys = cr.fetchall()
                if std_qtys and std_qtys[0][0]:
                    std_qty = std_qtys[0][0]
                else:
                    std_qty = 1.00
                done_qty_piece = std_qty * vals['product_qty']
                vals.update(std_qty=std_qty)
            vals.update(done_qty=done_qty_piece)
            # 每小时标准产量
            avg_qty = (done_qty_piece*1.00)/total_time_piece if total_time_piece else 0.00
            if vals.get('time_line', False):
                for line in vals.get('time_line', False):
                    if line[2] and type(line[2]) is not list:
                        time_line_qty = round(avg_qty * line[2].get('attend_time', 0.00), 2)
                        line[2].update({'done_qty': time_line_qty})
        #按工+产品计件录入
        total_time_pstep = 0.00
        done_qty_pstep = 0.00
        if vals.get('type', False) and vals.get('type', False) == 'pstep':
            if vals.get('time_line', False):
                for line in vals.get('time_line', False):
                    if line[2] and type(line[2]) is not list:
                        employee = self.pool.get('bs.employee').browse(cr, uid, line[2].get('bs_employee_id',False))
                        # user = self.pool.get('res.users').browse(cr, uid, employee.res_users_id.id)
                        wgmf_rolestatus = employee.res_types.id
                        line[2].update({'wgmf_rolestatus': wgmf_rolestatus})
                        attend_time = line[2].get('attend_time', 0.00)
                        total_time_pstep += attend_time
            vals.update(total_time=total_time_pstep)
            if vals.get('product_line', False):
                for line in vals.get('product_line', False):
                    if line[2] and type(line[2]) is not list:
                        product_qty = line[2].get('product_qty', 0.00)
                        piece_stdqty = self.pool.get('bs.piece.stdqty').browse(cr, uid, line[2].get('bs_piece_stdqty_id', False))
                        std_qty = piece_stdqty.std_qty if piece_stdqty.std_qty else 1.00
                        line_done_qty = product_qty * std_qty
                        line[2].update({'std_qty': std_qty, 'done_qty': line_done_qty, 'product_id2': piece_stdqty.product_id.id})
                        done_qty_pstep += line_done_qty
            vals.update(done_qty=done_qty_pstep)
            # 每小时标准产量
            avg_qty = (done_qty_pstep*1.00)/total_time_pstep if total_time_pstep else 0.00
            if vals.get('time_line', False):
                for line in vals.get('time_line', False):
                    if line[2] and type(line[2]) is not list:
                        time_line_qty = round(avg_qty * line[2].get('attend_time', 0.00), 2)
                        line[2].update({'done_qty': time_line_qty})
        return super(mm_timepiece, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        write_flg = super(mm_timepiece, self).write(cr, uid, ids, vals, context=context)
        if isinstance(ids, (int, long)):
            ids = [ids]
        for timepiece in self.browse(cr, uid, ids):
            #按产品计件录入
            if timepiece.type == 'product':
                total_time = sum([line.attend_time for line in timepiece.time_line])
                done_qty = sum([line.done_qty for line in timepiece.product_line])
                #平均每时标准产量
                avg_qty = (done_qty*1.00)/total_time if total_time else 0.00
                if total_time != timepiece.total_time:
                    super(mm_timepiece,self).write(cr, uid, timepiece.id, {'total_time': total_time})
                if done_qty != timepiece.done_qty:
                    super(mm_timepiece,self).write(cr, uid, timepiece.id, {'done_qty': done_qty})
                if timepiece.time_line:
                    for line in timepiece.time_line:
                        attend_time = line.attend_time
                        done_qty = avg_qty * attend_time
                        mm_timepiece_id = line.id
                        cr.execute(''' UPDATE mm_timepiece_d SET done_qty=%.2f WHERE id=%d; ''' % (done_qty, mm_timepiece_id))
            #按工序计件录入
            if timepiece.type == 'step':
                total_time = sum([line.attend_time for line in timepiece.time_line])
                done_qty = sum([line.done_qty for line in timepiece.product_line])
                #平均每时标准产量
                avg_qty = (done_qty*1.00)/total_time if total_time else 0.00
                if total_time != timepiece.total_time:
                    super(mm_timepiece,self).write(cr, uid, timepiece.id, {'total_time': total_time})
                if done_qty != timepiece.done_qty:
                    super(mm_timepiece,self).write(cr, uid, timepiece.id, {'done_qty': done_qty})
                if timepiece.time_line:
                    for line in timepiece.time_line:
                        attend_time = line.attend_time
                        done_qty = avg_qty * attend_time
                        mm_timepiece_id = line.id
                        cr.execute(''' UPDATE mm_timepiece_d SET done_qty=%.2f WHERE id=%d; ''' % (done_qty, mm_timepiece_id))
            #按计件组计件录入
            if timepiece.type == 'piece':
                total_time = sum([line.attend_time for line in timepiece.time_line])
                done_qty = timepiece.done_qty
                #平均每时标准产量
                avg_qty = (done_qty*1.00)/total_time if total_time else 0.00
                if total_time != timepiece.total_time:
                    super(mm_timepiece,self).write(cr, uid, timepiece.id, {'total_time': total_time})
                if timepiece.time_line:
                    for line in timepiece.time_line:
                        attend_time = line.attend_time
                        done_qty = avg_qty * attend_time
                        mm_timepiece_id = line.id
                        cr.execute(''' UPDATE mm_timepiece_d SET done_qty=%.2f WHERE id=%d; ''' % (done_qty, mm_timepiece_id))
            #按工序+产品计件录入
            if timepiece.type == 'pstep':
                total_time = sum([line.attend_time for line in timepiece.time_line])
                done_qty = sum([line.done_qty for line in timepiece.product_line])
                #平均每时标准产量
                avg_qty = (done_qty*1.00)/total_time if total_time else 0.00
                if total_time != timepiece.total_time:
                    super(mm_timepiece,self).write(cr, uid, timepiece.id, {'total_time': total_time})
                if done_qty != timepiece.done_qty:
                    super(mm_timepiece,self).write(cr, uid, timepiece.id, {'done_qty': done_qty})
                if timepiece.time_line:
                    for line in timepiece.time_line:
                        attend_time = line.attend_time
                        done_qty = avg_qty * attend_time
                        mm_timepiece_id = line.id
                        cr.execute(''' UPDATE mm_timepiece_d SET done_qty=%.2f WHERE id=%d; ''' % (done_qty, mm_timepiece_id))
        return write_flg

    def check_done(self, cr, uid, ids, context=None):
        context = context or {}
        for check in self.browse(cr, uid, ids, context=context):
            self._check_timepiece_daycheck(cr, uid, [check.id], context=context)
            if check.type == 'piece' and check.product_qty <= 0.00:
                raise osv.except_osv(_('错误!'),_('产量必须大于0。'))
            time_list = []
            product_list = []
            if check.time_line:
                for line in check.time_line:
                    if line.attend_time == 0.00:
                        self.pool.get('mm.timepiece.d').unlink(cr, uid, [line.id])
                    if line.attend_time != 0.00:
                        time_list.append(line.id)
            if check.product_line:
                for line in check.product_line:
                    if line.product_qty == 0.00:
                        self.pool.get('mm.productpiece.d').unlink(cr, uid, [line.id])
                    if line.product_qty != 0.00:
                        product_list.append(line.id)
            if check.type == 'piece' and time_list:
                vals = {
                    'v_wfstate_id': TOTAL_STATE_ID['done'],
                    'check_date':  time.strftime('%Y-%m-%d %H:%M:%S'),
                    'res_users_idcheck': uid,
                }
                self.write(cr, uid, [check.id], vals, context=context)
            elif time_list and product_list:
                vals = {
                    'v_wfstate_id': TOTAL_STATE_ID['done'],
                    'check_date':  time.strftime('%Y-%m-%d %H:%M:%S'),
                    'res_users_idcheck': uid,
                }
                self.write(cr, uid, [check.id], vals, context=context)
            elif not time_list:
                raise osv.except_osv(_('错误!'),_('不能审核没有明细行（或计时明细出勤工时都为0）的计时计件，请核查。'))
            elif not product_list:
                raise osv.except_osv(_('错误!'),_('不能审核没有明细行（或计件明细产量都为0）的计时计件，请核查。'))

            if check.type == "product":
                context.update({
                    "subject": u"产品计件录入",
                    "body_html": u""" 产品计件录入：%s 已生效，请知悉； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            elif check.type == "step":
                context.update({
                    "subject": u"工序计件录入",
                    "body_html": u""" 工序计件录入：%s 已生效，请知悉； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            elif check.type == "piece":
                context.update({
                    "subject": u"计件组计件录入",
                    "body_html": u""" 计件组计件录入：%s 已生效，请知悉； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            elif check.type == "pstep":
                context.update({
                    "subject": u"产品与工序计件录入",
                    "body_html": u""" 产品与工序计件录入：%s 已生效，请知悉； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            context.update({
                'v_approve_id': 268,
                "res_company_id": check.res_company_id.id,
                "sys_billtype_id": self._SYS_BILLTYPE_ID,
                "res_id": check.id,
                "auto_delete":True,
            })
            self.pool.get('bs.approve').func_send_mail(cr, uid, context)
        return True

    def check_order(self,cr, uid, ids, context=None):
        context = context or {}
        for check in self.browse(cr, uid, ids, context=context):
            self._check_timepiece_daycheck(cr, uid, [check.id], context=context)
            if check.type == 'piece' and check.product_qty <= 0.00:
                raise osv.except_osv(_('错误!'),_('产量必须大于0。'))
            time_list = []
            product_list = []
            if check.time_line:
                for line in check.time_line:
                    if line.attend_time != 0.00:
                        time_list.append(line.id)
            if check.product_line:
                for line in check.product_line:
                    if line.product_qty != 0.00:
                        product_list.append(line.id)
            if check.type == 'piece' and time_list:
                vals = {
                    'v_wfstate_id': TOTAL_STATE_ID['approve'],
                    'check_date': False,
                    'res_users_idcheck': False,
                }
                self.write(cr, uid, [check.id], vals, context=context)
            elif time_list and product_list:
                vals = {
                    'v_wfstate_id': TOTAL_STATE_ID['approve'],
                    'check_date': False,
                    'res_users_idcheck': False,
                }
                self.write(cr, uid, [check.id], vals, context=context)
            elif not time_list:
                raise osv.except_osv(_('错误!'),_('不能提交没有明细行（或计时明细出勤工时都为0）的计时计件，请核查。'))
            elif not product_list:
                raise osv.except_osv(_('错误!'),_('不能提交没有明细行（或计件明细产量都为0）的计时计件，请核查。'))

            if check.type == "product":
                context.update({
                    "subject": u"产品计件录入",
                    "body_html": u""" 产品计件录入：%s 已提交，请审核； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            elif check.type == "step":
                context.update({
                    "subject": u"工序计件录入",
                    "body_html": u""" 工序计件录入：%s 已提交，请审核； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            elif check.type == "piece":
                context.update({
                    "subject": u"计件组计件录入",
                    "body_html": u""" 计件组计件录入：%s 已提交，请审核； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            elif check.type == "pstep":
                context.update({
                    "subject": u"产品与工序计件录入",
                    "body_html": u""" 产品与工序计件录入：%s 已提交，请审核； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            context.update({
                'v_approve_id': 267,
                "res_company_id": check.res_company_id.id,
                "sys_billtype_id": self._SYS_BILLTYPE_ID,
                "res_id": check.id,
                "auto_delete":True,
            })
            self.pool.get('bs.approve').func_send_mail(cr, uid, context)
        return True

    def check_cancel(self,cr, uid, ids, context=None):
        context = context or {}
        for check in self.browse(cr, uid, ids, context=context):
            vals = {
                'v_wfstate_id': TOTAL_STATE_ID['cancel'],
                'check_date': False,
                'res_users_idcheck': False,
            }
            self.write(cr, uid, [check.id], vals, context=context)
        return True

    def check_draft(self,cr, uid, ids, context=None):
        context = context or {}
        for check in self.browse(cr, uid, ids, context=context):
            self._check_timepiece_daycheck(cr, uid, [check.id], context=context)
            vals = {
                'v_wfstate_id': TOTAL_STATE_ID['draft'],
                'check_date': False,
                'res_users_idcheck': False
            }
            self.write(cr, uid, [check.id],vals)

            if check.type == "product":
                context.update({
                    "subject": u"产品计件录入",
                    "body_html": u""" 产品计件录入：%s 已反审，请处理； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            elif check.type == "step":
                context.update({
                    "subject": u"工序计件录入",
                    "body_html": u""" 工序计件录入：%s 已反审，请处理； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            elif check.type == "piece":
                context.update({
                    "subject": u"计件组计件录入",
                    "body_html": u""" 计件组计件录入：%s 已反审，请处理； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            elif check.type == "pstep":
                context.update({
                    "subject": u"产品与工序计件录入",
                    "body_html": u""" 产品与工序计件录入：%s 已反审，请处理； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                })
            context.update({
                'v_approve_id': 2002,
                "res_company_id": check.res_company_id.id,
                "sys_billtype_id": self._SYS_BILLTYPE_ID,
                "res_id": check.id,
                "auto_delete":True,
            })
            self.pool.get('bs.approve').func_send_mail(cr, uid, context)
        return True

    def unlink(self, cr, uid, ids, context=None):
        for timepiece in self.browse(cr, uid, ids, context=context):
            if timepiece.v_wfstate_id.id == TOTAL_STATE_ID['done']:
                raise osv.except_osv(_('错误!'), _('生效状态不能删除。'))
            if timepiece.v_wfstate_id.id == TOTAL_STATE_ID['cancel']:
                raise osv.except_osv(_('错误!'), _('单据已作废，不能删除。'))
            if timepiece.v_wfstate_id.id == TOTAL_STATE_ID['approve']:
                raise osv.except_osv(_('错误!'), _('审批中的单据，不能删除但可作废。'))
            cr.execute('DELETE FROM mm_timepiece_d WHERE mm_timepiece_id=%d;DELETE FROM mm_productpiece_d WHERE mm_timepiece_id=%d;' % (timepiece.id, timepiece.id))
        return super(mm_timepiece, self).unlink(cr, uid, ids, context=context)

    def copy(self, cr, uid, id, default=None, context=None):
        context = context or {}
        default = default or {}
        now = fields.date.context_today(self, cr, uid, context=context)
        default.update(billcode = u'系统自动生成')
        default.update(date = now)
        default.update(v_wfstate_id = TOTAL_STATE_ID['draft'])
        return super(mm_timepiece, self).copy(cr, uid, id, default, context=context)

    #批量审核
    def action_batch_check_done(self, cr, uid, ids, context=None):
        context = context or {}
        self.check_order(cr, uid, ids, context=context)
        self.check_done(cr, uid, ids, context=context)
        return True

mm_timepiece()

class mm_timepiece_d(osv.osv):
    _name = 'mm.timepiece.d'
    _description = u'计时明细'
    _order = 'id'

    _columns = {
        'mm_timepiece_id': fields.many2one('mm.timepiece', u'计时计件', required=True),
        'bs_employee_id': fields.many2one('bs.employee', u'出勤人员', required=True),
        # 'wgmf_rolestatus': fields.selection([('1', '操作工'), ('2', '物料员'), ('3', '组长')], u'岗位名称', readonly=True, size=16),
        'wgmf_rolestatus': fields.many2one('bs.publictype.d', u'岗位名称', domain=[('bs_publictype_id.viewname','=','v_bs_employee')], readonly=True),
        'attend_time': fields.float(u'出勤工时', digits=(8, 2)),
        'done_qty': fields.float(u'个人标准产量', digits=(16, 2), readonly=True),
        'remark': fields.char(u'备注', size=512),
        'create_date': fields.datetime(u'创建时间', readonly=True, help=u"当前系统时间"),
        'write_date': fields.datetime(u'修改时间', readonly=True, help=u"当前系统时间"),
        "create_uid": fields.many2one("res.users", u"创建人", readonly=True),
        'write_uid': fields.many2one("res.users", u"修改人", readonly=True),
    }

    _sql_constraints = [
        ('timepiece_employee_unique', 'unique(mm_timepiece_id, bs_employee_id)', u'班组人员唯一。'),
    ]

    _defaults = {
        'attend_time': 0.00,
        'done_qty': 0.00,
    }

    def onchange_bs_employee_id(self, cr, uid, ids, bs_employee_id=False, context=None):
        res = {}
        if bs_employee_id:
            employee = self.pool.get('bs.employee').browse(cr, uid, bs_employee_id, context=context)
            res = {'value': {'wgmf_rolestatus': employee.res_types.id}}
            return res
        return res
        #
        # def onchange_attend_time(self, cr, uid, ids, attend_time=0.00, bs_employee_id=False, context=None):
        #     res = {'value':{}}
        #     context = context or {}
        #     res_company_id = context.get('res_company_id', False)
        #     date = context.get('date', False)
        #     if attend_time < 0.00:
        #         res['warning'] = {'title': _('警告'), 'message': _('出勤工时必须大于等于0。')}
        #         res['value'].update({'attend_time': 0.00})
        #
        #     if bs_employee_id and res_company_id and date:
        #         vals = {
        #             'bs_employee_id': bs_employee_id,
        #             'res_company_id': res_company_id,
        #             'date': date,
        #         }
        #         cr.execute(''' SELECT SUM(COALESCE(a.attend_time, 0.00)) AS attend_time
        #                        FROM mm_timepiece_d a
        #                        LEFT JOIN mm_timepiece b ON b.id = a.mm_timepiece_id
        #                        WHERE a.bs_employee_id = %(bs_employee_id)s
        #                          AND b.res_company_id = %(res_company_id)s
        #                          AND b.date = '%(date)s';
        #                        ''' % vals)
        #         data = cr.fetchall()
        #         cr.execute(''' SELECT b.billcode, e.name
        #                        FROM mm_timepiece_d a
        #                        LEFT JOIN mm_timepiece b ON b.id = a.mm_timepiece_id
        #                        LEFT JOIN bs_employee c ON c.id = a.bs_employee_id
        #                        LEFT JOIN res_users d ON d.id = c.res_users_id
        #                        LEFT JOIN res_partner e ON e.id = d.partner_id
        #                        WHERE a.bs_employee_id = %(bs_employee_id)s
        #                           AND b.res_company_id = %(res_company_id)s
        #                           AND b.date = '%(date)s';
        #                        ''' % vals)
        #         data1 = cr.fetchall()
        #         billcodes = ','.join([i[0] for i in data1])
        #         if data:
        #             if data[0][0]:
        #                 attend_time_all = float(data[0][0]) + attend_time
        #                 if attend_time_all > MAX_ATTEND_TIME:
        #                     res['warning'] = {'title': _('警告'), 'message': _('请注意，%s的出勤总工时为%d(其中本次出勤工时为%d)，请结合以下单据进行核对：%s。' % (data1[0][1], attend_time_all, attend_time, billcodes) )}
        #                     res['value'].update({'attend_time': attend_time})
        #             elif attend_time > MAX_ATTEND_TIME:
        #                 res['warning'] = {'title': _('警告'), 'message': _('请注意，出勤工时已经超过%d。' % MAX_ATTEND_TIME)}
        #                 res['value'].update({'attend_time': attend_time})
        #     elif attend_time > MAX_ATTEND_TIME:
        #         res['warning'] = {'title': _('警告'), 'message': _('请注意，出勤工时已经超过%d。' % MAX_ATTEND_TIME)}
        #         res['value'].update({'attend_time': attend_time})
        #     return res

mm_timepiece_d()

class mm_productpiece_d(osv.osv):
    _name = 'mm.productpiece.d'
    _description = u'计件明细'
    _order = 'id'

    _columns = {
        'mm_timepiece_id': fields.many2one('mm.timepiece', u'计时计件', required=True),
        'bs_wprocess_id': fields.many2one('bs.wprocess', u'工序', domain="[('isvalid','=','t')]"),
        'bs_piece_stdqty_id': fields.many2one('bs.piece.stdqty', u'工序与产品', domain="[('isvalid','=','t')]"),
        'product_id': fields.many2one('product.product', u'产品', domain="[('supply_method', '=', 'produce')]"),
        'product_id2': fields.many2one('product.product', u'成本归集产品', domain="[('supply_method', '=', 'produce')]"),
        'product_qty': fields.float(u'产量', digits=(16, 2)),
        'std_qty': fields.float(u'标准件基数', digits=(8, 2), readonly=True),
        'done_qty': fields.float(u'总标准产量', digits=(16, 2), help=u'产出产量*标准件基数。', readonly=True),
        'remark': fields.char(u'备注', size=512),
        'create_date': fields.datetime(u'创建时间', readonly=True, help=u"当前系统时间"),
        'write_date': fields.datetime(u'修改时间', readonly=True, help=u"当前系统时间"),
        "create_uid": fields.many2one("res.users", u"创建人", readonly=True),
        'write_uid': fields.many2one("res.users", u"修改人", readonly=True),
    }

    _defaults = {
        'std_qty': 0.00,
        'product_qty': 0.00,
        'done_qty': 0.00,
    }

    def onchange_product_id_product(self, cr, uid, ids, product_id=False, product_qty=0.00, context=None):
        ''' 按产品计件录入 '''
        context = context or {}
        bs_wcenter_id = context.get('bs_wcenter_id', False)
        res = {'value':{
            'std_qty': 1.00,
            'product_id2': False,
            'done_qty': 0.00
        }}
        if not bs_wcenter_id:
            return res
        vals = {
            'bs_wcenter_id': bs_wcenter_id,
            'product_id': product_id,
        }
        if product_id:
            res['value'].update(product_id2=product_id)
            # cr.execute('''SELECT DISTINCT std_qty FROM bs_ability WHERE bs_wcenter_id = %(bs_wcenter_id)s AND product_id = %(product_id)s;''' % vals)
            cr.execute('''SELECT DISTINCT std_qty FROM bs_ability a
                LEFT JOIN bs_devicegroup b ON b.id = a.bs_devicegroup_id
                WHERE b.bs_wcenter_id = %(bs_wcenter_id)s
                AND a.product_id = %(product_id)s;''' % vals)
            std_qtys = cr.dictfetchall()
            if std_qtys and std_qtys[0]:
                std_qty = std_qtys[0]['std_qty']
                res['value'].update(std_qty=std_qty)
                res['value'].update(done_qty=product_qty * std_qty)
                return res
        res['value'].update(std_qty=1.00)
        res['value'].update(done_qty=product_qty)
        return res

    def onchange_bs_wprocess_id_step(self, cr, uid, ids, bs_wprocess_id=False, product_qty=0.00, context=None):
        ''' 按工序计件录入 '''
        context = context or {}
        res_company_id = context.get('res_company_id', False)
        bs_piecegroup_id = context.get('bs_piecegroup_id', False)
        res = {'value':{
            'std_qty': 1.00,
            'done_qty': 0.00
        }}
        if not res_company_id:
            return res
        vals = {
            'res_company_id': res_company_id,
            'bs_wprocess_id': bs_wprocess_id,
            'bs_piecegroup_id': bs_piecegroup_id,
        }
        if bs_wprocess_id:
            cr.execute(''' SELECT DISTINCT a.std_qty
                            FROM bs_wprocess a
                            LEFT JOIN bs_wcenter b ON b.id = a.bs_wcenter_id
                            --LEFT JOIN bs_piecegroup c ON c.bs_wcenter_id = b.id
                            LEFT JOIN bs_pshop d ON d.id = b.bs_pshop_id
                            WHERE a.id = %(bs_wprocess_id)s
                              AND d.res_company_id = %(res_company_id)s;
                            ''' % vals)
            std_qtys = cr.fetchall()
            if std_qtys and std_qtys[0][0]:
                std_qty = std_qtys[0][0]
                res['value'].update(std_qty=std_qty)
                res['value'].update(done_qty=product_qty * std_qty)
                return res
        res['value'].update(std_qty=1.00)
        res['value'].update(done_qty=product_qty)
        return res

    def onchange_bs_piece_stdqty_id(self, cr, uid, ids, bs_piece_stdqty_id=False, product_qty=0.00, context=None):
        ''' 按产品+工序计件录入 '''
        res = {'value':{
            'std_qty': 1.00,
            'product_id2': False,
            'done_qty': 0.00
        }}
        if bs_piece_stdqty_id:
            piece_stdqty = self.pool.get('bs.piece.stdqty').browse(cr, uid, bs_piece_stdqty_id)
            res['value'].update(product_id2=piece_stdqty.product_id.id)
            std_qty = piece_stdqty.std_qty
            # print '-------------stdqty', std_qty
            if std_qty:
                res['value'].update(std_qty=std_qty)
                res['value'].update(done_qty=product_qty * std_qty)
                return res
        # res['value'].update(std_qty=1.00)
        res['value'].update(done_qty=product_qty)
        return res

    def onchange_wprocess_product_id_pstep(self, cr, uid, ids, bs_wprocess_id=False, product_id=False, product_qty=0.00, context=None):
        ''' 按产品+工序计件录入 '''
        context = context or {}
        res_company_id = context.get('res_company_id', False)
        bs_piecegroup_id = context.get('bs_piecegroup_id', False)
        res = {'value':{
            'std_qty': 1.00,
            'product_id2': False,
            'done_qty': 0.00
        }}
        if not res_company_id:
            return res
        if not bs_wprocess_id or not product_id:
            return False
        vals = {
            'res_company_id': res_company_id,
            'bs_wprocess_id': bs_wprocess_id,
            'product_id': product_id,
            'bs_piecegroup_id': bs_piecegroup_id,
        }
        if bs_wprocess_id and product_id:
            res['value'].update(product_id2=product_id)
            cr.execute('''SELECT DISTINCT std_qty FROM bs_piece_stdqty WHERE bs_wprocess_id = %(bs_wprocess_id)s AND product_id = %(product_id)s AND res_company_id = %(res_company_id)s;''' % vals)
            std_qtys = cr.fetchall()
            if std_qtys and std_qtys[0][0]:
                std_qty = std_qtys[0][0]
                res['value'].update(std_qty=std_qty)
                res['value'].update(done_qty=product_qty * std_qty)
                return res
        res['value'].update(std_qty=1.00)
        res['value'].update(done_qty=product_qty)
        return res

    def onchange_product_qty_product(self, cr, uid, ids, product_qty=0.00, std_qty=0.00, context=None):
        ''' 按产品计件录入/按工序计件录入/按工序+产品计件录入 '''
        res = {'value':{}}
        if product_qty  < 0.00:
            res['warning'] = {'title': _('警告'), 'message': _('产量必须大于等于0。')}
            res['value'].update({'product_qty': 0.00})
            res['value'].update({'done_qty': 0.00})
            return res
        res = {'value':{'done_qty': product_qty * std_qty,}}
        return res

mm_productpiece_d()

class product_product(osv.osv):
    _inherit = 'product.product'

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        ''' 按产品计件录入 '''
        context = context or {}
        if context.get('is_mm_productpiece_line',False) == '1':
            ids = []
            vals = {
                'res_company_id': context.get('res_company_id',False),
                'bs_piecegroup_id': context.get('bs_piecegroup_id',False),
            }
            if vals['res_company_id'] and vals['bs_piecegroup_id']:
                # sql = '''
                # SELECT DISTINCT product_id
                # FROM bs_ability a
                # LEFT JOIN bs_devicegroup b ON b.id = a.bs_devicegroup_id
                # WHERE a.bs_devicegroup_id = (
                #   SELECT bs_devicegroup_id FROM bs_piecegroup WHERE id = %(bs_piecegroup_id)s AND res_company_id = %(res_company_id)s
                # );''' % vals
                # 返回虚拟机群下的产品id
                sql = """SELECT DISTINCT product_id
                FROM bs_ability a
                LEFT JOIN bs_devicegroup b ON b.id = a.bs_devicegroup_id
                WHERE b.id=37 """
                cr.execute(sql)
                ids = [r[0] for r in cr.fetchall()]
            args.append((('id', 'in', ids)))

        if context.get('is_bs_piece_stdqty_product_id',False) == '1':
            ids = []
            vals = {
                'res_company_id': context.get('res_company_id',False),
                'bs_wcenter_id': context.get('bs_wcenter_id',False),
            }
            if vals['res_company_id'] and vals['bs_wcenter_id']:
                sql = '''
                SELECT DISTINCT a.product_id
                FROM bs_ability a
                LEFT JOIN bs_devicegroup b ON b.id = a.bs_devicegroup_id
                LEFT JOIN bs_wcenter c ON c.id = b.bs_wcenter_id
                WHERE b.bs_wcenter_id = %(bs_wcenter_id)s;''' % vals
                cr.execute(sql)
                ids = [r[0] for r in cr.fetchall()]
            args.append((('id', 'in', ids)))
        res = super(product_product, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)
        return res

product_product()

class mm_timepiece_daycheck(osv.osv):
    _name = 'mm.timepiece.daycheck'
    _inherit = ['mail.thread']
    _description = u'计时计件日审'
    _order = 'date desc'
    _rec_name = "billcode"

    _columns = {
        'billcode': fields.char(u'日审单号', size=64, required=True, readonly=True),
        'res_company_id': fields.many2one('res.company', u'所属公司', required=True),
        'date': fields.date(u'日期', required=True),
        'remark': fields.char(u'备注', size=512),
        'v_wfstate_id': fields.many2one('bs.publictype.d', u'状态', domain="[('bs_publictype_id.viewname','=','v_wfstate')]", widget='seletion',  select=True, required=True),
        'sys_billtype_id': fields.many2one('sys.billtype', u'单据类别', required=True, readonly=True),
        'res_users_idcheck': fields.many2one('res.users', u'审核人', readonly=True, select=True),
        'check_date': fields.datetime(u'审核时间', readonly=True, select=True),
        'create_date': fields.datetime(u'创建时间', readonly=True, help=u"当前系统时间"),
        'write_date': fields.datetime(u'修改时间', readonly=True, help=u"当前系统时间"),
        "create_uid": fields.many2one("res.users", u"创建人", readonly=True),
        'write_uid': fields.many2one("res.users", u"修改人", readonly=True),
        'daycheck_line': fields.one2many('mm.timepiece.daycheck.d', 'mm_timepiece_daycheck_id', u'计时计件日审明细'),
        'daycheck_line1': fields.one2many('mm.timepiece.daycheck.d', 'mm_timepiece_daycheck_id', u'计时计件日审明细', domain=[('bs_wcenter_id.name', '=', '烘烤工作中心')]),
        'daycheck_line2': fields.one2many('mm.timepiece.daycheck.d', 'mm_timepiece_daycheck_id', u'计时计件日审明细', domain=[('bs_wcenter_id.name', '=', '外包工作中心')]),
        'daycheck_line3': fields.one2many('mm.timepiece.daycheck.d', 'mm_timepiece_daycheck_id', u'计时计件日审明细', domain=[('bs_wcenter_id.name', '=', '内包工作中心')]),
        'daycheck_line4': fields.one2many('mm.timepiece.daycheck.d', 'mm_timepiece_daycheck_id', u'计时计件日审明细', domain=[('bs_wcenter_id.name', '=', '糕点工作中心')]),
        'daycheck_line5': fields.one2many('mm.timepiece.daycheck.d', 'mm_timepiece_daycheck_id', u'计时计件日审明细', domain=[('bs_wcenter_id.name', '=', '伴侣工作中心')]),
        'daycheck_line6': fields.one2many('mm.timepiece.daycheck.d', 'mm_timepiece_daycheck_id', u'计时计件日审明细', domain=[('bs_wcenter_id.name', '=', '磨粉工作中心')]),
    }

    _sql_constraints = [
        ('billcode_unique', 'unique(billcode)', u'单号重复!'),
    ]

    def _check_write(self, cr, uid, ids, context=None):
        ''' 校验公司+日期 '''
        ids = [ids] if isinstance(ids, (int, long)) else ids
        for daycheck in self.browse(cr, uid, ids, context=context):
            res_company_id = daycheck.res_company_id.id
            date = daycheck.date
            vals = {
                'date': date,
                'res_company_id': res_company_id,
                'v_wfstate_id': TOTAL_STATE_ID['cancel'],
                'id': daycheck.id,
            }
            cr.execute('''SELECT id, billcode FROM mm_timepiece_daycheck WHERE res_company_id = %(res_company_id)s AND date = '%(date)s' AND v_wfstate_id != %(v_wfstate_id)s AND id != %(id)s;''' % vals)
            data = cr.fetchall()
            if len(data) >= 1:
                raise osv.except_osv(_('警告!'), _('%s %s 的计时计件日审单已存在, 请找到单号‘%s’进行操作。'% (daycheck.res_company_id.name, date, data[0][1]) ))
        return True

    _constraints = [
        (_check_write, '', []),
    ]

    _SYS_BILLTYPE_ID = TOTAL_SYS_BILLTYPE_ID['mm_timepiece_daycheck']

    def _get_res_company_id(self, cr, uid, context=None):
        return self.pool.get('res.company')._company_default_get(cr, uid, 'mm.timepiece.daycheck', context=context)

    def default_get(self, cr, uid, fields, context=None):
        context = context or {}
        res = super(mm_timepiece_daycheck, self).default_get(cr, uid, fields, context=context)
        date = openerp.osv.fields.date.context_today(self, cr, uid, context=context)
        value = {
            'billcode': u'系统自动生成',
            'date': date,
            'sys_billtype_id': self._SYS_BILLTYPE_ID,
            'v_wfstate_id': TOTAL_STATE_ID['draft'],
            'res_company_id': self._get_res_company_id(cr, uid, context=context),
        }
        res.update(value)
        return res

    def create(self, cr, uid, vals, context=None):
        vals = vals or {}
        now = fields.date.context_today(self, cr, uid, context=context)
        dt = vals.get('date', now)
        sys_billtype_id = self._SYS_BILLTYPE_ID
        if vals.get('billcode', u'系统自动生成') == u'系统自动生成':
            billcode = self.pool.get('sys.billtype').get(cr, uid, sys_billtype_id, dt, context=context)
            vals.update(billcode=billcode)
        value ={
            'res_company_id': vals.get('res_company_id',0),
            'billtype': sys_billtype_id,
            'billdate': dt,
        }
        data = self.pool.get('sys.fnclose.filter').check_close(cr, uid, value)
        if data.get('isclosed', False):
            raise osv.except_osv(_('警告！'), _('计划生产模块已关账，不能创建单据。'))
        return super(mm_timepiece_daycheck, self).create(cr, uid, vals, context=context)

    def unlink(self, cr, uid, ids, context=None):
        for daycheck in self.browse(cr, uid, ids, context=context):
            if daycheck.v_wfstate_id.id == TOTAL_STATE_ID['done']:
                raise osv.except_osv(_('错误!'), _('生效状态不能删除。'))
            if daycheck.v_wfstate_id.id == TOTAL_STATE_ID['cancel']:
                raise osv.except_osv(_('错误!'), _('单据已作废，不能删除。'))
            if daycheck.v_wfstate_id.id == TOTAL_STATE_ID['approve']:
                raise osv.except_osv(_('错误!'), _('审批中的单据，不能删除但可作废。'))
            for line in daycheck.daycheck_line:
                self.pool.get('mm.timepiece.daycheck.d').unlink(cr, uid, line.id, context=context)
        return super(mm_timepiece_daycheck, self).unlink(cr, uid, ids, context=context)

    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        now = fields.date.context_today(self, cr, uid, context=context)
        default.update(billcode = u'系统自动生成')
        default.update(date = now)
        default.update(v_wfstate_id = TOTAL_STATE_ID['draft'])
        default.update(daycheck_line = [])
        return super(mm_timepiece_daycheck, self).copy(cr, uid, id, default, context=context)

    def action_batch_check_done(self, cr, uid, ids, context=None):
        self.check_done(cr, uid, ids, context=context)
        return True

    def check_done(self, cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids, context=context):
            if not check.daycheck_line:
                raise osv.except_osv(_('错误!'),_('没有日审明细，不能审核。'))
            vals = {
                'v_wfstate_id': TOTAL_STATE_ID['done'],
                'check_date':  time.strftime('%Y-%m-%d %H:%M:%S'),
                'res_users_idcheck': uid,
            }
            self.write(cr, uid, [check.id], vals, context=context)
            context.update({
                'v_approve_id': 268,
                "res_company_id": check.res_company_id.id,
                "sys_billtype_id": self._SYS_BILLTYPE_ID,
                "res_id": check.id,
                "subject": u"计时计件日审单",
                "body_html": u""" 计时计件日审单：%s 已生效， 请知悉； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                "auto_delete":True,
            })
            self.pool.get('bs.approve').func_send_mail(cr, uid, context)
        return True

    def check_order(self,cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids, context=context):
            if not check.daycheck_line:
                raise osv.except_osv(_('错误!'),_('没有日审明细，不能提交。'))
            vals = {
                'v_wfstate_id': TOTAL_STATE_ID['approve'],
                'check_date': False,
                'res_users_idcheck': False,
            }
            self.write(cr, uid, [check.id], vals, context=context)
            context.update({
                'v_approve_id': 267,
                "res_company_id": check.res_company_id.id,
                "sys_billtype_id": self._SYS_BILLTYPE_ID,
                "res_id": check.id,
                "subject": u"计时计件日审单",
                "body_html": u""" 计时计件日审单：%s 已提交， 请审核； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                "auto_delete":True,
            })
            self.pool.get('bs.approve').func_send_mail(cr, uid, context)
        return True

    def check_cancel(self,cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids, context=context):
            vals = {
                'v_wfstate_id': TOTAL_STATE_ID['cancel'],
                'check_date': False,
                'res_users_idcheck': False,
            }
            self.write(cr, uid, [check.id], vals, context=context)
        return True

    def check_draft(self,cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids, context=context):
            vals = {
                'v_wfstate_id': TOTAL_STATE_ID['draft'],
                'check_date': False,
                'res_users_idcheck': False
            }
            self.write(cr, uid, [check.id],vals)
            context.update({
                'v_approve_id': 2002,
                "res_company_id": check.res_company_id.id,
                "sys_billtype_id": self._SYS_BILLTYPE_ID,
                "res_id": check.id,
                "subject": u"计时计件日审单",
                "body_html": u""" 计时计件日审单：%s 已反审， 请处理； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                "auto_delete":True,
            })
            self.pool.get('bs.approve').func_send_mail(cr, uid, context)
        return True

    def export_mm_timepiece_daycheck_d(self, cr, uid, ids, context=None):
        ''' 导出日审明细 '''
        vals = {'mm_timepiece_daycheck_id': ids[0],}
        sql = '''
        SELECT a.date,  --日期
                c.name,  --工作中心
                d.name,  --班次
                a.jt_name,  --机台名称
                a.product_name,  --品名
                a.product_qty,  --产量
                a.std_qty,  --标准件基数
                h.name AS wgmf_rolestatus , --岗位名称
                g.res_name as name,  --出勤人员
                a.attend_time, --出勤工时
                a.product_done_qty, --总标准产量
                a.done_qty,  --总和
                a.total_time, --标准件分配份额
                a.user_done_qty, --个人标准产量
                a.aidtime_qty,
                --round(COALESCE(a.aidtime_qty,0.00), 2),  --洗罐子工资
                a.remark  --备注
        FROM mm_timepiece_daycheck_d a
        LEFT JOIN mm_timepiece_daycheck b ON b.id = a.mm_timepiece_daycheck_id
        LEFT JOIN bs_wcenter c ON c.id = a.bs_wcenter_id
        LEFT JOIN bs_class d ON d.id = a.bs_class_id
        LEFT JOIN bs_employee g ON g.id = a.res_users_id
        LEFT JOIN bs_publictype_d h ON h.id = g.res_types
        WHERE b.id = %(mm_timepiece_daycheck_id)s
        ORDER BY a.date, d.name, c.name, a.jt_name, a.product_name;
        ''' % vals
        from openerp.addons.web.controllers.main import ExportData2Excel as _Export
        import uuid
        _name = str(uuid.uuid1())
        _command = sql
        _field = [u'日期', u'工作中心', u'班次', u'机台名称', u'品名', u'数量', u'标准件基数', u'岗位名称',
                  u'出勤人员', u'出勤工时', u'总标准产量', u'总和', u'标准件分配份额', u'个人标准产量', u'洗罐子工资', u'备注']
        _file_name = u'计时计件日审'
        _Export.do(self, cr, uid, _name, _file_name, _command, _field)
        return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name}

    def refresh_mm_timepiece_daycheck_d(self, cr, uid, ids, context=None):
        ''' 获取日审明细 '''
        ids = [ids] if isinstance(ids, (int, long)) else ids
        for record in self.browse(cr, uid, ids, context=context):
            vals = {
                'mm_timepiece_daycheck_id': record.id,
                'date': record.date,
                'res_company_id': record.res_company_id.id,
                'std_qty': WASH_STD_QTY,
                'v_wfstate_id': TOTAL_STATE_ID['done']
            }
            print 'vals',vals
            # F_mm_timepiece_daycheck -- PostgreSQL函数/存储过程
            cr.execute( "SELECT * FROM f_mm_timepiece_daycheck(%(mm_timepiece_daycheck_id)s, '%(date)s', %(res_company_id)s, %(std_qty)s, %(v_wfstate_id)s);"  % vals)
        return True

mm_timepiece_daycheck()

class mm_timepiece_daycheck_d(osv.osv):
    _name = 'mm.timepiece.daycheck.d'
    _description = u'计时计件日审明细'
    _order = 'bs_class_id, bs_wcenter_id, type, jt_name, product_name, res_users_id'

    def _get_wgmf_rolestatus(self, cr, uid, context=None):
        context = context or {}
        user_obj = self.pool.get('res.users')
        res = user_obj.fields_get(cr, uid, allfields=['wgmf_rolestatus'], context=context)['wgmf_rolestatus']['selection']
        return res

    def _get_type(self, cr, uid, context=None):
        context = context or {}
        timepiece_obj = self.pool.get('mm.timepiece')
        res = timepiece_obj.fields_get(cr, uid, allfields=['type'], context=context)['type']['selection']
        res = res + [('odd', '异常工时')]
        return res

    def _get_yc_type(self, cr, uid, context=None):
        context = context or {}
        aidtime_obj = self.pool.get('mm.aidtime')
        res = aidtime_obj.fields_get(cr, uid, allfields=['type'], context=context)['type']['selection']
        return res

    _columns = {
        'mm_timepiece_daycheck_id': fields.many2one('mm.timepiece.daycheck', u'计时计件日审'),
        'type': fields.selection(_get_type, u'计件方式', readonly=True, size=16),
        'yc_type': fields.selection(_get_yc_type, u'异常工时录入方式', readonly=True, size=16),
        'date': fields.date(u'日期', readonly=True),
        'bs_wcenter_id': fields.many2one('bs.wcenter', u'工作中心', readonly=True),
        'bs_class_id': fields.many2one('bs.class', u'班次', readonly=True),
        'bs_piecegroup_id': fields.many2one('bs.piecegroup', u'计件组', readonly=True),
        'product_id': fields.many2one('product.product', u'产品', readonly=True),
        'bs_wprocess_id': fields.many2one('bs.wprocess', u'工序', readonly=True),
        'v_aidtype_id': fields.many2one('bs.publictype.d', u'辅助项目', domain="[('bs_publictype_id.viewname','=','v_aidtype')]", readonly=True),
        'product_qty': fields.float(u'产量', digits=(16, 2), readonly=True),
        'std_qty': fields.float(u'标准件基数', digits=(8, 2), readonly=True),
        # 'wgmf_rolestatus': fields.selection([('1','营养顾问'),('2','流动营养顾问'),('3','促销主管'),('4','机修'),('5','办事处文员'),('6','终端经理'),('7','区域经理'),('8','大区总监'),('9','人事经理')], u'岗位名称', readonly=True, size=16),
        'wgmf_rolestatus': fields.many2one('bs.publictype.d', u'岗位名称', domain=[('bs_publictype_id.viewname','=','v_bs_employee')], readonly=True),
        'res_users_id': fields.many2one('bs.employee', u'出勤人员', readonly=True),
        'attend_time': fields.float(u'出勤工时', digits=(8, 2), readonly=True),
        'product_done_qty': fields.float(u'总标准产量', digits=(16, 2), readonly=True),
        'done_qty': fields.float(u'总和', digits=(16, 2), readonly=True),
        'total_time': fields.float(u'标准件分配份额', digits=(8, 2), readonly=True),
        'user_done_qty': fields.float(u'个人标准产量', digits=(16, 2), readonly=True),
        'remark': fields.char(u'备注', size=512),
        'aidtime_qty': fields.float(u'洗罐子工资', digits=(8, 2), readonly=True),
        'jt_name': fields.char(u'机台名称', size=128, readonly=True),
        'product_name': fields.char(u'品名', size=128, readonly=True),
    }

mm_timepiece_daycheck_d()

class mm_equipment_opt(osv.osv):
    _name = 'mm.equipment.opt'
    _inherit = ['mail.thread']
    _description = u'设备运行录入'
    _order = 'date desc'
    _rec_name = "billcode"

    _columns = {
        'billcode': fields.char(u'单号', size=64, readonly=True),
        'v_wfstate_id': fields.many2one('bs.publictype.d', u'状态', domain="[('bs_publictype_id.viewname','=','v_wfstate')]", widget='seletion',  select=True, readonly=True),
        'sys_billtype_id': fields.many2one('sys.billtype', u'单据类别', required=True, readonly=True),
        'res_company_id': fields.many2one('res.company', u'所属公司', required=True),
        'date': fields.date(u'日期', required=True),
        'bs_wcenter_id': fields.many2one('bs.wcenter', u'工作中心', required=True),
        'remark': fields.char(u'备注', size=512),
        'res_users_idcheck': fields.many2one('res.users', u'审核人', readonly=True, select=True),
        'check_date': fields.datetime(u'审核时间', readonly=True, select=True),
        'create_date': fields.datetime(u'创建时间', readonly=True, help=u"当前系统时间"),
        'write_date': fields.datetime(u'修改时间', readonly=True, help=u"当前系统时间"),
        "create_uid": fields.many2one("res.users", u"创建人", readonly=True),
        'write_uid': fields.many2one("res.users", u"修改人", readonly=True),
        'equipment_line': fields.one2many('mm.equipment.opt.d', 'mm_equipment_opt_id', string=u'运行记录'),
        'bs_class_id': fields.many2one('bs.class', u'班次', required=True),
    }

    _sql_constraints = [
        ('billcode_unique', 'unique(billcode)', u'单号重复!'),
    ]

    def _check_write(self, cr, uid, ids, context=None):
        ''' 校验公司+日期+工作中心+班次 '''
        ids = [ids] if isinstance(ids, (int, long)) else ids
        for check in self.browse(cr, uid, ids, context=context):
            vals = {
                'date': check.date,
                'res_company_id': check.res_company_id.id,
                'v_wfstate_id': TOTAL_STATE_ID['cancel'],
                'bs_wcenter_id': check.bs_wcenter_id.id,
                'bs_class_id':check.bs_class_id.id,
                'id': check.id,
            }
            cr.execute('''SELECT id, billcode 
                    FROM mm_equipment_opt 
                    WHERE res_company_id = %(res_company_id)s 
                    AND bs_wcenter_id = %(bs_wcenter_id)s 
                    AND date = '%(date)s' 
                    AND v_wfstate_id != %(v_wfstate_id)s 
                    AND bs_class_id = %(bs_class_id)s 
                    AND id != %(id)s;''' % vals)
            data = cr.fetchall()
            if len(data) >= 1:
                raise osv.except_osv(_('警告!'), _('%s %s 的设备运行录入单已存在, 请找到单号‘%s’进行操作。'% (check.bs_wcenter_id.name, check.date, data[0][1]) ))
        return True

    _constraints = [
        (_check_write, '', []),
    ]

    def onchange_res_company_id(self, cr, uid, ids, res_company_id=False, context=None):
        res = {}
        if res_company_id:
            return {'value':{'bs_wcenter_id': False,}}
        return res

    def onchange_bs_wcenter_id(self, cr, uid, ids, res_company_id=False, bs_wcenter_id=False, context=None):
        res = {'value': {'equipment_line': False}}
        if not res_company_id or not bs_wcenter_id:
            return {}
        vals = {
            'res_company_id': res_company_id,
            'bs_wcenter_id': bs_wcenter_id,
        }
        if res_company_id and bs_wcenter_id:
            sql = '''
            SELECT a.id AS bs_device_id, --设备
                    a.std_time, --机器计划开机时间
                    a.bs_name
            FROM bs_device a
            LEFT JOIN bs_devicegroup b ON b.id = a.bs_devicegroup_id
            WHERE a.res_company_id = %(res_company_id)s
              AND b.bs_wcenter_id = %(bs_wcenter_id)s
              AND a.isvalid = 't';
            ''' % vals
            cr.execute(sql)
            equipment_line = cr.dictfetchall()
            res['value'].update(equipment_line=equipment_line)
            return res
        return {}

    _SYS_BILLTYPE_ID = TOTAL_SYS_BILLTYPE_ID['mm_equipment_opt']

    def _get_res_company_id(self, cr, uid, context=None):
        return self.pool.get('res.company')._company_default_get(cr, uid, 'mm.equipment.opt', context=context)

    def default_get(self, cr, uid, fields, context=None):
        res = super(mm_equipment_opt, self).default_get(cr, uid, fields, context=context)
        value = {
            'billcode': u'系统自动生成',
            'sys_billtype_id': self._SYS_BILLTYPE_ID,
            'v_wfstate_id': TOTAL_STATE_ID['draft'],
            'res_company_id': self._get_res_company_id(cr, uid, context=context),
            'date': datetime.now().strftime('%Y-%m-%d'),
        }
        res.update(value)
        return res

    def create(self, cr, uid, vals, context=None):
        vals = vals or {}
        sys_billtype_id = self._SYS_BILLTYPE_ID
        dt = fields.date.context_today(self, cr, uid, context=context)
        date = vals.get('date', False) if vals.get('date', False) else dt
        if vals.get('billcode', u'系统自动生成') == u'系统自动生成':
            billcode = self.pool.get('sys.billtype').get(cr, uid, sys_billtype_id, date, context=context)
            vals.update(billcode=billcode)
        value ={
            'res_company_id': vals.get('res_company_id',0),
            'billtype': sys_billtype_id,
            'billdate': dt,
        }
        data = self.pool.get('sys.fnclose.filter').check_close(cr, uid, value)
        if data.get('isclosed', False):
            raise osv.except_osv(_('警告！'), _('计划生产模块已关账，不能创建单据。'))
        return super(mm_equipment_opt, self).create(cr, uid, vals, context=context)

    def unlink(self, cr, uid, ids, context=None):
        for opt in self.browse(cr, uid, ids, context=context):
            if opt.v_wfstate_id.id == TOTAL_STATE_ID['done']:
                raise osv.except_osv(_('错误!'), _('生效状态不能删除。'))
            if opt.v_wfstate_id.id == TOTAL_STATE_ID['cancel']:
                raise osv.except_osv(_('错误!'), _('单据已作废，不能删除。'))
            if opt.v_wfstate_id.id == TOTAL_STATE_ID['approve']:
                raise osv.except_osv(_('错误!'), _('审批中的单据，不能删除但可作废。'))
            for line in opt.equipment_line:
                self.pool.get('mm.equipment.opt.d').unlink(cr, uid, line.id, context=context)
        return super(mm_equipment_opt, self).unlink(cr, uid, ids, context=context)

    def check_done(self, cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids, context=context):
            if not check.equipment_line:
                raise osv.except_osv(_('错误!'),_('没有运行记录，不能审核。'))
            vals = {
                'v_wfstate_id': TOTAL_STATE_ID['done'],
                'check_date':  time.strftime('%Y-%m-%d %H:%M:%S'),
                'res_users_idcheck': uid,
            }
            self.write(cr, uid, [check.id], vals, context=context)
            context.update({
                'v_approve_id': 268,
                "res_company_id": check.res_company_id.id,
                "sys_billtype_id": self._SYS_BILLTYPE_ID,
                "res_id": check.id,
                "subject": u"设备运行录入",
                "body_html": u""" 设备运行录入：%s 已生效， 请知悉； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                "auto_delete":True,
            })
            self.pool.get('bs.approve').func_send_mail(cr, uid, context)
        return True

    def check_order(self,cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids, context=context):
            if not check.equipment_line:
                raise osv.except_osv(_('错误!'),_('没有运行记录，不能提交。'))
            vals = {
                'v_wfstate_id': TOTAL_STATE_ID['approve'],
                'check_date': False,
                'res_users_idcheck': False,
            }
            self.write(cr, uid, [check.id], vals, context=context)
            context.update({
                'v_approve_id': 267,
                "res_company_id": check.res_company_id.id,
                "sys_billtype_id": self._SYS_BILLTYPE_ID,
                "res_id": check.id,
                "subject": u"设备运行录入",
                "body_html": u""" 设备运行录入：%s 已提交， 请审核； 单据链接 <a href="http://192.168.5.34:8069/#id=%d&view_type=form&model=%s">请点击</a>""" % (check.billcode,check.id,self._name),
                "auto_delete":True,
            })
            self.pool.get('bs.approve').func_send_mail(cr, uid, context)
        return True

    def check_cancel(self,cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids, context=context):
            vals = {
                'v_wfstate_id': TOTAL_STATE_ID['cancel'],
                'check_date': False,
                'res_users_idcheck': False,
            }
            self.write(cr, uid, [check.id], vals, context=context)
        return True

    def do_equipment_line(self,cr, uid, ids, context=None):
        for rec in self.browse(cr, uid, ids, context=context):
            vals = {
                'mm_equipment_opt_id': rec.id,
                'res_company_id': rec.res_company_id.id,
                'bs_wcenter_id': rec.bs_wcenter_id.id,
            }
            # delete_sql = '''DELETE FROM mm_equipment_opt_d WHERE mm_equipment_opt_id = %(mm_equipment_opt_id)s; ''' % vals
            sql = '''
            INSERT INTO mm_equipment_opt_d(mm_equipment_opt_id, bs_device_id, std_time)
            SELECT %(mm_equipment_opt_id)s AS mm_equipment_opt_id,
                    a.id, --设备
                    a.std_time --机器计划开机时间
            FROM bs_device a
            LEFT JOIN bs_asset b ON b.id = a.bs_asset_id
            LEFT JOIN bs_devicegroup c ON c.id = a.bs_devicegroup_id
            WHERE a.res_company_id = %(res_company_id)s
              AND c.bs_wcenter_id = %(bs_wcenter_id)s
              AND a.isvalid = 't'
              AND a.id NOT IN (SELECT bs_device_id FROM mm_equipment_opt_d WHERE mm_equipment_opt_id = %(mm_equipment_opt_id)s);
            ''' % vals
            cr.execute(sql)
        return True

mm_equipment_opt()

class mm_equipment_opt_d(osv.osv):
    _name = 'mm.equipment.opt.d'
    _description = u'异常记录'
    _order = 'id desc'

    _columns = {
        'mm_equipment_opt_id': fields.many2one('mm.equipment.opt', u'设备运行录入'),
        'bs_device_id': fields.many2one('bs.device', u'设备', domain="[('isvalid','=','t')]", required=True),
        'bs_name':fields.char(u'设备编号', size=128, required=True),
        'std_time': fields.float(u'计划开机时间(时/天)', digits=(8, 2)),
        'down_time': fields.float(u'主动停机时间(时/天)', digits=(8, 2)),
        'fault_time': fields.float(u'故障时长(时/天)', digits=(8, 2)),
        'remark': fields.char(u'备注', size=512),
        'create_date': fields.datetime(u'创建时间', readonly=True, help=u"当前系统时间"),
        'write_date': fields.datetime(u'修改时间', readonly=True, help=u"当前系统时间"),
        "create_uid": fields.many2one("res.users", u"创建人", readonly=True),
        'write_uid': fields.many2one("res.users", u"修改人", readonly=True),
    }

    _sql_constraints = [
        ('equipment_device_unique', 'unique(mm_equipment_opt_id, bs_device_id)', u'设备唯一。'),
    ]

    _defaults = {
        'down_time': 0.00,
        'fault_time': 0.00,
    }

    def onchange_bs_device_id(self, cr, uid, ids, bs_device_id=False, context=None):
        res = {}
        if bs_device_id:
            device = self.pool.get('bs.device').browse(cr, uid, bs_device_id, context=context)
            return {'value':{'std_time': device.std_time,'bs_name':device.bs_name}}
        return res

    def onchange_std_time(self, cr, uid, ids, std_time=0.00, down_time=0.00, fault_time=0.00, context=None):
        res = {'value':{}}
        if std_time < 0.00:
            res['warning'] = {'title': _('警告'), 'message': _('计划开机时间不能小于0。')}
            res['value'].update({'std_time':12})
            return res
        if std_time < down_time:
            res['warning'] = {'title': _('警告'), 'message': _('计划开机时间不能小于主动停机时间。')}
            res['value'].update({'std_time':12})
            return res
        if std_time < fault_time:
            res['warning'] = {'title': _('警告'), 'message': _('计划开机时间不能小于故障时长。')}
            res['value'].update({'std_time':12})
            return res
        res['value'].update({'std_time':std_time})
        return res

    def onchange_down_time(self, cr, uid, ids, down_time=0.00, std_time=0.00, context=None):
        res = {'value':{}}
        if down_time < 0.00:
            res['warning'] = {'title': _('警告'), 'message': _('主动停机时间不能小于0。')}
            res['value'].update({'down_time': 0.00})
            return res
        if down_time > std_time:
            res['warning'] = {'title': _('警告'), 'message': _('主动停机时间不能大于计划开机时间。')}
            res['value'].update({'down_time': 0.00})
            return res
        res['value'].update({'down_time': down_time})
        return res

    def onchange_fault_time(self, cr, uid, ids, fault_time=0.00, std_time=0.00, context=None):
        res = {'value':{}}
        if fault_time < 0.00:
            res['warning'] = {'title': _('警告'), 'message': _('故障时长不能小于0。')}
            res['value'].update({'fault_time': 0.00})
            return res
        if fault_time > std_time:
            res['warning'] = {'title': _('警告'), 'message': _('故障时长不能大于计划开机时间。')}
            res['value'].update({'fault_time': 0.00})
            return res
        res['value'].update({'fault_time': fault_time})
        return res

mm_equipment_opt_d()