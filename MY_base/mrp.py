-- Function: check_bom(integer, integer)

-- DROP FUNCTION check_bom(integer, integer);

CREATE OR REPLACE FUNCTION check_bom(a integer, b integer)
  RETURNS integer AS
$BODY$

declare i character varying;

		line  record;
		change_id integer;
		bom_id integer;
		flag integer;
		qty float;
		remark varchar(200);
begin
change_id=0;
bom_id=0;
flag=0;

for line in (select * from v_mrp_version where product_id=a and oldbom=b ) loop
        --第一  
	if change_id<>line.bomchange_id then
	    change_id=line.bomchange_id;
            if flag=1 then
               remark=''
               return bom_id;
            elsif  line.line_id is null then         
	        return line.newbom;
	    else 
            bom_id=line.newbom;		
	    --库存
	    if line.method='stock' then
	      if 
	         (line.logical='<'  and line.qty<cast(line.value as integer) )
	          or (line.logical='>'  and line.qty>cast(line.value as integer) )
	          or (line.logical='>='  and line.qty>=cast(line.value as integer) )
	          or (line.logical='<='  and line.qty<=cast(line.value as integer) )
	          or (line.logical='='  and line.qty=cast(line.value as integer) )
	      then 
		 flag=1; 
	      else
	         flag=0;
              end if;
            --时间
            else 
	      if 
	         (line.logical='<'  and line.calc_time<cast(line.value as timestamp) )
	          or (line.logical='>'  and line.calc_time>cast(line.value as timestamp) )
	          or (line.logical='>='  and line.calc_time>=cast(line.value as timestamp) )
	          or (line.logical='<='  and line.calc_time<=cast(line.value as timestamp) )
	          or (line.logical='='  and line.calc_time=cast(line.value as timestamp) )
	      then 
		 flag=1;
	      else	       
	         flag=0; 
              end if;
             end if;
             end if;
        --非第一次
	else
        --库存
          if line.method='stock' then
	   if 
	    (line.logical='<'  and line.qty<cast(line.value as integer) )
	    or (line.logical='>'  and line.qty>cast(line.value as integer) )
	    or (line.logical='>='  and line.qty>=cast(line.value as integer) )
	    or (line.logical='<='  and line.qty<=cast(line.value as integer) )
	    or (line.logical='='  and line.qty=cast(line.value as integer) )
           then 
             if line.connect='or' 
             then
               flag=1;
             else   
             end if;                     
             else
             if line.connect='and' 
             then
             flag=0;
             else   
             end if;			 
	   end if;
           --时间
	   else 
	   if 
	   (line.logical='<'  and line.calc_time<cast(line.value as timestamp) )
	   or (line.logical='>'  and line.calc_time>cast(line.value as timestamp) )
	   or (line.logical='>='  and line.calc_time>=cast(line.value as timestamp) )
	   or (line.logical='<='  and line.calc_time<=cast(line.value as timestamp) )
	   or (line.logical='='  and line.calc_time=cast(line.value as timestamp) )
	   then 
	    if line.connect='or' 
	    then
	      flag=1;
            else   
	    end if;
	   else
	    if line.connect='and' 
            then
	      flag=0;
	    else   
	    end if;
	  end if;    
          end if;        
	end if;
end loop;
if flag=1 then 
    return bom_id;
  else  
    return 0; 
end if;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION check_bom(integer, integer)
  OWNER TO openerp;





--select * from mm_mrp_produce

--select * from mrp_log


