#查询产品销售出库数据
with md_sale as (

select md_no,sum(qty) as qty from (
with md as (
select a.stores_no as md_no,b.lot_stock_id as md_location from sale_shop a 
left join stock_warehouse b on b.id=a.warehouse_id
where a.yymode='DZD'
)
---单品出库
select md.md_no,sum(product_qty) as qty from stock_move a
inner join md on md.md_location=a.location_id
where a.state='done' and a.date>='2015-04-30 16:00:00' and a.date<'2015-05-31 16:00:00' and product_id=246 
and a.location_dest_id=9 group by md.md_no 
---单品退库
union all 
select md.md_no,-sum(product_qty)as qty  from stock_move a
inner join md on md.md_location=a.location_dest_id 
where a.state='done' and a.date>='2015-04-30 16:00:00' and a.date<'2015-05-31 16:00:00' and product_id=246 
and a.location_id=9 group by md.md_no 
) res  group by md_no order by md_no)

select md_no, sum(qty) from md_sale group by md_no

#查询产品销售明细
 with all_res as
(
---套装合计
with res as (select c.stores_no,b.id,b.default_code,sum(a.qty) as qty from pos_superorder_line a 
left join product_product b on b.id=a.product_id
left join sale_shop c on c.id=a.shop_id
left join pos_superorder d on d.id=a.superorder_id
where d.sale_date>='2015-04-30 16:00:00' 
and d.sale_date<'2015-05-31 16:00:00' and a.price!=0 and a.id=0---and c.state='approved'
and a.product_id  in (select product_id from mrp_bom where bom_id is null)
group by b.id,c.id )

select res.stores_no,c.default_code,sum(res.qty*a.product_qty ) as qty
from mrp_bom a
left join mrp_bom b on b.id =a.bom_id
left join product_product c on c.id =a.product_id
inner join res on b.product_id=res.id
group by c.id,res.stores_no---order by c.default_code
union all
--单品销售
select c.stores_no,b.default_code,sum(a.qty) as qty from pos_superorder_line a 
left join product_product b on b.id=a.product_id
left join sale_shop c on a.shop_id=c.id
inner join pos_superorder d on d.id=a.superorder_id
where d.sale_date>='2015-04-30 16:00:00' 
 and d.sale_date<'2015-05-31 16:00:00'  and a.price!=0 and a.id!=0   ---and c.state='approved'
 and a.product_id not in (select product_id from mrp_bom where bom_id is null)
group by b.id,c.stores_no) 

select all_res.stores_no,b.default_code,sum(all_res.qty),a.price_tzq,sum(a.price_tzq*all_res.qty) from standard_months_price a 
left join product_product b on a.product_id=b.id
inner join all_res on all_res.default_code=b.default_code
where a.months=32 and all_res.default_code='2.20050' 
group by b.id,a.id ,all_res.stores_no order by all_res.stores_no