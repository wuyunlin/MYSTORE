with a as 
(
select h.name as 期间,f.name as 简称,f.code as 简称代码,e.name as 渠道,e.code as 渠道编码,g.name as 凭证号,a.name as 费用摘要,d.name as 门店名称,c.id as 科目ID,c.name as 科目,a.amount as 金额 from account_voucher_other_line  a
left join account_voucher b on b.id=a.voucher_ids
left join account_account c on c.id=a.account_id
left join sale_shop d on d.project_id=a.analytic_account_id
left join res_partner e on e.id=b.partner_id
left join res_partner f on f.id=e.parent_id
left join account_move g on g.id=b.move_id  
left join account_period h on h.id=g.period_id
where d.name is not null and g.name is not null

),
x as 
(
select 科目ID,科目,count(*) as count from a group by 科目,科目ID 
order by 科目ID 
),

b as (
select 科目ID,科目,cast(row_number()over( order by count desc) as integer) as no  from x order by count desc),
c as (select 门店名称 from a group by 门店名称),
d as (
select 期间,简称,简称代码,凭证号,渠道,渠道编码,费用摘要,门店名称,科目,sum(金额) as 金额 
from a group by a.门店名称,a.科目,a.凭证号,简称,简称代码,渠道,渠道编码,费用摘要,期间),
e as (
select d.期间,d.简称,d.简称代码,d.渠道,d.渠道编码,d.凭证号,d.费用摘要,d.科目 ,d.门店名称,  b.no,
case when b.no=1 then d.金额  else 0 end as 商场管理其它费用,
case when b.no=2 then d.金额  else 0 end as 商场水电费,
case when b.no=3 then d.金额  else 0 end as 返利,
case when b.no=4 then d.金额  else 0 end as 促销员管理费,
case when b.no=5 then d.金额  else 0 end as 节庆费,
case when b.no=6 then d.金额  else 0 end as 商场电子称费,
case when b.no=7 then d.金额  else 0 end as 管理罚款,
case when b.no=8 then d.金额  else 0 end as 进场费,
case when b.no=9 then d.金额  else 0 end as 销售扣点,
case when b.no=10 then d.金额  else 0 end as 手续费,
case when b.no=11 then d.金额  else 0 end as 保底罚款,
case when b.no=12 then d.金额  else 0 end as 卖场押金、质保金,
case when b.no=13 then d.金额  else 0 end as 商场销售,
case when b.no=14 then d.金额  else 0 end as 租金,
case when b.no=15 then d.金额  else 0 end as 专卖店房租,
case when b.no=16 then d.金额  else 0 end as 租赁费,
case when b.no=17 then d.金额  else 0 end as 推广费,
case when b.no=18 then d.金额  else 0 end as 水电费,
case when b.no=19 then d.金额  else 0 end as 公司福利,
case when b.no=20 then d.金额  else 0 end as 专卖店物管费,
case when b.no=21 then d.金额  else 0 end as 业务招待费,
case when b.no=22 then d.金额  else 0 end as 水电费2,
case when b.no=23 then d.金额  else 0 end as 工资,
case when b.no=24 then d.金额  else 0 end as 顾问咨询费专柜
from c 
left join d on c.门店名称=d.门店名称 
left join b on d.科目=b.科目 order by d.期间 desc,d.凭证号 desc)



select 期间,简称,简称代码,渠道,渠道编码,凭证号,门店名称,
sum(商场管理其它费用) as 商场管理其它费用,
sum(商场水电费) as 商场水电费,
sum(返利) as 返利,
sum(促销员管理费) as 促销员管理费,
sum(节庆费) as 节庆费,
sum(商场电子称费) as 商场电子称费,
sum(管理罚款) as 管理罚款,
 
sum(进场费) as 进场费,
sum(销售扣点) as 销售扣点,

sum(手续费) as 手续费,
sum(保底罚款) as 保底罚款,
sum(卖场押金、质保金) as 卖场押金、质保金,

sum(商场销售) as 商场销售,
sum(租金) as 租金,
sum(专卖店房租) as 专卖店房租,
sum(租赁费) as 租赁费,
sum(推广费) as 推广费,
sum(水电费2) as 水电费2,
sum(工资) as 工资,
sum(顾问咨询费专柜) as 顾问咨询费专柜
from e 
group by 期间,简称,简称代码,渠道,渠道编码,凭证号,门店名称 order by 期间 desc ,凭证号


