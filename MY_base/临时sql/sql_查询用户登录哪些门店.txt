with RECURSIVE cte as 
 ( 
 select a.id from wgmf_sale_organization a where id=(select sale_organization from res_users where login='76900522')
  union all  
  select b.id  from wgmf_sale_organization b inner join cte c on c.id = b.parent_id 
  )


  select stores_no from sale_shop where sale_organization =any(select id from cte)


#菜单
with a as (
  select a.id,a.name as name1 ,b.name as name2,c.name as name3 ,d.name as name4 from ir_ui_menu a
  left join ir_ui_menu b on b.id=a.parent_id
  left join ir_ui_menu c on c.id=b.parent_id
  left join ir_ui_menu d on d.id=c.parent_id
  left join ir_ui_menu e on e.id=d.parent_id
  where a.id in (select res_id from ir_values)
  and (c.id in (322,364,254,606,1266,1391,1419,1620) or d.id in (322,364,254,606,1266,1391,1419,1620)) ),
   b as(
  select id,
  case when name4 is not null then name4 else name3 end as name4,
  case when name4 is not null then name3 else name2 end as name3,
  case when name4 is not null then name2 else name1 end as name2,
  case when name4 is not null then name1 else null end as name1
  from a)
  select replace(replace(replace(name4,'Manufacturing','生产'),'Warehouse','仓库'),'Purchases','采购') as 一级菜单,
  name3 as 二级菜单,
  name2 as 三级菜单,
  name1 as 四级菜单,
  replace(replace(replace(name4,'Manufacturing','生产'),'Warehouse','仓库'),'Purchases','采购')||'/'||name3||'/'||name2||'/'||coalesce(name1,'')
  ,id  from b where 
  name3 not in 
  ('Configuration','Inventory Control','Products','Planning','Manufacturing','Incoming Products','Invoice Control',
  'Receive/Deliver By Orders','Receive/Deliver Products','Traceability','Schedulers','一般设置')
  order by name4,name3,name2,name1,id


create view shop
with RECURSIVE cte as 
 ( 
 select a.id from wgmf_sale_organization a where id=382
  union all  
  select b.id  from wgmf_sale_organization b inner join cte c on c.id = b.parent_id 
  )
  select b.lot_stock_id from sale_shop  a
  left join stock_warehouse b on b.id=a.warehouse_id
  where sale_organization =any(select id from cte) 









