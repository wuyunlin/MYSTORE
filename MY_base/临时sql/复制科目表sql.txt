#加字段
alter table account_account add colunmn oldid integer;
alter table account_account add colunmn oldparent_id integer;

#插入记录
insert into account_account (create_uid,create_date,write_date,write_uid,code,reconcile,currency_id,user_type,active,name,level,
company_id,shortcut,note,parent_id,currency_mode,type,wg_fzpz,wg_btywhb,centralized,wg_selection,oldid,oldparent_id) 
select create_uid,create_date,write_date,write_uid,code,reconcile,currency_id,user_type,active,name,level,
4,shortcut,note,parent_id,currency_mode,type,wg_fzpz,wg_btywhb,centralized,wg_selection,id,parent_id from account_account
#更新上下级关系
update account_account set parent_id=b.id
from account_account b 
where account_account.company_id=4 and b.company_id=4 and b.oldid=account_account.oldparent_id

#删除字段

alter table account_account drop colunmn oldid;
alter table account_account drop colunmn oldparent_id;