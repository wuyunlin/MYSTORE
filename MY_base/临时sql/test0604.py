# -*- encoding: utf-8 -*-
import psycopg2
import psycopg2.extras
import os
import time
import datetime
import sys
default_encoding = 'utf-8'
import logging
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)
    
dbname = 'WGMF_Pro'
host='192.168.5.17'
CONNS = []
CRS = []
for i in range(1000):
    try:
        conn = psycopg2.connect(host=host, port=5432, user='openerp', password='postgres', database=dbname)
        CONNS.append(conn)
        cr = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cr.execute("""select password from res_users where id=1""")
        CRS.append(cr)
        print 'CONN %s: %s' % (i, cr.fetchall() )
        time.sleep(0.1)
    except Exception,e:  
        print Exception,":",e
    finally:
        pass
time.sleep(600)        
for c in CRS:
    c.close()
for con in CONNS:
    con.close()
