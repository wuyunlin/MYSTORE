with data as (
                with shop_prd as (
                ---门店组织信息
                select sp.name as name, sp.stores_no as num, a.name as zg,b.name as bsc,c.name as dq,sw.lot_stock_id as location
                from sale_shop as sp
                left join stock_warehouse as sw on sw.id=sp.warehouse_id
                left join wgmf_sale_organization a on sp.sale_organization=a.id
                left join wgmf_sale_organization b on a.parent_id=b.id
                left join wgmf_sale_organization c on b.parent_id=c.id
                where  1=1  and sp.yymode in ('DZD','ZMD')

                and sp.stores_no not in ('001','99', '99999')
                ),
                ---期初move汇总
                qcmv as (
                select pp.id as product_id,pt.name,pp.default_code, st.location_id, st.location_dest_id,
                case when uom2.factor!=uom1.factor then
                sum(round((st.product_qty*uom2.factor/uom1.factor),2))
                else sum(st.product_qty) end
                  as qty,uom2.name as uom
                from
                stock_move st
                left join product_uom uom1 on uom1.id=st.product_uom
                left join product_product pp on pp.id=st.product_id
                left join product_template pt on pp.product_tmpl_id=pt.id
                left join product_uom uom2 on uom2.id=pt.uom_id

                where 1=1 and pt.sale_ok='t' and st.date <'2015-11-30 16:00:00' and st.state='done'
                group by pp.id,pt.id,st.location_id,st.location_dest_id,uom1.id,uom2.id),
                ---期末move汇总

               qmmv as (
                select pp.id as product_id,pt.name,pp.default_code, st.location_id, st.location_dest_id,
                case when uom2.factor!=uom1.factor then
                sum(round((st.product_qty*uom2.factor/uom1.factor),2))
                else sum(st.product_qty) end
                  as qty,
                uom2.name as uom
                from
                stock_move st
                left join product_uom uom1 on uom1.id=st.product_uom
                left join product_product pp on pp.id=st.product_id
                left join product_template pt on pp.product_tmpl_id=pt.id
                left join product_uom uom2 on uom2.id=pt.uom_id

                where 1=1 and pt.sale_ok='t' and st.date <'2015-12-31 16:00:00' and st.state='done'
                group by pp.id,pt.id,st.location_id,st.location_dest_id,uom1.id,uom2.id),
                ----期间move 汇总

                qjmv as (
                select pp.id as product_id, pt.name,pp.default_code,st.location_id, st.location_dest_id,
                case when uom2.factor!=uom1.factor then
                sum(round((st.product_qty*uom2.factor/uom1.factor),2))
                else sum(st.product_qty) end
                  as qty,
                uom2.name as uom
                from
                stock_move st
                left join product_uom uom1 on uom1.id=st.product_uom
                left join product_product pp on pp.id=st.product_id
                left join product_template pt on pp.product_tmpl_id=pt.id
                left join product_uom uom2 on uom2.id=pt.uom_id
                where 1=1 and pt.sale_ok='t' and st.date >='2015-11-30 16:00:00' and st.date <'2015-12-31 16:00:00' and st.state='done'

                group by pp.id,pt.id,st.location_id,st.location_dest_id,uom1.id,uom2.id)

               ----开始查询
                    ---期初负数
                 select a.dq,a.bsc,a.zg,a.name as name ,a.num,a.location,b.product_id,b.name as product_name,b.default_code,b.uom,-sum(b.qty) as qc_qty,0 as pr_qty,0 as fc_qty,0 as qm_qty
                 from shop_prd   a
                 left join qcmv  b on b.location_id=a.location where b.product_id is not null
                 group by a.dq,a.bsc,a.zg,a.name,a.location,a.num,b.product_id,b.name,b.default_code,b.uom
                 union all
                   ---期初正数
                 select a.dq,a.bsc,a.zg,a.name,a.num,a.location,b.product_id,b.name,b.default_code,b.uom,sum(b.qty) as qc_qty,0 as pr_qty, 0 as fc_qty,0 as qm_qty
                 from shop_prd   a
                 left join qcmv  b on b.location_dest_id=a.location where b.product_id is not null
                 group by a.dq,a.bsc,a.zg,a.name,a.location,a.num,b.product_id,b.name,b.default_code,b.uom
                 union all
                    ---期末负数
                 select a.dq,a.bsc,a.zg,a.name,a.num,a.location,b.product_id,b.name,b.default_code,b.uom,0 as qc_qty,0 as pr_qty,0 as fc_qty,-sum(b.qty) as qm_qty
                 from shop_prd   a
                 left join qmmv  b on b.location_id=a.location where b.product_id is not null
                 group by a.dq,a.bsc,a.zg,a.name,a.location,a.num,b.product_id,b.name,b.default_code,b.uom
                 union all
                   ---期末正数
                 select a.dq,a.bsc,a.zg,a.name ,a.num,a.location,b.product_id,b.name,b.default_code,b.uom,0 as qc_qty,0 as pr_qty, 0 as fc_qty,sum(b.qty) as qm_qty
                 from shop_prd   a
                 left join qmmv  b on b.location_dest_id=a.location where b.product_id is not null
                 group by a.dq,a.bsc,a.zg,a.name,a.location,b.product_id,b.name,b.default_code,a.num,b.uom
                 union all

                   ---本期发出负数
                 select a.dq,a.bsc,a.zg,a.name,a.num,a.location,b.product_id,b.name,b.default_code,b.uom,0 as qc_qty,0 as pr_qty, -sum(b.qty) as fc_qty,0 as qm_qty
                 from shop_prd   a
                 left join qjmv  b on b.location_dest_id=a.location
                 where b.location_id  in (76,77,78,79,80,81,82,84,5,7,9,16278,16839 )
                 and b.product_id is not null
                 group by a.dq,a.bsc,a.zg,a.name,a.num,a.location,b.product_id,b.name,b.default_code,b.uom
                 union all
                    ---本期发出正数
                 select a.dq,a.bsc,a.zg,a.name,a.num,a.location,b.product_id,b.name,b.default_code,b.uom,0 as qc_qty,0 as pr_qty, sum(b.qty) as fc_qty,0 as qm_qty
                 from shop_prd   a
                 left join qjmv  b on b.location_id=a.location
                 where b.location_dest_id  in (76,77,78,79,80,81,82,84,5,7,9,16278,16839 )
                 and b.product_id is not null
                  group by a.dq,a.bsc,a.zg, a.name,a.num,a.location,b.product_id,b.name,b.default_code,b.uom
                  union all
                  ---本期配入负数
                 select a.dq,a.bsc,a.zg,a.name,a.num,a.location,b.product_id,b.name,b.default_code,b.uom,0 as qc_qty,-sum(b.qty) as pr_qty, 0 as fc_qty,0 as qm_qty
                 from shop_prd   a
                 left join qjmv  b on b.location_id=a.location
                 where b.location_dest_id  =89
                 and b.product_id is not null
                 group by a.dq,a.bsc,a.zg,a.name,a.num,a.location,b.product_id,b.name,b.default_code,b.uom
                 union all

                 ---本期配入正数
                 select a.dq,a.bsc,a.zg,a.name,a.num,a.location,b.product_id,b.name,b.default_code,b.uom,0 as qc_qty,sum(b.qty) as pr_qty, 0 as fc_qty,0 as qm_qty
                 from shop_prd   a
                 left join qjmv  b on b.location_dest_id=a.location
                 where b.location_id  =89
                 and b.product_id is not null  group by a.dq,a.bsc,a.zg,a.name,a.num,a.location,b.product_id,b.name,b.default_code ,b.uom

                 ),
                 ----月末价格信息
                 period_price as (
                    with aa as (
                     select product_id,coalesce(price,0) as price_last,0 as price_in,0 as price_tzq,0 as price
                     from standard_months_price
                     where months=38
                     union all
                     select product_id,0 as price_last, price_in, price_tzq, price
                     from standard_months_price
                     where months=39)
                     select product_id,sum(price_last) as price_last,sum(price_in) as price_in,
                     sum(price_tzq) as price_tzq, sum(price) as price
                     from aa group by product_id)select data.product_name,data.default_code,data.uom,
                 sum(data.qc_qty) as qc_qty,b.price_last as price_qc,sum(data.qc_qty*b.price_last) as qc_amount,
                 sum(data.pr_qty) as pr_qty,b.price_in as price_rk,sum(data.pr_qty*b.price_in) as pr_amount,
                 sum(data.fc_qty) as fc_qty,b.price_tzq as price_fc,sum(data.fc_qty*b.price_tzq) as fc_amount,
                 sum(data.qm_qty) as qm_qty,b.price as price_qm,sum(data.qm_qty*b.price) as qm_amount,
                 1,1,'2016-01-22 08:48:36','2016-01-22 08:48:36',984
                 from data
                 left join period_price b on b.product_id=data.product_id
                 group by
                 data.product_id,data.product_name,data.default_code ,data.uom
                          ,b.price_last,b.price_in,b.price_tzq,b.price
                 order by data.default_code