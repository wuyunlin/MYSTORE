# -*- coding: utf-8 -*- #
import sys
import time
from datetime import datetime
from operator import itemgetter

from lxml import etree

from openerp import netsvc
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp import tools

class account_account(osv.osv):
    _inherit = "account.account"
    def _compute2(self, cr, uid, ids, field_names, filter, context=None,):
        mapping = {
            'balance': "COALESCE(SUM(l.debit),0) - COALESCE(SUM(l.credit), 0) as balance",
            'debit': "COALESCE(SUM(l.debit), 0) as debit",
            'credit': "COALESCE(SUM(l.credit), 0) as credit",
        }
        children_and_consolidated = self._get_children_and_consol(cr, uid, ids, context=context)
        accounts = {}
        res = {}
        #print '====================children_and_consolidated==========%s'%children_and_consolidated
        null_result = dict((fn, 0.0) for fn in field_names)
        #print '==============context==================%s'%context.get('state_sql')
        #print '==============mapping==================%s'%mapping.values()
        if children_and_consolidated:
            request = ("SELECT l.account_id as id, aa.code as code,aa.name as name," +\
                       ', '.join(mapping.values()) +
                       " FROM  account_move m inner join account_move_line l on m.id=l.move_id  left join account_account aa on aa.id=l.account_id " \
                       +" WHERE l.account_id IN %s" \
                       +filter+context.get('state_sql')+context.get('company_sql')+context.get('zmd_sql')+
                       " GROUP BY l.account_id,aa.code,aa.name")
            params = (tuple(children_and_consolidated),)
            cr.execute(request, params)
            #print '*********************========cr.dictfetchall()=========***********************%s'%cr.dictfetchall()
            for row in cr.dictfetchall():
                accounts[row['id']] = row
            children_and_consolidated.reverse()
            brs = list(self.browse(cr, uid, children_and_consolidated, context=context))
            sums = {}
            while brs:
                current = brs.pop(0)
                for fn in field_names:
                    if fn in ['name','code']:continue
                    sums.setdefault(current.id, {})[fn] = accounts.get(current.id, {}).get(fn, 0.0)
                    for child in current.child_id:
                        sums[current.id][fn] += sums[child.id][fn]
            for id in ids:
                res[id] = sums.get(id, null_result)
                if accounts.get(id):
                    res[id]['name']=accounts.get(id)['name']
                    res[id]['code']=accounts.get(id)['code']
                else:
                    res[id]['name']=self.browse(cr,uid,id).name
                    res[id]['code']=self.browse(cr,uid,id).code
        else:
            for id in ids:
                res[id] = null_result
                if accounts.get(id):
                    res[id]['name']=self.pool.get(id)['name']
                    res[id]['code']=accounts.get(id)['code']
                else:
                    res[id]['name']=self.browse(cr,uid,id).name
                    res[id]['code']=self.browse(cr,uid,id).code
        return res
account_account()

class account_analytic_account(osv.osv):
    _inherit = "account.analytic.account"
    _columns = {
        'default_code': fields.char(u'内部编码',size=64),
        'create_uid': fields.many2one('res.users',u'制单人',readonly=True),
        }
account_analytic_account()

class account_move(osv.osv):
    _inherit = "account.move"
    _columns = {
        'create_uid': fields.many2one('res.users',u'制单人',readonly=True),
        }
account_move()

class account_move_line(osv.osv):
    _inherit = "account.move.line"
    _columns = {
        'balance': fields.related('move_id','balance',type='float',string=u'余额',),
        }
        
    def _prepare_analytic_line(self, cr, uid, obj_line, context=None):
        """
        Prepare the values given at the create() of account.analytic.line upon the validation of a journal item having
        an analytic account. This method is intended to be extended in other modules.

        :param obj_line: browse record of the account.move.line that triggered the analytic line creation
        """
        res = super(account_move_line, self)._prepare_analytic_line(cr,uid, obj_line,context=context)
        res=res or {}
        res.update({'period_id':obj_line.period_id and obj_line.period_id.id or False})
        return res
account_move_line()


class account_analytic_line(osv.osv):
    _inherit = "account.analytic.line"
    _columns = {
        'period_id': fields.many2one('account.period', u'会计期间(凭证明细)'),
        }
account_analytic_line()
