# -*- coding: utf-8 -*-

import time
from datetime import datetime

from openerp.osv import fields, osv
from openerp import tools
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class account_analytic_account(osv.osv):
    _inherit = 'account.analytic.account'

    def _debit_credit_bal_qtty(self, cr, uid, ids, fields, arg, context=None):
        res = {}
        if context is None:
            context = {}
        child_ids = tuple(self.search(cr, uid, [('parent_id', 'child_of', ids)]))
        for i in child_ids:
            res[i] =  {}
            for n in fields:
                res[i][n] = 0.0

        if not child_ids:
            return res
        if not context.get('sql_where') :
            context['sql_from']=' '
            context['sql_where']=' '
        where_date = ''
        where_clause_args = [tuple(child_ids)]
        if context.get('from_date', False):
            where_date += " AND l.date >= %s"
            where_clause_args  += [context['from_date']]
        if context.get('to_date', False):
            where_date += " AND l.date <= %s"
            where_clause_args += [context['to_date']]
        cr.execute("""
              SELECT a.id,
                     sum(
                         CASE WHEN l.amount > 0
                         THEN l.amount
                         ELSE 0.0
                         END
                          ) as debit,
                     sum(
                         CASE WHEN l.amount < 0
                         THEN -l.amount
                         ELSE 0.0
                         END
                          ) as credit,
                     COALESCE(SUM(l.amount),0) AS balance,
                     COALESCE(SUM(l.unit_amount),0) AS quantity
              FROM account_analytic_account a
                  LEFT JOIN account_analytic_line l ON (a.id = l.account_id)"""+context.get('sql_from')+ """
              WHERE a.id IN %s
              """ + where_date + context.get('sql_where')+"""
              GROUP BY a.id""", where_clause_args)
        for row in cr.dictfetchall():
            res[row['id']] = {}
            for field in fields:
                res[row['id']][field] = row[field]
        return self._compute_level_tree(cr, uid, ids, child_ids, res, fields, context)

    _columns={
        'balance': fields.function(_debit_credit_bal_qtty, type='float', string='Balance', multi='debit_credit_bal_qtty', digits_compute=dp.get_precision('Account')),
        'debit': fields.function(_debit_credit_bal_qtty, type='float', string='Debit', multi='debit_credit_bal_qtty', digits_compute=dp.get_precision('Account')),
        'credit': fields.function(_debit_credit_bal_qtty, type='float', string='Credit', multi='debit_credit_bal_qtty', digits_compute=dp.get_precision('Account')),
        'quantity': fields.function(_debit_credit_bal_qtty, type='float', string='Quantity', multi='debit_credit_bal_qtty'),
        }

account_analytic_account()

class account_analytic_chart(osv.osv_memory):
    _inherit ='account.analytic.chart'
    _columns={
        'checked': fields.selection([('to_check',u'已审核'),('un_check',u'未审核')],u'审核状态'),
        'posted': fields.selection([('to_post',u'已过账'),('un_post',u'未过账')],u'过账状态'),
    }
    
    def analytic_account_chart_open_window(self, cr, uid, ids, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        result_context = {}
        if context is None:
            context = {}
        result = mod_obj.get_object_reference(cr, uid, 'account', 'action_account_analytic_account_tree2')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        data = self.read(cr, uid, ids, [])[0]
        if data['from_date']:
            result_context.update({'from_date': data['from_date']})
        if data['to_date']:
            result_context.update({'to_date': data['to_date']})
        sql_where=' '
        sql_from=' '
        if data['checked'] == 'to_check':
            sql_where=' '.join([sql_where,"and c.to_check = 'true'"])
        elif data['checked'] == 'un_check' :
            sql_where=' '.join([sql_where,"and c.to_check = 'false'"])
        else : pass
        if data['posted'] == 'to_post' :
            sql_where=' '.join([sql_where,"and c.state = 'posted'"])
        elif data['posted'] == 'un_post' :
            sql_where=' '.join([sql_where,"and c.state <> 'posted'"])
        else : pass
        if sql_where !=' ':
            sql_from=' '.join([sql_from,'inner join account_move_line b on l.move_id =b.id inner join account_move c on b.move_id=c.id'])
        result_context.update({'sql_from':sql_from, 'sql_where':sql_where,})
        result['context'] = str(result_context)
        return result
        
account_analytic_chart()

