# -*- coding: utf-8 -*- #

import time
from osv import osv, fields
import logging
import netsvc
import uuid
from openerp.addons.web.controllers.main import ExportData2Excel as _Export
logger = logging.getLogger(__name__)
class wgaccount_balance_report(osv.osv_memory):
    _name = 'wgaccount.balance.report'
    def _get_fiscalyear(self, cr, uid, context=None):
        """Return default Fiscalyear value"""
        fiscalyear_id=self.pool.get('account.fiscalyear').find(cr, uid, context=context)
        context=context or {}
        if context.get('company_id'):
            sql="""select id from account_fiscalyear where date_start<=now() AND date_stOP>=NOW() AND company_id=%s LIMIT 1"""%context.get('company_id')
            cr.execute(sql)
            fet=cr.fetchall()
            if not fet:
                raise osv.except_osv(_('警告!'), _('未找到当前公司的会计年度'))
            fiscalyear_id=fet[0][0]
        cr.execute('''
            SELECT * FROM (SELECT p.id
                           FROM account_period p
                           LEFT JOIN account_fiscalyear f ON (p.fiscalyear_id = f.id)
                           WHERE f.id = %s
                           AND COALESCE(p.special, FALSE) = FALSE
                           ORDER BY p.date_start ASC
                           LIMIT 1) AS period_start
            UNION ALL
            SELECT * FROM (SELECT p.id
                           FROM account_period p
                           LEFT JOIN account_fiscalyear f ON (p.fiscalyear_id = f.id)
                           WHERE f.id = %s
                           AND p.date_start < NOW()
                           AND COALESCE(p.special, FALSE) = FALSE
                           ORDER BY p.date_stop DESC
                           LIMIT 1) AS period_stop''', (fiscalyear_id, fiscalyear_id))
        periods = [i[0] for i in cr.fetchall()]
        if periods:
            start_period = end_period = periods[0]
            if len(periods) > 1:
                end_period = periods[1]
        return fiscalyear_id, start_period, end_period   
        
    def _get_company_id(self, cr, uid, context=None):
        cr.execute("""select company_id from res_users where id=%s"""%uid)
        fet=cr.fetchall()
        return  fet and fet[0][0] or False
        
    _columns = {
        'company_id': fields.many2one('res.company',u'公司',required=True,help=u'所属公司'),
        'is_zmd': fields.boolean(u'是否专卖店'),
        'account_id': fields.many2one('account.account',u'科目',help=u'不选即为查询所有科目'),
        'date_from': fields.date(u'开始日期'),
        'date_to': fields.date(u'截止日期'),
        'fiscalyear': fields.many2one('account.fiscalyear',u'本年年度',),
        'period_from': fields.many2one('account.period',u'开始会计期间', required = True,),
        'period_to': fields.many2one('account.period',u'截止会计期间',required = True,),
        'periods': fields.many2many('account.period','account_periods_report','report_id','period_id',u'查询会计期间',),
        'journal_ids': fields.many2many('account.journal','account_journal_report','report_id','journal_id',u'查询凭证字',),
        'checked': fields.selection([('to_check',u'已审核'),('un_check',u'未审核')],u'审核状态'),
        'posted': fields.selection([('to_post',u'已过账'),('un_post',u'未过账')],u'过账状态'),
    }
    def default_get(self, cr, uid, fields, context=None):
        context = context or {}
        res = super(wgaccount_balance_report, self).default_get(cr, uid, fields, context=context)
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        value = {
            'company_id': user.company_id.id,
            'fiscalyear': self._get_fiscalyear(cr, uid, context={})[0],
        }
        res.update(value)
        return res
    def csv_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        datas={}
        wiz_instance=self.browse(cr,uid,ids[0])
        account_obj=self.pool.get('account.account')
        account_id=wiz_instance.account_id and wiz_instance.account_id.id or False
        period_from=wiz_instance.period_from and wiz_instance.period_from.id or False
        period_to=wiz_instance.period_to and wiz_instance.period_to.id or False
        
        company_id=wiz_instance.company_id and wiz_instance.company_id.id or False
        fiscalyear=wiz_instance.fiscalyear and wiz_instance.fiscalyear.id or False
        is_zmd=wiz_instance.is_zmd or False
        state_sql=' '
        if wiz_instance.checked == 'to_check':
            state_sql=' '.join([state_sql,"and b.to_check = 'true'"])
        elif wiz_instance.checked == 'un_check' :
            state_sql=' '.join([state_sql,"and b.to_check = 'false'"])
        else : pass
        if wiz_instance.posted == 'to_post' :
            state_sql=' '.join([state_sql,"and b.state = 'posted'"])
        elif wiz_instance.posted == 'un_post' :
            state_sql=' '.join([state_sql,"and b.state <> 'posted'"])
        else : pass
        account_sql ='1=1'
        if  account_id:
            account_ids=account_obj.search(cr,uid,[('parent_id','child_of',account_id)])
            account_sql='a.id in %s'%(tuple(account_ids+[0]),)
        zmd_sql=' 1=1'
        if is_zmd and company_id==1:
             zmd_sql="""  b.move_id  in (select id from account_move where wgmf_type in (1,7))"""
        if company_id==1 and (not   is_zmd      ):
             zmd_sql="""b.move_id not in (select id from account_move where wgmf_type in (1,7))"""
        dic={'company_id':company_id,'fiscalyear':fiscalyear,'period_from':period_from,'period_to':period_to,'zmd_sql':zmd_sql,
                'account_sql':account_sql,'state_sql':state_sql,'uid':uid,'line_id':ids[0]}
        if context.get('print'):
            sql=""" with account_type as (
                                     select id,code,'zc' as type from account_account where code like '1%%' or code='0111'
                                     union all
                                     select id,code,'fz' from account_account where code like '2%%' or code='0112'
                                      union all
                                     select id,code,'fy' from account_account where code like '5%%' or code='0121'
                                      union all
                                     select id,code,'sr' from account_account where code like '6%%' or code='0122'),
                                    account_base as
                                    (select id,parent_id,code,name,type from account_account 
                                    where company_id=%(company_id)s and type<>'view' and active='t' order by code
                                    ),
                                    account_view as 
                                    (select id,parent_id,code,name,type from account_account 
                                    where company_id=%(company_id)s and type='view' and active='t' order by code
                                    ),
                                    account_relation as 
                                    (
                                    select a.id as id1,
                                    b.id as id2,
                                    c.id as id3,
                                    d.id as id4,
                                    e.id as id5,
                                    f.id as id6,
                                    g.id as id7
                                    from account_base a
                                    left join  account_view b on b.id=a.parent_id
                                    left join account_view c on c.id=b.parent_id
                                    left join account_view d on d.id=c.parent_id
                                    left join account_view e on e.id=d.parent_id
                                    left join account_view f on f.id=e.parent_id
                                    left join account_view g on g.id=f.parent_id
                                    ),
                                    base_view as (
                                    select base,view from 
                                    (
                                    select id1 as base,id2 as view from account_relation
                                    union all 
                                    select id1 as base,id3 as view from account_relation
                                    union all 
                                    select id1 as base,id4 as view from account_relation
                                    union all 
                                    select id1 as base,id5 as view from account_relation
                                    union all 
                                    select id1 as base,id6 as view from account_relation
                                    union all 
                                    select id1 as base,id7 as view from account_relation
                                    ) res
                                    where res.view is not null order by view),
                                    data_base_qc as
                                    (select a.id as account_id,a.code,a.name,coalesce(sum(debit),0) as debit,coalesce(sum(credit),0) as credit ,coalesce(sum(debit),0)-coalesce(sum(credit),0) as debit_credit from account_base a 
                                    left join account_move_line b on b.account_id=a.id and b.period_id<%(period_from)s and company_id=%(company_id)s and
                                     %(zmd_sql)s
                                     group by a.id,a.code,a.name order by a.code),
                                    data_base_qj as
                                    (select a.id as account_id,a.code,a.name,coalesce(sum(debit),0) as debit,coalesce(sum(credit),0) as credit ,coalesce(sum(debit),0)-coalesce(sum(credit),0) as debit_credit from account_base a 
                                    left join account_move_line b on b.account_id=a.id and company_id=%(company_id)s and  b.period_id>=%(period_from)s  and b.period_id<=%(period_to)s 
                                    and %(zmd_sql)s
                                     group by a.id,a.code,a.name order by a.code),
                                    data_base_year as
                                    (select a.id as account_id,a.code,a.name,coalesce(sum(debit),0) as debit,coalesce(sum(credit),0) as credit ,coalesce(sum(debit),0)-coalesce(sum(credit),0) as debit_credit from account_base a 
                                    left join account_move_line b on b.account_id=a.id and company_id=%(company_id)s and
                                     b.period_id in (select id from account_period where fiscalyear_id=%(fiscalyear)s)  and %(zmd_sql)s
                                     group by a.id,a.code,a.name order by a.code),
                                    xxx as 
                                     (select c.id,c.code as code,c.name,coalesce(sum(qc.debit),0) as qc_debit ,coalesce(sum(qc.credit),0) as qc_credit,sum(qc.debit_credit) as qc_debit_credit,
                                     coalesce(sum(qj.debit),0) as qj_debit ,coalesce(sum(qj.credit),0) as qj_credit,sum(qj.debit_credit) as qj_debit_credit,
                                     coalesce(sum(year.debit),0) as year_debit ,coalesce(sum(year.credit),0) as year_credit,sum(year.debit_credit) as year_debit_credit
                                      from base_view
                                      left join data_base_qc qc on qc.account_id=base_view.base
                                      left join data_base_qj qj on qj.account_id=base_view.base
                                      left join data_base_year year on year.account_id=base_view.base
                                      left join account_account c on c.id=base_view.view
                                      group by c.id 
                                      union all
                                     select cc.id,cc.code,cc.name,
                                     coalesce(sum(a.debit),0) as qc_debit ,coalesce(sum(a.credit),0) as qc_credit,sum(a.debit_credit) as qc_debit_credit,
                                     coalesce(sum(b.debit),0) as qj_debit ,coalesce(sum(b.credit),0) as qj_credit,sum(b.debit_credit) as qj_debit_credit,
                                     coalesce(sum(c.debit),0) as year_debit ,coalesce(sum(c.credit),0) as qj_credit,sum(c.debit_credit) as year_debit_credit
                                     from  data_base_qc a
                                     left join data_base_qj b on a.account_id=b.account_id
                                     left join data_base_year c on c.account_id=a.account_id
                                     left  join account_account cc on cc.id=a.account_id and cc.id=b.account_id
                                     group by cc.id )
                                    --select * from xxx order by code
                                    select a.code,name, 
                                    ---期初
                                    case when b.type in ('zc','fy')--借方
                                    then qc_debit_credit
                                    when b.type in ('sr','fz')--贷方
                                    then 0
                                    else qc_debit
                                    end as qc_debit,
                                    case when b.type in ('zc','fy')--借方
                                    then 0
                                    when b.type in ('sr','fz')--贷方
                                    then -qc_debit_credit
                                    else qc_credit
                                    end as qc_credit,
                                    ---期间
                                    qj_debit,
                                    qj_credit,
                                    ---本年
                                    year_debit,
                                    year_credit,
                                    ---期末
                                    case when b.type in ('zc','fy')--借方
                                    then qc_debit_credit+qj_debit_credit
                                    when b.type in ('sr','fz')--贷方
                                    then 0
                                    else qc_debit+qj_debit
                                    end as qm_debit,
                                    case when b.type in ('zc','fy')--借方
                                    then 0
                                    when b.type in ('sr','fz')--贷方
                                    then -qc_debit_credit-qj_debit_credit
                                    else qc_credit+qj_debit
                                    end as qm_credit
                                     from xxx a
                                     left join account_type b on b.id=a.id
                                     where %(account_sql)s
                                     order by a.code"""%dic
            _name = str(uuid.uuid1())
            _field=['编码','名称','期初借方','期初贷方','本期借方','本期贷方','本年借方','本年贷方','期末借方','期末贷方']
            _file_name = u'科目余额表'
            _Export.do(self, cr, uid, _name, _file_name, sql, _field)
            return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name}
        sql=""" delete from wgaccount_balance_report_line where create_uid=%(uid)s or line_id=%(line_id)s;
                   insert into wgaccount_balance_report_line(code,name,qc_debit,qc_credit,qj_debit,qj_credit,year_debit,year_credit,qm_debit,qm_credit,create_uid,line_id)
                    with account_type as (
                                 select id,code,'zc' as type from account_account where code like '1%%' or code='0111'
                                 union all
                                 select id,code,'fz' from account_account where code like '2%%' or code='0112'
                                  union all
                                 select id,code,'fy' from account_account where code like '5%%' or code='0121'
                                  union all
                                 select id,code,'sr' from account_account where code like '6%%' or code='0122'),
                                account_base as
                                (select id,parent_id,code,name,type from account_account 
                                where company_id=%(company_id)s and type<>'view' and active='t' order by code
                                ),
                                account_view as 
                                (select id,parent_id,code,name,type from account_account 
                                where company_id=%(company_id)s and type='view' and active='t' order by code
                                ),
                                account_relation as 
                                (
                                select a.id as id1,
                                b.id as id2,
                                c.id as id3,
                                d.id as id4,
                                e.id as id5,
                                f.id as id6,
                                g.id as id7
                                from account_base a
                                left join  account_view b on b.id=a.parent_id
                                left join account_view c on c.id=b.parent_id
                                left join account_view d on d.id=c.parent_id
                                left join account_view e on e.id=d.parent_id
                                left join account_view f on f.id=e.parent_id
                                left join account_view g on g.id=f.parent_id
                                ),
                                base_view as (
                                select base,view from 
                                (
                                select id1 as base,id2 as view from account_relation
                                union all 
                                select id1 as base,id3 as view from account_relation
                                union all 
                                select id1 as base,id4 as view from account_relation
                                union all 
                                select id1 as base,id5 as view from account_relation
                                union all 
                                select id1 as base,id6 as view from account_relation
                                union all 
                                select id1 as base,id7 as view from account_relation
                                ) res
                                where res.view is not null order by view),
                                data_base_qc as
                                (select a.id as account_id,a.code,a.name,coalesce(sum(debit),0) as debit,coalesce(sum(credit),0) as credit ,coalesce(sum(debit),0)-coalesce(sum(credit),0) as debit_credit from account_base a 
                                left join account_move_line b on b.account_id=a.id and b.period_id<%(period_from)s and company_id=%(company_id)s and
                                 %(zmd_sql)s
                                 group by a.id,a.code,a.name order by a.code),
                                data_base_qj as
                                (select a.id as account_id,a.code,a.name,coalesce(sum(debit),0) as debit,coalesce(sum(credit),0) as credit ,coalesce(sum(debit),0)-coalesce(sum(credit),0) as debit_credit from account_base a 
                                left join account_move_line b on b.account_id=a.id and company_id=%(company_id)s and  b.period_id>=%(period_from)s  and b.period_id<=%(period_to)s 
                                and %(zmd_sql)s
                                 group by a.id,a.code,a.name order by a.code),
                                data_base_year as
                                (select a.id as account_id,a.code,a.name,coalesce(sum(debit),0) as debit,coalesce(sum(credit),0) as credit ,coalesce(sum(debit),0)-coalesce(sum(credit),0) as debit_credit from account_base a 
                                left join account_move_line b on b.account_id=a.id and company_id=%(company_id)s and
                                 b.period_id in (select id from account_period where fiscalyear_id=%(fiscalyear)s)  and %(zmd_sql)s
                                 group by a.id,a.code,a.name order by a.code),
                                xxx as 
                                 (select c.id,c.code as code,c.name,coalesce(sum(qc.debit),0) as qc_debit ,coalesce(sum(qc.credit),0) as qc_credit,sum(qc.debit_credit) as qc_debit_credit,
                                 coalesce(sum(qj.debit),0) as qj_debit ,coalesce(sum(qj.credit),0) as qj_credit,sum(qj.debit_credit) as qj_debit_credit,
                                 coalesce(sum(year.debit),0) as year_debit ,coalesce(sum(year.credit),0) as year_credit,sum(year.debit_credit) as year_debit_credit
                                  from base_view
                                  left join data_base_qc qc on qc.account_id=base_view.base
                                  left join data_base_qj qj on qj.account_id=base_view.base
                                  left join data_base_year year on year.account_id=base_view.base
                                  left join account_account c on c.id=base_view.view
                                  group by c.id 
                                  union all
                                 select cc.id,cc.code,cc.name,
                                 coalesce(sum(a.debit),0) as qc_debit ,coalesce(sum(a.credit),0) as qc_credit,sum(a.debit_credit) as qc_debit_credit,
                                 coalesce(sum(b.debit),0) as qj_debit ,coalesce(sum(b.credit),0) as qj_credit,sum(b.debit_credit) as qj_debit_credit,
                                 coalesce(sum(c.debit),0) as year_debit ,coalesce(sum(c.credit),0) as qj_credit,sum(c.debit_credit) as year_debit_credit
                                 from  data_base_qc a
                                 left join data_base_qj b on a.account_id=b.account_id
                                 left join data_base_year c on c.account_id=a.account_id
                                 left  join account_account cc on cc.id=a.account_id and cc.id=b.account_id
                                 group by cc.id )
                                --select * from xxx order by code
                                select a.code,name, 
                                ---期初
                                case when b.type in ('zc','fy')--借方
                                then qc_debit_credit
                                when b.type in ('sr','fz')--贷方
                                then 0
                                else qc_debit
                                end as qc_debit,
                                case when b.type in ('zc','fy')--借方
                                then 0
                                when b.type in ('sr','fz')--贷方
                                then -qc_debit_credit
                                else qc_credit
                                end as qc_credit,
                                ---期间
                                qj_debit,
                                qj_credit,
                                ---本年
                                year_debit,
                                year_credit,
                                ---期末
                                
                                case when b.type in ('zc','fy')--借方
                                then qc_debit_credit+qj_debit_credit
                                when b.type in ('sr','fz')--贷方
                                then 0
                                else qc_debit+qj_debit
                                end as qm_debit,
                                
                                case when b.type in ('zc','fy')--借方
                                then 0
                                when b.type in ('sr','fz')--贷方
                                then -qc_debit_credit-qj_debit_credit
                                else qc_credit+qj_credit
                                end as qm_credit,%(uid)s,%(line_id)s
                                 from xxx a
                                 left join account_type b on b.id=a.id
                                 where %(account_sql)s
                                 order by a.code"""%dic
        cr.execute(sql)                         
        domain=[('line_id','=',ids[0])]
        return {
            "name": "科目余额",
            "type": "ir.actions.act_window",
            "res_model": "wgaccount.balance.report.line",
            "view_type": "form",
            "view_mode": "tree",
            'views': [(False, 'tree')],
            'domain':domain,
            'limit':600,
            "context": {},
        }
        
    def onchange_company_id(self, cr, uid, ids, company_id=False,fiscalyear=0,  context=None):
        res = {'value':{}}
        if company_id and fiscalyear:
            fiscalyear,period_from,period_to=self._get_fiscalyear(cr, uid, context={'company_id':company_id})
            return {'value':{'account_id':False,'period_from':period_from,'period_to':period_to}}
        return res        

wgaccount_balance_report()


class wgaccount_balance_report_line(osv.osv_memory):
    _name = "wgaccount.balance.report.line"
    _order = "code"
    _field=['编码','名称','期初借方','期初贷方','本期借方','本期贷方','本年借方','本年贷方','期末借方','期末贷方']
    _columns = {
        "line_id": fields.integer(u"序号",),
        "code": fields.char(u"编码",size=100),
        "name": fields.char(u"名称",size=100),
        "qc_debit": fields.float(u"期初借方",digit=(15,4)),
        "qc_credit": fields.float(u"期初贷方",digit=(15,4)),
        "qj_debit": fields.float(u"本期借方",digit=(15,4)),
        "qj_credit": fields.float(u"本期贷方",digit=(15,4)),
        "year_debit": fields.float(u"本年借方",digit=(15,4)),
        "year_credit": fields.float(u"本年贷方",digit=(15,4)),
        "qm_debit": fields.float(u"期末借方",digit=(15,4)),
        "qm_credit": fields.float(u"期末贷方",digit=(15,4)),
    }
wgaccount_balance_report_line()










