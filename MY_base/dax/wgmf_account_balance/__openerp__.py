# -*- encoding: utf-8 -*-

{
    'name': '科目余额表',
    'author': 'OSCG',
    'website': 'http://www.oscg.com.hk',
    'summary': '报表',
    'version': '1.0',
    "category" : "Report",
    'sequence': 300,
    'description': """
     """,
    'depends': ['account','report_aeroo','analytic'],
    'data': [
        'wizard/account_report.xml',
        'oscg_report.xml',
        'wizard/account_analytic_report_view.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
