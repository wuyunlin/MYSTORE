# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import os
import pooler
import netsvc
import time
import tools
import inspect
from report.interface import report_int
from report.render.rml2pdf.trml2pdf import parseString
import logging
_logger = logging.getLogger(__name__)

class print_account_balance(report_int): 
    """ report made in python """
    cr = None
    uid = None
    ids = None
    datas = None
    context = None
    pool = None
    objects_name = None
    objects = None
    def __init__(self, openerp_name, objects_name=None):
        """ constructor """
        self.objects_name = objects_name
        super(print_account_balance, self).__init__(openerp_name)
        
    def create(self, cr, uid, ids, datas, context={}):
        """ construct the report """
        #init
        self.cr = cr
        self.uid= uid
        self.datas = datas
        self.context = context              
        self.pool = pooler.get_pool(self.cr.dbname)
        
        #lines ids come from a wizard form
        if 'form' in self.datas and 'ids' in self.datas['form']:
            self.ids = self.datas['form']['ids']
        else:
            self.ids = ids
        #get objects
        if self.objects_name:
            self.objects = pooler.get_pool(self.cr.dbname).get(self.objects_name).browse(self.cr, self.uid, self.ids)
        csv = self.createPDF()
        return (csv,'csv')

    def createPDF(self):
        #pdf = parseString(rml)
        return self.datas['csv'].encode('gb2312')

print_account_balance('report.print_account_balance', 'account.account')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
