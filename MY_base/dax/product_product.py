# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.tools.safe_eval import safe_eval
from openerp import SUPERUSER_ID
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp
import time
import calendar
from calendar import monthrange
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import logging
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class product_product(osv.osv):
    _inherit = 'product.product'
    ############材质结构  分摊方式
    _order ="v_bigcat_id asc,default_code desc"
    
    def _get_area(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = {
                'area': 0,
                'realsize': 0,
            }
            if line.length <> 0.0 and line.width <> 0.0:
                res[line.id]['area']=line.length*line.width
                if line.height<>0:
                    res[line.id]['realsize']=line.length*line.width*line.height
        return res    
    
    def update_parent(self, cr, uid, ids, context=None):
        sql="""select a.id,a.parent_id from ir_ui_menu_copy0819 a"""
        cr.execute(sql)
        fet=cr.fetchall()
        y=0
        for x in fet:
            print '===========',y
            y+=1
            id_new=self.pool.get('ir.ui.menu').create(cr,uid,{'name':'测试菜单请注意' })
            cr.execute("""update ir_ui_menu_copy0819 set id_new=%s where id=%s"""%(int(id_new),x[0]))
        sql="""select a.id,a.parent_id ,a.id_new,b.id_new from ir_ui_menu_copy0819 a
                   left join ir_ui_menu_copy0819 b on b.id=a.parent_id """
        cr.execute(sql)
        fet=cr.fetchall()
        y=0
        for x in fet:
            print '===========',y
            y+=1
            self.pool.get('ir.ui.menu').write(cr,uid,x[2],{'parent_id':x[3] or False })
        return True    
    
    _columns={
        'v_mtstruct_id':fields.many2one('bs.publictype.d', u'材质结构',domain="[('bs_publictype_id.viewname','=','v_mtstruct')]",widget='selection'),
        'v_commandtype_id':fields.many2one('bs.publictype.d', u'分摊方式',domain="[('bs_publictype_id.viewname','=','v_commandtype')]",widget='selection'),
        'v_bigcat_id':fields.many2one('bs.publictype.d', u'物料大类',domain="[('bs_publictype_id.viewname','=','v_bigcat')]",widget='selection',required=True),
        'mtstruct':fields.char(u'材质结构',size=200),
        'simplecode':fields.char(u'简码',size=200),
        'length':fields.float(u'长度',digits=(18, 8)),
        'width':fields.float(u'宽度',digits=(18, 8)),
        'height':fields.float(u'高度',digits=(18, 8)),
        'area':fields.function(_get_area,type='float',string=u'面积',digits=(18, 8),multi='area',store=True),
        'realsize':fields.function(_get_area,type='float',string=u'体积',digits=(18, 8),multi='realsize',store=True),
        'zhuangx_tiji':fields.float(u'装箱体积',digits=(18, 8)),
        'zhuangx_zl':fields.float(u'装箱重量',digits=(18, 8)),
        'uom_id_use':fields.many2one('product.uom', u'领用单位'),
        'use_switch':fields.float(u'领用换算',digits=(18, 8)),
        'uom_id_sale':fields.many2one('product.uom', u'销售单位'),
        'sale_switch':fields.float(u'销售换算',digits=(18, 8)),
        'uom_id_sca':fields.many2one('product.uom', u'零散单位'),
        'po_switch':fields.float(u'采购换算',digits=(18, 8)),
        'sca_switch':fields.float(u'零散换算',digits=(18, 8)),
        'often_switch':fields.float(u'常用换算',digits=(18, 8)),
        'uom_id_often':fields.many2one('product.uom', u'常用单位',required=True),        
        'nweight':fields.float(u'净重',digits=(18, 4)),
        'gweight':fields.float(u'毛重',digits=(18, 4)),
        'gweightp':fields.float(u'单个毛重',digits=(18, 8)),
        'nweightp':fields.float(u'单个净重',digits=(18, 8)),
        'weightpvat':fields.float(u'标准缸重',digits=(18, 8)),
        'issecret':fields.boolean(u'保密否',),
        'mcode_origin':fields.char(u'原版代码',size=50),
        'listingdate':fields.date(u'上市日期'),
        'eliminatedate':fields.date(u'淘汰日期'),
        'po_advancedays':fields.integer(u'采购提前期'),
        'make_advancedays':fields.integer(u'生产提前期'),
        'isvirtual':fields.boolean(u'是否虚项',),
        'jg_type':fields.selection([('deep',u'深加工'),('simple',u'初加工')],u'加工类型',size=1000),
        'remark':fields.char(u'说明',size=1000),
        'corp_lines':fields.one2many('bs.material.corp', 'product_id', u'公司属性'),
       "charging_mode": fields.publicType(view_name="v_pricingmode", string=u"计费方式", required=True),        
        'save_condition':fields.char(u'保存条件',size=120),
#         'guige_wl':fields.char(u'物料装箱规格',size=120),
        'guige_wl':fields.float(u'净重',digits=(18, 4)),
    }
    _defaults={
        'listingdate':lambda *a: time.strftime('%Y-%m-%d'),
        'charging_mode':90,
        'unit':1,
        'often_switch':0,
        'jg_type':'simple',
        'zhuangx_tiji':0
    }  
    def create(self, cr, uid, vals, context=None):
        if vals.get('name'):
            vals['name']=vals.get('name').replace(' ','')
        if vals.get('uom_id'):
            #包装单位
            if not vals.get('wg_packaging_uom'):
                vals.update({'wg_packaging_uom':vals['uom_id'],'dhpack':1})
            #领用单位
            if not vals.get('uom_id_use'):
                vals.update({'uom_id_use':vals['uom_id'],'use_switch':1}) 
            #销售单位
            if not vals.get('uom_id_sale'):
                vals.update({'uom_id_sale':vals['uom_id'],'sale_switch':1}) 
            #零散单位
            if not vals.get('uom_id_sca'):
                vals.update({'uom_id_sca':vals['uom_id'],'sca_switch':1})  
            #常用单位
            if not vals.get('uom_id_often'):
                vals.update({'uom_id_often':vals['uom_id'],'often_switch':1})    
        return super(product_product, self).create(cr, uid, vals, context)
        
    def onchange_uom_id(self, cr, uid, ids, uom_id, wg_packaging_uom,uom_id_use,uom_id_sale,uom_id_sca,context=None):
        res = {}
        if uom_id:
            #if not wg_packaging_uom:
            res.update({'wg_packaging_uom':uom_id,'dhpack':1})
            #if not uom_id_use:
            res.update({'uom_id_use':uom_id,'use_switch':1})
            #if not uom_id_sale:
            res.update({'uom_id_sale':uom_id,'sale_switch':1})
            #if not uom_id_sca:
            res.update({'uom_id_sca':uom_id,'sca_switch':1})
            res.update({'uom_po_id':uom_id,'po_switch':1})
            res.update({'uom_id_often':uom_id,'often_switch':1})
        return {'value': res}
        
    def onchange_bigcat(self, cr, uid, ids, v_bigcat_id=False,supply_method='',context=None):
        res = {} 
        if v_bigcat_id in (105,106) and supply_method !='produce':
            res={'value':{'supply_method':'produce'}}
        if v_bigcat_id not in (105,106) and supply_method=='produce':
            res={'value':{'supply_method':'buy'}}
        return res        
product_product()

#ondelete="cascade"
class bs_material_corp(osv.osv):
    _name = 'bs.material.corp'
    _description = u"工厂属性"
    _order =" res_company_id"
    ############物料来源 工作中心
    _columns={
        'v_mtsource_id':fields.many2one('bs.publictype.d', u'物料来源',domain="[('bs_publictype_id.viewname','=','v_mtsource')]",widget='selection'),
        'bs_workcentre_id':fields.many2one('bs.wcenter', u'工作中心',domain="[('isvalid','=',True),('bs_pshop_id.res_company_id','=',res_company_id)]"),
        'product_id':fields.many2one('product.product', u'产品',ondelete="cascade"),
        'res_company_id':fields.many2one('res.company', u'公司',ondelete="cascade"),
        'rop':fields.float(u'再订购点',digits=(18, 8)),
        'ropqty':fields.float(u'再订购量',digits=(18, 8)),
        'ismrp':fields.boolean(u'决定是否展开BOM',),
        'trunkqty':fields.integer(u'每板箱数',),
        'storeulqty':fields.float(u'库存上限',digits=(18, 8)),
        'storellqty':fields.float(u'库存下限',digits=(18, 8)),
        'safestoreqty':fields.integer(u'安全库存量',),
        'costprice':fields.float(u'成本价',digits=(18, 8)),
        'wasterate':fields.float(u'超耗率',digits=(18, 8)),
        'outrate':fields.float(u'产出率',digits=(18, 8)),
        'finerate':fields.float(u'良品率',digits=(18, 8)),
        'electcost':fields.float(u'电耗标准',digits=(18, 4)),
        'manout':fields.float(u'人均产能',digits=(18, 4)),
        'std_qty':fields.float(u'标准件系数',digits=(4, 2)),
        'is_batch':fields.boolean(u'启用批号管理'),
        'is_cbhs':fields.boolean(u'启用成本核算'),
        'stock_location_id':fields.many2one('stock.location', u'盘点库位',),
    }
    _defaults={
        'res_company_id':lambda s, cr, uid, c: s.pool.get('res.users').browse(cr,uid,uid).company_id.id,
        'is_cbhs':True,
        'is_batch':True,
    }    
    
bs_material_corp()







