# -*- encoding: utf-8 -*-
{
    'name': "五谷财务月末价格调整",
    'description': "五谷财务月末价格调整",
    'version': '0.1',
    'category': 'Generic Modules/report',
    'depends': ['wgmf_account_adjustable'],
    'data': [],
    'update_xml': [
                   'wgmf_change_view.xml',
                  ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
