# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.tools.safe_eval import safe_eval
from openerp import SUPERUSER_ID
from tools.translate import _
import datetime
from datetime import date, timedelta
import time

class account_change_price(osv.osv):
    _name = 'account.change.price'
    _columns = {
        'period_id': fields.many2one('account.period', '期间', required='1',select=True),
        'product_id': fields.many2one('product.product', '产品',required='1', select=True),
        'date': fields.date('调整日期'),
        'qty': fields.float('月末数量',digits=(16, 2)),
        'price_original': fields.float('调整前单价',digits=(16, 9)),
        'price': fields.float('调整后单价',digits=(16, 9)),
        'amount': fields.float('调整金额',digits=(16, 9)),
        'reason': fields.char('调整原因',size=128),
        'description': fields.char('备注',size=128),
        'name': fields.char('名称',size=32),
        'state': fields.selection([('draft','草稿'), ('cancel','取消'), ('done','生效')], '状态', readonly=True),
    }
    _defaults = {
        'name':u'月末单价调整',
        'state':'draft',
        'date': date.today().strftime("%Y-%m-%d"),
    }    
    def price_original_onchange(self, cr, uid, ids, period_id,product_id, context=None):
        res = {'price_original':0,'qty':0}
        if period_id and product_id:
            sql="""select COALESCE(price,0.0),COALESCE(qty_md,0.0) from standard_months_price where months=%s and product_id=%s"""%(period_id,product_id)
            cr.execute(sql)
            dic=cr.fetchall()
            price_original,qty=dic[0][0],dic[0][1]
            res = {'price_original':price_original,'qty':qty}
        return {'value': res}
    def amount_onchange(self, cr, uid, ids, price_original,price,qty,context=None):
        res = {'amount':0}
        if price and qty:
            res = {'amount':qty*(price-price_original)}
        return {'value': res}        
    def agree(self, cr, uid, ids, context=None):
        for change in self.browse(cr,uid,ids):
            if not change.price:raise osv.except_osv(('警告'),_('请先填写调整后的价格'))
            cr.execute("""update account_change_price set state='done' where id=%s"""%(change.id))
            cr.execute("""update standard_months_price set price=%s where product_id=%s and months=%s"""%(change.price,change.product_id.id,change.period_id.id))
        return True
    def refuse(self, cr, uid, ids, context=None):
        for change in self.browse(cr,uid,ids):
            cr.execute("""update account_change_price set state='draft' where id=%s"""%(change.id))
            cr.execute("""update standard_months_price set price=price_tzq where product_id=%s and months=%s"""%(change.product_id.id,change.period_id.id))
        return True
    def cancel(self, cr, uid, ids, context=None):
        for change in self.browse(cr,uid,ids):
            if change.state!='draft':raise osv.except_osv(('警告'),_('请先将单据反审核或者重置再作废'))
        return self.write(cr,uid,ids,{'state':'cancel'}) 
account_change_price()

class standard_months_price(osv.osv):
    _inherit = 'standard.months.price'
    _columns = {
        'price_tzq': fields.float('调整前单价',digits=(16, 9)),
        'qty_in': fields.float('本月入库数量(工厂-在途)',),
    }
standard_months_price()

