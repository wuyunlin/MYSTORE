﻿# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
import psycopg2
import base64
import time
from datetime import datetime
import openerp.addons.decimal_precision as dp

class wg_pzgz(osv.osv_memory):
    _name = "wg.pzgz"
    _order = "id desc"

    def wgmf_pzgz(self, cr, uid, ids, context=None):
        move_obj = self.pool.get('account.move')
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        move_ids = context.get('active_ids', [])
        if len(move_ids)> 0:
           move_obj.button_validate(cr, uid, move_ids, context=context)
        result = mod_obj.get_object_reference(cr, uid, 'account', 'action_move_journal_line')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id])[0]
        result['domain'] = [('id','in',move_ids)]
        return result
         
wg_pzgz()
