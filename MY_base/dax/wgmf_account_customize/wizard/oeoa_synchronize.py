# -*- coding: utf-8 -*- #
from openerp.osv import fields, osv
from openerp.tools.safe_eval import safe_eval
from openerp import SUPERUSER_ID
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp
import time
import MySQLdb
import calendar
from calendar import monthrange
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import logging
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class oeoa_synchronize(osv.osv_memory):
    _name = 'oeoa.synchronize'
    _order = "id desc"

    def _get_period(self, cr, uid, context=None):
        ctx = dict(context or {}, account_period_prefer_normal=True)
        period_ids = self.pool.get('account.period').find(cr, uid, context=ctx)
        return period_ids and period_ids[0] or False

    _columns = {
        'st_period_id': fields.many2one('account.period', u'开始期间', required=True),
        'end_period_id': fields.many2one('account.period', u'结束期间', required=True),
    }
    _defaults = {
        'st_period_id': _get_period,
        'end_period_id': _get_period,
    }

    def check_account_name(self, cr, uid, name, type, context=None):
        cr.execute("select account_id from oe_synchronize_oa where type = %s and name = %s and active = True", (type,name.strip(),))
        account_id = cr.fetchone()
        if account_id:
            account_id = account_id[0]
        if not account_id:
            account_id = self.pool.get('account.account').search(cr, uid, [('name', '=', name.strip()),('type', '<>', 'view'),('active', '=', True)])
            if not account_id:
                if type == 'bx':
                    return False,'OE中未找到名为(%s)的报销类型科目!' % name.strip()
                    #raise osv.except_osv(_('错误!'), _('OE中未找到名为(%s)的报销类型科目!' % name.strip()))
                if type == 'sk':
                    return False,'OE中未找到名为(%s)的申款类型科目!' % name.strip()
                    #raise osv.except_osv(_('错误!'), _('OE中未找到名为(%s)的申款类型科目!' % name.strip()))
                if type == 'fk':
                    return False,'OE中未找到名为(%s)的付款类型科目!' % name.strip()
                    #raise osv.except_osv(_('错误!'), _('OE中未找到名为(%s)的付款类型科目!' % name.strip()))
            if account_id:
                account_id = account_id[0]
        return account_id,''

    def check_partner_name(self, cr, uid, name, type, context=None):
        cr.execute("select partner_id from wgoa_partner_guanli where type = %s and name = %s and active = True", (type,name.strip(),))
        partner_id = cr.fetchone()
        if partner_id:
            partner_id = partner_id[0]
        if not partner_id:
            partner_id = self.pool.get('res.partner').search(cr, uid, [('name', '=', name.strip()),('active', '=', True)])
            if not partner_id:
                if type == 'bx':
                    return False,'OE中未找到名为(%s)的报销类型业务伙伴!' % name.strip()
                    #raise osv.except_osv(_('错误!'), _('OE中未找到名为(%s)的报销类型业务伙伴!' % name.strip()))
                if type == 'sk':
                    return False,'OE中未找到名为(%s)的申款类型业务伙伴!' % name.strip()
                    #raise osv.except_osv(_('错误!'), _('OE中未找到名为(%s)的申款类型业务伙伴!' % name.strip()))
                if type == 'fk':
                    return False,'OE中未找到名为(%s)的付款类型业务伙伴!' % name.strip()
                    #raise osv.except_osv(_('错误!'), _('OE中未找到名为(%s)的付款类型业务伙伴!' % name.strip()))
            if partner_id:
                partner_id = partner_id[0]
        return partner_id,''

    def check_analytic_name(self, cr, uid, name, type, context=None):
        cr.execute("select analytic_account_id from wgoa_fuzuhesuan_guanli where type = %s and name = %s and active = True", (type,name.strip(),))
        analytic_account_id = cr.fetchone()
        if analytic_account_id:
            analytic_account_id = analytic_account_id[0]
        if not analytic_account_id:
            analytic_account_id = self.pool.get('account.analytic.account').search(cr, uid, [('name', '=', name.strip()),('type', '=', 'normal')])
            if not analytic_account_id:
                if type == 'bx':
                    #raise osv.except_osv(_('错误!'), _('OE中未找到名为(%s)的报销辅助核算项!' % name.strip()))
                    return False,'OE中未找到名为(%s)的报销辅助核算项!'% name.strip()
                if type == 'sk':
                    return False,'OE中未找到名为(%s)的申款辅助核算项!'% name.strip()
                if type == 'fk':
                    return False,'OE中未找到名为(%s)的付款辅助核算项!'% name.strip()
                    #raise osv.except_osv(_('错误!'), _('OE中未找到名为(%s)的付款辅助核算项!' % name.strip()))
            if analytic_account_id:
                analytic_account_id = analytic_account_id[0]
        return analytic_account_id,''

    def check_oa_baoxiao(self, cr, uid, bx_user_name,st_period,end_period, context=None):
        conn = None
        result = []
        try:
            icp = self.pool.get('ir.config_parameter')
            h = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_host', 'False'))
            p = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_port', 'False'))
            u = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_user', 'False'))
            passwd = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_passwd', 'False'))
            db = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_db', 'False'))
            conn = MySQLdb.connect(host=h,port=3336,user=u,passwd=passwd,db=db,charset="utf8")
            cursor=conn.cursor()
            cursor.execute("""select DATA_330 from flow_data_193 
                where DATA_670 like '记%%' 
                and DATA_673 >= '%s' 
                and DATA_673 <= '%s'
                and data_685 ='%s' """ % (st_period,end_period,bx_user_name,))
            oa_inv_name = [x[0].encode('utf-8') for x in cursor.fetchall()]
            if len(oa_inv_name) > 0:
                sql = "select ref from account_move where wg_type='oa' and create_uid=%s and ref in %s" % (uid,tuple(oa_inv_name),)
                cr.execute("select ref from account_move where wg_type='oa' and create_uid=%s and ref in %s",(uid,tuple(oa_inv_name),))
                oe_inv_name = [x[0] for x in cr.fetchall()]
                if(len(oa_inv_name) != len(oe_inv_name)):
                    err_oa_inv = [val for val in oa_inv_name if val not in oe_inv_name]
                    if len(err_oa_inv) > 1:
                        cursor.execute("update flow_data_193 set DATA_670='' where DATA_330 in %s", (tuple(err_oa_inv),))
                    if len(err_oa_inv) == 1:
                        cursor.execute("update flow_data_193 set DATA_670='' where DATA_330 = %s", (err_oa_inv[0],))
        except Exception as e:
            #print "e=============%s" % str(e)
            if str(e).find("Can't connect to MySQL server on 'newoa.szwgmf.com'") != -1:
                result.append("连接OA系统超时，请重试！")
            else:
                result.append(str(e))
            import traceback
            _logger.error("Oracle OA synchronization Error: %s" % e)
            _logger.error(traceback.format_exc())
            if conn:
                conn.rollback()
        finally:
            try:
                if conn:
                    cursor.close()
                    conn.close()
            except:
                pass
        return result


    def get_oa_baoxiao(self, cr, uid, bx_user_name,st_period,end_period, context=None):
        conn = None
        result = [[],[]]
        try:
            icp = self.pool.get('ir.config_parameter')
            h = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_host', 'False'))
            p = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_port', 'False'))
            u = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_user', 'False'))
            passwd = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_passwd', 'False'))
            db = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_db', 'False'))
            conn = MySQLdb.connect(host=h,port=3336,user=u,passwd=passwd,db=db,charset="utf8")
            cursor=conn.cursor()
            cursor.execute("""select DATA_495,DATA_496,DATA_556,DATA_588,DATA_633,DATA_498,DATA_507,DATA_560,
                DATA_589,DATA_637,DATA_499,DATA_508,DATA_583,DATA_590,DATA_638,DATA_500,DATA_509,DATA_587,DATA_591,
                DATA_639,DATA_501,DATA_510,DATA_586,DATA_592,DATA_640,DATA_502,DATA_511,DATA_565,DATA_593,DATA_641,
                DATA_503,DATA_512,DATA_584,DATA_594,DATA_642,DATA_504,DATA_513,DATA_571,DATA_595,DATA_643,
                
                DATA_62,DATA_33,DATA_422,DATA_614,DATA_470,DATA_650,DATA_466,DATA_42,DATA_423,DATA_615,DATA_471,DATA_651,
                DATA_467,DATA_43,DATA_424,DATA_616,DATA_472,DATA_652,DATA_468,DATA_44,DATA_425,DATA_617,DATA_473,
                DATA_653,DATA_469,DATA_45,DATA_426,DATA_618,DATA_474,DATA_654,DATA_475,DATA_485,DATA_516,DATA_619,
                DATA_604,DATA_655,DATA_476,DATA_486,DATA_598,DATA_620,DATA_605,DATA_656,DATA_477,DATA_487,DATA_521,
                DATA_621,DATA_606,DATA_657,DATA_478,DATA_488,DATA_527,DATA_622,DATA_607,DATA_658,DATA_479,DATA_489,
                DATA_599,DATA_623,DATA_608,DATA_659,DATA_480,DATA_490,DATA_532,DATA_624,DATA_609,DATA_660,DATA_481,
                DATA_491,DATA_538,DATA_625,DATA_610,DATA_661,DATA_482,DATA_492,DATA_543,DATA_626,DATA_611,DATA_662,
                
                DATA_330,DATA_341,DATA_447,DATA_449,DATA_665,DATA_461,DATA_462,DATA_673,data_685 
                from flow_data_193 
                where data_684 = '已销账' 
                and (DATA_670 is null or DATA_670 = '已传入，未做凭证' or DATA_670 = '') 
                and DATA_673 >= '%s' 
                and DATA_673 <= '%s' 
                and data_685 ='%s' 
                limit 30""" % (st_period,end_period,bx_user_name,)) #and DATA_330 in ('100655','100591')
            bx_order = cursor.fetchall()
            #print "len(total)=================%s" % (len(bx_order))
            if bx_order:
                for bx in bx_order:
                    res = {}
                    order_baoxiao = bx[len(bx)-9:len(bx)]
                    line = bx[:40]
                    line2 = bx[40:len(bx)-9]
                    list_line = []
                    list_line2 = []
                    for l in range(0,len(line),5):
                        list_line.append(line[l:l+5])
                    for l2 in range(0,len(line2),6):
                        list_line2.append(line2[l2:l2+6])
                    res.update({'ref':order_baoxiao[0],'wgmf_type':order_baoxiao[1],'amount':order_baoxiao[2],'analytic_account_id':order_baoxiao[3],'bumen':order_baoxiao[4],'chongxiao':order_baoxiao[5],'baoxiao':order_baoxiao[6],'date':order_baoxiao[7],'create_name':order_baoxiao[8],'line5':list_line,'line6':list_line2})
                    #print "res====================%s" % str(res)
                    result[0].append(res)
        except Exception as e:
            #print "e=============%s" % str(e)
            if str(e).find("Can't connect to MySQL server on 'newoa.szwgmf.com'") != -1:
                result[1].append("连接OA系统超时，请重试！")
            else:
                result[1].append(str(e))
            import traceback
            _logger.error("Oracle OA synchronization Error: %s" % e)
            _logger.error(traceback.format_exc())
            if conn:
                conn.rollback()
        finally:
            try:
                if conn:
                    cursor.close()
                    conn.close()
            except:
                pass
        return result

    def get_oa_shenkuan(self, cr, uid, context=None):
        conn = None
        result = [[],[]]
        try:
            icp = self.pool.get('ir.config_parameter')
            h = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_host', 'False'))
            p = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_port', 'False'))
            u = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_user', 'False'))
            passwd = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_passwd', 'False'))
            db = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_db', 'False'))
            conn = MySQLdb.connect(host=h,port=3336,user=u,passwd=passwd,db=db,charset="utf8")
            cursor=conn.cursor()
            cursor.execute("""select DATA_495,DATA_687,DATA_679,DATA_588,DATA_688,DATA_712,DATA_699,
                    DATA_680,DATA_589,DATA_713,DATA_690,DATA_700,DATA_681,DATA_590,DATA_714,DATA_691,
                    DATA_701,DATA_682,DATA_591,DATA_715,DATA_692,DATA_702,DATA_683,DATA_592,DATA_716,
                    DATA_693,DATA_703,DATA_684,DATA_593,DATA_717,DATA_694,DATA_704,DATA_685,DATA_594,
                    DATA_718,DATA_695,DATA_705,DATA_686,DATA_595,DATA_719,DATA_696,DATA_706,DATA_708,
                    DATA_710,DATA_720,DATA_697,DATA_707,DATA_709,DATA_711,DATA_721,
                    
                    DATA_330,DATA_341,DATA_447,DATA_449,DATA_665 from flow_data_206_COPY where DATA_677 = '审核通过' limit 100""") #   and DATA_670 is null
            sk_order = cursor.fetchall()
            #print "len(sk_order)=================%s" % (len(sk_order))
            if sk_order:
                for sk in sk_order:
                    res = {}
                    order_shenkuan = sk[len(sk)-5:len(sk)]
                    line = sk[:50]
                    list_line = []
                    for l in range(0,len(line),5):
                        list_line.append(line[l:l+5])
                    res.update({'ref':order_shenkuan[0],'fzhs':order_shenkuan[1],'amount':order_shenkuan[2],'bumen':order_shenkuan[3],'analytic_account_id':order_shenkuan[4],'line5':list_line})
                    result[0].append(res)
        except Exception as e:
            #print "e=============%s" % str(e)
            if str(e).find("Can't connect to MySQL server on 'newoa.szwgmf.com'") != -1:
                result[1].append("连接OA系统超时，请重试！")
            else:
                result[1].append(str(e))
            conn.rollback()
            import traceback
            _logger.error("Oracle OA synchronization Error: %s" % e)
            _logger.error(traceback.format_exc())
        finally:
            try:
                if conn:
                    cursor.close()
                    conn.close()
            except:
                pass
        return result

    def get_oa_fukuan(self, cr, uid, context=None):
        conn = None
        result = [[],[]]
        try:
            icp = self.pool.get('ir.config_parameter')
            h = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_host', 'False'))
            p = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_port', 'False'))
            u = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_user', 'False'))
            passwd = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_passwd', 'False'))
            db = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_db', 'False'))
            conn = MySQLdb.connect(host=h,port=3336,user=u,passwd=passwd,db=db,charset="utf8")
            cursor=conn.cursor()
            cursor.execute("""select DATA_495,DATA_687,DATA_679,DATA_588,DATA_688,DATA_712,DATA_699,
                    DATA_680,DATA_589,DATA_713,DATA_690,DATA_700,DATA_681,DATA_590,DATA_714,DATA_691,
                    DATA_701,DATA_682,DATA_591,DATA_715,DATA_692,DATA_702,DATA_683,DATA_592,DATA_716,
                    DATA_693,DATA_703,DATA_684,DATA_593,DATA_717,DATA_694,DATA_704,DATA_685,DATA_594,
                    DATA_718,DATA_695,DATA_705,DATA_686,DATA_595,DATA_719,DATA_696,DATA_706,DATA_708,
                    DATA_710,DATA_720,DATA_697,DATA_707,DATA_709,DATA_711,DATA_721,
                    
                    DATA_330,DATA_341,DATA_447,DATA_449,DATA_665 from flow_data_207_copy where DATA_677 = '审核通过' """) #  and DATA_670 is null
            fk_order = cursor.fetchall()
            #print "len(fk_order)=================%s" % (len(fk_order))
            if fk_order:
                for fk in fk_order:
                    res = {}
                    order_fukuan = fk[len(fk)-5:len(fk)]
                    line = fk[:50]
                    list_line = []
                    for l in range(0,len(line),5):
                        list_line.append(line[l:l+5])
                    res.update({'ref':order_fukuan[0],'fzhs':order_fukuan[1],'amount':order_fukuan[2],'bumen':order_fukuan[3],'analytic_account_id':order_fukuan[4],'line5':list_line})
                    result[0].append(res)
        except Exception as e:
            if str(e).find("Can't connect to MySQL server on 'newoa.szwgmf.com'") != -1:
                result[1].append("连接OA系统超时，请重试！")
            else:
                result[1].append(str(e))
            #print "e=============%s" % str(e)
            conn.rollback()
            import traceback
            _logger.error("Oracle OA synchronization Error: %s" % e)
            _logger.error(traceback.format_exc())
        finally:
            try:
                if conn:
                    cursor.close()
                    conn.close()
            except:
                pass
        return result

    def create_baoxoao_account(self, cr, uid,st_period,end_period, context=None):
        conn = None
        res = []
        i= 0
        x = 0
        j = 0
        accmove_line_obj = self.pool.get('account.move.line')
        journal_obj = self.pool.get('account.journal')
        accmove_obj = self.pool.get('account.move')
        period_obj = self.pool.get('account.period')
        user_obj = self.pool.get('res.users')
        obj_sequence = self.pool.get('ir.sequence')
        bx_user = user_obj.browse(cr, uid, uid, context=context)
        bx_account_id2 = self.pool.get('account.account').search(cr, uid, [('code', '=', '1221.01')])
        if not bx_account_id2:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(1221.01)的报销类型会计科目!"))
        bx_account_id3 = self.pool.get('account.account').search(cr, uid, [('code', '=', '2241.13')])
        if not bx_account_id3:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(2241.13)的报销类型会计科目!"))
        check_order = self.check_oa_baoxiao(cr, uid, bx_user.name,st_period,end_period, context=context)
        if len(check_order) != 0:
            raise osv.except_osv(
                    _('错误!'), _(str(check_order)))
        baoxiao = self.get_oa_baoxiao(cr, uid, bx_user.name,st_period,end_period, context=context)
        #print "一共--%s条记录" % len(baoxiao[0])
        #print  "baoxiao===============%s" % str(baoxiao)
        if baoxiao[1] != []:
            raise osv.except_osv(
                    _('错误!'),
                    _(baoxiao[1]))
        elif len(baoxiao[0]) > 0:
            for bx_order in baoxiao[0]:
                bx_account_order = accmove_obj.search(cr, uid, [('ref', '=', bx_order.get('ref',''))])
                if bx_order.get('wgmf_type',False) == u'发票':
                    bx_journal_id = journal_obj.search(cr, uid, [('name', '=', '记办')])
                    if not bx_journal_id:
                        raise osv.except_osv(
                            _('错误!'),
                                _("未找到'记办'凭证账簿!"))
                if bx_order.get('wgmf_type',False) == u'收据/白条':
                    bx_journal_id = journal_obj.search(cr, uid, [('name', '=', '记五')])
                    if not bx_journal_id:
                        raise osv.except_osv(
                            _('错误!'),
                                _("未找到'记五'凭证账簿!"))
                if bx_account_order:
                    i += 1
                    bx_accmove1 = accmove_obj.browse(cr, uid, bx_account_order[0])
                    try:
                        icp = self.pool.get('ir.config_parameter')
                        h = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_host', 'False'))
                        p = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_port', 'False'))
                        u = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_user', 'False'))
                        passwd = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_passwd', 'False'))
                        db = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_db', 'False'))
                        conn = MySQLdb.connect(host=h,port=3336,user=u,passwd=passwd,db=db,charset="utf8")
                        cursor=conn.cursor()
                        cursor.execute("update flow_data_193 set DATA_670 = \'%s\' where DATA_330 = %s and DATA_677='审核通过'" % (bx_accmove1.name,bx_order.get('ref','')))
                    except Exception as e:
                        list = []
                        #print "e=============%s" % str(e)
                        conn.rollback()
                        import traceback
                        _logger.error("Oracle OA synchronization Error: %s" % e)
                        _logger.error(traceback.format_exc())
                    finally:
                        try:
                            if conn:
                                cursor.close()
                                conn.close()
                        except:
                            pass
                    #print "已存在--%s条记录名为%s" % (i,bx_order.get('ref',''))
                    continue
                if not bx_order.get('date',False):
                    j +=1
                    #print "没有项目归属日期的单据有--%s条记录" % j
                if bx_order.get('date',False):
                    bx_year = bx_order.get('date',False)[:4]
                    bx_mouth = bx_order.get('date',False)[len(bx_order.get('date',False)) - 3:len(bx_order.get('date',False)) - 1]
                    bx_monthRange = calendar.monthrange(int(bx_year),int(bx_mouth))[1]
                    bx_period_name = bx_mouth + "/" + bx_year
                    bx_date = bx_year + "-" + bx_mouth + "-" + str(bx_monthRange)
                    bx_period_id = period_obj.search(cr, uid, [('name', '=', bx_period_name),('special','=', False)])
                    if not bx_period_id:
                        raise osv.except_osv(_('错误!'), _('OE中未找到打开的会计期间(%s)!' % bx_period_name))
                    if bx_journal_id and bx_journal_id[0]:
                        bx_journal = journal_obj.browse(cr, uid, bx_journal_id[0], context=context)
                        bx_period = period_obj.browse(cr, uid, bx_period_id[0], context=context)
                        bx_c = {'fiscalyear_id': bx_period.fiscalyear_id.id, 'seq_month':time.strftime('%Y%m',time.strptime(bx_period.date_start,'%Y-%m-%d'))}
                        bx_new_name = obj_sequence.next_by_id(cr, uid, bx_journal.sequence_id.id, bx_c)
                    else:
                        bx_new_name = '/'
                    user_id = user_obj.search(cr, uid, [('name', '=', bx_order.get('create_name',False)),('active','=', True)])
                    if not user_id:
                        #raise osv.except_osv(_('错误!'), _('OE中未找到名为(%s)的用户!' % bx_order.get('create_name',False)))
                        continue
                    else:
                        user_id = user_id[0]
                    bx_accmove_id = accmove_obj.create(cr, uid, {
                        'name': bx_new_name,
                        'journal_id': bx_journal_id[0],
                        'ref': bx_order.get('ref',''),
                        'period_id': bx_period_id[0],
                        'date': bx_date,
                        'date_maturity': bx_date,
                        'to_check': False,
                        'wg_type': 'oa',
                    })
                    accmove_obj.write(cr,uid,[bx_accmove_id],{'create_uid': user_id})
                    res.append(bx_accmove_id)
                    #print 'len(res)======================%s' % len(res)
                    bx_lines = []
                    for bx_line in bx_order.get('line5',False):
                        if bx_line[2]:
                            bx_analytic_account_id,error = self.check_analytic_name(cr, uid, bx_line[2], 'bx', context=context)
                            if error:
                                raise osv.except_osv(_('错误!'), _('%s' % error))
                        if bx_line[0]:
                            if bx_line[0] == '=此行作废=':
                                continue
                            bx_account_id,error = self.check_account_name(cr, uid, bx_line[0], 'bx', context=context)
                            if error:
                                raise osv.except_osv(_('错误!'), _('%s' % error))
                            
                            name = bx_line[3] + bx_line[4]
                            if float(bx_line[1]) != 0.0:
                                bx_accmove_line_id = accmove_line_obj.create(cr, uid, {
                                    'name': name,
                                    'centralisation': 'debit',
                                    'partner_id': False,
                                    'account_id': bx_account_id,
                                    'move_id': bx_accmove_id,
                                    'journal_id': bx_journal_id[0],
                                    'analytic_account_id': bx_analytic_account_id,
                                    'period_id': bx_period_id[0],
                                    'date': bx_date,
                                    'date_maturity':bx_date,
                                    'credit': 0.0,
                                    'debit': float(bx_line[1]),
                                })
                                bx_lines.append(bx_accmove_line_id)
                    for bx_line1 in bx_order.get('line6',False):
                        if bx_line1[0]:
                            if bx_line1[0] == '=此行作废=':
                                continue
                            bx_account_id1,error = self.check_account_name(cr, uid, bx_line1[0], 'bx', context=context)
                            if error:
                                raise osv.except_osv(_('错误!'), _('%s' % error))
                            if bx_line1[2]:
                                bx_analytic_account_id1,error = self.check_analytic_name(cr, uid, bx_line1[2], 'bx', context=context)
                                if error:
                                    raise osv.except_osv(_('错误!'), _('%s' % error))
                            bx_name1 = bx_line1[3] + bx_line1[4] + bx_line1[5]
                            if float(bx_line1[1]) != 0.0:
                                bx_accmove_line_id1 = accmove_line_obj.create(cr, uid, {
                                    'name': bx_name1,
                                    'centralisation': 'debit',
                                    'partner_id': False,
                                    'account_id': bx_account_id1,
                                    'move_id': bx_accmove_id,
                                    'journal_id': bx_journal_id[0],
                                    'analytic_account_id': bx_analytic_account_id1,
                                    'period_id': bx_period_id[0],
                                    'date': bx_date,
                                    'date_maturity':bx_date,
                                    'credit': 0.0,
                                    'debit': float(bx_line1[1]),
                                })
                                bx_lines.append(bx_accmove_line_id1)
                    if float(bx_order.get('chongxiao',False)) != 0.0:
                        if bx_order.get('analytic_account_id',False):
                            bx_partner_id,error = self.check_partner_name(cr, uid, bx_order.get('analytic_account_id',False), 'bx', context=context)
                            if error:
                                raise osv.except_osv(_('错误!'), _('%s' % error))
                        bx_accmove_line_id2 = accmove_line_obj.create(cr, uid, {
                            'name': '其他应收款_办事处借款',
                            'centralisation': 'credit',
                            'partner_id': bx_partner_id,
                            'account_id': bx_account_id2[0],
                            'move_id': bx_accmove_id,
                            'journal_id': bx_journal_id[0],
                            'period_id': bx_period_id[0],
                            'date': bx_date,
                            'date_maturity':bx_date,
                            'credit': float(bx_order.get('chongxiao',False)),
                            'debit': 0.0,
                        })
                        bx_lines.append(bx_accmove_line_id2)
                    if float(bx_order.get('baoxiao',False)) != 0.0:
                        bx_accmove_line_id3 = accmove_line_obj.create(cr, uid, {
                            'name': '其他应付款_费用报销款',
                            'centralisation': 'credit',
                            'partner_id': False,
                            'account_id': bx_account_id3[0],
                            'move_id': bx_accmove_id,
                            'journal_id': bx_journal_id[0],
                            'period_id': bx_period_id[0],
                            'date': bx_date,
                            'date_maturity':bx_date,
                            'credit': float(bx_order.get('baoxiao',False)),
                            'debit': 0.0,
                        })
                        bx_lines.append(bx_accmove_line_id3)
                    if float(bx_order.get('chongxiao',False)) != 0.0 and float(bx_order.get('baoxiao',False)) != 0.0:
                        x += 1
                        #print "冲销跟报销都为0删除的有--%s条记录" % x
                        accmove_line_obj.unlink(cr, uid, bx_lines)
                        accmove_obj.unlink(cr, uid, [bx_accmove_id])
                    else:
                        x+=1
                        #print "会写--%s条记录" % x
                        bx_accmove = accmove_obj.browse(cr, uid, bx_accmove_id)
                        try:
                            icp = self.pool.get('ir.config_parameter')
                            h = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_host', 'False'))
                            p = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_port', 'False'))
                            u = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_user', 'False'))
                            passwd = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_passwd', 'False'))
                            db = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_db', 'False'))
                            conn = MySQLdb.connect(host=h,port=3336,user=u,passwd=passwd,db=db,charset="utf8")
                            cursor=conn.cursor()
                            cursor.execute("update flow_data_193 set DATA_670 = \'%s\' where DATA_330 = %s and DATA_677='审核通过'" % (bx_accmove.name,bx_order.get('ref','')))
                        except Exception as e:
                            list = []
                            #print "e=============%s" % str(e)
                            conn.rollback()
                            import traceback
                            _logger.error("Oracle OA synchronization Error: %s" % e)
                            _logger.error(traceback.format_exc())
                        finally:
                            try:
                                if conn:
                                    cursor.close()
                                    conn.close()
                            except:
                                pass
        return res

    def create_shenkuan_account(self, cr, uid, context=None):
        conn = None
        res = []
        accmove_line_obj = self.pool.get('account.move.line')
        journal_obj = self.pool.get('account.journal')
        accmove_obj = self.pool.get('account.move')
        period_obj = self.pool.get('account.period')
        obj_sequence = self.pool.get('ir.sequence')
        sk_account_id2 = self.pool.get('account.account').search(cr, uid, [('code', '=', '1001.10.03')])
        if not sk_account_id2:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(1001.10)的申款类型会计科目!"))
        sk_account_id3 = self.pool.get('account.account').search(cr, uid, [('code', '=', '1002.01.34')])
        if not sk_account_id3:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(1002.01.34)的申款类型会计科目!"))
        shenkuan = self.get_oa_shenkuan(cr, uid, context=context)
        if shenkuan[1] != []:
            raise osv.except_osv(
                    _('错误!'),
                    _(shenkuan[1]))
        elif len(shenkuan[0]) > 0:
            for sk_order in shenkuan[0]:
                sk_account_order = accmove_obj.search(cr, uid, [('ref', '=', sk_order.get('ref',''))])
                if sk_account_order:
                    continue
                sk_sign = True
                sk_lines = []
                sk_shouju = 0.0
                sk_fapiao = 0.0
                sk_journal_id = journal_obj.search(cr, uid, [('name', '=', '销售收入')])
                if not sk_journal_id:
                    continue
                sk_year = time.strftime('%Y')
                sk_mouth = time.strftime('%m')
                sk_monthRange = calendar.monthrange(int(sk_year),int(sk_mouth))[1]
                sk_period_name = sk_mouth + "/" + sk_year
                sk_date = sk_year + "-" + sk_mouth + "-" + str(sk_monthRange)
                sk_period_id = period_obj.search(cr, uid, [('name', '=', sk_period_name),('special','=', False),('biz_closed','=', False)])
                if not sk_period_id:
                    raise osv.except_osv(_('错误!'), _('OE中未找到打开的会计期间(%s)!' % sk_period_name))
                if sk_order.get('analytic_account_id',False):
                    sk_analytic_account_id,error = self.check_analytic_name(cr, uid, sk_order.get('analytic_account_id',False), 'sk', context=context)
                    if error:
                        raise osv.except_osv(_('错误!'), _('%s' % error))
                if sk_journal_id and sk_journal_id[0]:
                    sk_journal = journal_obj.browse(cr, uid, sk_journal_id[0], context=context)
                    sk_period = period_obj.browse(cr, uid, sk_period_id[0], context=context)
                    sk_c = {'fiscalyear_id': sk_period.fiscalyear_id.id, 'seq_month':time.strftime('%Y%m',time.strptime(sk_period.date_start,'%Y-%m-%d'))}
                    sk_new_name = obj_sequence.next_by_id(cr, uid, sk_journal.sequence_id.id, sk_c)
                else:
                    sk_new_name = '/'
                #print "sk_new_name======%s" % str(sk_new_name)
                sk_accmove_id = accmove_obj.create(cr, uid, {
                        'name': sk_new_name,
                        'journal_id': sk_journal_id[0],
                        'ref': sk_order.get('ref',''),
                        'period_id': sk_period_id[0],
                        'date': sk_date,
                        'date_maturity':sk_date,
                        'wg_type': 'oa',
                    })
                res.append(sk_accmove_id)
                for sk_line in sk_order.get('line5',False):
                    if not sk_line[0] or not sk_line[1]:
                        sk_sign = False
                        continue
                    if sk_line[0] or sk_line[1]:
                        #print "sk_line[0]=====%s" % sk_line[0]
                        if sk_line[0] == '=此行作废=':
                            continue
                        sk_account_id,error = self.check_account_name(cr, uid, sk_line[0], 'sk', context=context)
                        if error:
                            raise osv.except_osv(_('错误!'), _('%s' % error))
                        if float(sk_line[2]) != 0.0 and sk_sign != False:
                            name = sk_line[1] + sk_line[3]
                            sk_accmove_line_id = accmove_line_obj.create(cr, uid, {
                                'name': name,
                                'centralisation': 'debit',
                                'partner_id': False,
                                'account_id': sk_account_id,
                                'move_id': sk_accmove_id,
                                'journal_id': sk_journal_id[0],
                                'analytic_account_id': sk_analytic_account_id,
                                'period_id': sk_period_id[0],
                                'date': sk_date,
                                'date_maturity':sk_date,
                                'credit': 0.0,
                                'debit': float(sk_line[2]),
                            })
                            if sk_line[4] == u'收据':
                                sk_shouju += float(sk_line[2])
                            if sk_line[4] == u'发票':
                                sk_fapiao += float(sk_line[2])
                            sk_lines.append(sk_accmove_line_id)
                #print "111111111111111"
                if sk_sign == True and sk_shouju != 0.0:
                    sk_accmove_line_id1 = accmove_line_obj.create(cr, uid, {
                        'name': name,
                        'centralisation': 'credit',
                        'partner_id': False,
                        'account_id': sk_account_id2[0],
                        'move_id': sk_accmove_id,
                        'journal_id': sk_journal_id[0],
                        'period_id': sk_period_id[0],
                        'date': sk_date,
                        'date_maturity':sk_date,
                        'credit': sk_shouju,
                        'debit': 0.0,
                    })
                    sk_lines.append(sk_accmove_line_id1)
                if sk_sign == True and sk_fapiao != 0.0:
                    sk_accmove_line_id2 = accmove_line_obj.create(cr, uid, {
                        'name': name,
                        'centralisation': 'credit',
                        'partner_id': False,
                        'account_id': sk_account_id3[0],
                        'move_id': sk_accmove_id,
                        'journal_id': sk_journal_id[0],
                        'period_id': sk_period_id[0],
                        'date': sk_date,
                        'date_maturity':sk_date,
                        'credit': sk_fapiao,
                        'debit': 0.0,
                    })
                    sk_lines.append(sk_accmove_line_id2)
                if sk_sign == False:
                    accmove_line_obj.unlink(cr, uid, sk_lines)
                    accmove_obj.unlink(cr, uid, [sk_accmove_id])
                else:
                    sk_accmove = accmove_obj.browse(cr, uid, sk_accmove_id)
                    try:
                        icp = self.pool.get('ir.config_parameter')
                        h = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_host', 'False'))
                        p = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_port', 'False'))
                        u = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_user', 'False'))
                        passwd = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_passwd', 'False'))
                        db = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_db', 'False'))
                        conn = MySQLdb.connect(host=h,port=3336,user=u,passwd=passwd,db=db,charset="utf8")
                        cursor=conn.cursor()
                        cursor.execute("update flow_data_206_COPY set DATA_670 = \'%s\' where DATA_330 = %s and DATA_677='审核通过'" % (sk_accmove.name,sk_order.get('ref','')))
                    except Exception as e:
                        list = []
                        #print "e=============%s" % str(e)
                        conn.rollback()
                        import traceback
                        _logger.error("Oracle OA synchronization Error: %s" % e)
                        _logger.error(traceback.format_exc())
                    finally:
                        try:
                            if conn:
                                cursor.close()
                                conn.close()
                        except:
                            pass
        return res

    def create_fukuan_account(self, cr, uid, context=None):
        conn = None
        res = []
        accmove_line_obj = self.pool.get('account.move.line')
        journal_obj = self.pool.get('account.journal')
        accmove_obj = self.pool.get('account.move')
        period_obj = self.pool.get('account.period')
        obj_sequence = self.pool.get('ir.sequence')
        sk_account_id2 = self.pool.get('account.account').search(cr, uid, [('code', '=', '1001.10.03')])
        if not sk_account_id2:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(1001.10)的申款类型会计科目!"))
        sk_account_id3 = self.pool.get('account.account').search(cr, uid, [('code', '=', '1002.01.34')])
        if not sk_account_id3:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(1002.01.34)的申款类型会计科目!"))
        fukuan = self.get_oa_fukuan(cr, uid, context=context)
        if fukuan[1] != []:
            raise osv.except_osv(
                    _('错误!'),
                    _(fukuan[1]))
        elif len(fukuan[0]) > 0:
            for fu_order in fukuan[0]:
                #print "fu_order==========%s" % str(fu_order)
                fu_sign = True
                fu_lines = []
                fu_shouju = 0.0
                fu_fapiao = 0.0
                fu_account_order = accmove_obj.search(cr, uid, [('ref', '=', fu_order.get('ref',''))])
                if fu_account_order:
                    continue
                fu_journal_id = journal_obj.search(cr, uid, [('name', '=', '销售收入')])
                if not fu_journal_id:
                    continue
                if fu_order.get('analytic_account_id',False):
                    fu_analytic_account_id,error = self.check_analytic_name(cr, uid, fu_order.get('analytic_account_id',False), 'fk', context=context)
                    if error:
                        raise osv.except_osv(_('错误!'), _('%s' % error))
                fu_year = time.strftime('%Y')
                fu_mouth = time.strftime('%m')
                fu_monthRange = calendar.monthrange(int(fu_year),int(fu_mouth))[1]
                fu_period_name = fu_mouth + "/" + fu_year
                fu_date = fu_year + "-" + fu_mouth + "-" + str(fu_monthRange)
                fu_period_id = period_obj.search(cr, uid, [('name', '=', fu_period_name),('special','=', False),('biz_closed','=', False)])
                if not fu_period_id:
                    raise osv.except_osv(_('错误!'), _('OE中未找到打开的会计期间(%s)!' % fu_period_name))
                if fu_journal_id and fu_journal_id[0]:
                    fu_journal = journal_obj.browse(cr, uid, fu_journal_id[0], context=context)
                    fu_period = period_obj.browse(cr, uid, fu_period_id[0], context=context)
                    fu_c = {'fiscalyear_id': fu_period.fiscalyear_id.id, 'seq_month':time.strftime('%Y%m',time.strptime(fu_period.date_start,'%Y-%m-%d'))}
                    fu_new_name = obj_sequence.next_by_id(cr, uid, fu_journal.sequence_id.id, fu_c)
                else:
                    fu_new_name = '/'
                fu_accmove_id = accmove_obj.create(cr, uid, {
                        'name': fu_new_name,
                        'journal_id': fu_journal_id[0],
                        'ref': fu_order.get('ref',''),
                        'period_id': fu_period_id[0],
                        'date': fu_date,
                        'date_maturity':fu_date,
                        'wg_type': 'oa',
                    })
                res.append(fu_accmove_id)
                for fu_line in fu_order.get('line5',False):
                    if not fu_line[0] or not fu_line[1]:
                        fu_sign = False
                        continue
                    if fu_line[0] or fu_line[1]:
                        if fu_line[0] == '=此行作废=':
                            continue
                        fu_account_id,error = self.check_account_name(cr, uid, fu_line[0], 'fk', context=context)
                        if error:
                            raise osv.except_osv(_('错误!'), _('%s' % error))
                        if float(fu_line[2]) != 0.0 and fu_sign != False:
                            name = fu_line[1] + fu_line[3]
                            fu_accmove_line_id = accmove_line_obj.create(cr, uid, {
                                'name': name,
                                'centralisation': 'debit',
                                'partner_id': False,
                                'account_id': fu_account_id,
                                'move_id': fu_accmove_id,
                                'journal_id': fu_journal_id[0],
                                'analytic_account_id': fu_analytic_account_id,
                                'period_id': fu_period_id[0],
                                'date': fu_date,
                                'date_maturity':fu_date,
                                'credit': 0.0,
                                'debit': float(fu_line[2]),
                            })
                            if fu_line[4] == u'收据':
                                fu_shouju += float(fu_line[2])
                            if fu_line[4] == u'发票':
                                fu_fapiao += float(fu_line[2])
                            fu_lines.append(fu_accmove_line_id)
                #print "111111111111111"
                if fu_sign == True and fu_shouju != 0.0:
                    fu_accmove_line_id1 = accmove_line_obj.create(cr, uid, {
                        'name': name,
                        'centralisation': 'credit',
                        'partner_id': False,
                        'account_id': sk_account_id2[0],
                        'move_id': fu_accmove_id,
                        'journal_id': fu_journal_id[0],
                        'period_id': fu_period_id[0],
                        'date': fu_date,
                        'date_maturity':fu_date,
                        'credit': fu_shouju,
                        'debit': 0.0,
                    })
                    fu_lines.append(fu_accmove_line_id1)
                if fu_sign == True and fu_fapiao != 0.0:
                    fu_accmove_line_id2 = accmove_line_obj.create(cr, uid, {
                        'name': name,
                        'centralisation': 'credit',
                        'partner_id': False,
                        'account_id': sk_account_id3[0],
                        'move_id': fu_accmove_id,
                        'journal_id': fu_journal_id[0],
                        'period_id': fu_period_id[0],
                        'date': fu_date,
                        'date_maturity':fu_date,
                        'credit': fu_fapiao,
                        'debit': 0.0,
                    })
                    fu_lines.append(fu_accmove_line_id2)
                if fu_sign == False:
                    accmove_line_obj.unlink(cr, uid, fu_lines)
                    accmove_obj.unlink(cr, uid, [fu_accmove_id])
                else:
                    fu_accmove = accmove_obj.browse(cr, uid, fu_accmove_id)
                    try:
                        icp = self.pool.get('ir.config_parameter')
                        h = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_host', 'False'))
                        p = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_port', 'False'))
                        u = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_user', 'False'))
                        passwd = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_passwd', 'False'))
                        db = safe_eval(icp.get_param(cr, uid, 'wgmf.oa_db', 'False'))
                        conn = MySQLdb.connect(host=h,port=3336,user=u,passwd=passwd,db=db,charset="utf8")
                        cursor=conn.cursor()
                        cursor.execute("update flow_data_207_copy set DATA_670 = \'%s\' where DATA_330 = %s and DATA_677='审核通过'" % (fu_accmove.name,fu_order.get('ref','')))
                    except Exception as e:
                        list = []
                        #print "e=============%s" % str(e)
                        conn.rollback()
                        import traceback
                        _logger.error("Oracle OA synchronization Error: %s" % e)
                        _logger.error(traceback.format_exc())
                    finally:
                        try:
                            if conn:
                                cursor.close()
                                conn.close()
                        except:
                            pass
        return res

    def oa_synchronize(self, cr, uid, ids, context=None):
        ##########检查############空凭证删除
        empty_sql="""select id from account_move where wg_type ='oa' 
               and create_uid=%s 
               and period_id in (select id from account_period where state!='done')
               and id not in (select move_id from account_move_line)"""%(uid,)
        cr.execute(empty_sql)
        empty_ids=[x[0] for x in cr.fetchall()]
        if empty_ids:
            self.pool.get('account.move').unlink(cr,uid,empty_ids)
        #######################
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        list = []
        res = self.read(cr, uid, ids, ['st_period_id','end_period_id'], context=context)
        st_period_id = res[0] and res[0]['st_period_id']
        end_period_id = res[0] and res[0]['end_period_id']
        st = st_period_id[1].split('/')
        end = end_period_id[1].split('/')
        st_period = str(st[1]) + "年" + str(st[0]) + "月"
        end_period = str(end[1]) + "年" + str(end[0]) + "月"
        baoxiao_ids = self.create_baoxoao_account(cr, uid,st_period,end_period, context=context)
        baoxiao_ids = [int(x) for x in baoxiao_ids]
        ##########检查############
        if baoxiao_ids:
            empty_sql="""select id from account_move where id in %s 
                   and id not in (select move_id from account_move_line)"""%(tuple(baoxiao_ids+[0]),)
            cr.execute(empty_sql)
            empty_ids=[x[0] for x in cr.fetchall()]
            if empty_ids:
                self.pool.get('account.move').unlink(cr,uid,empty_ids)
        ##########################空明细的删除
        list.extend(baoxiao_ids)
        result = mod_obj.get_object_reference(cr, uid, 'account', 'action_move_journal_line')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id])[0]
        result['domain'] = [('id','in',list)]
        return result
oeoa_synchronize()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: