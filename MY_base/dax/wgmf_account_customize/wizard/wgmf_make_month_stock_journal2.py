# -*- coding: utf-8 -*- #
import datetime
from datetime import timedelta, date
import time
import calendar
from calendar import monthrange
from tools.translate import _
from openerp.osv import fields, osv
import logging
from report.interface import report_int
import pooler
import xlrd
from xlrd import open_workbook,xldate_as_tuple

_logger = logging.getLogger(__name__)
class wgmf_make_month_stock_journal(osv.osv_memory):
    _name = 'wgmf.make.month.stock.journal'
    _order = "id desc"
    def _get_period(self, cr, uid, context=None):
        ctx = dict(context or {}, account_period_prefer_normal=True)
        period_ids = self.pool.get('account.period').find(cr, uid, context=ctx)
        return period_ids and period_ids[0] or False

    _columns = {
        'period_id': fields.many2one('account.period', u'会计期间', required=True),
    }
    _defaults = {
        'period_id': _get_period,
    }
    #快速创建凭证明细
    def fast_create(self,cr,uid,ids,vals):
        debit=vals.get('debit',0)
        credit=vals.get('credit',0)
        partner_id=vals.get('partner_id',0)
        analytic_account_id=vals.get('analytic_account_id',0)
        vals.update({'company_id':1,'abs_debit':abs(debit),'state':'valid'})      
        if partner_id and analytic_account_id:                                                                                      
            cr.execute("insert into account_move_line (name,centralisation,partner_id,account_id,move_id,journal_id,period_id,date,date_maturity,credit,debit,ref,company_id,analytic_account_id,abs_debit,state) values ('%(name)s','%(centralisation)s','%(partner_id)s','%(account_id)s','%(move_id)s','%(journal_id)s','%(period_id)s','%(date)s','%(date_maturity)s','%(credit)s','%(debit)s','%(ref)s','%(company_id)s','%(analytic_account_id)s','%(abs_debit)s','%(state)s') returning id;"% (vals))
            line_id=cr.fetchall()
            if not line_id:raise osv.except_osv(_('错误'), _(' 操作失败,请重试'))
            line_id=line_id[0][0]
            vals.update({'line_id':line_id,'user_id':uid,'amount':credit-debit,'unit_amount':0,'fz_journal_id':1})
            cr.execute("insert into account_analytic_line (move_id,name,period_id,general_account_id,user_id,date,account_id,journal_id,amount,unit_amount) values ('%(line_id)s','%(name)s','%(period_id)s','%(account_id)s','%(user_id)s','%(date)s','%(analytic_account_id)s','%(fz_journal_id)s','%(amount)s','%(unit_amount)s') returning id;"% (vals))
        elif partner_id or analytic_account_id:
            if partner_id:
                cr.execute("insert into account_move_line (name,centralisation,partner_id,account_id,move_id,journal_id,period_id,date,date_maturity,credit,debit,ref,company_id,abs_debit,state) values ('%(name)s','%(centralisation)s','%(partner_id)s','%(account_id)s','%(move_id)s','%(journal_id)s','%(period_id)s','%(date)s','%(date_maturity)s','%(credit)s','%(debit)s','%(ref)s','%(company_id)s','%(abs_debit)s','%(state)s') returning id;"% (vals))
            else:
                cr.execute("insert into account_move_line (name,centralisation,account_id,move_id,journal_id,period_id,date,date_maturity,credit,debit,ref,company_id,analytic_account_id,abs_debit,state) values ('%(name)s','%(centralisation)s','%(account_id)s','%(move_id)s','%(journal_id)s','%(period_id)s','%(date)s','%(date_maturity)s','%(credit)s','%(debit)s','%(ref)s','%(company_id)s','%(analytic_account_id)s','%(abs_debit)s','%(state)s') returning id;"% (vals))
                line_id=cr.fetchall()
                if not line_id:raise osv.except_osv(_('错误'), _(' 操作失败,请重试'))
                line_id=line_id[0][0]
                vals.update({'line_id':line_id,'user_id':uid,'amount':credit-debit,'unit_amount':0,'fz_journal_id':1})
                cr.execute("insert into account_analytic_line (move_id,name,period_id,general_account_id,user_id,date,account_id,journal_id,amount,unit_amount) values ('%(line_id)s','%(name)s','%(period_id)s','%(account_id)s','%(user_id)s','%(date)s','%(analytic_account_id)s','%(fz_journal_id)s','%(amount)s','%(unit_amount)s') returning id;"% (vals))
        else:
            cr.execute("insert into account_move_line (name,centralisation,account_id,move_id,journal_id,period_id,date,date_maturity,credit,debit,ref,company_id,abs_debit,state) values ('%(name)s','%(centralisation)s','%(account_id)s','%(move_id)s','%(journal_id)s','%(period_id)s','%(date)s','%(date_maturity)s','%(credit)s','%(debit)s','%(ref)s','%(company_id)s','%(abs_debit)s','%(state)s') returning id;"% (vals))
        cr.commit()
        return True
    #月末价格
    def wgmf_standard_price2(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        wiz_obj=self.browse(cr,uid,ids[0])
        product_obj = self.pool.get('product.product')
        uom_obj = self.pool.get('product.uom')
        standard_obj = self.pool.get('standard.months.price')
        move_obj = self.pool.get('stock.move')
        location_obj = self.pool.get('stock.location')
        wgkchs_obj = self.pool.get('account.wgkchs')
        #会计期间
        period_instance = wiz_obj.period_id
        sql="""select id from standard_months_price where months=%s"""%(period_instance.id)
        cr.execute(sql)
        sql_standard=cr.fetchall()
        if sql_standard:
            raise osv.except_osv(_('错误!'),_("本月月末价格已生成 如需重新计算 请先删除!"))
        if period_instance.state=='done':
            raise osv.except_osv(_('错误!'),_("当前会计期间已经关账!"))
        wgkchs_id = wgkchs_obj.search(cr, uid, [('period_id', '=', period_instance.id),('state', '!=', 'cancel')])
        if wgkchs_id:
            wgkchs = self.pool.get('account.wgkchs').browse(cr, uid, wgkchs_id[0])
            if wgkchs.state == 'approved':
                raise osv.except_osv(_('错误!'),_("本月会计凭证已过账!"))
        #from_date=period_instance.date_start + " 16:00:00"  #本月月初时间
        #计算日
        use_date = (datetime.datetime.strptime(period_instance.date_start, '%Y-%m-%d') + timedelta(days=-1)).strftime('%Y-%m-%d')
        from_date=use_date + " 16:00:00"  #本月月初时间
        #last_to_date=use_date + " 15:59:59"#上月月末时间
        to_date=period_instance.date_stop + " 15:59:59"#本月月末时间
        #可销售产品
        product_ids = product_obj.search(cr, uid, ['|','&',('active','=',True),('active','=',False),('sale_ok', '=', True)])
        state_ids='done'
        #本月入库  工厂-->在途
        cr.execute(
            'select sum(product_qty), product_id, product_uom '\
            'from stock_move '\
            'where location_id =12 '\
            'and location_dest_id =89 '\
            'and product_id IN %s '\
            'and date>=%s and date<=%s and state =%s'\
            'group by product_id,product_uom',(tuple(product_ids,),from_date,to_date,state_ids,))
        results = cr.fetchall()
        qtys_in = {}.fromkeys(product_ids, 0.0)
        for amount, prod_id, prod_uom in results:
            to_uom=product_obj.browse(cr, uid, prod_id, context=context).uom_id
            prod_uom_instance=uom_obj.browse(cr,uid,prod_uom)
            amount = uom_obj._compute_qty_obj(cr, uid, prod_uom_instance, amount,
                     to_uom, context=context)
            qtys_in[prod_id] += amount
        #本月入库退货  在途-->工厂
        cr.execute(
            'select sum(product_qty), product_id, product_uom '\
            'from stock_move '\
            'where location_id =89 '\
            'and location_dest_id =12 '\
            'and product_id IN %s '\
            'and date>=%s and date<=%s and state =%s'\
            'group by product_id,product_uom',(tuple(product_ids,),from_date,to_date,state_ids,))
        results2 = cr.fetchall()
        for amount, prod_id, prod_uom in results2:
            to_uom=product_obj.browse(cr, uid, prod_id, context=context).uom_id
            prod_uom_instance=uom_obj.browse(cr,uid,prod_uom)
            amount = uom_obj._compute_qty_obj(cr, uid, prod_uom_instance, amount,
                     to_uom, context=context)
            qtys_in[prod_id] -= amount
        #standard_months_price
        lastdate = (datetime.datetime.strptime(period_instance.date_start, '%Y-%m-%d') + timedelta(days=-1)).strftime('%Y-%m-%d')
        last_period=self.pool.get('account.period').search(cr,uid,[('date_stop','=',lastdate),('special','!=',True)])[0]
        if not last_period:raise osv.except_osv(u'警告', u'没有找到上一个会计期间')
        #求月末门店,在途的数量    
        sql="""select location.id from stock_location location 
               left join stock_warehouse w on w.lot_stock_id=location.id
               left join sale_shop shop on shop.warehouse_id=w.id 
               where shop.yymode  in ('DZD','ZMD') and location.name in ('库存','Stock')
               and location.id!=12 and location.id!=47 order by location.id desc"""
        cr.execute(sql)
        location_ids=cr.fetchall()
        md_location_ids=[line[0] for line in location_ids]
        c={ 'states': ('done',), 'what': ('in', 'out'),'from_date':from_date,'to_date': to_date ,'location':md_location_ids,'compute_child':False}#月初数量
        qtys_in_md = product_obj.get_product_available(cr, uid, product_ids, context=c)
        c={ 'states': ('done',), 'what': ('in', 'out'),'from_date':from_date,'to_date': to_date ,'location':89,'compute_child':False}
        qtys_in_zt = product_obj.get_product_available(cr, uid, product_ids, context=c)
        for prod in product_ids:
            #本月入库价
            price_in=product_obj.browse(cr, uid, prod, context=context).standard_price
            #本月入库数
            qty_in=qtys_in[prod]
            sql="""select coalesce(price,0),coalesce(qty_md,0),coalesce(qty_zt,0) from standard_months_price where product_id=%s and months=%s"""%(prod,last_period)
            cr.execute(sql)
            sql_res=cr.fetchall()
            #月初库存 月初价
            if sql_res:
                price_last=sql_res[0][0]
                qty_last_md=sql_res[0][1]
                qty_last_zt=sql_res[0][2]
            else:
                price_last=price_in
                qty_last_md=0
                qty_last_zt=0
            qty_last=qty_last_md+qty_last_zt
            price_need=price_in
            if qty_last+qty_in:
                price_need=(price_last*qty_last+price_in*qty_in)/(qty_last+qty_in)
            price_need = '%.9f' % price_need
            qty_md=qtys_in_md[prod]+qty_last_md
            qty_zt=qtys_in_zt[prod]+qty_last_zt
            cr.execute('insert into standard_months_price (product_id,months,price_tzq,price,price_in,qty_in,qty_md,qty_zt) values (%s,%s,%s,%s,%s,%s,%s,%s)',(prod,period_instance.id,price_need,price_need,price_in,qty_in,qty_md,qty_zt))
        cr.commit()
        return True
    #月末凭证信息采集
    def get_account_msg(self, cr, uid, ids, period_id, date_from, date_to, context):
        list = {}
        #主营业务成本非二次加工 #门店-->客户
        _logger.warning("%s 主营业务成本" % time.strftime('%Y-%m-%d %H:%M:%S'))
        cr.execute("""select s.name, s.project_id, sum(m.product_qty * pr.price) as price  
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr
            where m.location_id = w.lot_stock_id
            and w.id = s.warehouse_id  
                    and m.product_id = pr.product_id
            and m.state='done' 
                    and pr.months = %s
            and m.date >= %s
            and m.date <= %s
            ---and m.product_id not in (select product_id from mrp_bom where bom_id is NULL)
            -- 9: 客户库位 
            and m.location_dest_id = 9
            and s.name not in ('Your Company','积分测试')
                    ---and s.wgmf_usable = True
            group by s.name, s.project_id """,(period_id,date_from,date_to))
        zyywcb = cr.fetchall()
        for line in zyywcb:
            if line[0] not in list:
                list.update({line[0]:{'analytic_account':line[1],"zyywcb":line[2]}})
            else:
                list.get(line[0])["zyywcb"] += line[2]
        _logger.warning("%s 主营业务成本结束" % time.strftime('%Y-%m-%d %H:%M:%S'))
        #主营业务成本非二次加工退货   #客户-->门店
        cr.execute("""select s.name, s.project_id, sum(m.product_qty * pr.price) as price  
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr
            where m.location_dest_id = w.lot_stock_id
            and w.id = s.warehouse_id  
                    and m.product_id = pr.product_id
            and m.state='done' 
                    and pr.months = %s
            and m.date >= %s
            and m.date <= %s
            ---and m.product_id not in (select product_id from mrp_bom where bom_id is NULL)
            -- 9: 客户库位 
            and m.location_id = 9
            and s.name not in ('Your Company','积分测试')
                    ---and s.wgmf_usable = True
            group by s.name, s.project_id """,(period_id,date_from,date_to))
        zyywcb = cr.fetchall()
        for line in zyywcb:
            if line[0] not in list:
                list.update({line[0]:{'analytic_account':line[1],"zyywcb":-line[2]}})
            else:
                list.get(line[0])["zyywcb"] -= line[2]
        _logger.warning("%s 主营业务成本退货结束" % time.strftime('%Y-%m-%d %H:%M:%S'))
        #主营业务成本二次加工
        cr.execute("""select s.name, s.project_id, sum(m.product_qty * pr.price) as price  
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr
            where m.location_id = w.lot_stock_id
            and w.id = s.warehouse_id  
            and m.product_id = pr.product_id
            and m.state='done' 
            and pr.months = %s
            and m.date >= %s 
            and m.date <= %s
            -- 生产库位 7
            and m.location_dest_id = 7
            ---and m.product_id not in (select product_id from mrp_bom where bom_id is NULL)
            and m.origin ilike 'ECJG'
            and s.name not in ('Your Company','积分测试')
                    ---and s.wgmf_usable = True
            group by s.name, s.project_id""",(period_id,date_from,date_to))
        zyywcb2 = cr.fetchall()
        for line in zyywcb2:
            if line[0] not in list:
                list.update({line[0]:{'analytic_account':line[1],"zyywcb":line[2]}})
            else:
                if list.get(line[0]).get('zyywcb',False):
                    list.get(line[0])["zyywcb"] += line[2]
                else:
                    list.get(line[0]).update({"zyywcb":line[2]})
        #主营业务成本二次加工退货
        cr.execute("""select s.name, s.project_id, sum(m.product_qty * pr.price) as price  
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr
            where m.location_dest_id = w.lot_stock_id
            and w.id = s.warehouse_id  
            and m.product_id = pr.product_id
            and m.state='done' 
            and pr.months = %s
            and m.date >= %s 
            and m.date <= %s
            -- 生产库位 7
            and m.location_id = 7
            ---and m.product_id not in (select product_id from mrp_bom where bom_id is NULL)
            and m.origin ilike 'ECJG'
            and s.name not in ('Your Company','积分测试')
                    ---and s.wgmf_usable = True
            group by s.name, s.project_id""",(period_id,date_from,date_to))
        zyywcb2 = cr.fetchall()
        for line in zyywcb2:
            if line[0] not in list:
                list.update({line[0]:{'analytic_account':line[1],"zyywcb":-line[2]}})
            else:
                if list.get(line[0]).get('zyywcb',False):
                    list.get(line[0])["zyywcb"] -= line[2]
                else:
                    list.get(line[0]).update({"zyywcb":-line[2]})
        
        #外购入库和盘点盈亏  #门店-->在途  卖场仓  门店-->盘点
        cr.execute("""select s.name, s.project_id, m.location_dest_id as location_id, sum(m.product_qty * pr.price) as price   
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr
            where m.location_id = w.lot_stock_id 
            and w.id = s.warehouse_id 
            and m.product_id = pr.product_id
            and m.state='done' 
            and pr.months = %s
            and m.date >= %s
            and m.date <= %s
            -- 5: 盘点盈亏, 89: 在途仓 
            and m.location_dest_id in (5,89) 
            and s.name not in ('Your Company','积分测试')
            ---and s.wgmf_usable = True
            group by s.name, s.project_id, m.location_dest_id""",(period_id,date_from,date_to))
        kui = cr.fetchall()
        for line in kui:
            if line[0] not in list:
                if line[2] == 5:
                    list.update({line[0]:{'analytic_account':line[1],"pandian":line[3]}})
                if line[2] == 89:
                    list.update({line[0]:{'analytic_account':line[1],"zaitu":-line[3]}})
            else:
                if line[2] == 5:
                    if list.get(line[0]).get('pandian',False):
                        list.get(line[0])["pandian"] += line[2]
                    else:
                        list.get(line[0]).update({"pandian":line[3]})
                if line[2] == 89:
                    if list.get(line[0]).get('zaitu',False):
                        list.get(line[0])["zaitu"] -= line[3]
                    else:
                        list.get(line[0]).update({"zaitu":-line[3]})
        #在途-->门店 卖场仓----在途算内部库位用平均价
        cr.execute("""select s.name, s.project_id, m.location_id as location_id, sum(m.product_qty * pr.price) as price  
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr  
            where m.location_dest_id = w.lot_stock_id
            and w.id = s.warehouse_id 
            and m.product_id = pr.product_id
            and m.state='done' 
            and m.date >= %s
            and m.date <= %s
            and pr.months = %s
            -- 89: 在途仓 
            and m.location_id = 89
            and s.name not in ('Your Company','积分测试')
                    ---and s.wgmf_usable = True
            group by s.name, s.project_id, m.location_id""",(date_from,date_to,period_id))
        ying = cr.fetchall()
        for line in ying:
            if line[0] not in list:
                list.update({line[0]:{'analytic_account':line[1],"zaitu":line[3]}})
            else:
                if list.get(line[0]).get('zaitu',False):
                    list.get(line[0])["zaitu"]+= line[3]
                else:
                    list.get(line[0]).update({"zaitu":line[3]})
        #盘点-->门店
        cr.execute("""select s.name, s.project_id, m.location_id as location_id, sum(m.product_qty * pr.price) as price  
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr  
            where m.location_dest_id = w.lot_stock_id
            and w.id = s.warehouse_id 
            and m.product_id = pr.product_id
            and m.state='done' 
            and pr.months = %s
            and m.date >= %s
            and m.date <= %s
            -- 5: 盘点盈亏 
            and m.location_id = 5 
            and s.name not in ('Your Company','积分测试')
            ---and s.wgmf_usable = True
            group by s.name, s.project_id, m.location_id""",(period_id,date_from,date_to))
        ying = cr.fetchall()
        for line in ying:
            if line[0] not in list:
                list.update({line[0]:{'analytic_account':line[1],"pandian":-line[3]}})
            else:
                if list.get(line[0]).get('pandian',False):
                    list.get(line[0])["pandian"] -= line[3]
                else:
                    list.get(line[0]).update({"pandian":-line[3]})
        #76: 积分兑换, 81: 推广, 82: 辅料
        cr.execute("""select s.name, s.project_id, m.location_id, m.location_dest_id,  sum(m.product_qty * pr.price) as price   
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr
            where m.location_id = w.lot_stock_id 
            and w.id = s.warehouse_id 
            and m.product_id = pr.product_id
            and m.state='done' 
            and pr.months = %s
            and m.date >= %s
            and m.date <= %s
            -- 76: 积分兑换, 81: 推广, 82: 辅料 16278:加班食品
            and m.location_dest_id in (76,81,82,16278)
            and s.name not in ('Your Company','积分测试')
            ---and s.wgmf_usable = True
            group by s.name, s.project_id, m.location_id, m.location_dest_id""",(period_id,date_from,date_to))
        jf_tg_fl = cr.fetchall()
        for line in jf_tg_fl:
            if line[0] not in list:
                if line[3] == 76:
                    list.update({line[0]:{'analytic_account':line[1],"jf":line[4]}})
                if line[3] == 81:
                    list.update({line[0]:{'analytic_account':line[1],"tg":line[4]}})
                if line[3] == 82:
                    list.update({line[0]:{'analytic_account':line[1],"fl":line[4]}})
                if line[3] == 16278:
                    list.update({line[0]:{'analytic_account':line[1],"jbsp":line[4]}})
                    
            else:
                if line[3] == 76:
                    if list.get(line[0]).get('jf',False):
                        list.get(line[0])["jf"] += line[4]
                    else:
                        list.get(line[0]).update({"jf":line[4]})
                if line[3] == 81:
                    if list.get(line[0]).get('tg',False):
                        list.get(line[0])["tg"] += line[4]
                    else:
                        list.get(line[0]).update({"tg":line[4]})
                if line[3] == 82:
                    if list.get(line[0]).get('fl',False):
                        list.get(line[0])["fl"] += line[4]
                    else:
                        list.get(line[0]).update({"fl":line[4]})
                if line[3] == 16278:
                    if list.get(line[0]).get('jbsp',False):
                        list.get(line[0])["jbsp"] += line[4]
                    else:
                        list.get(line[0]).update({"jbsp":line[4]})
        #退货  #76: 积分兑换, 81: 推广, 82: 辅料 16278:加班食品
        cr.execute("""select s.name, s.project_id, m.location_dest_id, m.location_id,  sum(m.product_qty * pr.price) as price   
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr
            where m.location_dest_id = w.lot_stock_id 
            and w.id = s.warehouse_id
            and m.product_id = pr.product_id
            and m.state='done' 
            and pr.months = %s
            and m.date >= %s
            and m.date <= %s
            -- 76: 积分兑换, 81: 推广, 82: 辅料
            and m.location_id in (76,81,82,16278)
            and s.name not in ('Your Company','积分测试')
            ---and s.wgmf_usable = True
            group by s.name, s.project_id, m.location_id, m.location_dest_id""",(period_id,date_from,date_to))
        return_jf_tg_fl = cr.fetchall()
        for line in return_jf_tg_fl:
            if line[0] not in list:
                if line[3] == 76:
                    list.update({line[0]:{'analytic_account':line[1],"jf":-line[4]}})
                if line[3] == 81:
                    list.update({line[0]:{'analytic_account':line[1],"tg":-line[4]}})
                if line[3] == 82:
                    list.update({line[0]:{'analytic_account':line[1],"fl":-line[4]}})
                if line[3] == 16278:
                    list.update({line[0]:{'analytic_account':line[1],"jbsp":-line[4]}})
            else:
                if line[3] == 76:
                    if list.get(line[0]).get('jf',False):
                        list.get(line[0])["jf"] -= line[4]
                    else:
                        list.get(line[0]).update({"jf":-line[4]})
                if line[3] == 81:
                    if list.get(line[0]).get('tg',False):
                        list.get(line[0])["tg"] -= line[4]
                    else:
                        list.get(line[0]).update({"tg":-line[4]})
                if line[3] == 82:
                    if list.get(line[0]).get('fl',False):
                        list.get(line[0])["fl"] -= line[4]
                    else:
                        list.get(line[0]).update({"fl":-line[4]})
                if line[3] == 16278:
                    if list.get(line[0]).get('jbsp',False):
                        list.get(line[0])["jbsp"] -= line[4]
                    else:
                        list.get(line[0]).update({"jbsp":-line[4]})                        
        #到货误差，冲顶加工，试吃，报损，赠品
        cr.execute("""select s.name, s.project_id, m.location_id, m.location_dest_id,  sum(m.product_qty * pr.price) as price   
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr
            where m.location_id = w.lot_stock_id 
            and w.id = s.warehouse_id 
            and m.product_id = pr.product_id
            and m.state='done' 
            and pr.months = %s
            and m.date >= %s
            and m.date <= %s
            -- 'DHWC': , 'CHONGDINGJIAGONG': , 'SHICHI', 'BAOSUN': ,'ZENGPIN': 
            ---and m.origin in ('DHWC', 'CHONGDINGJIAGONG', 'SHICHI', 'BAOSUN','ZENGPIN')
            ---冲顶 80,到货误差 84,试吃 77,报损 79,赠品 78
            and m.location_dest_id in (77,78,79,80,84)
            and s.name not in ('Your Company','积分测试')
            ---and s.wgmf_usable = True
            group by s.name, s.project_id, m.location_id, m.location_dest_id""",(period_id,date_from,date_to))
        wx_cd_sc_bs_zp = cr.fetchall()
        for line in wx_cd_sc_bs_zp:
            if line[0] not in list:
                if line[3] == 84:
                    list.update({line[0]:{'analytic_account':line[1],"DHWC":line[4]}})
                if line[3] == 80:
                    list.update({line[0]:{'analytic_account':line[1],"CHONGDINGJIAGONG":line[4]}})
                if line[3] == 77:
                    list.update({line[0]:{'analytic_account':line[1],"SHICHI":line[4]}})
                if line[3] == 79:
                    list.update({line[0]:{'analytic_account':line[1],"BAOSUN":line[4]}})
                if line[3] == 78:
                    list.update({line[0]:{'analytic_account':line[1],"ZENGPIN":line[4]}})
            else:
                if line[3] == 84:
                    if list.get(line[0]).get('DHWC',False):
                        list.get(line[0])["DHWC"] += line[4]
                    else:
                        list.get(line[0]).update({"DHWC":line[4]})
                if line[3] == 80:
                    if list.get(line[0]).get('CHONGDINGJIAGONG',False):
                        list.get(line[0])["CHONGDINGJIAGONG"] += line[4]
                    else:
                        list.get(line[0]).update({"CHONGDINGJIAGONG":line[4]})
                if line[3] == 77:
                    if list.get(line[0]).get('SHICHI',False):
                        list.get(line[0])["SHICHI"] += line[4]
                    else:
                        list.get(line[0]).update({"SHICHI":line[4]})
                if line[3] == 79:
                    if list.get(line[0]).get('',False):
                        list.get(line[0])["BAOSUN"] += line[4]
                    else:
                        list.get(line[0]).update({"BAOSUN":line[4]})
                if line[3] == 78:
                    if list.get(line[0]).get('ZENGPIN',False):
                        list.get(line[0])["ZENGPIN"] += line[4]
                    else:
                        list.get(line[0]).update({"ZENGPIN":line[4]})
        #退货#到货误差，冲顶加工，试吃，报损，赠品
        cr.execute("""select s.name, s.project_id, m.location_dest_id, m.location_id,  sum(m.product_qty * pr.price) as price   
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr
            where m.location_dest_id = w.lot_stock_id 
            and w.id = s.warehouse_id 
            and m.product_id = pr.product_id
            and m.state='done' 
            and pr.months = %s
            and m.date >= %s
            and m.date <= %s
            -- 'DHWC': , 'CHONGDINGJIAGONG': , 'SHICHI', 'BAOSUN': ,'ZENGPIN': 
            ---and m.origin in ('DHWC', 'CHONGDINGJIAGONG', 'SHICHI', 'BAOSUN','ZENGPIN')
            ---冲顶 80,到货误差 84,试吃 77,报损 79,赠品 78
            and m.location_id in (77,78,79,80,84)
            and s.name not in ('Your Company','积分测试')
            group by s.name, s.project_id, m.location_dest_id, m.location_id""",(period_id,date_from,date_to))
        return_wx_cd_sc_bs_zp = cr.fetchall()
        for line in return_wx_cd_sc_bs_zp:
            if line[0] not in list:
                if line[3] == 84:
                    list.update({line[0]:{'analytic_account':line[1],"DHWC":-line[4]}})
                if line[3] == 80:
                    list.update({line[0]:{'analytic_account':line[1],"CHONGDINGJIAGONG":-line[4]}})
                if line[3] ==77:
                    list.update({line[0]:{'analytic_account':line[1],"SHICHI":-line[4]}})
                if line[3] == 79:
                    list.update({line[0]:{'analytic_account':line[1],"BAOSUN":line[4]}})
                if line[3] == 78:
                    list.update({line[0]:{'analytic_account':line[1],"ZENGPIN":-line[4]}})
            else:
                if line[3] == 84:
                    if list.get(line[0]).get('DHWC',False):
                        list.get(line[0])["DHWC"] -= line[4]
                    else:
                        list.get(line[0]).update({"DHWC":-line[4]})
                if line[3] == 80:
                    if list.get(line[0]).get('CHONGDINGJIAGONG',False):
                        list.get(line[0])["CHONGDINGJIAGONG"] -= line[4]
                    else:
                        list.get(line[0]).update({"CHONGDINGJIAGONG":-line[4]})
                if line[3] == 77:
                    if list.get(line[0]).get('SHICHI',False):
                        list.get(line[0])["SHICHI"] -= line[4]
                    else:
                        list.get(line[0]).update({"SHICHI":-line[4]})
                if line[3] == 79:
                    if list.get(line[0]).get('',False):
                        list.get(line[0])["BAOSUN"] -= line[4]
                    else:
                        list.get(line[0]).update({"BAOSUN":-line[4]})
                if line[3] == 78:
                    if list.get(line[0]).get('ZENGPIN',False):
                        list.get(line[0])["ZENGPIN"] -= line[4]
                    else:
                        list.get(line[0]).update({"ZENGPIN":-line[4]})
        #总部客情16839 
        cr.execute("""select s.name, sum(m.product_qty * pr.price),coalesce(m.origin,'bm_qt') as price  
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr  
            where m.location_id = w.lot_stock_id
            and w.id = s.warehouse_id 
            and m.product_id = pr.product_id
            and m.state='done' 
            and m.date >= %s
            and m.date <= %s
            and pr.months = %s
            -- 16839 :总部客情
            and m.location_dest_id = 16839 
            and s.name not in ('Your Company','积分测试')
            group by s.name,m.origin""",(date_from,date_to,period_id))
        zb = cr.fetchall()
        for line in zb:
            dic={"zbkq":{line[2]:line[1]} }
            xx=line[0]
            if line[0] not in list:
                list[line[0]]=dic
            else:
                if list.get(line[0]).get('zbkq',False):
                    if list.get(line[0])["zbkq"].get(line[2],False):
                        list.get(line[0])["zbkq"][line2]+=line[1]
                    else:
                        list.get(line[0])["zbkq"].update({line[2]:line[1]})
                else:
                    list.get(line[0]).update(dic)
        #总部客情退货16839 
        cr.execute("""select s.name, -sum(m.product_qty * pr.price),coalesce(m.origin,'bm_qt') as price  
            from stock_move m, stock_warehouse w, sale_shop s, standard_months_price pr  
            where m.location_dest_id = w.lot_stock_id
            and w.id = s.warehouse_id 
            and m.product_id = pr.product_id
            and m.state='done' 
            and m.date >= %s
            and m.date <= %s
            and pr.months = %s
            -- 16839 :总部客情
            and m.location_id = 16839 
            and s.name not in ('Your Company','积分测试')
            group by s.name,m.origin""",(date_from,date_to,period_id))
        zb = cr.fetchall()
        for line in zb:
            dic={"zbkq":{line[2]:line[1]} }
            xx=line[0]
            if line[0] not in list:
                list[line[0]]=dic
            else:
                if list.get(line[0]).get('zbkq',False):
                    if not list.get(line[0])["zbkq"].get(line[2],False):
                        list.get(line[0])["zbkq"].update({line[2]:line[1]})
                    else:
                        list.get(line[0])["zbkq"][line[2]]+= line[1]
                else:
                    list.get(line[0]).update(dic)
        return list
    #生成月末凭证
    def wgmf_make_stock_journal(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        datas={}
        location_list = []
        #shop_obj = self.pool.get('sale.shop')
        move_obj = self.pool.get('stock.move')
        journal_obj = self.pool.get('account.journal')
        accmove_obj = self.pool.get('account.move')
        wgkchs_obj = self.pool.get('account.wgkchs')
        accmove_line_obj = self.pool.get('account.move.line')
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        res = self.read(cr, uid, ids, ['period_id'], context=context)
        period_id = res[0] and res[0]['period_id']
        #period = self.pool.get('account.period').browse(cr, uid, period_id[0])
        wgkchs_id = wgkchs_obj.search(cr, uid, [('period_id', '=', period_id[0]),('state', '!=', 'cancel')])
        if wgkchs_id:
            wgkchs = self.pool.get('account.wgkchs').browse(cr, uid, wgkchs_id[0])
            if wgkchs.state == 'approved':
                raise osv.except_osv(_('错误!'),_("本月会计凭证已过账!"))
            if wgkchs.state == 'draft':
                wgkchs_id = wgkchs_id[0]
        if not wgkchs_id:
            wgkchs_name = self.pool.get('ir.sequence').get(cr, uid, 'account.wgkchs') or '/'
            wgkchs_id = wgkchs_obj.create(cr, uid, {
                'name': wgkchs_name,
                'period_id': period_id[0],
            })
        d = period_id[1].split('/')
        day = calendar.monthrange(int(d[1]),int(d[0]))[1]
        date1 = str(d[1]) + "-" + str(d[0]) + "-" + str(day)
        fro = str(d[1]) + "-" + str(d[0]) + "-" + "01" + " 00:00:00"
        date_from = (datetime.datetime.strptime(fro,'%Y-%m-%d %H:%M:%S') - datetime.timedelta(hours=8)).strftime('%Y-%m-%d %H:%M:%S')
        date_to = date1 + " 15:59:59"
        src = "外购入库" + "/" + period_id[1]
        journal_id = journal_obj.search(cr, uid, [('name', '=', '记销五')])[0]
        if not journal_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到对应会计凭证!"))
        _logger.warning("%s 准备生成凭证开始" % time.strftime('%Y-%m-%d %H:%M:%S'))
        list = self.get_account_msg(cr, uid, ids, period_id[0], date_from, date_to, context)
        _logger.warning("%s 获取生成凭证信息结束" % time.strftime('%Y-%m-%d %H:%M:%S'))
        sql="""select ref from account_move where period_id=%s and ref like '%%进销存%%'"""%period_id[0]
        cr.execute(sql)
        ref_list=[x[0] for x in cr.fetchall()]
        for k,v in list.items():
            s = k + "进销存" + period_id[1]
            if s in ref_list:continue
            jbsp=zyywcb = zaitu = dhwc = tg = fl = pandian = cj = sc = bs = zp = jf = zbkq=0.0
            if v.get('zyywcb',False) is not None:
                zyywcb = v.get('zyywcb',False)
            if v.get('zaitu',False) is not None:
                zaitu = v.get('zaitu',False)
            if v.get('DHWC',False) is not None:
                dhwc = v.get('DHWC',False)
            if v.get('tg',False) is not None:
                tg = v.get('tg',False)
            if v.get('fl',False) is not None:
                fl = v.get('fl',False)
            if v.get('pandian',False) is not None:
                pandian = v.get('pandian',False)
            if v.get('CHONGDINGJIAGONG',False) is not None:
                cj = v.get('CHONGDINGJIAGONG',False)
            if v.get('SHICHI',False) is not None:
                sc = v.get('SHICHI',False)
            if v.get('BAOSUN',False) is not None:
                bs = v.get('BAOSUN',False)
            if v.get('ZENGPIN',False) is not None:
                zp = v.get('ZENGPIN',False)
            if v.get('jf',False) is not None:
                jf = v.get('jf',False)
            if v.get('jbsp',False) is not None:
                jbsp = v.get('jbsp',False)
            if v.get('zbkq',False) is not None:
                zbkq = sum(a for a in v.get('zbkq',{}).values())
            total = float('%.2f' % zyywcb) + float('%.2f' % dhwc) + float('%.2f' % tg) + float('%.2f' % fl) + float('%.2f' % pandian) + float('%.2f' % cj) + float('%.2f' % sc) + float('%.2f' % bs) + float('%.2f' % zp) + float('%.2f' % jf)+float('%.2f' % jbsp)+float('%.2f' % zbkq)
            #加条件方便继续生成
            if zyywcb!=0 or dhwc!=0 or tg!=0 or fl!=0 or pandian!=0 or cj!=0 or sc!=0 or bs!=0 or zp!=0 or jf!=0 or jbsp!=0 or zbkq!=0 or zaitu!=0:
            #if total != 0.00 or float('%.2f' % zaitu) != 0.00:
                account_move_id = accmove_obj.create(cr, uid, {
                    'name': '/',
                    'journal_id': journal_id,
                    'ref': s,
                    'period_id': period_id[0],
                    'date': date1,
                    'date_maturity': date1,
                })
                if float('%.2f' % zaitu) != 0.00:
                    self.fast_create(cr, uid,ids,{
                        'name': "卖场仓(外购入库)",
                        'centralisation': 'credit',
                        'partner_id': False,
                        'account_id': 594, # 2241.01.08-->1405.02.01
                        'move_id': account_move_id,
                        #'analytic_account_id': v.get('analytic_account',False),
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': float('%.2f' % zaitu),
                        'debit': 0.0,
                        'ref': s,
                    })
                    self.fast_create(cr, uid,ids, {
                        'name': "卖场仓",
                        'centralisation': 'credit',
                        'partner_id': False,
                        'account_id': 595,#1405.03.02-->1405.02.02
                        'move_id': account_move_id,
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % zaitu),
                        'ref': s,
                    })
                if zyywcb != 0.0:
                    self.fast_create(cr, uid,ids, {
                        'name': "主营业务成本",
                        'centralisation': 'debit',
                        'partner_id': False,
                        'account_id': 801, # 6401
                        'move_id': account_move_id,
                        'analytic_account_id': v.get('analytic_account',False),
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % zyywcb),
                        'ref': s,
                    })
                if dhwc != 0.0:
                    self.fast_create(cr, uid,ids, {
                        'name': "到货误差",
                        'centralisation': 'debit',
                        'partner_id': False,
                        'account_id': 880, # 6601.05.04
                        'move_id': account_move_id,
                        'analytic_account_id':6769, #v.get('analytic_account',False),
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % dhwc),
                        'ref': s,
                    })
                if tg != 0.0:
                    self.fast_create(cr, uid,ids, {
                        'name': "推广费",
                        'centralisation': 'debit',
                        'partner_id': False,
                        'account_id': 873, # 6601.04.04
                        'move_id': account_move_id,
                        'analytic_account_id': 6768,
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % tg),
                        'ref': s,
                    })
                if fl != 0.0:
                    self.fast_create(cr, uid,ids ,{
                        'name': "常用辅料",
                        'centralisation': 'debit',
                        'partner_id': False,
                        'account_id': 886, # 6601.06.03
                        'move_id': account_move_id,
                        'analytic_account_id': v.get('analytic_account',False),
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % fl),
                        'ref': s,
                    })
                if pandian != 0.0:
                    self.fast_create(cr, uid, ids,{
                        'name': "盘点盈亏",
                        'centralisation': 'debit',
                        'partner_id': False,
                        'account_id': 888, # 6601.06.05
                        'move_id': account_move_id,
                        'analytic_account_id': 6769,
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % pandian),
                        'ref': s,
                    })
                if cj != 0.0:
                    self.fast_create(cr, uid,ids, {
                        'name': "冲顶加工",
                        'centralisation': 'debit',
                        'partner_id': False,
                        'account_id': 878, # 6601.05.02
                        'move_id': account_move_id,
                        'analytic_account_id': v.get('analytic_account',False),
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % cj),
                        'ref': s,
                    })
                if sc != 0.0:
                    self.fast_create(cr, uid,ids, {
                        'name': "试吃",
                        'centralisation': 'debit',
                        'partner_id': False,
                        'account_id': 870, # 6601.04.01
                        'move_id': account_move_id,
                        'analytic_account_id': v.get('analytic_account',False),
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % sc),
                        'ref': s,
                    })
                if bs != 0.0:
                    self.fast_create(cr, uid, ids,{
                        'name': "报损",
                        'centralisation': 'debit',
                        'partner_id': False,
                        'account_id': 879, # 6601.05.03
                        'move_id': account_move_id,
                        'analytic_account_id': 6769,#v.get('analytic_account',False),
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % bs),
                        'ref': s,
                    })
                if zp != 0.0:
                    self.fast_create(cr, uid, ids,{
                        'name': "赠品",
                        'centralisation': 'debit',
                        'partner_id': False,
                        'account_id': 1011,#871, # 6601.04.02
                        'move_id': account_move_id,
                        'analytic_account_id': v.get('analytic_account',False),
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % zp),
                        'ref': s,
                    })
                if jf != 0.0:
                    self.fast_create(cr, uid,ids, {
                        'name': "会员兑换",
                        'centralisation': 'debit',
                        'partner_id': False,
                        'account_id': 875, # 6601.04.06
                        'move_id': account_move_id,
                        'analytic_account_id': 6769,
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % jf),
                        'ref':s,
                    })
                if jbsp != 0.0:
                    self.fast_create(cr, uid,ids, {
                        'name': "加班福利",
                        'centralisation': 'debit',
                        'partner_id': False,
                        'account_id': 688, # 6601.04.06
                        'move_id': account_move_id,
                        'analytic_account_id': False,
                        'journal_id': journal_id,
                        'period_id': period_id[0],
                        'date': date1,
                        'date_maturity' : date1,
                        'credit': 0.0,
                        'debit': float('%.2f' % jbsp),
                        'ref':s,
                    })
                self.fast_create(cr, uid,ids, {
                    'name': "卖场仓",
                    'centralisation': 'credit',
                    'partner_id': False,
                    'account_id': 595,#1405.03.02-->1405.02.02
                    'move_id': account_move_id,
                    'journal_id': journal_id,
                    'period_id': period_id[0],
                    'date': date1,
                    'date_maturity' : date1,
                    'credit': total,
                    'debit': 0.0,
                    'ref':s,
                })
                if zbkq != 0.0:
                    zbkq_dic=v.get('zbkq',False)
                    bm_dic={'bm_ka':("总部客情:KA部",813,7433),'bm_zd':('总部客情:终端管理部',813,6767),
                     'bm_xg':("总部客情:销售管理部",813,6769),'bm_dc':("总部客情:督察部",813,6773), 
                     'bm_zj':("总部客情:总经办",813,6772),'bm_cw':("总部客情:财务部",813,6765),
                     'bm_qt':("总部客情:其他部门",688,False)
                               }
                    for bm in zbkq_dic.keys():
                        amount=zbkq_dic[bm]
                        if bm not in bm_dic.keys():bm='bm_qt'
                        self.fast_create(cr, uid,ids, {
                            'name': bm_dic[bm][0],
                            'centralisation': 'credit',
                            'partner_id': False,
                            'account_id': bm_dic[bm][1],
                            'analytic_account_id': bm_dic[bm][2],
                            'move_id': account_move_id,
                            'journal_id': journal_id,
                            'period_id': period_id[0],
                            'date': date1,
                            'date_maturity' : date1,
                            'credit': 0,
                            'debit': float('%.2f' % amount),
                            'ref':s,
                        })
        _logger.warning("%s 结束生成凭证" % time.strftime('%Y-%m-%d %H:%M:%S'))
        #工厂到在途 用入库价
        cr.execute("""select COALESCE(sum(m.product_qty * pr.price_in),0) as price  
            from stock_move m left join standard_months_price pr on pr.product_id=m.product_id
            where m.location_dest_id = 89
                    and m.location_id = 12
            and m.state='done' 
            and m.date >= %s
            and m.date <= %s
            and pr.months=%s
            ---and m.product_id not in (select product_id from mrp_bom where bom_id is NULL)
             """,(date_from,date_to,period_id[0]))
        gc_zc= cr.fetchall()
        #在途-->gc 用入库价
        cr.execute("""select COALESCE(sum(m.product_qty * pr.price_in),0) as price  
            from stock_move m left join standard_months_price pr on pr.product_id=m.product_id
            where m.location_dest_id = 12
                    and m.location_id = 89
            and m.state='done'
            and m.date >= %s
            and m.date <= %s
            and pr.months=%s
            ---and m.product_id not in (select product_id from mrp_bom where bom_id is NULL)
             """,(date_from,date_to,period_id[0]))
        zc_gc= cr.fetchall()
        amount= gc_zc[0][0]-zc_gc[0][0]
        if amount:
            line_id = accmove_obj.create(cr, uid, {
                    'name': '/',
                    'journal_id': journal_id,
                    'ref': '工厂到在途供应链'+period_id[1],
                    'period_id': period_id[0],
                    'date': date1,
                    'date_maturity': date1,
                })
            self.fast_create(cr, uid,ids, {
                'name': "制造事业部",
                'centralisation': 'credit',
                'partner_id': False,
                'account_id': 650,#制造事业部 2241.01.08
                'move_id': line_id,
                'journal_id': journal_id,
                'period_id': period_id[0],
                'date': date1,
                'date_maturity' : date1,
                'credit': amount,
                'debit': 0.0,
                'state':'valid',
                'ref': '工厂到在途供应链'+period_id[1],
            })
            self.fast_create(cr, uid,ids,{
                'name': "五谷总仓",
                'centralisation': 'credit',
                'partner_id': False,
                'account_id': 594,#五谷总仓 1405.02.01
                'move_id': line_id,
                'journal_id': journal_id,
                'period_id': period_id[0],
                'date': date1,
                'date_maturity' : date1,
                'credit': 0.0,
                'debit': amount,
                'state':'valid',
                'ref': '工厂到在途供应链'+period_id[1],
            })            
        _logger.warning("%s 结束生成工厂--在途凭证" % time.strftime('%Y-%m-%d %H:%M:%S'))
        return True
    #优化生成月末价格    
    def wgmf_standard_price2_sql(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        wiz_obj=self.browse(cr,uid,ids[0])
        product_obj = self.pool.get('product.product')
        uom_obj = self.pool.get('product.uom')
        standard_obj = self.pool.get('standard.months.price')
        move_obj = self.pool.get('stock.move')
        location_obj = self.pool.get('stock.location')
        wgkchs_obj = self.pool.get('account.wgkchs')
        #会计期间
        period_instance = wiz_obj.period_id
        sql="""select id from standard_months_price where months=%s"""%(period_instance.id)
        cr.execute(sql)
        sql_standard=cr.fetchall()
        if sql_standard:
            raise osv.except_osv(_('错误!'),_("本月月末价格已生成 如需重新计算 请先删除!"))
        if period_instance.state=='done':
            raise osv.except_osv(_('错误!'),_("当前会计期间已经关账!"))
        wgkchs_id = wgkchs_obj.search(cr, uid, [('period_id', '=', period_instance.id),('state', '!=', 'cancel')])
        if wgkchs_id:
            wgkchs = self.pool.get('account.wgkchs').browse(cr, uid, wgkchs_id[0])
            if wgkchs.state == 'approved':
                raise osv.except_osv(_('错误!'),_("本月会计凭证已过账!"))
        #from_date=period_instance.date_start + " 16:00:00"  #本月月初时间
        #计算日
        use_date = (datetime.datetime.strptime(period_instance.date_start, '%Y-%m-%d') + timedelta(days=-1)).strftime('%Y-%m-%d')
        #---------------------------------------------------------
        start_time=use_date + " 16:00:00"  #本月月初时间
        end_time=period_instance.date_stop + " 15:59:59"#本月月末时间
        #---------------------------------------------------------
        #standard_months_price
        lastdate = (datetime.datetime.strptime(period_instance.date_start, '%Y-%m-%d') + timedelta(days=-1)).strftime('%Y-%m-%d')
        #---------------------------------------------------------
        last_period=self.pool.get('account.period').search(cr,uid,[('date_stop','=',lastdate),('special','!=',True)])[0]
        period_id = wiz_obj.period_id.id
        #---------------------------------------------------------
        if not last_period:raise osv.except_osv(u'警告', u'没有找到上一个会计期间')
        dic={'start_time':start_time,'end_time':end_time,'last_period':last_period,'period_id':period_id}
        sql="""
        insert into standard_months_price (product_id,months,price_tzq,price,qty_md,qty_zt,price_in,qty_in)
            (
            with data_sql as 
            (
            ------------------------------------------------------
            ---外购入库
            ------------------------------------------------------
            with data_in as 
            (---外购入库退货(zt-gc)
            select id,standard_price,sum(qty) as qty from 
            (
            select b.id,c.standard_price,-sum(a.product_qty) as qty from stock_move a 
            left join product_product b on b.id=a.product_id
            left join product_template c on c.id=b.product_tmpl_id
            where a.location_id=89 and a.location_dest_id=12 and a.state='done' 
            and a.date>='%(start_time)s' and a.date<='%(end_time)s' ---and  b.id=171
            group by b.id,c.id 
            union all
            ----外购入库(gc-zt)
            select b.id,c.standard_price,sum(a.product_qty) as qty from stock_move a 
            left join product_product b on b.id=a.product_id
            left join product_template c on c.id=b.product_tmpl_id
            where a.location_id=12 and a.location_dest_id=89 and a.state='done' 
            and a.date>='%(start_time)s' and a.date<='%(end_time)s' ---and b.id=171
            group by b.id,c.id ) data_in_ori group by id,standard_price
            ),
            -------------------------------------------
            ---在途仓新增量(zt add)
            -------------------------------------------
            data_zt_add as (
            select product_id,sum(qty) as qty from 
            (
            select product_id,-sum(product_qty) as qty from stock_move
            where state='done' and date>='%(start_time)s' and date<='%(end_time)s' ---and product_id=171 
            and location_id=89 and location_dest_id!=89  
            group by product_id
            union all
            select product_id,sum(product_qty) as qty from stock_move 
            where state='done' and date>='%(start_time)s' and date<='%(end_time)s' ---and product_id=171 
            and location_id!=89 and location_dest_id=89 
             group by product_id) data_add_ori group by product_id
            ),
            ---------------------------------------------
            ----门店增量
            -------------------------------------------
            data_md_add as (
            select product_id ,sum(qty) as qty from 
            (--门店本月发出
            select product_id,-sum(product_qty) as qty  from stock_move 
            where state='done' and date>='%(start_time)s' and date<='%(end_time)s' ---and product_id=171 
            and location_id in (--门店仓库位
            select b.lot_stock_id as id from sale_shop a 
            left join stock_warehouse b on a.warehouse_id=b.id
            where a.yymode  in ('DZD','ZMD') and b.lot_stock_id not in (12,47)
            )
            and location_dest_id not in (--门店仓库位
            select b.lot_stock_id as id from sale_shop a 
            left join stock_warehouse b on a.warehouse_id=b.id
            where a.yymode  in ('DZD','ZMD') and b.lot_stock_id not in (12,47)
            )  group by product_id
            union all
            --门店本月入库
            select product_id,sum(product_qty) as qty from stock_move 
            where state='done' and date>='%(start_time)s' and date<='%(end_time)s' ---and product_id=171 
            and location_id not in (--门店仓库位
            select b.lot_stock_id as id from sale_shop a 
            left join stock_warehouse b on a.warehouse_id=b.id
            where a.yymode  in ('DZD','ZMD') and b.lot_stock_id not in (12,47)
            )
            and location_dest_id  in (--门店仓库位
            select b.lot_stock_id as id from sale_shop a 
            left join stock_warehouse b on a.warehouse_id=b.id
            where a.yymode  in ('DZD','ZMD') and b.lot_stock_id not in (12,47)
            ) group by product_id
            ) date_md_add_ori group by product_id
            )
            -------------------------------------------
            ---主查询
            -------------------------------------------
            select 
            ---data_in.id as 入库id,
            ---a.product_id as 上月月末价产品id,
            ---data_zt_add.product_id as 在途新增id,
            ---data_md_add.product_id as 门店新增id,
            pp.id as 产品id ,
            ---data_in.standard_price as 本月入库价, 
            coalesce(data_in.standard_price,pt.standard_price) as 本月入库价, 
            coalesce(data_in.qty,0) as 本月入库数,
            coalesce(a.qty_md,0) as 上月门店数量,
            coalesce(a.qty_zt,0) as 上月在途数量,
            coalesce(data_zt_add.qty,0) as 在途新增量,
            coalesce(a.qty_zt,0)+coalesce(data_zt_add.qty,0) as 月末在途数量,
            coalesce(data_md_add.qty,0) as 门店新增量,
            coalesce(data_md_add.qty,0)+coalesce(a.qty_md,0) as 月末门店数量,
            coalesce(data_in.standard_price,0)*coalesce(data_in.qty,0)+coalesce(a.price,0)*coalesce(a.qty_md+a.qty_zt,0) as 总金额,
            coalesce(a.qty_md+a.qty_zt,0)+coalesce(data_in.qty,0) as 总数量,
            case when coalesce(a.qty_md+a.qty_zt,0) +coalesce( data_in.qty,0)!=0
            then (coalesce(data_in.standard_price,0)*coalesce(data_in.qty,0)+coalesce(a.price,0)*coalesce(a.qty_md+a.qty_zt,0)) /  (coalesce(a.qty_md+a.qty_zt,0) +coalesce( data_in.qty,0))
            else coalesce(data_in.standard_price,a.price,0) end as 本月月末价格
            from product_product pp
            join product_template pt on pt.id=pp.product_tmpl_id
            full join data_in on data_in.id=pp.id 
            full join standard_months_price a on (a.product_id=pp.id and a.months=%(last_period)s)
            full join data_zt_add on data_zt_add.product_id=pp.id
            full join data_md_add on data_md_add.product_id=pp.id
            where  pt.sale_ok='t'
            order by 产品id)
            select 产品id as product_id,%(period_id)s as months, 本月月末价格 as price_tzq, 本月月末价格 as price, 月末门店数量 as qty_md,
            月末在途数量 as qty_zt,本月入库价 as price_in,本月入库数 as qty_in
            from data_sql)"""%dic
        _logger.warning("================开始执行sql:%s" % time.strftime('%Y-%m-%d %H:%M:%S'))
        cr.execute(sql)
        _logger.warning("================结算sql:%s" % time.strftime('%Y-%m-%d %H:%M:%S'))
        cr.commit()
        return True
