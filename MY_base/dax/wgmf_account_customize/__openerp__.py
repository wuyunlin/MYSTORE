# -*- encoding: utf-8 -*-
{
    'name': "五谷财务开发",
    'description': "wgmf_account_customize",
    'version': '0.1',
    'category': 'Generic Modules/report',
    'depends': ['wgmf_customize','account','account_currency_fix','pos_super','account_voucher','oscg_approve'],
    'data': [],
    'update_xml': [
                   'wgmf_account_seq.xml',
                   'wizard/wgmf_make_month_stock_journal_view.xml',
                   'wizard/oeoa_synchronize_view.xml',
                   'wgmf_account_customize_view.xml',
                   'wizard/wg_pzgz_view.xml',
                   'wizard/wg_pzsh_view.xml',
                   #'place_sequence.xml',
                  ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
