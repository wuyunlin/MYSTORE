# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.tools.safe_eval import safe_eval
from openerp import SUPERUSER_ID
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp
import time
import MySQLdb
import calendar
from calendar import monthrange
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import logging
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class account_analytic_account(osv.osv):
    _inherit = 'account.analytic.account'

    _columns = {
        'default_code': fields.char(u'内部编码', size=128),
        'analytic_show': fields.boolean(u'分门店利润表显示空值'),
    }

    def write_code(self,cr,uid,pp_id,code,seq):
        for pid in pp_id:
            cr.execute("select id from account_analytic_account where parent_id = %s order by type desc,id"%(pid[0]))
            new_ids = cr.fetchall()
            code2='%s.%02d'%(code,seq)
            cr.execute("update account_analytic_account set default_code='%s'  where id = %s"%(code2,pid[0]))
            seq+=1
            self.write_code(cr,uid,new_ids,code2,1)
        return True

    def make_new_code(self, cr, uid, ids, context=None):
        cr.execute("select id from account_analytic_account where parent_id =599 order by type desc,id")
        pp_id = cr.fetchall()
        self.write_code(cr,uid,pp_id,'A',1)
        return True

    def name_get(self, cr, uid, ids, context=None):
        res = []
        if not ids:
            return res
        if isinstance(ids, (int, long)):
            ids = [ids]
        for id in ids:
            if not id:continue
            elmt = self.browse(cr, uid, id, context=context)
            res.append((id, elmt.name))
        return res

account_analytic_account()

class res_partner(osv.osv):
    _inherit = 'res.partner'

    _columns = {
        'statement_payday': fields.integer(u'回款天数'),
        'statement_pay2': fields.integer(u'对账天数'),
        'analytic_id': fields.many2one('account.analytic.account', u'辅助核算项', domain=[('parent_id', '!=', False),('type', '!=', 'view')]),
        'bm_id': fields.boolean(u'部门'),
        'zy_id': fields.boolean(u'职员'),        
        'main_person': fields.char(u'区域负责人',size=16), 
        'main_person2': fields.char(u'财务负责人',size=200), 
    }

res_partner()

class wg_delete_mg(osv.osv):
    _name = 'wg.delete.mg'
    _order = "id desc"

    def _get_period(self, cr, uid, context=None):
        ctx = dict(context or {}, account_period_prefer_normal=True)
        period_ids = self.pool.get('account.period').find(cr, uid, context=ctx)
        return period_ids and period_ids[0] or False

    _columns = {
        'name': fields.char(u'删除的凭证编号', size=128, required=True),
        'journal_id': fields.many2one('account.journal', '所属凭证', required=True),
        'period_id': fields.many2one('account.period', u'会计期间', required=True),
        'active': fields.boolean('是否已恢复使用'),
    }

    _defaults = {
        'period_id': _get_period,
    }

wg_delete_mg()

class stock_move(osv.osv):
    _inherit = 'stock.move'

    _columns = {
        'wgkchs_id': fields.many2one('account.wgkchs', '库存核算单', ondelete='cascade', select=True),
        'cost_price': fields.float('成本价', digits=(7,9)),
        'tmp_qty': fields.float(u'TMP数量', digits=(12,3)),
        'tmp_price': fields.float('TMP成本价', digits=(7,9)),
    }

stock_move()

class standard_months_price(osv.osv):
    _name = 'standard.months.price'
    _order = "id desc"

    def _get_period(self, cr, uid, context=None):
        ctx = dict(context or {}, account_period_prefer_normal=True)
        period_ids = self.pool.get('account.period').find(cr, uid, context=ctx)
        return period_ids and period_ids[0] or False

    _columns = {
        'product_id': fields.many2one('product.product', u'产品', domain=[('sale_ok', '=', True),('type', '!=', '7')], change_default=True),
        'months': fields.many2one('account.period', u'会计期间', required=True),
        'price': fields.float('月末单价', digits=(7,9)),
        'price_in': fields.float('本月入库价', digits=(7,9)),
        'qty_md': fields.float('月末门店数量',),
        'qty_zt': fields.float('月末在途数量',),
    }

    _defaults = {
        'months': _get_period,
    }

standard_months_price()

class cost_standard_price(osv.osv_memory):
    _name = 'cost.standard.price'
    _order = "id desc"

    _columns = {
        'product_id': fields.many2one('product.product', u'产品', domain=[('sale_ok', '=', True),('type', '!=', '7')], change_default=True),
        'qty': fields.float(u'数量', digits=(12,3)),
        'price': fields.float('单价', digits=(7,9)),
    }

cost_standard_price()

class account_wgkchs(osv.osv):
    _name = 'account.wgkchs'
    _description = u"库存核算单"
    _inherit = ['mail.thread']

    def _get_period(self, cr, uid, context=None):
        ctx = dict(context or {}, account_period_prefer_normal=True)
        period_ids = self.pool.get('account.period').find(cr, uid, context=ctx)
        return period_ids and period_ids[0] or False

    _columns = {
        'name': fields.char(u'名称', size=128),
        'period_id': fields.many2one('account.period', u'会计期间', required=True),
        'state': fields.selection([('draft','未记账'), ('approved','已记账'), ('cancel','取消')], u'状态', required=True, readonly=True),
        'wgkchs_line': fields.one2many('account.move', 'wgkchs_id', 'wgkchs Lines'),
    }

    _defaults = {
        'state': 'draft',
        'name': '/',
        'period_id': _get_period,
    }
    def action_cancel(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        if context is None:
            context = {}
        self.write(cr, uid, ids, {'state': 'cancel'})
        return True

    def action_done(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        if context is None:
            context = {}
        todo = []
        shop_obj = self.pool.get('sale.shop')
        for o in self.browse(cr, uid, ids):
            if not o.wgkchs_line:
                raise osv.except_osv(_('错误!'),_('不能确认没有明细行的库存核算单.'))
            for line in o.wgkchs_line:
                shop_name = line.ref.split('/')[0]
                if shop_name != '外购入库':
                    shop_id = shop_obj.search(cr, uid, [('name','=',shop_name),('wgmf_usable','=',True)])
                    if shop_id:
                        shop = shop_obj.browse(cr, uid, shop_id[0])
                        for move in line.line_id:
                            if move.account_id.code != '1405.03.02':
                                self.pool.get('account.move.line').write(cr, uid, move.id, {'analytic_account_id': shop.project_id.id})
                if line.state=='draft':
                    todo.append(line.id)
        self.pool.get('account.move').button_validate(cr, uid, todo, context)
        self.write(cr, uid, ids, {'state': 'approved'})
        return True

    def create(self, cr, uid, vals, context=None):
        if vals.get('period_id',False):
            period_id = self.search(cr, uid, [('period_id', '=', vals.get('period_id',False)),('state', '!=', 'cancel')])
            if period_id:
                raise osv.except_osv(_('错误!'),_('每月只能有一张库存核算单.'))
        if vals.get('name','/')=='/':
            vals['name'] = vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'account.wgkchs') or '/'
        return super(account_wgkchs, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if vals.get('period_id',False):
            period_id = self.search(cr, uid, [('period_id', '=', vals.get('period_id',False)),('state', '!=', 'cancel')])
            if period_id:
                raise osv.except_osv(_('错误!'),_('每月只能有一张库存核算单.'))
        return super(account_wgkchs, self).write(cr, uid, ids, vals, context=context)

account_wgkchs()

class wgmf_account_pzz(osv.osv):
    _name = "wgmf.account.pzz"
    _order = "id desc"
    _description = u"凭证字"

    _columns = {
        'name': fields.char(u'凭证字', size=64),
        'active': fields.boolean(u'启用'),
        'note': fields.text(u'备注'),
    }

    _defaults = {
        'active': True,
    }

wgmf_account_pzz()

class account_journal(osv.osv):
    _inherit = "account.journal"

    _columns = {
        'pzz_type': fields.many2one('wgmf.account.pzz', u'凭证字号', domain=[('active', '=', True)]),
    }

account_journal()

class pos_statement(osv.osv):
    _inherit = 'pos.statement'

    _columns = {
        'move_lines': fields.one2many('account.move', 'statement_ids', 'move Lines'),
    }

pos_statement()

class oe_synchronize_oa(osv.osv):
    _name = 'oe.synchronize.oa'
    _order = "id desc"

    _columns = {
        'type': fields.selection([('bx','报销'), ('sk','申款'), ('fk','付款')], '类型', required=True),
        'account_id': fields.many2one('account.account', u'科目', required=True, ondelete="cascade", domain=[('type','<>','view'), ('type', '<>', 'closed')], select=2),
        'name': fields.char('对应OA科目名称', size=128, required=True),
        'active': fields.boolean(u'启用'),
        'budget_active': fields.boolean(u'超预算二次审批'),
        'note': fields.text(u'备注'),
    }

    _defaults = {
        'active': True,
    }

oe_synchronize_oa()

class wgoa_fuzuhesuan_guanli(osv.osv):
    _name = 'wgoa.fuzuhesuan.guanli'
    _order = "id desc"

    _columns = {
        'type': fields.selection([('bx','报销'), ('sk','申款'), ('fk','付款')], u'类型', required=True),
        'analytic_account_id': fields.many2one('account.analytic.account', u'辅组核算项', required=True, domain=[('type','<>','view')]),
        'name': fields.char(u'对应OA辅助科目名称', size=128, required=True),
        'active': fields.boolean(u'启用'),
        'note': fields.text(u'备注'),
    }

    _defaults = {
        'active': True,
    }

wgoa_fuzuhesuan_guanli()

class wgoa_partner_guanli(osv.osv):
    _name = 'wgoa.partner.guanli'
    _order = "id desc"

    _columns = {
        'type': fields.selection([('bx','报销'), ('sk','申款'), ('fk','付款')], u'类型', required=True),
        'partner_id': fields.many2one('res.partner', u'业务伙伴', required=True, domain=[('active','=',True)]),
        'name': fields.char(u'对应OA领款人所属部门', size=128, required=True),
        'active': fields.boolean(u'启用'),
        'note': fields.text(u'备注'),
    }

    _defaults = {
        'active': True,
    }

wgoa_partner_guanli()

class account_account(osv.osv):
    _inherit = "account.account"

    _columns = {
        'wg_fzpz': fields.boolean('必填辅助凭证'),
        'wg_btywhb': fields.boolean('必填业务伙伴'),
    }

account_account()

class account_move(osv.osv):
    _inherit = 'account.move'

    def _default_journal_id(self, cr, uid, context=None):
        cr.execute("select journal_id from account_move where create_uid=%s order by create_date desc",(uid,))
        journal_id = cr.fetchone()
        if not journal_id:
            journal_id = False
        if journal_id:
            journal_id = journal_id[0]
        return journal_id

    def _check_active(self, cr, uid, ids, context=None):
        for move in self.browse(cr, uid, ids, context=context):
            if float('%.2f' % sum(line.debit for line in move.line_id)) != float('%.2f' % sum(line.credit for line in move.line_id)):
                raise osv.except_osv(('错误'),_('凭证:%s 借贷不平，不能保存!'%move.name))
        return True

    _columns = {
        'wgkchs_id': fields.many2one('account.wgkchs', '库存核算单', ondelete='cascade', select=True),
        'date_maturity' : fields.date(u'到期日期',states={'posted':[('readonly',True)]}),
        'wgmf_type': fields.many2one('wgmf.account.pzz', u'K3凭证字', domain=[('active', '=', True)]),
        'statement_ids': fields.many2one('pos.statement', u'对账单', ondelete='cascade', select=True),
        'to_check': fields.boolean('To Review', help='Check this box if you are unsure of that journal entry and if you want to note it as \'to be reviewed\' by an accounting expert.'),
        'checkperson': fields.many2one('res.users','审核人'),
        'checkdate': fields.date(u'审核日期'),
        'journal_id': fields.many2one('account.journal', 'Journal', required=True, states={'posted':[('readonly',True)]}),
        'wg_type': fields.selection([('oa','OA导入'), ('oe','OE创建')], '单据来源', readonly=True),
        'create_uid': fields.many2one('res.users','制单人'),
        'write_uid': fields.many2one('res.users','修改人'),
        'write_date': fields.date(u'修改日期'),
        'del_id': fields.many2one('wg.delete.mg', '已删除的凭证号', ondelete='cascade', select=True),
    }

    _defaults = {
        'date_maturity' : lambda *a: time.strftime('%Y-%m-%d'),
        'to_check': False,
        'wg_type': 'oe',
        'journal_id': _default_journal_id,
    }

    _constraints =[(_check_active,'Warning',['contract_id']),]

    def onchange_account_date(self, cr, uid, ids, date):
        journal_obj = self.pool.get('account.journal')
        period_obj = self.pool.get('account.period')
        return {}
#        if date:
#            print"=========",date,type(date)
#            s = date.split('-')
#            period = str(s[1]) + "/" + str(s[0])
#            period_id = period_obj.search(cr, uid, [('name', '=', period),('state','!=', 'done')])
#            if not period_id:
#                raise osv.except_osv(_('错误!'), _('OE中未找到打开的会计期间(%s)!' % period))
#            else:
#                return {'value': {'period_id': period_id}}
#        else:
#            return {}

    def onchange_del_id(self, cr, uid, ids, del_id):
        vals = {}
        del_obj = self.pool.get('wg.delete.mg')
        if del_id:
            del_msg = del_obj.browse(cr, uid, del_id)
            vals.update({'name':del_msg.name})
        return {'value': vals}


    def onchange_wgmf_journal_id(self, cr, uid, ids, journal_id):
        vals = {}
        journal_obj = self.pool.get('account.journal')
        del_obj = self.pool.get('wg.delete.mg')
        if journal_id:
            journal = journal_obj.browse(cr, uid, journal_id)
            type = False
            if journal.pzz_type:
                vals.update({'wgmf_type': journal.pzz_type.id})
            else:
                vals.update({'wgmf_type': False})
        return {'value': vals}

    def button_cancel(self, cr, uid, ids, context=None):
        for line in self.browse(cr, uid, ids, context=context):
            if not line.journal_id.update_posted:
                raise osv.except_osv(_('Error!'), _('You cannot modify a posted entry of this journal.\nFirst you should set the journal to allow cancelling entries.'))
            if line.period_id and line.period_id.state=='done':
                raise osv.except_osv(_('警告!'), _('您不可以反核销已结账期间的凭证(%s),请先重新开启这个会计期间.'%(line.name)))
        if ids:
            cr.execute('UPDATE account_move '\
                       'SET state=%s '\
                       'WHERE id IN %s', ('draft', tuple(ids),))
        return True

    def button_cancel2(self, cr, uid, ids, context=None):
        for line in self.browse(cr, uid, ids, context=context):
            if not line.journal_id.update_posted:
                raise osv.except_osv(_('Error!'), _('You cannot modify a posted entry of this journal.\nFirst you should set the journal to allow cancelling entries.'))
            if line.period_id and line.period_id.state=='done':
                raise osv.except_osv(_('警告!'), _('您不可以反审核已结账期间的凭证 (%s),请先重新开启这个会计期间.'%(line.name)))
        if ids:
            cr.execute('UPDATE account_move '\
                       'SET to_check=%s, checkperson = null, checkdate = null '\
                       'WHERE id IN %s', ('f',tuple(ids),))
        return True

    def button_check(self, cr, uid, ids, context=None):
        sql="""update account_move set to_check='t',checkperson=%s,checkdate='%s'"""%(uid,time.strftime('%Y-%m-%d'))
        if isinstance(ids,(long,int)):
            sql+=' where id=%s'%(ids)
        else:
            if len(ids)>1:
                sql+=""" where id in %s"""%(tuple(ids),)
            elif len(ids)==1:
                sql+=' where id = %s'%(ids[0])
        cr.execute(sql)
        return True
    def account_hongchong(self, cr, uid, ids, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        new_move_id = self.copy(cr, uid, ids[0], None, context=None)
        #复制凭证后将借贷设置为相反数 并改摘要为 红冲:
        sql="""update account_move set ref='红冲:'||(select name from account_move where id=%s ) where id=%s"""%(ids[0],new_move_id)
        cr.execute(sql)        
        sql="""update account_move_line set name=(select '红冲:'||name from  account_move where id= %s)||'('||name||')',credit=-credit,
        debit=-debit where move_id=%s"""%(ids[0],new_move_id)
        cr.execute(sql)
        sql="""update account_analytic_line set amount=-amount where move_id in 
        (select id from account_move_line  
         where move_id =%s and analytic_account_id is not null)"""%(new_move_id)
        cr.execute(sql)

        cr.commit()
        result = mod_obj.get_object_reference(cr, uid, 'account', 'action_move_journal_line')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id])[0]
        result['domain'] = [('id','in',[new_move_id])]
        return result

    def button_validate(self, cursor, user, ids, context=None):
        for move in self.browse(cursor, user, ids, context=context):
            # check that all accounts have the same topmost ancestor
            if move.to_check == False:
                raise osv.except_osv(_('错误!'),
                                         _('请先审核凭证(%s),在过账.') % (move.name))
            top_common = None
            for line in move.line_id:
                account = line.account_id
                top_account = account
                while top_account.parent_id:
                    top_account = top_account.parent_id
                if not top_common:
                    top_common = top_account
                elif top_account.id != top_common.id:
                    raise osv.except_osv(_('Error!'),
                                         _('You cannot validate this journal entry because account "%s" does not belong to chart of accounts "%s".') % (account.name, top_common.name))
        return self.post(cursor, user, ids, context=context)

    def post(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if len(ids) <= 0:
            return True
        cr.execute('UPDATE account_move '\
                   'SET state=%s '\
                   'WHERE id IN %s',
                   ('posted', tuple(ids),))
        return True

    def copy(self, cr, uid, id, default=None, context=None):
        default = {} if default is None else default.copy()
        context = {} if context is None else context.copy()
        default.update({
            'state':'draft',
            'ref': False,
            'to_check': False,
            'name':'/',
            'check_id': False,
            'wgkchs_id':False,
            'statement_ids':False,
            'checkperson' : False,
            'checkdate' : False,
        })
        context.update({
            'copy':True
        })
        return super(account_move, self).copy(cr, uid, id, default, context)

    def unlink(self, cr, uid, ids, context=None, check=True):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        toremove = []
        obj_move_line = self.pool.get('account.move.line')
        for move in self.browse(cr, uid, ids, context=context):
            if move['state'] != 'draft':
                raise osv.except_osv(_('User Error!'),
                        _('You cannot delete a posted journal entry "%s".') % \
                                move['name'])
            for line in move.line_id:
                if line.invoice:
                    raise osv.except_osv(_('User Error!'),
                            _("Move cannot be deleted if linked to an invoice. (Invoice: %s - Move ID:%s)") % \
                                    (line.invoice.number,move.name))
            line_ids = map(lambda x: x.id, move.line_id)
            context['journal_id'] = move.journal_id.id
            context['period_id'] = move.period_id.id
            obj_move_line._update_check(cr, uid, line_ids, context)
            obj_move_line.unlink(cr, uid, line_ids, context=context)
            toremove.append(move.id)
            sql="""select count(*) from wg_delete_mg  where name ='%s' and active='f' """%(move.name)
            cr.execute(sql)
            if cr.fetchall()[0][0]==0:
                self.pool.get('wg.delete.mg').create(cr, uid, {'name': move.name,'journal_id': move.journal_id.id,'period_id': move.period_id.id}, context)
        result = super(account_move, self).unlink(cr, uid, toremove, context)
        return result

    def write(self, cr, uid, ids, vals, context=None):
        sql="""delete  from  wg_delete_mg where active=False and period_id in (select id from account_period where state<>'done')
                and name in (select name from account_move)
                """
        cr.execute(sql)            
        
        if context is None:
            context = {}
        c = context.copy()
        if vals.get('journal_id',False):
            journal = self.pool.get('account.journal').browse(cr, uid, vals.get('journal_id',False), context)
            if journal.pzz_type:
                vals.update({'wgmf_type':journal.pzz_type.id})
            else:
                vals.update({'wgmf_type':False})
        for move in self.browse(cr, uid, ids, context):
            if vals.get('del_id',False) and move.name not in ('/',''):
                old_seq = move.journal_id and move.journal_id.sequence_id.number_next_actual
                old_name = int(move.name[12:])
                if (old_name + 1) == int(old_seq):
                    self.pool.get('ir.sequence').write(cr, uid, [move.journal_id and move.journal_id.sequence_id.id], {'number_next_actual': old_name})
                else:
                    self.pool.get('wg.delete.mg').create(cr, uid, {'name': move.name,'journal_id': move.journal_id.id,'period_id': move.period_id.id})
                self.pool.get('wg.delete.mg').write(cr, uid, vals.get('del_id',False), {'active': True})
            if move.name in ('/',''):
                self.pool.get('wg.delete.mg').write(cr, uid, vals.get('del_id',False), {'active': True})
        result = super(account_move, self).write(cr, uid, ids, vals, c)
        for move in self.browse(cr, uid, ids, context):
            if vals.get('date_maturity',False) or vals.get('period_id',False):
                obj_move_line=self.pool.get('account.move.line')
                for line in move.line_id:
                    if vals.get('date_maturity',False):
                        obj_move_line.write(cr,uid,line.id,{'date_maturity':vals.get('date_maturity',False)})
                    if vals.get('period_id',False):
                        if isinstance(ids,(int,long)):
                            text='id=%s'%int(ids)
                        else:
                            new_ids=[int(x) for x in ids]
                            text='id in %s'%(tuple(new_ids+[0]),)
                        sql="""update account_analytic_line set period_id=%s
                               where move_id in (select id from account_move_line 
                               where move_id in (select id from account_move where 
                               %s))"""%(vals['period_id'],text)
                        cr.execute(sql)
        cr.commit()
        return result

    def create(self, cr, uid, vals, context=None):
        sql="""delete  from  wg_delete_mg where active=False and period_id in (select id from account_period where state<>'done')
                and name in (select name from account_move)
                """
        cr.execute(sql)        
        if context is None:
            context = {}
        obj_sequence = self.pool.get('ir.sequence')
        delete_mg_obj = self.pool.get('wg.delete.mg')
        invoice = context.get('invoice', False)
        journal = self.pool.get('account.journal').browse(cr, uid, vals.get('journal_id',False), context)
        if journal.pzz_type:
            vals.update({'wgmf_type':journal.pzz_type.id})
        else:
            vals.update({'wgmf_type':False})
        new_name = False
        new_id = False
        if vals.get('del_id',False):
            delete_mg_obj.write(cr, uid, vals.get('del_id',False), {'active': True})
        #if vals.get('name',False) == '/' and not vals.get('del_id',False):
        if vals.get('name',False) == '/':#and not vals.get('del_id',False):
            #cr.execute('select id,name from wg_delete_mg where active = False and journal_id = %s and period_id = %s order by name',(vals.get('journal_id',False),vals.get('period_id',False)))
            sql="""select id,name from wg_delete_mg where active = False and 
                            journal_id in (select id from account_journal 
                            where pzz_type=(select pzz_type from account_journal where id=%s)
                            )
                            and period_id = %s order by name limit 1"""%(vals.get('journal_id',False),vals.get('period_id',False) )
            cr.execute(sql)                
            res = cr.fetchone()
            if res:
                new_name = res[1]
                new_id = res[0]
            elif not res:
                if journal.sequence_id:
                    period = self.pool.get('account.period').browse(cr, uid, vals.get('period_id',False), context)
                    c = {'fiscalyear_id': period.fiscalyear_id.id, 'seq_month':time.strftime('%Y%m',time.strptime(period.date_start,'%Y-%m-%d'))}
                    new_name = obj_sequence.next_by_id(cr, uid, journal.sequence_id.id, c)
                else:
                    raise osv.except_osv(_('Error!'), _('Please define a sequence on the journal.'))
            vals.update({'name':new_name})
        id = super(account_move, self).create(cr, uid, vals, context)
        move = self.browse(cr, uid, id, context)
        if new_id != False:
            delete_mg_obj.write(cr, uid, new_id, {'active': True})
        if vals.get('date_maturity',False):
            for line in move.line_id:
                self.pool.get('account.move.line').write(cr,uid,line.id,{'date_maturity':vals.get('date_maturity',False)})
        return id
    def func_create_pz(self, cr, uid, vals, context=None): 
        #创建凭证OE方法
        vals=vals or {}
        if not vals or  (not vals.has_key('line')):raise osv.except_osv(_('格式不正确!'), _("传入的值为空,或者无明细表"))
        period_id,journal_id,company_id,date_maturity=vals.get('period_id',0),vals.get('journal_id',0),vals.get('company_id',0),vals.get('date_maturity',0)
        if  period_id*journal_id*company_id==0:raise osv.except_osv(_('格式不正确!'), _("期间,账簿,公司必填:现发现数据格式不正确,或者为空"))
        sql= """select count(*) from account_period a
        left join account_journal b on b.company_id=a.company_id
        where a.company_id=%(company_id)s and a.id=%(period_id)s and b.id=%(journal_id)s"""%vals
        cr.execute(sql)
        if cr.fetchall()[0][0]==0:
            raise osv.except_osv(_('格式不正确!'), _("期间%s,账簿%s,公司%s不一致"%(period_id,journal_id,company_id)  ))
        dic=vals.copy()
        dic['name']='/'
        dic['line_id']=vals['line']
        dif=0
        line=[]
        for e in vals['line']:
            e['credit']=round(e.get('credit',0),2)
            e['debit']=round(e.get('debit',0),2)
            e['period_id']=period_id
            e['journal_id']=journal_id
            e['company_id']=company_id
            e['state']='valid'
            e['date_maturity']=date_maturity
            dif+=round(e.get('credit',0),2)-round(e.get('debit',0),2)
            line.append([0,False,e])
        if round(dif,2):
            raise osv.except_osv(_('借贷不平衡!'), _("借贷差值为%s"%(dif)  ))
        dic['line_id']=line
        dic.pop('line')
        move_id= self.create(cr,uid,dic,context)
        return move_id
account_move()

class account_move_line(osv.osv):
    _inherit = "account.move.line"
    def _abs_debit(self, cr, uid, ids, prop, unknow_none, context=None):
        """ Calculates total hours and total no. of cycles for a production order.
        """
        result = {}
        for prod in self.browse(cr, uid, ids, context=context):
            result[prod.id] = abs(prod.debit)
        return result
    def _get_flag1(self, cr, uid, ids, prop, unknow_none, context=None):
        """ Calculates total hours and total no. of cycles for a production order.
        """
        result = {}
        for prod in self.browse(cr, uid, ids, context=context):
            result[prod.id] =prod.account_id.wg_fzpz
        return result
    def _get_flag2(self, cr, uid, ids, prop, unknow_none, context=None):
        """ Calculates total hours and total no. of cycles for a production order.
        """
        result = {}
        for prod in self.browse(cr, uid, ids, context=context):
            result[prod.id] =prod.account_id.wg_btywhb
        return result              
    _columns = {
        'account_id': fields.many2one('account.account', 'Account', required=True, ondelete="cascade", domain=[('type','<>','view'), ('type', '<>', 'closed')], select=2),
        'currency_rate': fields.float('Rate', digits=(16,4)),
        'analytic_account_id': fields.many2one('account.analytic.account', 'Analytic Account', domain=[('type','<>','view')]),
        'required_analytic': fields.function(_get_flag1,type='boolean',string='必填辅助凭证参数',store=True),
        'required_partner': fields.function(_get_flag2,type='boolean',string='必填业务伙伴参数',store=True),
        'abs_debit': fields.function(_abs_debit, type='float', string=u'借方数量(绝对值)',store=True),
    }
    _order = "move_id desc, abs_debit desc, id desc"
    _sql_constraints = [
        ('credit_debit1', 'CHECK (credit*debit=0)',  'Wrong credit or debit value in accounting entry !'),
        ('credit_debit2', 'CHECK (0=0)', 'Wrong credit or debit value in accounting entry !'),
    ]

    def onchange_partner_id(self, cr, uid, ids, move_id, partner_id, account_id=None, debit=0, credit=0, date=False, journal=False, context=None):
        partner_obj = self.pool.get('res.partner')
        payment_term_obj = self.pool.get('account.payment.term')
        journal_obj = self.pool.get('account.journal')
        fiscal_pos_obj = self.pool.get('account.fiscal.position')
        val = {}
        val['date_maturity'] = False

        if not partner_id:
            return {'value':val}
        if not date:
            date = datetime.now().strftime('%Y-%m-%d')
        jt = False
        if journal:
            jt = journal_obj.browse(cr, uid, journal, context=context).type
        part = partner_obj.browse(cr, uid, partner_id, context=context)

        if part.analytic_id:
            val['analytic_account_id'] = part.analytic_id.id
        payment_term_id = False
        if jt and jt in ('purchase', 'purchase_refund') and part.property_supplier_payment_term:
            payment_term_id = part.property_supplier_payment_term.id
        elif jt and part.property_payment_term:
            payment_term_id = part.property_payment_term.id
        if payment_term_id:
            res = payment_term_obj.compute(cr, uid, payment_term_id, 100, date)
            if res:
                val['date_maturity'] = res[0][0]
        if not account_id:
            id1 = part.property_account_payable.id
            id2 =  part.property_account_receivable.id
            if jt:
                if jt in ('sale', 'purchase_refund'):
                    val['account_id'] = fiscal_pos_obj.map_account(cr, uid, part and part.property_account_position or False, id2)
                elif jt in ('purchase', 'sale_refund'):
                    val['account_id'] = fiscal_pos_obj.map_account(cr, uid, part and part.property_account_position or False, id1)
                elif jt in ('general', 'bank', 'cash'):
                    if part.customer:
                        val['account_id'] = fiscal_pos_obj.map_account(cr, uid, part and part.property_account_position or False, id2)
                    elif part.supplier:
                        val['account_id'] = fiscal_pos_obj.map_account(cr, uid, part and part.property_account_position or False, id1)
                if val.get('account_id', False):
                    d = self.onchange_account_id(cr, uid, ids, account_id=val['account_id'], partner_id=part.id, context=context)
                    val.update(d['value'])
        return {'value':val}

    def wgmf_onchange_account_id(self, cr, uid, ids, account_id):
        vals = {}
        if account_id:
            account = self.pool.get('account.account').browse(cr, uid, account_id)
            if account.wg_fzpz == True:
                vals.update({'required_analytic': 't'})
            else:
                vals.update({'required_analytic': 'f'})
            if account.wg_btywhb == True:
                vals.update({'required_partner': 't'})
            else:
                vals.update({'required_partner': 'f'})
        return {'value': vals}
        
    def _amount_residual_fukuan(self, cr, uid, ids,context=None):
        """
           This function returns the residual amount on a receivable or payable account.move.line.
           By default, it returns an amount in the currency of this journal entry (maybe different
           of the company currency), but if you pass 'residual_in_company_currency' = True in the
           context then the returned amount will be in company currency.
        """
        if context is None:
            context = {}
        cur_obj = self.pool.get('res.currency')
        for move_line in self.browse(cr, uid, ids, context=context):
            if move_line.reconcile_id:
                continue
            if not move_line.account_id.type in ('payable', 'receivable'):
                #this function does not suport to be used on move lines not related to payable or receivable accounts
                continue

            if move_line.currency_id:
                move_line_total = move_line.amount_currency
                sign = move_line.amount_currency < 0 and -1 or 1
            else:
                move_line_total = move_line.debit - move_line.credit
                sign = (move_line.debit - move_line.credit) < 0 and -1 or 1
            line_total_in_company_currency =  move_line.debit - move_line.credit
            context_unreconciled = context.copy()
            if move_line.reconcile_partial_id:
                for payment_line in move_line.reconcile_partial_id.line_partial_ids:
                    if payment_line.id == move_line.id:
                        continue
                    if payment_line.currency_id and move_line.currency_id and payment_line.currency_id.id == move_line.currency_id.id:
                            move_line_total += payment_line.amount_currency
                    else:
                        if move_line.currency_id:
                            context_unreconciled.update({'date': payment_line.date})
                            amount_in_foreign_currency = cur_obj.compute(cr, uid, move_line.company_id.currency_id.id, move_line.currency_id.id, (payment_line.debit - payment_line.credit), round=False, context=context_unreconciled)
                            move_line_total += amount_in_foreign_currency
                        else:
                            move_line_total += (payment_line.debit - payment_line.credit)
                    line_total_in_company_currency += (payment_line.debit - payment_line.credit)
            res = sign * line_total_in_company_currency
        return res        
account_move_line()

class account_voucher_line(osv.osv):
    _inherit = 'account.voucher.line'

    def _compute_balance(self, cr, uid, ids, name, args, context=None):
        currency_pool = self.pool.get('res.currency')
        rs_data = {}
        for line in self.browse(cr, uid, ids, context=context):
            ctx = context.copy()
            ctx.update({'date': line.voucher_id.date})
            voucher_rate = self.pool.get('res.currency').read(cr, uid, line.voucher_id.currency_id.id, ['rate'], context=ctx)['rate']
            ctx.update({
                'voucher_special_currency': line.voucher_id.payment_rate_currency_id and line.voucher_id.payment_rate_currency_id.id or False,
                'voucher_special_currency_rate': line.voucher_id.payment_rate * voucher_rate})
            res = {}
            company_currency = line.voucher_id.journal_id.company_id.currency_id.id
            voucher_currency = line.voucher_id.currency_id and line.voucher_id.currency_id.id or company_currency
            move_line = line.move_line_id or False

            if not move_line:
                res['amount_original'] = 0.0
                res['amount_unreconciled'] = 0.0
            elif move_line.currency_id and voucher_currency==move_line.currency_id.id:
                res['amount_original'] = move_line.amount_currency
                res['amount_unreconciled'] = move_line.amount_residual_currency
            else:
                #always use the amount booked in the company currency as the basis of the conversion into the voucher currency
                res['amount_original'] = currency_pool.compute(cr, uid, company_currency, voucher_currency, move_line.credit or move_line.debit or 0.0, context=ctx)
                res['amount_unreconciled'] = currency_pool.compute(cr, uid, company_currency, voucher_currency, move_line.amount_residual, context=ctx)

            rs_data[line.id] = res
        return rs_data

    _columns = {
        'wgmf_statement_ids': fields.many2one('pos.statement', u'对账单'),
        'amount_original': fields.function(_compute_balance, multi='dc', type='float', string='Original Amount', store=True, digits_compute=dp.get_precision('Account')),
        'amount_unreconciled': fields.function(_compute_balance, multi='dc', type='float', string='Open Balance', store=True, digits_compute=dp.get_precision('Account')),
    }

    def onchange_move_line_id(self, cr, user, ids, move_line_id, context=None):
        """
        Returns a dict that contains new values and context

        @param move_line_id: latest value from user input for field move_line_id
        @param args: other arguments
        @param context: context arguments, like lang, time zone

        @return: Returns a dict which contains new values, and context
        """
        res = {}
        move_line_pool = self.pool.get('account.move.line')
        if move_line_id:
            move_line = move_line_pool.browse(cr, user, move_line_id, context=context)
            if move_line.credit:
                ttype = 'dr'
            else:
                ttype = 'cr'
            res.update({
                'account_id': move_line.account_id.id,
                'type': ttype,
                'currency_id': move_line.currency_id and move_line.currency_id.id or move_line.company_id.currency_id.id,
                'wgmf_statement_ids' : move_line.move_id.statement_ids and move_line.move_id.statement_ids.id or False,
            })
        return {
            'value':res,
        }

account_voucher_line()

class account_voucher_other_line(osv.osv):
    _name = 'account.voucher.other.line'
    _order = "id desc"

    _columns = {
        'name':fields.char(u'摘要', size=256,required=True,),
        'account_id': fields.many2one('account.account', u'科目', required=True, ondelete="cascade", domain=[('type','<>','view'), ('type', '<>', 'closed')], select=2),
        'analytic_account_id': fields.many2one('account.analytic.account', u'辅助核算项',required=True,),
        'amount': fields.float(u'金额', digits_compute=dp.get_precision('Account')),
        'voucher_ids':fields.many2one('account.voucher', 'Voucher', required=1, ondelete='cascade'),
        
    }

account_voucher_other_line()

class account_voucher(osv.osv):
    _name = 'account.voucher'
    _inherit = ['account.voucher','mail.thread', 'approve.base']

    def _get_writeoff_amount(self, cr, uid, ids, name, args, context=None):
        if not ids: return {}
        currency_obj = self.pool.get('res.currency')
        res = {}
        debit = credit = 0.0
        for voucher in self.browse(cr, uid, ids, context=context):
            #oscg update sign = voucher.type == 'payment' and -1 or 1
            sign = voucher.type == 'payment' and 1 or 1
            for l in voucher.line_dr_ids:
                debit += l.amount
            for l in voucher.line_cr_ids:
                credit += l.amount
            flag=credit - debit
            for l in voucher.other_move_line_id:
                #if credit - debit < 0:
                if flag < 0:
                   credit +=  l.amount
                else:
                    debit += l.amount
            currency = voucher.currency_id or voucher.company_id.currency_id
            res[voucher.id] =  currency_obj.round(cr, uid, currency, voucher.amount - sign * (credit - debit))
        return res

    def _get_period(self, cr, uid, context=None):
        if context is None: context = {}
        if context.get('period_id', False):
            return context.get('period_id')
        ctx = dict(context, account_period_prefer_normal=True)
        periods = self.pool.get('account.period').find(cr, uid, context=ctx)
        return periods and periods[0] or False

    def _get_partner(self, cr, uid, context=None):
        if context is None: context = {}
        return context.get('partner_id', False)

    def _get_tax(self, cr, uid, context=None):
        if context is None: context = {}
        journal_pool = self.pool.get('account.journal')
        journal_id = context.get('journal_id', False)
        if not journal_id:
            ttype = context.get('type', 'bank')
            res = journal_pool.search(cr, uid, [('type', '=', ttype)], limit=1)
            if not res:
                return False
            journal_id = res[0]

        if not journal_id:
            return False
        journal = journal_pool.browse(cr, uid, journal_id, context=context)
        account_id = journal.default_credit_account_id or journal.default_debit_account_id
        if account_id and account_id.tax_ids:
            tax_id = account_id.tax_ids[0].id
            return tax_id
        return False

    def _get_payment_rate_currency(self, cr, uid, context=None):
        """
        Return the default value for field payment_rate_currency_id: the currency of the journal
        if there is one, otherwise the currency of the user's company
        """
        if context is None: context = {}
        journal_pool = self.pool.get('account.journal')
        journal_id = context.get('journal_id', False)
        if journal_id:
            journal = journal_pool.browse(cr, uid, journal_id, context=context)
            if journal.currency:
                return journal.currency.id
        #no journal given in the context, use company currency as default
        return self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.currency_id.id

    _columns = {
        'state':fields.selection(
            [('draft','Draft'),
            ('approving','Approving'),
             ('cancel','Cancelled'),
             ('proforma','Pro-forma'),
             ('posted','Posted')
            ], 'Status', readonly=True, size=32,
            help=' * The \'Draft\' status is used when a user is encoding a new and unconfirmed Voucher. \
                        \n* The \'Pro-forma\' when voucher is in Pro-forma status,voucher does not have an voucher number. \
                        \n* The \'Posted\' status is used when user create voucher,a voucher number is generated and voucher entries are created in account \
                        \n* The \'Cancelled\' status is used when user cancel voucher.'),
        'other_move_line_id': fields.one2many('account.voucher.other.line','voucher_ids',u'其他', readonly=True, states={'draft':[('readonly',False)]}),
        'writeoff_amount': fields.function(_get_writeoff_amount, string='Difference Amount', type='float', readonly=True, help="Computed as the difference between the amount stated in the voucher and the sum of allocation on the voucher lines."),
        'create_id': fields.many2one('res.users', '制单人'),
        'shenhe_id': fields.many2one('res.users', '审核人'),
   }

    _defaults = {
        'state': 'draft',
        'create_id':lambda self, cr, uid, c:uid,
    }

    def recompute_voucher_lines_fangqi(self, cr, uid, ids, partner_id, journal_id, price, currency_id, ttype, date, context=None):
        """
        Returns a dict that contains new values and context

        @param partner_id: latest value from user input for field partner_id
        @param args: other arguments
        @param context: context arguments, like lang, time zone

        @return: Returns a dict which contains new values, and context
        """
        def _remove_noise_in_o2m():
            """if the line is partially reconciled, then we must pay attention to display it only once and
                in the good o2m.
                This function returns True if the line is considered as noise and should not be displayed
            """
            if line.reconcile_partial_id:
                if currency_id == line.currency_id.id:
                    if self.pool.get('account.move.line')._amount_residual_fukuan(cr,uid,[line.id])<=0:
                        return True
                else:
                    #if line.amount_residual <= 0:
                    if self.pool.get('account.move.line')._amount_residual_fukuan(cr,uid,[line.id])<=0:
                        return True
            return False

        if context is None:
            context = {}
        context_multi_currency = context.copy()

        currency_pool = self.pool.get('res.currency')
        move_line_pool = self.pool.get('account.move.line')
        partner_pool = self.pool.get('res.partner')
        journal_pool = self.pool.get('account.journal')
        line_pool = self.pool.get('account.voucher.line')

        #set default values
        default = {
            'value': {'line_dr_ids': [] ,'line_cr_ids': [] ,'pre_line': False,},
        }

        #drop existing lines
        line_ids = ids and line_pool.search(cr, uid, [('voucher_id', '=', ids[0])]) or False
        if line_ids:
            line_pool.unlink(cr, uid, line_ids)

        if not partner_id or not journal_id:
            return default

        journal = journal_pool.browse(cr, uid, journal_id, context=context)
        partner = partner_pool.browse(cr, uid, partner_id, context=context)
        currency_id = currency_id or journal.company_id.currency_id.id

        total_credit = 0.0
        total_debit = 0.0
        account_type = 'receivable'
        if ttype == 'payment':
            account_type = 'payable'
            total_debit = price or 0.0
        else:
            total_credit = price or 0.0
            account_type = 'receivable'

        if not context.get('move_line_ids', False):
            ids = move_line_pool.search(cr, uid, [('state','=','valid'), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', partner_id)], context=context)
        else:
            ids = context['move_line_ids']
        invoice_id = context.get('invoice_id', False)
        company_currency = journal.company_id.currency_id.id
        move_line_found = False

        #order the lines by most old first
        ids.reverse()
        account_move_lines = move_line_pool.browse(cr, uid, ids, context=context)

        #compute the total debit/credit and look for a matching open amount or invoice
        for line in account_move_lines:
            if _remove_noise_in_o2m():
                continue

            if invoice_id:
                if line.invoice.id == invoice_id:
                    #if the invoice linked to the voucher line is equal to the invoice_id in context
                    #then we assign the amount on that line, whatever the other voucher lines
                    move_line_found = line.id
                    break
            elif currency_id == company_currency:
                #otherwise treatments is the same but with other field names
                if line.amount_residual == price:
                    #if the amount residual is equal the amount voucher, we assign it to that voucher
                    #line, whatever the other voucher lines
                    move_line_found = line.id
                    break
                #otherwise we will split the voucher amount on each line (by most old first)
                total_credit += line.credit or 0.0
                total_debit += line.debit or 0.0
            elif currency_id == line.currency_id.id:
                if line.amount_residual_currency == price:
                    move_line_found = line.id
                    break
                total_credit += line.credit and line.amount_currency or 0.0
                total_debit += line.debit and line.amount_currency or 0.0

        #voucher line creation
        for line in account_move_lines:

            if _remove_noise_in_o2m():
                continue

            if line.currency_id and currency_id == line.currency_id.id:
                amount_original = abs(line.amount_currency)
                amount_unreconciled = abs(line.amount_residual_currency)
            else:
                #always use the amount booked in the company currency as the basis of the conversion into the voucher currency
                amount_original = currency_pool.compute(cr, uid, company_currency, currency_id, line.credit or line.debit or 0.0, context=context_multi_currency)
                amount_unreconciled = currency_pool.compute(cr, uid, company_currency, currency_id, abs(line.amount_residual), context=context_multi_currency)
            line_currency_id = line.currency_id and line.currency_id.id or company_currency
            rs = {
                'name':line.move_id.name,
                'type': line.credit and 'dr' or 'cr',
                'move_line_id':line.id,
                'account_id':line.account_id.id,
                'amount_original': amount_original,
                'amount': (move_line_found == line.id) and min(abs(price), amount_unreconciled) or 0.0,
                'date_original':line.date,
                'date_due':line.date_maturity,
                'amount_unreconciled': amount_unreconciled,
                'currency_id': line_currency_id,
            }
            #in case a corresponding move_line hasn't been found, we now try to assign the voucher amount
            #on existing invoices: we split voucher amount by most old first, but only for lines in the same currency
            if not move_line_found:
                if currency_id == line_currency_id:
                    if line.credit:
                        amount = min(amount_unreconciled, abs(total_debit))
                        rs['amount'] = amount
                        total_debit -= amount
                    else:
                        amount = min(amount_unreconciled, abs(total_credit))
                        rs['amount'] = amount
                        total_credit -= amount

            if rs['amount_unreconciled'] == rs['amount']:
                rs['reconcile'] = True

            if rs['type'] == 'cr':
                default['value']['line_cr_ids'].append(rs)
            else:
                default['value']['line_dr_ids'].append(rs)

            if ttype == 'payment' and len(default['value']['line_cr_ids']) > 0:
                default['value']['pre_line'] = 1
            elif ttype == 'receipt' and len(default['value']['line_dr_ids']) > 0:
                default['value']['pre_line'] = 1
            default['value']['writeoff_amount'] = self._compute_writeoff_amount(cr, uid, default['value']['line_dr_ids'], default['value']['line_cr_ids'], price, ttype)
        return default

    def approve_agree(self, cr, uid, ids, context=None):#审核
        if context is None:context={}
        t = time.strftime('%Y-%m-%d %H:%M:%S')
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        for id in ids:
            self.write(cr, uid, id, {'approve_stage':'approved','state':'posted','shenhe_id':uid}, context=context)
            form = self.browse(cr, uid, id, context=context)
            body = u"%s 审批了表单 <em>%s</em> 。" % ( user.name, form.name)
            self.message_post(cr, uid, [form.id], body=body, context=context)
        return True
    def approve_submit_nahao(self, cr, uid, ids, context=None):#抢号
        context=context or {}
        for id in ids:
            form = self.browse(cr, uid, id, context=context)
            context.update({'seq_month': time.strftime('%Y%m',time.strptime(form.period_id.date_start,'%Y-%m-%d'))})
            if form.move_id:return True
            move_dic=self.account_move_get(cr, uid, id, context=context)
            move_dic.update({'to_check':False})
            move_pool = self.pool.get('account.move')
            move_id = move_pool.create(cr, uid,move_dic , context=context)
            name=move_pool.browse(cr,uid,move_id).name
            self.write(cr,uid,id,{'move_id':move_id,'state':'posted','number':name,'approve_stage':'approving','create_id':uid})
        return True
        
    def approve_submit(self, cr, uid, ids, context=None):#生成凭证
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        #self.cancel_voucher(cr, uid, ids, context=context)
        for id in ids:
            form = self.browse(cr, uid, id, context=context)
            if form.writeoff_amount:  raise osv.except_osv(_('错误!'), _('差异金额为%s 不允许生成凭证,请先保证差异金额为0.')%(form.writeoff_amount))
            self.write(cr, uid, id, {'approve_stage':'approving'}, context=context)
            if form.move_id:
                move_id=form.move_id.id
                move_line_pool=self.pool.get('account.move.line')
                move_pool=self.pool.get('account.move')
                move_line_ids=move_line_pool.search(cr,uid,[('move_id','=',move_id)])
                move_pool.write(cr,uid,[move_id],{'state':'draft'})
                if move_line_ids:
                    move_line_pool.unlink(cr,uid,move_line_ids)
                form = self.browse(cr, uid, id, context=context)
                self.action_move_line_create(cr, uid, ids, context=context)
                self.write(cr, uid, [id], {'state': 'posted','create_id':uid})
                body = u"%s 更新了表单 <em>%s</em> 的凭证信息。" % ( user.name, form.name)
                return True    
            form = self.browse(cr, uid, id, context=context)
            body = u"%s 提交了表单 <em>%s</em> 。" % ( user.name, form.name)
            self.message_post(cr, uid, [form.id], body=body, context=context)
            self.action_move_line_create(cr, uid, ids, context=context)
            self.write(cr, uid, [id], {'state': 'posted','create_id':uid})
        return True

    def approve_refuse(self, cr, uid, ids, context=None):#反审 //取消凭证
        if context is None:context={}
        user_obj = self.pool.get('res.users')
        model_id = self.pool.get('ir.model').search(cr,uid,[('model','=',self._name)])
        self.cancel_voucher_fukuan(cr, 1, ids, context=context)
        self.action_cancel_draft(cr, 1, ids, context=context)
        user = user_obj.browse(cr, uid, uid)
        for form in self.browse(cr, uid, ids, context=context):
            self.write(cr, uid, form.id, {'approve_stage':'draft'}, context=context)
            body = u"%s 拒绝了表单 <em>%s</em> 。" % ( user.name, form.name)
            self.message_post(cr, uid, [form.id], body=body, context=context)
            self.write(cr,uid,[form.id],{'state':'draft','shenhe_id':False,'create_id':False})
        return True

    def writeoff_move_line_get_gao(self, cr, uid, voucher_id, line_total, rec_list_ids, move_id, name, company_currency, current_currency, context=None):
        currency_obj = self.pool.get('res.currency')
        move_line = {}

        voucher = self.pool.get('account.voucher').browse(cr,uid,voucher_id,context)
        current_currency_obj = voucher.currency_id or voucher.journal_id.company_id.currency_id
        list = []
        remain_total=line_total
        for line in voucher.other_move_line_id:
            remain_total+=line.amount
            move_line_id = self.pool.get('account.move.line').create(cr, uid, {
                'name': line.name or line.account_id.name,
                'account_id': line.account_id.id,
                'move_id': move_id,
                'partner_id': voucher.partner_id.id,
                'date': voucher.date,
                'debit':round(line.amount,4) or 0.0000, #float(line.amount) or 0.0,
                'credit': 0.0,
                'state': 'valid',
                'amount_currency': company_currency <> current_currency or 0.0,
                'currency_id': company_currency <> current_currency and current_currency or False,
                'analytic_account_id': line.analytic_account_id and line.analytic_account_id.id or False,
            }, context)
        return remain_total
    #sql创建凭证明细 越过检查   
    def fast_create(self,cr,uid,vals,context):
        _logger.warning("创建 %s" % time.strftime('%Y-%m-%d %H:%M:%S'))
        debit=vals.get('debit',0)
        credit=vals.get('credit',0)
        partner_id=vals.get('partner_id',0)
        analytic_account_id=vals.get('analytic_account_id',0)
        vals['date_maturity']=vals.get('date',0)
        vals.update({'ref':'客户付款','company_id':1,'abs_debit':abs(debit),'state':'valid','centralisation':'credit'})      
        if partner_id and analytic_account_id:                                                                                      
            cr.execute("insert into account_move_line (name,centralisation,partner_id,account_id,move_id,journal_id,period_id,date,date_maturity,credit,debit,ref,company_id,analytic_account_id,abs_debit,state) values ('%(name)s','%(centralisation)s','%(partner_id)s','%(account_id)s','%(move_id)s','%(journal_id)s','%(period_id)s','%(date)s','%(date_maturity)s','%(credit)s','%(debit)s','%(ref)s','%(company_id)s','%(analytic_account_id)s','%(abs_debit)s','%(state)s') returning id;"% (vals))
            line_id=cr.fetchall()
            if not line_id:raise osv.except_osv(_('错误'), _(' 操作失败,请重试'))
            line_id=line_id[0][0]
            vals.update({'line_id':line_id,'user_id':uid,'amount':credit-debit,'unit_amount':0,'fz_journal_id':1})
            cr.execute("insert into account_analytic_line (move_id,name,period_id,general_account_id,user_id,date,account_id,journal_id,amount,unit_amount) values ('%(line_id)s','%(name)s','%(period_id)s','%(account_id)s','%(user_id)s','%(date)s','%(analytic_account_id)s','%(fz_journal_id)s','%(amount)s','%(unit_amount)s') returning id;"% (vals))
        elif partner_id or analytic_account_id:
            if partner_id:
                cr.execute("insert into account_move_line (name,centralisation,partner_id,account_id,move_id,journal_id,period_id,date,date_maturity,credit,debit,ref,company_id,abs_debit,state) values ('%(name)s','%(centralisation)s','%(partner_id)s','%(account_id)s','%(move_id)s','%(journal_id)s','%(period_id)s','%(date)s','%(date_maturity)s','%(credit)s','%(debit)s','%(ref)s','%(company_id)s','%(abs_debit)s','%(state)s') returning id;"% (vals))
                line_id=cr.fetchall()
                if not line_id:raise osv.except_osv(_('错误'), _(' 操作失败,请重试'))
                line_id=line_id[0][0]
            else:
                cr.execute("insert into account_move_line (name,centralisation,account_id,move_id,journal_id,period_id,date,date_maturity,credit,debit,ref,company_id,analytic_account_id,abs_debit,state) values ('%(name)s','%(centralisation)s','%(account_id)s','%(move_id)s','%(journal_id)s','%(period_id)s','%(date)s','%(date_maturity)s','%(credit)s','%(debit)s','%(ref)s','%(company_id)s','%(analytic_account_id)s','%(abs_debit)s','%(state)s') returning id;"% (vals))
                line_id=cr.fetchall()
                if not line_id:raise osv.except_osv(_('错误'), _(' 操作失败,请重试'))
                line_id=line_id[0][0]
                vals.update({'line_id':line_id,'user_id':uid,'amount':credit-debit,'unit_amount':0,'fz_journal_id':1})
                cr.execute("insert into account_analytic_line (move_id,name,period_id,general_account_id,user_id,date,account_id,journal_id,amount,unit_amount) values ('%(line_id)s','%(name)s','%(period_id)s','%(account_id)s','%(user_id)s','%(date)s','%(analytic_account_id)s','%(fz_journal_id)s','%(amount)s','%(unit_amount)s') returning id;"% (vals))
        else:
            cr.execute("insert into account_move_line (name,centralisation,account_id,move_id,journal_id,period_id,date,date_maturity,credit,debit,ref,company_id,abs_debit,state) values ('%(name)s','%(centralisation)s','%(account_id)s','%(move_id)s','%(journal_id)s','%(period_id)s','%(date)s','%(date_maturity)s','%(credit)s','%(debit)s','%(ref)s','%(company_id)s','%(abs_debit)s','%(state)s') returning id;"% (vals))
            line_id=cr.fetchall()
            if not line_id:raise osv.except_osv(_('错误'), _(' 操作失败,请重试'))
            line_id=line_id[0][0]
        #cr.commit()
        return line_id
    def action_move_line_create(self, cr, uid, ids, context=None):
        '''
        Confirm the vouchers given in ids and create the journal entries for each of them
        '''
        if context is None:
            context = {}
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        for voucher in self.browse(cr, uid, ids, context=context):
            local_context = dict(context, force_company=voucher.journal_id.company_id.id)
            company_currency = self._get_company_currency(cr, uid, voucher.id, context)
            current_currency = self._get_current_currency(cr, uid, voucher.id, context)
            # we select the context to use accordingly if it's a multicurrency case or not
            context.update({'seq_month': time.strftime('%Y%m',time.strptime(voucher.period_id.date_start,'%Y-%m-%d'))})
            context = self._sel_context(cr, uid, voucher.id, context)
            # But for the operations made by _convert_amount, we always need to give the date in the context
            ctx = context.copy()
            ctx.update({'date': voucher.date})
            # Create the account move record.
            if voucher.move_id:
                move_id=voucher.move_id.id
                move_line_ids=move_line_pool.search(cr,uid,[('move_id','=',move_id)])
                move_pool.write(cr,uid,[move_id],{'state':'draft'})
                if move_line_ids:
                    move_line_pool.unlink(cr,uid,move_line_ids)
            else:
                move_dic=self.account_move_get(cr, uid, voucher.id, context=context)
                move_dic.update({'to_check':False})
                move_id = move_pool.create(cr, uid,move_dic , context=context)
            # Get the name of the account_move just created
            name = move_pool.browse(cr, uid, move_id, context=context).name
            # Create the first line of the voucher
            move_line_id = self.fast_create(cr, uid, self.first_move_line_get(cr,uid,voucher.id, move_id, company_currency, current_currency, local_context), local_context)
            move_line_brw = move_line_pool.browse(cr, uid, move_line_id, context=context)
            line_total = move_line_brw.debit - move_line_brw.credit
            rec_list_ids = []
            if voucher.type == 'sale':
                line_total = line_total - self._convert_amount(cr, uid, voucher.tax_amount, voucher.id, context=ctx)
            elif voucher.type == 'purchase':
                line_total = line_total + self._convert_amount(cr, uid, voucher.tax_amount, voucher.id, context=ctx)
            # Create one move line per voucher line where amount is not 0.0
            line_total, rec_list_ids = self.voucher_move_line_create(cr, uid, voucher.id, line_total, move_id, company_currency, current_currency, context)

            # Create the writeoff line if needed
            if line_total != 0:
                remain_total = self.writeoff_move_line_get_gao(cr, uid, voucher.id, line_total, rec_list_ids, move_id, name, company_currency, current_currency, local_context)
                ml_writeoff = self.writeoff_move_line_get(cr, uid, voucher.id, remain_total,move_id, name, company_currency, current_currency, local_context)
                if ml_writeoff:
                    move_line_pool.create(cr, uid, ml_writeoff, local_context)
            # We post the voucher.
            self.write(cr, uid, [voucher.id], {
                'move_id': move_id,
                'state': 'posted',
                'number': name,
            })
            if voucher.journal_id.entry_posted:
                move_pool.post(cr, uid, [move_id], context={})
            # We automatically reconcile the account move lines.
            reconcile = False
            for rec_ids in rec_list_ids:
                if len(rec_ids) >= 2:
                    reconcile = move_line_pool.reconcile_partial(cr, uid, rec_ids, writeoff_acc_id=voucher.writeoff_acc_id.id, writeoff_period_id=voucher.period_id.id, writeoff_journal_id=voucher.journal_id.id)
        return True

    def recompute_voucher_lines(self, cr, uid, ids, partner_id, journal_id, price, currency_id, ttype, date, context=None):
        """
        Returns a dict that contains new values and context

        @param partner_id: latest value from user input for field partner_id
        @param args: other arguments
        @param context: context arguments, like lang, time zone

        @return: Returns a dict which contains new values, and context
        """
        def _remove_noise_in_o2m():
            """if the line is partially reconciled, then we must pay attention to display it only once and
                in the good o2m.
                This function returns True if the line is considered as noise and should not be displayed
            """
            if line.reconcile_partial_id:
                if currency_id == line.currency_id.id:
                    if self.pool.get('account.move.line')._amount_residual_fukuan(cr,uid,[line.id])<=0:
                        return True
                else:
                    #if line.amount_residual <= 0:
                    if self.pool.get('account.move.line')._amount_residual_fukuan(cr,uid,[line.id])<=0:
                        return True
            return False
        if context is None:
            context = {}
        context_multi_currency = context.copy()

        currency_pool = self.pool.get('res.currency')
        move_line_pool = self.pool.get('account.move.line')
        partner_pool = self.pool.get('res.partner')
        journal_pool = self.pool.get('account.journal')
        line_pool = self.pool.get('account.voucher.line')

        #set default values
        default = {
            'value': {'line_dr_ids': [] ,'line_cr_ids': [] ,'pre_line': False,},
        }

        #drop existing lines
        line_ids = ids and line_pool.search(cr, uid, [('voucher_id', '=', ids[0])]) or False
        if line_ids:
            line_pool.unlink(cr, uid, line_ids)

        if not partner_id or not journal_id:
            return default

        journal = journal_pool.browse(cr, uid, journal_id, context=context)
        partner = partner_pool.browse(cr, uid, partner_id, context=context)
        currency_id = currency_id or journal.company_id.currency_id.id

        total_credit = 0.0
        total_debit = 0.0
        account_type = 'receivable'
        if ttype == 'payment':
            account_type = 'payable'
            total_debit = price or 0.0
        else:
            total_credit = price or 0.0
            account_type = 'receivable'

        if not context.get('move_line_ids', False):
            ids = move_line_pool.search(cr, uid, [('state','=','valid'), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', partner_id)], context=context)
        else:
            ids = context['move_line_ids']
        invoice_id = context.get('invoice_id', False)
        company_currency = journal.company_id.currency_id.id
        move_lines_found = []

        #order the lines by most old first
        ids.reverse()
        account_move_lines = move_line_pool.browse(cr, uid, ids, context=context)

        #compute the total debit/credit and look for a matching open amount or invoice
        for line in account_move_lines:
            if _remove_noise_in_o2m():
                continue

            if invoice_id:
                if line.invoice.id == invoice_id:
                    #if the invoice linked to the voucher line is equal to the invoice_id in context
                    #then we assign the amount on that line, whatever the other voucher lines
                    move_lines_found.append(line.id)
            elif currency_id == company_currency:
                #otherwise treatments is the same but with other field names
                if line.amount_residual == price:
                    #if the amount residual is equal the amount voucher, we assign it to that voucher
                    #line, whatever the other voucher lines
                    move_lines_found.append(line.id)
                    break
                #otherwise we will split the voucher amount on each line (by most old first)
                total_credit += line.credit or 0.0
                total_debit += line.debit or 0.0
            elif currency_id == line.currency_id.id:
                if line.amount_residual_currency == price:
                    move_lines_found.append(line.id)
                    break
                total_credit += line.credit and line.amount_currency or 0.0
                total_debit += line.debit and line.amount_currency or 0.0

        #voucher line creation
        for line in account_move_lines:

            if _remove_noise_in_o2m():
                continue

            if line.currency_id and currency_id == line.currency_id.id:
                amount_original = abs(line.amount_currency)
                amount_unreconciled = abs(line.amount_residual_currency)
            else:
                #always use the amount booked in the company currency as the basis of the conversion into the voucher currency
                amount_original = currency_pool.compute(cr, uid, company_currency, currency_id, line.credit or line.debit or 0.0, context=context_multi_currency)
                amount_unreconciled = currency_pool.compute(cr, uid, company_currency, currency_id, line.amount_residual, context=context_multi_currency)
            line_currency_id = line.currency_id and line.currency_id.id or company_currency
            rs = {
                'name':line.move_id.name,
                'type': line.credit and 'dr' or 'cr',
                'move_line_id':line.id,
                'account_id':line.account_id.id,
                'wgmf_statement_ids': line.move_id.statement_ids and line.move_id.statement_ids.id or False,
                'amount_original': amount_original,
                'amount': (line.id in move_lines_found) and min(abs(price), amount_unreconciled) or 0.0,
                'date_original':line.date,
                'date_due':line.date_maturity,
                'amount_unreconciled': amount_unreconciled,
                'currency_id': line_currency_id,
            }
            price -= rs['amount']
            #in case a corresponding move_line hasn't been found, we now try to assign the voucher amount
            #on existing invoices: we split voucher amount by most old first, but only for lines in the same currency
            if not move_lines_found:
                if currency_id == line_currency_id:
                    if line.credit:
                        amount = min(amount_unreconciled, total_debit)
                        rs['amount'] = amount
                        total_debit -= amount
                    else:
                        amount = min(amount_unreconciled, abs(total_credit))
                        rs['amount'] = amount
                        total_credit -= amount

            if rs['amount_unreconciled'] == rs['amount']:
                rs['reconcile'] = True

            if rs['type'] == 'cr':
                default['value']['line_cr_ids'].append(rs)
            else:
                default['value']['line_dr_ids'].append(rs)

            if ttype == 'payment' and len(default['value']['line_cr_ids']) > 0:
                default['value']['pre_line'] = 1
            elif ttype == 'receipt' and len(default['value']['line_dr_ids']) > 0:
                default['value']['pre_line'] = 1
            default['value']['writeoff_amount'] = self._compute_writeoff_amount(cr, uid, default['value']['line_dr_ids'], default['value']['line_cr_ids'], price, ttype)
        return default
        
    def first_move_line_get(self, cr, uid, voucher_id, move_id, company_currency, current_currency, context=None):
        '''
        Return a dict to be use to create the first account move line of given voucher.

        :param voucher_id: Id of voucher what we are creating account_move.
        :param move_id: Id of account move where this line will be added.
        :param company_currency: id of currency of the company to which the voucher belong
        :param current_currency: id of currency of the voucher
        :return: mapping between fieldname and value of account move line to create
        :rtype: dict
        '''
        voucher = self.pool.get('account.voucher').browse(cr,uid,voucher_id,context)
        debit = credit = 0.0
        # TODO: is there any other alternative then the voucher type ??
        # ANSWER: We can have payment and receipt "In Advance".
        # TODO: Make this logic available.
        # -for sale, purchase we have but for the payment and receipt we do not have as based on the bank/cash journal we can not know its payment or receipt
        if voucher.type in ('purchase', 'payment'):
            credit = voucher.paid_amount_in_company_currency
        elif voucher.type in ('sale', 'receipt'):
            debit = voucher.paid_amount_in_company_currency
        if debit < 0: credit = -debit; debit = 0.0
        if credit < 0: debit = -credit; credit = 0.0
        #OSCG Fixed: 供应商红字发票时候，Payment Order上的amount_currency 是负数。
        #sign = debit - credit < 0 and -1 or 1
        amount_currency = voucher.amount
        if debit - credit > 0: amount_currency = abs(amount_currency)
        if debit - credit < 0: amount_currency = -abs(amount_currency)
        #set the first line of the voucher
        move_line = {
                #'name': voucher.name or '/',
                'name': voucher.partner_id.name or '/',
                'debit': debit,
                'credit': credit,
                'account_id': voucher.account_id.id,
                'move_id': move_id,
                'journal_id': voucher.journal_id.id,
                'period_id': voucher.period_id.id,
                'partner_id': voucher.partner_id.id,
                'currency_id': company_currency <> current_currency and  current_currency or False,
                'amount_currency': (sign * abs(voucher.amount) # amount < 0 for refunds
                    if company_currency != current_currency else 0.0),
                'date': voucher.date,
                'date_maturity': voucher.date_due,
                'state':'valid',
            }
        return move_line
        
    def cancel_voucher_fukuan(self, cr, uid, ids, context=None):
        reconcile_pool = self.pool.get('account.move.reconcile')
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        for voucher in self.browse(cr, uid, ids, context=context):
            # refresh to make sure you don't unlink an already removed move
            voucher.refresh()
            for line in voucher.move_ids:
                # refresh to make sure you don't unreconcile an already unreconciled entry
                line.refresh()
                if line.reconcile_id:
                    move_lines = [move_line.id for move_line in line.reconcile_id.line_id]
                    move_lines.remove(line.id)
                    reconcile_pool.unlink(cr, uid, [line.reconcile_id.id])
                    if len(move_lines) >= 2:
                        move_line_pool.reconcile_partial(cr, uid, move_lines, 'auto',context=context)
            if voucher.move_id:
                move_id=voucher.move_id.id
                move_line_ids=move_line_pool.search(cr,uid,[('move_id','=',move_id)])
                move_pool.write(cr,uid,[move_id],{'state':'draft'})
                if move_line_ids:
                    move_line_pool.unlink(cr,uid,move_line_ids)
        res = {
            'state':'draft',
        }
        self.write(cr, uid, ids, res)
        return True
        
account_voucher()



