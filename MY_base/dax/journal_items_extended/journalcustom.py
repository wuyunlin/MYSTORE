# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
from openerp.tools.safe_eval import safe_eval
from openerp import SUPERUSER_ID
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp
import time
import MySQLdb
import calendar
from calendar import monthrange
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import logging
import openerp.addons.decimal_precision as dp
import uuid
from openerp.addons.web.controllers.main import ExportData2Excel as _Export

class journal_balance_extended(osv.osv_memory):
    _name = 'journal.with_balance'
    def _get_period(self, cr, uid, context=None):
        period_obj=self.pool.get('account.period')
        current_period = period_obj.find(cr, uid, context=context)[0]
        return current_period
        #domain="[('company_id','=',res_company_id)]"
    _columns={
       'res_company_id':fields.many2one('res.company',u'公司',required=True),
       'account_ids':fields.many2one('account.account',u'科目',required=True,domain="[('company_id','=',res_company_id)]"),
       'analytic_ids':fields.many2one('account.analytic.account',u'辅助科目',domain="[('company_id','=',res_company_id),('type','!=','view')]"),
       'partner_id':fields.many2one('res.partner',u'业务伙伴',domain="['|',('v_partnertype_id','!=',False),('code','!=',False)]"),
       'period_id':fields.many2one('account.period',u'开始期间',required=True,domain="[('company_id','=',res_company_id)]"),
       'period_id2':fields.many2one('account.period',u'结束期间(空值默认与开始期间相同)',domain="[('company_id','=',res_company_id)]"),
       'period_id2':fields.many2one('account.period',u'结束期间(空值默认与开始期间相同)',domain="[('company_id','=',res_company_id)]"),
       'query_line': fields.one2many('move.line.yue', 'line_id', u'明细账余额',readonly=True),
     }
    def default_get(self, cr, uid, fields, context=None):
        context = context or {}
        res = super(journal_balance_extended, self).default_get(cr, uid, fields, context=context)
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        value = {
            'res_company_id': user.company_id.id,
            'period_id': self._get_period(cr, uid, context={}),
        }
        res.update(value)
        return res   
    def export(self, cr, uid, ids, context=None):
         myobj=self.browse(cr,uid,ids[0])
         _file_name = u'明细账余额查询'
         _field =[u'序号',u'科目',u'日期',u'凭证字',u'凭证号',u'凭证号',u'摘要',u'借方',u'贷方', u'余额','业务伙伴','辅助核算项']  
         if myobj.query_line and 0:
             _command="""select date,pzz_name,pz_name,name,period,debit,credit,balance,partner,analytic
                                     from move_line_yue where line_id=%s order by row_id"""%(ids[0])
         else:
             account_ids=myobj.account_ids.id
             partner_id=myobj.partner_id and myobj.partner_id.id
             period_id=myobj.period_id and myobj.period_id.id
             period_id2=myobj.period_id2 and myobj.period_id2.id or myobj.period_id.id
             analytic_ids=myobj.analytic_ids and myobj.analytic_ids.id
             res_company_id=myobj.res_company_id.id
             sql_state=sql_analytic=sql_partner='1=1'
             account_idlist=self.pool.get('account.account').search(cr,1,[('id','child_of',account_ids),('type','!=','view')])+[0]
             sql_account='line.account_id in %s and line.account_id!=0'%(tuple(account_idlist),)
             sql_account22='line.id in %s and line.id!=0'%(tuple(account_idlist),)
             sql_period='line.period_id=%s'%period_id
             sql_beforeperiod='line.period_id<%s'%period_id
             sql_company='line.company_id=%s'%myobj.res_company_id.id
             #if myobj.state!='posted':sql_state="""  move.state = 'posted' """
             if analytic_ids:sql_analytic="""  line.analytic_account_id = %s"""%(analytic_ids)
             if partner_id:sql_partner="""  line.partner_id = %s"""%(partner_id)
             if period_id2:sql_period='line.period_id>=%s and line.period_id<=%s'%(period_id,period_id2)
             sql_zmd='1=1'
             if res_company_id==1 and context.get('zmd'):
                 sql_zmd=' line.move_id in (select id from account_move where wgmf_type in (1,7) )'
             if res_company_id==1 and not context.get('zmd'):
                 sql_zmd=' line.move_id in (select id from account_move where wgmf_type not  in (1,7) )'
             dic={'sql_analytic':sql_analytic,'sql_period':sql_period,'sql_beforeperiod':sql_beforeperiod,
                       'sql_account':sql_account,'sql_partner':sql_partner,'ids':ids[0],'uid':uid,'sql_company':sql_company,'sql_zmd':sql_zmd,'sql_account22':sql_account22}
             sql_test="""select count(*) from account_move_line line
                        ---where partner_id=845 and period_id>=34  ),
                        where %(sql_partner)s and %(sql_period)s and %(sql_analytic)s and %(sql_account)s and %(sql_company)s and %(sql_zmd)s"""%dic
             cr.execute(sql_test)   
             fet= cr.fetchall()[0][0]
             if fet>10000:
                 raise osv.except_osv(_('错误提示!'), _('请您添加过滤条件 避免数据过多,目前查询的有%s条明细账,请控制在10000条以下!'%fet))
             _name = str(uuid.uuid1())
             _command=""" with 
                        ---期间内明细账
                        wu_test as (
                        select account_id, id,coalesce(credit,0) as credit,coalesce(debit,0) as debit,period_id from account_move_line line
                        ---where partner_id=845 and period_id>=34  ),
                        where %(sql_partner)s and %(sql_period)s and %(sql_analytic)s and %(sql_account)s  and %(sql_company)s and %(sql_zmd)s),
                        ---期间内发生数据的余额
                        data as 
                        (SELECT t.account_id,t.id, t.credit,t.debit,(SELECT coalesce(SUM(debit),0)-coalesce(sum(credit),0)
                        FROM wu_test WHERE id <= t.id and account_id=t.account_id) AS 余额,period_id FROM wu_test AS t order by account_id, period_id,id),
                        tot as (select 100000000000 as id,sum(credit) as credit, sum(debit) as debit,0 as 余额 ,period_id ,account_id
                        from wu_test a 
                        group by period_id,account_id
                        ),
                        ----期初值
                        qc as (select 0 as id, 0 as period_id,coalesce(b.debit,0) as debit,coalesce(b.credit,0) as credit,a.id as account_id from (select id from account_account line where %(sql_account22)s  ) a
                        left join 
                        (select coalesce(sum(debit),0) as debit,coalesce(sum(credit),0) as credit,account_id
                        from account_move_line line
                        ---where partner_id=845 and period_id<34),
                        where %(sql_partner)s and %(sql_beforeperiod)s and %(sql_analytic)s and  %(sql_account)s and %(sql_company)s and %(sql_zmd)s group by account_id) b on b.account_id=a.id),
                        ---期间汇总值
                        lastone as (
                        select row_number()over(partition by a.period_id,a.account_id order by a.id desc) as row,a.id,a.period_id,a.credit,a.debit,a.account_id,
                        a.余额+coalesce(qc.debit,0)-coalesce(qc.credit,0) as 余额 from data a
                        left join qc on qc.account_id=a.account_id  where a.id!=100000000000 
                        ),
                        ----期初,期间,主数据汇总
                        dd as(
                        select a.account_id ,a.id,a.period_id,a.credit,a.debit,a.余额+coalesce(qc.debit,0)-coalesce(qc.credit,0) as 余额 from data a
                        left join qc on qc.account_id=a.account_id 
                        union all 
                        select qc.account_id ,id,period_id,coalesce(debit,0),coalesce(credit,0),coalesce(debit,0)-coalesce(credit,0) as 余额 from qc
                        union all
                        select a.account_id,a.id,a.period_id,a.credit,a.debit,b.余额 from tot a
                        left join lastone b on b.period_id=a.period_id and b.row=1 and b.account_id=a.account_id)
                        select 
                        row_number()over(partition by  a.account_id order by a.period_id ,a.id) as row,
                        h.code||'--'||h.name,
                        case when a.id=0 then '期初'
                                    when a.id=100000000000 then cast(c.date_stop as varchar(20))
                                    else cast(b.date as varchar(20)) end as 日期,
                               case when a.id=0 then '期初'
                                    when a.id=100000000000 then '期间汇总'
                                    else e.name end as 凭证字,
                               case when a.id=0 then '--'
                                    when a.id=100000000000 then '--'
                                    else d.name end as 凭证号,                                
                               case when a.id=0 then '期初'
                                    when a.id=100000000000 then '期间汇总'
                                    else b.name end as 摘要,
                               a.debit as 借方,
                               a.credit as 贷方,
                               a.余额,
                               coalesce(coalesce(f.code,'')||'--'||f.name,'') as 业务伙伴,
                               coalesce(coalesce(g.code,'')||'--'||g.name,'') as 辅助核算
                        from dd a
                        left join account_move_line b on b.id=a.id
                        left join account_period c on c.id=a.period_id
                        left join account_move d on d.id=b.move_id
                        left join wgmf_account_pzz e on e.id=d.wgmf_type
                        left join res_partner f on f.id=b.partner_id
                        left join account_analytic_account g on g.id=b.analytic_account_id
                    left join account_account h on h.id=a.account_id
                        order by a.account_id,a.period_id,a.id"""%dic
         _Export.do(self, cr, uid, _name, _file_name, _command, _field)
         return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name}
        
        
    def button_click_method(self, cr, uid, ids, context=None):
         myobj=self.browse(cr,uid,ids[0])
         context=context or {}
         account_ids=myobj.account_ids.id
         partner_id=myobj.partner_id and myobj.partner_id.id
         period_id=myobj.period_id and myobj.period_id.id
         period_id2=myobj.period_id2 and myobj.period_id2.id or myobj.period_id.id
         analytic_ids=myobj.analytic_ids and myobj.analytic_ids.id
         res_company_id=myobj.res_company_id.id
         sql_state=sql_analytic=sql_partner='1=1'
         account_idlist=self.pool.get('account.account').search(cr,1,[('id','child_of',account_ids),('type','!=','view')])+[0]
         sql_account='line.account_id in %s and line.account_id!=0'%(tuple(account_idlist),)
         sql_account22='line.id in %s and line.id!=0'%(tuple(account_idlist),)         
         sql_period='line.period_id=%s'%period_id
         sql_beforeperiod='line.period_id<%s'%period_id
         sql_company='line.company_id=%s'%myobj.res_company_id.id
         #if myobj.state!='posted':sql_state="""  move.state = 'posted' """
         if analytic_ids:sql_analytic="""  line.analytic_account_id = %s"""%(analytic_ids)
         if partner_id:sql_partner="""  line.partner_id = %s"""%(partner_id)
         if period_id2:sql_period='line.period_id>=%s and line.period_id<=%s'%(period_id,period_id2)
         sql_zmd='1=1'
         if res_company_id==1 and context.get('zmd'):
             sql_zmd=' line.move_id in (select id from account_move where wgmf_type in (1,7) )'
         if res_company_id==1 and not context.get('zmd'):
             sql_zmd=' line.move_id in (select id from account_move where wgmf_type not  in (1,7) )'
         dic={'sql_analytic':sql_analytic,'sql_period':sql_period,'sql_beforeperiod':sql_beforeperiod,
                   'sql_account':sql_account,'sql_partner':sql_partner,'ids':ids[0],'uid':uid,'sql_company':sql_company,'sql_zmd':sql_zmd,'sql_account22':sql_account22}
         sql_test="""select count(*) from account_move_line line
                    ---where partner_id=845 and period_id>=34  ),
                    where %(sql_partner)s and %(sql_period)s and %(sql_analytic)s and %(sql_account)s and %(sql_company)s and %(sql_zmd)s"""%dic
         cr.execute(sql_test)   
         fet= cr.fetchall()[0][0]
         if fet>10000:
             raise osv.except_osv(_('错误提示!'), _('请您添加过滤条件 避免数据过多,目前查询的有%s条明细账,请控制在10000条以下!'%fet))
         sql="""delete from move_line_yue where create_uid=%(uid)s or line_id=%(ids)s;
                    insert into move_line_yue(account_name,row_id,date,pzz_name,pz_name,name,debit,credit,balance,partner,analytic,line_id,create_uid,create_date)
                    with 
                    ---期间内明细账
                    wu_test as (
                    select account_id,id,coalesce(credit,0) as credit,coalesce(debit,0) as debit,period_id from account_move_line line
                    ---where partner_id=845 and period_id>=34  ),
                    where %(sql_partner)s and %(sql_period)s and %(sql_analytic)s and %(sql_account)s  and %(sql_company)s and %(sql_zmd)s),
                    ---期间内发生数据的余额
                    data as 
                    (SELECT t.account_id,t.id, t.credit,t.debit,(SELECT coalesce(SUM(debit),0)-coalesce(sum(credit),0)
                    FROM wu_test WHERE id <= t.id and account_id=t.account_id ) AS 余额,period_id FROM wu_test AS t order by period_id,id),
                    tot as (select 100000000000 as id,sum(credit) as credit, sum(debit) as debit,0 as 余额 ,period_id ,account_id
                    from wu_test a 
                    group by period_id,account_id
                    ),
                    ----期初值
                    qc as (select 0 as id, 0 as period_id,coalesce(b.debit,0) as debit,coalesce(b.credit,0) as credit,a.id as account_id from (select id from account_account line where %(sql_account22)s  ) a
                    left join 
                    (select coalesce(sum(debit),0) as debit,coalesce(sum(credit),0) as credit,account_id
                    from account_move_line line
                    ---where partner_id=845 and period_id<34),
                    where %(sql_partner)s and %(sql_beforeperiod)s and %(sql_analytic)s and  %(sql_account)s and %(sql_company)s and %(sql_zmd)s group by account_id) b on b.account_id=a.id),
                    ---期间汇总值
                    lastone as (
                    select row_number()over(partition by a.account_id,a.period_id order by a.id desc) as row,a.id,a.period_id,a.credit,a.debit,a.account_id,
                    a.余额+coalesce(qc.debit,0)-coalesce(qc.credit,0) as 余额 from data a
                    left join qc on qc.account_id=a.account_id  where a.id!=100000000000 
                    ),
                    ----期初,期间,主数据汇总
                    dd as(
                    select a.account_id,a.id,a.period_id,a.credit,a.debit,a.余额+coalesce(qc.debit,0)-coalesce(qc.credit,0) as 余额 from data a
                    left join qc on qc.account_id=a.account_id
                    union all 
                    select qc.account_id,id,period_id,coalesce(debit,0),coalesce(credit,0),coalesce(debit,0)-coalesce(credit,0) as 余额 from qc
                    union all
                    select a.account_id,a.id,a.period_id,a.credit,a.debit,b.余额 from tot a
                    left join lastone b on b.period_id=a.period_id and b.row=1 and a.account_id=b.account_id)
                    select 
                    h.code||'--'||h.name,
                    row_number()over(order by a.account_id,a.period_id ,a.id) as row,
                    case when a.id=0 then '期初'
                                when a.id=100000000000 then cast(c.date_stop as varchar(20))
                                else cast(b.date as varchar(20)) end as 日期,
                           case when a.id=0 then '期初'
                                when a.id=100000000000 then '期间汇总'
                                else e.name end as 凭证字,
                           case when a.id=0 then '--'
                                when a.id=100000000000 then '--'
                                else d.name end as 凭证号,                                
                           case when a.id=0 then '期初'
                                when a.id=100000000000 then '期间汇总'
                                else b.name end as 摘要,
                           a.debit as 借方,
                           a.credit as 贷方,
                           a.余额,
                           coalesce(coalesce(f.code,'')||'--'||f.name,'') as 业务伙伴,
                           coalesce(coalesce(g.code,'')||'--'||g.name,'') as 辅助核算,
                           %(ids)s, %(uid)s,now()-interval'8 hours'
                    from dd a
                    left join account_move_line b on b.id=a.id
                    left join account_period c on c.id=a.period_id
                    left join account_move d on d.id=b.move_id
                    left join wgmf_account_pzz e on e.id=d.wgmf_type
                    left join res_partner f on f.id=b.partner_id
                    left join account_analytic_account g on g.id=b.analytic_account_id
                    left join account_account h on h.id=a.account_id
                    order by a.account_id,a.period_id,a.id"""%dic
         cr.execute(sql)
         return True
         domain=[('line_id','=',ids[0])]
         return {
            "name": "科目余额明细",
            "type": "ir.actions.act_window",
            "res_model": "move.line.yue",
            "view_type": "form",
            "view_mode": "tree",
            'views': [(False, 'tree')],
            'domain':domain,
            "context": {},
          }

class move_line_yue(osv.osv_memory):
    _name = "move.line.yue"
    _order = "row_id"
    _columns = {
        "row_id": fields.integer(u"序列号"),
        "line_id": fields.many2one('journal.with_balance',u"流水id"),
        "date": fields.char(u"日期",size=120),
        "pzz_name": fields.char(u"凭证字",size=120),
        "pz_name": fields.char(u"凭证号",size=120),
        "name": fields.char(u"摘要",size=1200),
        "period": fields.char(u"会计期间",size=120),
        "debit": fields.float(u"借方",digit=(15,4)),
        "credit": fields.float(u"贷方",digit=(15,4)),
        "balance": fields.float(u"余额",digit=(15,4)),
        "partner": fields.char(u"业务伙伴(编码--名称)",size=120),
        "analytic": fields.char(u"辅助核算项目(编码--名称)",size=120),
        "account_name": fields.char(u"科目(编码--名称)",size=120),
    }
move_line_yue()        
        
        
        
