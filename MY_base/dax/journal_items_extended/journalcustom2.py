# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
from openerp.tools.safe_eval import safe_eval
from openerp import SUPERUSER_ID
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp
import time
import MySQLdb
import calendar
from calendar import monthrange
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import logging
import openerp.addons.decimal_precision as dp

class journal_balance_extended(osv.osv_memory):
   _name = 'journal.with_balance'
   _columns={
       'account_ids':fields.many2one('account.account',u'科目',required=True),
       'analytic_ids':fields.many2one('account.analytic.account',u'辅助科目'),
       'period_id':fields.many2one('account.period',u'期间',required=True),
       'state': fields.selection([('all',u'所有凭证'), ('posted','已过账凭证')], u'目标凭证'),
   }
   def button_click_method(self, cr, uid, ids,account_id, context=None):
        view_ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'journal_items_extended', 'view_balance_tree')
        view_id = view_ref and view_ref[1] or False
        for id in ids:
            myobj=self.browse(cr,uid,id)
        account_ids=self.pool.get('account.account').search(cr,uid,[('id','child_of',myobj.account_ids.id)])
        if len(account_ids)<1:
            raise osv.except_osv(_('警告'), _('未找到符合条件的明细账!'))
        context=context or {}
        context.update({'balance':True,'banlance_account_ids':account_ids,'period_id':myobj.period_id.id})
        if myobj.analytic_ids:
            analytic_ids=self.pool.get('account.analytic.account').search(cr,uid,[('id','child_of',myobj.analytic_ids.id)])+[0]
            context.update({'analytic_ids':analytic_ids})
        if myobj.state!='posted':
            if len(account_ids)>1:
                sql_line="""select id from account_move_line where account_id in %s and period_id=%s"""%(tuple(account_ids),myobj.period_id.id)
            else:
                sql_line="""select id from account_move_line where account_id = %s and period_id=%s"""%(account_ids[0],myobj.period_id.id)
            if myobj.analytic_ids:sql_line+=""" and analytic_account_id in %s"""%(tuple(analytic_ids),)
        else:
            if len(account_ids)>1:
                sql_line="""select line.id from account_move_line line
                            left join account_move am on am.id=line.move_id
                            where line.account_id in %s and am.state='posted'
                            and line.period_id=%s """%(tuple(account_ids),myobj.period_id.id)
            else:
                sql_line="""select line.id from account_move_line line
                            left join account_move am on am.id=line.move_id
                            where line.account_id = %s and am.state='posted'
                            and line.period_id=%s """%(account_ids[0],myobj.period_id.id)
            if myobj.analytic_ids:
                sql_line+=""" and line.analytic_account_id in %s"""%(tuple(analytic_ids),)
            context.update({'state':'posted'})
        cr.execute(sql_line)
        line_fetch=cr.fetchall()
        line_ids = [one_res[0] for one_res in line_fetch]
        domain=[('id','in',line_ids)]
        return {
            'type': 'ir.actions.act_window',
            'name': ('Journal Item'),
            'res_model': 'account.move.line',
            'view_type': 'form',
            'views': [(view_id, 'tree'),(False, 'form')],
            'view_mode': 'tree,form',
            'target': 'current',
            'nodestroy': True,
            'domain':domain,
            'context':context,
            }
class account_move_line(osv.osv):
    _inherit='account.move.line'
    def getbalance(self, cr, uid, ids, prop, unknow_none, context=None):
        """ Calculates balance for a period items.
        """
        result = {}
        context=context or {}
        if not context.get('balance'):
            for id in ids:
                result[id]={'balance':0,'init_balance':''}
            return result
        if isinstance(ids, (int, long)):
            ids = [ids]
        base_line=self.browse(cr, uid, ids[0], context=context)
        account_ids=context.get('banlance_account_ids') or (base_line.account_id.id)
        account_ids=tuple(account_ids)
        period_id=context.get('period_id') or base_line.period_id.id
        if context.get('state')!='posted':
            if len(account_ids)>1:
                base_sql="""select COALESCE(sum(debit-credit),0) from account_move_line where period_id<%s and account_id in %s"""%(base_line.period_id.id,account_ids)
            else:
                base_sql="""select COALESCE(sum(debit-credit),0) from account_move_line where period_id<%s and account_id = %s"""%(base_line.period_id.id,account_ids[0])
            if context.get('analytic_ids',''):
                base_sql+=""" and analytic_account_id in %s"""%(tuple(context.get('analytic_ids')),)
        else:
            if len(account_ids)>1:
                base_sql="""select COALESCE(sum(line.debit-line.credit),0) from account_move_line line
                            left join account_move am on am.id=line.move_id
                            where am.state='posted' and line.period_id<%s 
                            and line.account_id in %s"""%(period_id,account_ids)
            else:
                base_sql="""select COALESCE(sum(line.debit-line.credit),0) from account_move_line line
                            left join account_move am on am.id=line.move_id
                            where am.state='posted' and line.period_id<%s 
                            and line.account_id = %s"""%(period_id,account_ids[0])
            if context.get('analytic_ids',''):
                base_sql+=""" and line.analytic_account_id in %s"""%(tuple(context.get('analytic_ids')),)
        cr.execute(base_sql)
        base_init=cr.fetchall()[0][0]
        for line in self.browse(cr, uid, ids,context=context):
            if context.get('state')!='posted':
                if len(account_ids)>1:
                    if not context.get('analytic_ids',''):
                        period_sql="""select COALESCE(sum(debit-credit),0) from account_move_line where (account_id in %s and period_id=%s and date<'%s')
                                       or (account_id in %s and period_id=%s and date='%s' and id<=%s)"""%(account_ids,period_id,line.date,account_ids,period_id,line.date,line.id)
                    else:
                        analytic_ids=tuple(context.get('analytic_ids'))
                        period_sql="""select COALESCE(sum(debit-credit),0) from account_move_line where (account_id in %s and period_id=%s and analytic_account_id in %s and date<'%s')
                                       or (account_id in %s and period_id=%s and analytic_account_id in %s and date='%s' and id<=%s)"""%(account_ids,period_id,analytic_ids,line.date,account_ids,period_id,analytic_ids,line.date,line.id)
                else:
                    if not context.get('analytic_ids',''):
                        period_sql="""select COALESCE(sum(debit-credit),0) from account_move_line where (account_id = %s and period_id=%s and date<'%s')
                                       or (account_id = %s and period_id=%s and date='%s' and id<=%s)"""%(account_ids[0],period_id,line.date,account_ids[0],period_id,line.date,line.id)
                    else:
                        analytic_ids=tuple(context.get('analytic_ids'))
                        period_sql="""select COALESCE(sum(debit-credit),0) from account_move_line where (account_id = %s and period_id=%s and analytic_account_id in %s and date<'%s')
                                       or (account_id = %s and period_id=%s and analytic_account_id in %s and date='%s' and id<=%s)"""%(account_ids[0],period_id,analytic_ids,line.date,account_ids[0],period_id,analytic_ids,line.date,line.id)
            else:
                if len(account_ids)>1:
                    if not context.get('analytic_ids',''):
                        period_sql="""select COALESCE(sum(line.debit-line.credit),0) from account_move_line line
                                      left join account_move am on am.id=line.move_id
                                      where (am.state='posted' and line.account_id in %s and line.period_id=%s and line.date<'%s')
                                       or (am.state='posted' and line.account_id in %s and line.period_id=%s and line.date='%s' and line.id<=%s)"""%(account_ids,period_id,line.date,account_ids,period_id,line.date,line.id)
                    else:
                        analytic_ids=tuple(context.get('analytic_ids'))
                        period_sql="""select COALESCE(sum(line.debit-line.credit),0) from account_move_line line
                                      left join account_move am on am.id=line.move_id
                                      where (am.state='posted' and line.account_id in %s and line.period_id=%s and line.analytic_account_id in %s and line.date<'%s')
                                       or (am.state='posted' and line.account_id in %s and line.period_id=%s and line.analytic_account_id in %s and line.date='%s' and line.id<=%s)"""%(account_ids,period_id,analytic_ids,line.date,account_ids,period_id,analytic_ids,line.date,line.id)
                else:
                    if not context.get('analytic_ids',''):
                        period_sql="""select COALESCE(sum(line.debit-line.credit),0) from account_move_line line
                                      left join account_move am on am.id=line.move_id
                                      where (am.state='posted' and line.account_id = %s and line.period_id=%s and line.date<'%s')
                                       or (am.state='posted' and line.account_id = %s and line.period_id=%s and line.date='%s' and line.id<=%s)"""%(account_ids[0],period_id,line.date,account_ids[0],period_id,line.date,line.id)
                    else:
                        analytic_ids=tuple(context.get('analytic_ids'))
                        period_sql="""select COALESCE(sum(line.debit-line.credit),0) from account_move_line line
                                      left join account_move am on am.id=line.move_id
                                      where (am.state='posted' and line.account_id = %s and line.period_id=%s and line.analytic_account_id in %s and line.date<'%s')
                                       or (am.state='posted' and line.account_id = %s and line.period_id=%s and line.analytic_account_id in %s and line.date='%s' and line.id<=%s)"""%(account_ids[0],period_id,analytic_ids,line.date,account_ids[0],period_id,analytic_ids,line.date,line.id)
            cr.execute(period_sql)
            sql_init=cr.fetchall()[0][0]
            result[line.id]={'Balance':base_init+sql_init,'init_balance':''}
        for line in self.pool.get('account.move.line').search(cr, uid,[('id','in',ids)],order='period_id,date,id',limit=1):
            result[line]['init_balance']=base_init
        return result
    _columns={
        'Balance':fields.function(getbalance,string=u'余额',multi="qc"),
        'init_balance':fields.function(getbalance,string=u'期初',multi="qc")
    }
    def search(self, cr, uid, args, offset=0, limit=None, order=None,
            context=None, count=False):
        if context is None:
            context = {}
        if context.get('balance'):
            order="period_id,date,id"
            #if context.get('analytic_ids'):
                #order="period_id,date,analytic_account_id,id"
        res=super(account_move_line, self).search(cr, uid, args, offset, limit,
                order, context=context, count=count)
        return res
