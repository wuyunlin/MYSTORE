# -*- encoding: utf-8 -*-

{
    'name': '财务部分修改及优化',
    'author': 'OSCG',
    'website': 'http://www.oscg.com.hk',
    'summary': '整理',
    'version': '1.0',
    "category" : "Develop",
    'sequence': 330,
    'description': """
     """,
    'depends': ['wgmf_account_customize'],
    'data': [
        'account_develop.xml',
        'wizard/account_move_line_query.xml',
        'wizard/move_line_query.xml',
        'wizard/analytic_line_query.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
