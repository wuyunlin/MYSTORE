# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.tools.safe_eval import safe_eval
from openerp import SUPERUSER_ID
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp
import time
import MySQLdb
import calendar
from calendar import monthrange
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import logging
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class account_move(osv.osv):
    _inherit = 'account.move'
    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default = default.copy()
        date=time.strftime('%Y-%m-%d')
        period_id=self.pool.get('account.period').find(cr, uid, date, context=context)[0]
        default.update({'period_id': period_id,'date':date})
        return super(account_move, self).copy(cr, uid, id, default, context)
        
#    def check_active_name(self, cr, uid, ids, context=None):
#        for record in self.browse(cr, uid, ids, context=context):
#                history=self.pool.get('account.move').search(cr,uid,[('id','!=',record.id),('name','=',record.name)])
#                if history and record.name:
#                    raise osv.except_osv(u'警告',u'该凭证号 %s 已存在，请修改!'%(record.name))
#        return True
#    _constraints = [(check_active_name,'name should be unique!',['account_move']),]
account_move()

class account_account(osv.osv):
    _inherit='account.account'
    _columns={
        'wg_selection':fields.selection([
        ('bm',u'部门'),
        ('kh','客户'),
        ('bm_zy','部门/职员'),
        ('zy','职员'),
        ('gys','供应商'),
        ('kh_bm','客户/部门'),
        ('qdxt','渠道系统'),],string=u'K3选项'),
    }

account_account()