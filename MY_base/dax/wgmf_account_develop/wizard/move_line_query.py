# -*- coding: utf-8 -*- #

import time
from osv import osv, fields
import logging
import netsvc
from tools.translate import _
import uuid
from openerp.addons.web.controllers.main import ExportData2Excel as _Export

logger = logging.getLogger(__name__)
class move_line_query(osv.osv_memory):
    _name = "move.line.query"
    _order = "id desc"
    _description = u"凭证查询"
    _operate1=[('>=','大于等于'),('>','大于'),('=','等于'),('<=','小于等于'),('<','小于'),('!=','不等于')]
    _operate2=[('like','包含'),('not like','不包含')]
    _operate3=[('=','等于'),('child_of','从属于')]
    _operate4=[('or','或者'),('and','与')]
    _operate5=[('=','等于'),('child_of','从属于'),('unset','未设置')]
    _columns = {
        "period_id": fields.many2one("account.period", string=u"会计期间",domain="[('company_id','=',res_company_id)]"),#ok
        'period_id_operate':fields.selection(_operate1, u'期间比较符号'),
        
        "type_id": fields.many2one("wgmf.account.pzz", string=u"凭证字"),#主
        
        "account_id": fields.many2one("account.account", string=u"科目",domain="[('company_id','=',res_company_id)]"),#ok
        'account_id_operate':fields.selection(_operate3, u'科目比较符号'),
        
        "zhidan": fields.many2one("res.users", string=u"制单人"),#主
        "reconcile_id": fields.many2one("account.move.reconcile", string=u"核销号"),#明细
                
        "pz_name": fields.char(u"凭证号(包含)"),#主
        
        "line_amount": fields.float(u"明细金额"),#明细
        'line_amount_operate':fields.selection(_operate1, u'明细金额比较符号'),
        
        'zhaiyao1': fields.char(u'摘要', size=64),#明细
        'zhaiyao1_operate':fields.selection(_operate2, u'摘要比较符号'),
        
        'muli_operate':fields.selection(_operate4, u'摘要连接符号'),
        
        'zhaiyao2': fields.char(u'摘要', size=64),#ok
        'zhaiyao2_operate':fields.selection(_operate2, u'摘要比较符号'),
        
        "partner_id": fields.many2one("res.partner", string=u"业务伙伴",domain="[('code','!=',False)]"),
        
        "analytic_id": fields.many2one("account.analytic.account", string=u"核算项目",domain="[('company_id','=',res_company_id)]"),
        'analytic_id_operate':fields.selection(_operate5, u'核算项目比较符号'),
        
        "name": fields.char(u"名称"),
        'res_company_id':fields.many2one('res.company', u'公司', required=True),
    }
    _default={
       'name':'凭证查询',
       'res_company_id': lambda self, cr, uid, context: self.pool.get('res.users').read(cr,uid,uid,['company_id'])['company_id']
    }
    def onchange_partner_id(self, cr, uid, ids, period_id_operate,account_id_operate,line_amount_operate,zhaiyao1_operate,muli_operate,zhaiyao2_operate, analytic_id_operate,context=None):
        dic={}
        res={'value':dic}
        if not period_id_operate:
            res.update({'period_id':False})
        if not account_id_operate:
            res.update({'account_id':False})
        if not line_amount_operate:
            res.update({'line_amount':0})            
        if not zhaiyao1_operate:
            res.update({'zhaiyao1':''})  
        if not muli_operate:
            res.update({'zhaiyao2':'','zhaiyao2_operate':False})                
        if not zhaiyao2_operate:
            res.update({'zhaiyao2':''})   
        if not analytic_id_operate:
            res.update({'analytic_id':False})
        return res                  
    
    def query_line(self, cr, uid, ids, context=None):
        t_ir_model_date = self.pool.get("ir.model.data")
        tree_id = t_ir_model_date.get_object_reference(cr, uid, "wgmf_account_develop", "view_move_line_querytree")[1]
        wizard = self.browse(cr, uid, ids[0], context=None)
        res_ids=[]
        res_company_id=wizard.res_company_id.id
        where = ' line.company_id=%s'%res_company_id
        where_main='main.company_id=%s'%res_company_id
        #会计期间过滤
        if wizard.period_id_operate:
            where +=' and line.period_id %s %s'%(wizard.period_id_operate,wizard.period_id.id)
        #凭证字过滤
        if wizard.type_id:
            where_main +=""" and main.wgmf_type = %s"""%(wizard.type_id.id)
        else:
            #区分专卖店和专柜的账目
            xx=context.get('zmd')  and  """ and main.wgmf_type  in (1,7)"""  or """ and main.wgmf_type  not in (1,7)"""
            where_main +=xx
        #科目过滤   
        if wizard.account_id_operate:
            account_ids=self.pool.get('account.account').search(cr,uid,[('id',wizard.account_id_operate,wizard.account_id.id)])
            where +=' and line.account_id in %s'%(tuple(account_ids+[0]),)
        if wizard.partner_id:
            where +=' and line.partner_id=%s'%(wizard.partner_id.id)
        #辅助凭证
        if wizard.analytic_id_operate:
            if wizard.analytic_id_operate=='unset':
                where +=' and line.analytic_account_id is null'
            else:
                analytic_ids=self.pool.get('account.analytic.account').search(cr,uid,[('id',wizard.analytic_id_operate,wizard.analytic_id.id)])
                where +=' and line.analytic_account_id in %s'%(tuple(analytic_ids+[0]),)
        if wizard.zhidan:
            where_main +=' and main.create_uid=%s'%(wizard.zhidan.id)
        if wizard.reconcile_id:
            where +=' and (line.reconcile_id=%s or line.reconcile_partial_id=%s ) '%(wizard.reconcile_id.id,wizard.reconcile_id.id)
        if wizard.pz_name:
            where_main +=""" and main.name like '%%%s%%'"""%(wizard.pz_name)
        if wizard.line_amount_operate:
            where +=' and ((line.debit %s  %s and line.debit!=0 ) or (line.credit %s  %s and line.credit!=0 )) '%(wizard.line_amount_operate,wizard.line_amount,wizard.line_amount_operate,wizard.line_amount)
        if  wizard.muli_operate:
            where +=""" and (line.name %s '%%%s%%' %s 
             line.name %s '%%%s%%')"""%(wizard.zhaiyao1_operate,wizard.zhaiyao1,wizard.muli_operate,wizard.zhaiyao2_operate,wizard.zhaiyao2)
        elif  wizard.zhaiyao1_operate:
            where +=""" and line.name %s '%%%s%%'"""%(wizard.zhaiyao1_operate,wizard.zhaiyao1)
        if where_main=='1=1':
            sql="""select id from account_move_line line where %s order
                   by line.id desc"""%(where)
        else:
            sql="""select line.id from account_move_line line 
                   left join account_move main on main.id=line.move_id
                   where %s and %s order by main.name desc,line.id desc"""%(where,where_main)
        cr.execute(sql)
        res_ids=cr.fetchall()
        need_ids = [one_res[0] for one_res in res_ids]
        if not need_ids:raise osv.except_osv(_('警告!'), _('未找到您需要的明细账记录!'))
        domain=[('id','in',need_ids)]
        if context.get('print',False):
            _name = str(uuid.uuid1())
            _field =[u'凭证字号',u'摘要',u'制单人',u'科目编码',u'科目名称',u'借方',u'贷方', u'会计期间',u'业务伙伴',u'辅助核算项',u'到期日期']  
            _command="""select m.name,line.name,rp1.name,aa.code,aa.name,coalesce(line.debit,0),
            coalesce(line.credit,0),ap.name,rp.name,aaa.name,line.date_maturity
            from account_move_line line 
            left join account_move m on m.id=line.move_id
            left join account_account aa on aa.id=line.account_id
            left join account_period ap on ap.id=line.period_id
            left join account_analytic_account aaa on aaa.id=line.analytic_account_id
            left join res_users ru on ru.id=m.create_uid
            left join res_partner rp1 on rp1.id=ru.partner_id
            left join res_partner rp on rp.id=line.partner_id where line.id in %s"""%(tuple(need_ids+[0]),)
            _file_name = u'凭证条件查询'
            _Export.do(self, cr, uid, _name, _file_name, _command, _field)
            return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name}
        return {
            "name": "明细账",
            "type": "ir.actions.act_window",
            "res_model": "account.move.line",
            "view_type": "form",
            "view_mode": "tree,form",
            'views': [(tree_id, 'tree'),(False, 'form')],
            'domain':domain,
            "context": {},
        }
move_line_query()