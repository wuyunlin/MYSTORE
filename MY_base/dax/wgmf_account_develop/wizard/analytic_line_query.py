# -*- coding: utf-8 -*- #

import time
from osv import osv, fields
import logging
import netsvc
from tools.translate import _
import uuid
from openerp.addons.web.controllers.main import ExportData2Excel as _Export

logger = logging.getLogger(__name__)
class analytic_line_query(osv.osv_memory):
    _name = "analytic.line.query"
    _order = "id desc"
    _description = u"核算项明细查询"
    _operate1=[('>=','大于等于'),('>','大于'),('=','等于'),('<=','小于等于'),('<','小于'),('!=','不等于')]
    _operate2=[('like','包含'),('not like','不包含')]
    _operate3=[('=','等于'),('child_of','从属于')]
    _operate4=[('or','或者'),('and','与')]
    _operate5=[('=','等于'),('child_of','从属于'),('unset','未设置')]
    
    _columns = {
        #期间
        "period_id": fields.many2one("account.period", string=u"会计期间",domain="[('company_id','=',res_company_id)]"),#ok
        'period_id_operate':fields.selection(_operate1, u'期间比较符号'),
        
        #科目
        "general_account_id": fields.many2one("account.account", string=u"科目",domain="[('company_id','=',res_company_id)]"),#ok
        'general_account_id_operate':fields.selection(_operate3, u'科目比较符号'),
        #辅助核算按名字查询
        "account_name": fields.char('辅助核算项(按名称查询)',size=64),#ok
        'account_name_operate':fields.selection(_operate2, u'科目比较符号'),
        #辅助科目
        "account_id": fields.many2one("account.analytic.account", string=u"辅助核算项",domain="[('company_id','=',res_company_id)]"),#ok
        'account_id_operate':fields.selection(_operate3, u'辅助核算项比较符号'),
        
        'muli_operate2':fields.selection(_operate4, u'核算项连接符号'),
        
        "account_id2": fields.many2one("account.analytic.account", string=u"辅助核算项",domain="[('company_id','=',res_company_id)]"),#ok
        'account_id_operate2':fields.selection(_operate3, u'辅助核算项比较符号'), 
        #金额
        "line_amount": fields.float(u"明细金额"),#明细
        'line_amount_operate':fields.selection(_operate1, u'明细金额比较符号'),
        
        'zhaiyao1': fields.char(u'摘要(凭证明细)', size=64),#明细
        'zhaiyao1_operate':fields.selection(_operate2, u'摘要比较符号'),
        
        'muli_operate':fields.selection(_operate4, u'摘要连接符号'),
        
        'zhaiyao2': fields.char(u'摘要(凭证明细)', size=64),#ok
        'zhaiyao2_operate':fields.selection(_operate2, u'摘要比较符号'),
        "pz_name": fields.char(u"凭证号(包含)"),#主
        "name": fields.char(u"名称"),
        'res_company_id':fields.many2one('res.company', u'公司', required=True),
    }
    _default={
       'name':'辅助明细查询',
       'res_company_id': lambda self, cr, uid, context: self.pool.get('res.company')._company_default_get(cr, uid, 'analytic.line.query',context=context),
    }
    def query_line(self, cr, uid, ids, context=None):
        #t_ir_model_date = self.pool.get("ir.model.data")
        #tree_id = t_ir_model_date.get_object_reference(cr, uid, "wgmf_account_develop", "view_move_line_querytree")[1]
        wizard = self.browse(cr, uid, ids[0], context=None)
        res_company_id=wizard.res_company_id.id
        res_ids=[]
        where = ' line.company_id=%s'%res_company_id
        where_main='main.company_id=%s'%res_company_id
        #会计期间过滤
        if wizard.period_id_operate:
            where +=' and line.period_id %s %s'%(wizard.period_id_operate,wizard.period_id.id)
        #科目过滤   
        if wizard.general_account_id_operate:
            account_ids=self.pool.get('account.account').search(cr,uid,[('id',wizard.general_account_id_operate,wizard.general_account_id.id)])
            where +=' and line.general_account_id in %s'%(tuple(account_ids+[0]),)
        #辅助科目按名字过滤   
        if wizard.account_name_operate:
            where +=""" and line.account_id in 
                    (select id from account_analytic_account where name %s '%%%s%%')"""%(wizard.account_name_operate,wizard.account_name,)
        #辅助科目过滤   
        if wizard.muli_operate2:
            if wizard.account_id_operate!='unset':
                account_ids=self.pool.get('account.analytic.account').search(cr,uid,[('id',wizard.account_id_operate,wizard.account_id.id)])
                line1='line.account_id in %s'%(tuple(account_ids+[0]))
            else:
                line1='line.account_id is null'
            if wizard.account_id_operate2!='unset':
                account_ids2=self.pool.get('account.analytic.account').search(cr,uid,[('id',wizard.account_id_operate2,wizard.account_id2.id)])
                line2='line.account_id in %s'%(tuple(account_ids2+[0]))
            else:
                line2='line.account_id is null'
            where +=' and ( %s %s %s )'%(line1,wizard.muli_operate2,line2)
        elif wizard.account_id_operate:
            if wizard.account_id_operate!='unset':
                account_ids=self.pool.get('account.analytic.account').search(cr,uid,[('id',wizard.account_id_operate,wizard.account_id.id)])
                where +=' and line.account_id in %s'%(tuple(account_ids+[0]),)            
            else:
                where +=' and line.account_id is null ' 
        #金额
        if wizard.line_amount_operate:
            where +=' and line.amount %s  %s  '%(wizard.line_amount_operate,wizard.line_amount)
        #凭证明细摘要
        if  wizard.muli_operate:
            where_main +=""" and (main.name %s '%%%s%%' %s 
             main.name %s '%%%s%%')"""%(wizard.zhaiyao1_operate,wizard.zhaiyao1,wizard.muli_operate,wizard.zhaiyao2_operate,wizard.zhaiyao2)
        elif  wizard.zhaiyao1_operate:
            where_main +=""" and main.name %s '%%%s%%'"""%(wizard.zhaiyao1_operate,wizard.zhaiyao1)
        
        where_main2_join=''
        where_main2='1=1'
        #区分专卖店和专柜的账目
        if context.get('zmd'):
            where_main2+=' and wgmf_type in (1,7)'
        if context.get('zg'):
            where_main2+=' and wgmf_type not in (1,7)'
        #凭证号
        if wizard.pz_name:
            where_main2 +=""" and main2.name like '%%%s%%'"""%(wizard.pz_name)
            
        if  where_main2=='1=1':
            if  where_main=='1=1':
                sql="""select id from account_analytic_line line where %s order
                       by line.id desc"""%(where)
            else:
                sql="""select line.id from account_analytic_line line 
                       left join account_move_line main on main.id=line.move_id
                       where %s and %s order by line.period_id desc,line.id desc"""%(where,where_main)
        else:
            sql="""select line.id from account_analytic_line line 
                   left join account_move_line main on main.id=line.move_id
                   left join account_move main2 on main2.id=main.move_id
                   where %s and %s and %s 
                   order by main2.name,line.id desc"""%(where,where_main,where_main2)
        cr.execute(sql)
        res_ids=cr.fetchall()
        need_ids = [one_res[0] for one_res in res_ids]
        if not need_ids:raise osv.except_osv(_('警告!'), _('未找到您需要的核算项目记录!'))
        domain=[('line_id','=',wizard.id)]
        if context.get('print',False):
            _name = str(uuid.uuid1())
            _field =[u'凭证字号',u'会计期间',u'摘要',u'金额',u'辅助核算项目/门店',u'办事处',u'大区',u'科目名称', u'科目编码']  
            _command="""select c.name,g.name,b.name,-a.amount,d.name,e.name,f.name,h.name,h.code
            from account_analytic_line a
            left join account_move_line b on b.id=a.move_id
            left join account_move c on c.id=b.move_id
            left join account_analytic_account d on d.id=a.account_id
            left join account_analytic_account e on e.id=d.parent_id
            left join account_analytic_account f on f.id=e.parent_id
            left join account_period g on g.id=a.period_id
            left join account_account h on h.id=a.general_account_id
            where a.id in %s"""%(tuple(need_ids+[0]),)
            _file_name = u'核算项目明细查询'
            _Export.do(self, cr, uid, _name, _file_name, _command, _field)
            return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name}
        sql_now="""select to_char(now() - interval '8 hours', 'yyyy-mm-dd HH24:MI:SS')"""
        cr.execute(sql_now)
        time_now=cr.fetchall()[0][0]            
        _command="""insert into analytic_report_query (name,line_id,pz_name,period,amount,shop,shop_parent1,shop_parent2,account_name,account_code,create_uid,create_date,write_date)
        (select b.name,%s,c.name,g.name,-a.amount,d.name,e.name,f.name,h.name,h.code,%s,'%s','%s'
        from account_analytic_line a
        left join account_move_line b on b.id=a.move_id
        left join account_move c on c.id=b.move_id
        left join account_analytic_account d on d.id=a.account_id
        left join account_analytic_account e on e.id=d.parent_id
        left join account_analytic_account f on f.id=e.parent_id
        left join account_period g on g.id=a.period_id
        left join account_account h on h.id=a.general_account_id
        where a.id in %s)"""%(wizard.id,uid,time_now,time_now,tuple(need_ids+[0]),)
        _command_del="""delete from analytic_report_query where line_id=%s or create_uid=%s"""%(wizard.id,uid)
        cr.execute(_command_del)
        cr.execute(_command)
        cr.commit()
        return {
            "name": "辅助明细",
            "type": "ir.actions.act_window",
            "res_model": "analytic.report.query",
            "view_type": "form",
            "view_mode": "tree",
            'views': [(False, 'tree')],
            'domain':domain,
            "context": {},
        }
analytic_line_query()


class analytic_report_query(osv.osv_memory):
    _name = "analytic.report.query"
    _order = "id desc"
    _columns = {
        "line_id": fields.integer(u"辅助明细id"),
        "name": fields.char(u"摘要"),
        "pz_name": fields.char(u"凭证字号"),
        "period": fields.char(u"会计期间"),
        "amount": fields.float(u"金额",digit=(15,4)),
        "shop": fields.char(u"辅助核算项目/门店"),
        "shop_parent1": fields.char(u"办事处"),
        "shop_parent2": fields.char(u"大区"),
        "account_name": fields.char(u"科目名称"),
        "account_code": fields.char(u"科目编码"),
    }
analytic_report_query()












