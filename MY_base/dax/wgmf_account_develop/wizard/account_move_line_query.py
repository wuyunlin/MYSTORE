# -*- coding: utf-8 -*- #

import time
from osv import osv, fields
import logging
import netsvc
from tools.translate import _
import uuid
from openerp.addons.web.controllers.main import ExportData2Excel as _Export

logger = logging.getLogger(__name__)
class account_move_line_query(osv.osv_memory):
    _name = "account.move.line.query"
    _order = "id desc"
    _description = u"凭证查询"
    _operate3=[('like','包含'),('=','等于'),('not like','不包含')]
    _operate4=[('or','或者'),('and','与')]
    _columns = {
        "period_id": fields.many2one("account.period", string=u"会计期间",domain="[('company_id','=',res_company_id)]"),#ok
        'zhaiyao': fields.char(u'摘要', size=64),#ok
        'zhaiyao_operate':fields.selection(_operate3, u'摘要比较符号'), 
        'muli_operate':fields.selection(_operate4, u'链接关系'),
        'zhaiyao2': fields.char(u'摘要', size=64),#ok
        'zhaiyao2_operate':fields.selection(_operate3, u'摘要比较符号'), 
        
        "zhidan": fields.many2one("res.users", string=u"制单人"),#主
        "account_id": fields.many2one("account.account", string=u"科目",domain="[('company_id','=',res_company_id)]"),#ok
        "account_id2": fields.char(u"对方科目(包含)",size=64,domain="[('company_id','=',res_company_id)]"),#ok
        "line_amount": fields.float(u"明细金额"),
        "pz_amount": fields.float(u"凭证金额"),#主
        "type_id": fields.many2one("wgmf.account.pzz", string=u"凭证字"),#主
        "pz_name": fields.char(u"凭证号(包含)"),#主
        "date_start": fields.date(u"开始时间(生效时间)"),#ok
        "date_end": fields.date(u"结束时间(生效时间)"),#ok
        "name": fields.char(u"名称"),
        "name2": fields.char(u"xx"),
        'res_company_id':fields.many2one('res.company', u'公司', required=True),
    }
    _default={
       'name':'凭证查询',
       'res_company_id': lambda self, cr, uid, context: self.pool.get('res.company')._company_default_get(cr, uid, 'account.move.line.query',context=context),
    }
    def query_line(self, cr, uid, ids, context=None):
        t_ir_model_date = self.pool.get("ir.model.data")
        tree_id = t_ir_model_date.get_object_reference(cr, uid, "wgmf_account_develop", "view_move_line_querytree")[1]
        wizard = self.browse(cr, uid, ids[0], context=None)
        res_ids=[]
        first=True
        res_company_id=wizard.res_company_id.id
        where = '  and company_id=%s'%res_company_id
        if wizard.period_id:
            where +='and period_id=%s'%(wizard.period_id.id)
            
        #if wizard.zhaiyao:
            #where +="""and name like '%%%s%%'"""%(wizard.zhaiyao)
        if  wizard.muli_operate:
            where +="""and (name %s '%%%s%%'  %s  name %s '%%%s%%')"""%(wizard.zhaiyao_operate,wizard.zhaiyao,wizard.muli_operate,wizard.zhaiyao2_operate,wizard.zhaiyao2)
        elif wizard.zhaiyao_operate:
            where +="""and name %s '%%%s%%'"""%(wizard.zhaiyao_operate,wizard.zhaiyao)
        if wizard.account_id:
            account_ids=self.pool.get('account.account').search(cr,uid,[('id','child_of',wizard.account_id.id)])
            #where +='and account_id = %s'%(wizard.account_id.id)
            where +='and account_id in %s'%(tuple(account_ids+[0]),)
        if wizard.date_start:
            where +="""and date_maturity >= '%s'"""%(wizard.date_start)
        if wizard.date_end:
            where +="""and date_maturity <= '%s'"""%(wizard.date_end)
        if where:
            move_sql1="""select move_id from account_move_line where 1=1"""+where
            cr.execute(move_sql1)
            myid_1=cr.fetchall()
            res_ids = [one_res[0] for one_res in myid_1]
            first=False
        where2= ''
        if wizard.zhidan:
            where2 +='and create_uid=%s'%(wizard.zhidan.id)
        if wizard.type_id:
            where2 +='and wgmf_type=%s'%(wizard.type_id.id)
        else:
            if context.get('zmd'):
                where2 +='and wgmf_type in (1,7)'
            if context.get('zg'):
                where2 +='and wgmf_type not in (1,7)'                
        if wizard.pz_name:
            where2 +="""and name like '%%%s%%'"""%(wizard.pz_name)
        if where2:
            move_sql2= """select id from account_move where 1=1"""+where2
            cr.execute(move_sql2)
            myid_2=cr.fetchall()
            res_ids2 = [one_res[0] for one_res in myid_2]
            if first:
                res_ids=res_ids2
                first=False
            else:
                res_ids=[ i for i in res_ids if i in res_ids2 ]
        where3=''
        if wizard.line_amount:
            where3+='and debit=%s or credit=%s'%(wizard.line_amount,wizard.line_amount)
            move_sql3= """select move_id from account_move_line where 1=1"""+where3
            cr.execute(move_sql3)
            myid_3=cr.fetchall()
            res_ids3 = [one_res[0] for one_res in myid_3]
            if first:
                res_ids=res_ids3
                first=False
            else:
                res_ids=[ i for i in res_ids if i in res_ids3 ]
        where4=''
        if wizard.pz_amount:
            where4='and b.amount=%s'%(wizard.pz_amount)
            move_sql4="""select b.move_id from (select move_id,sum(debit) as amount from account_move_line group by move_id) b where 1=1"""+where4
            cr.execute(move_sql4)
            myid_4=cr.fetchall()
            res_ids4 = [one_res[0] for one_res in myid_4]
            if first:
                res_ids=res_ids4
                first=False
            else:
                res_ids=[ i for i in res_ids if i in res_ids4]
        if not res_ids:
            if not first:raise osv.except_osv(_('警告!'), _('未找到您需要的凭证记录!'))
            sql_line="""select id from account_move_line order by move_id,abs_debit desc"""
        else:
            if len(res_ids)!=1:
                if wizard.account_id2 and wizard.account_id:
                    account_id2=str(wizard.account_id2).strip()
                    account_id2_sql="""select distinct move_id from account_move_line where account_id in (select id from account_account where 
                    code like '%%%s%%' or name like '%%%s%%')
                    and move_id in %s"""%(account_id2,account_id2,tuple(res_ids,),)
                    cr.execute(account_id2_sql)
                    account_id2_id=cr.fetchall()
                    if not account_id2_id:raise osv.except_osv(_('警告!'), _('未找到您需要的凭证记录!'))
                    res_ids = [one_res[0] for one_res in account_id2_id]
                    if len(res_ids)!=1:
                        sql_line="""select id from account_move_line where move_id in %s order by move_id,abs_debit desc"""%(tuple(res_ids,),)
                    else:
                        sql_line="""select id from account_move_line where move_id = %s order by move_id,abs_debit desc"""%(res_ids[0])
                else:
                    sql_line="""select id from account_move_line where move_id in %s order by move_id,abs_debit desc"""%(tuple(res_ids,),)
            else:
                if wizard.account_id2 and wizard.account_id:
                    account_id2=str(wizard.account_id2).strip()
                    account_id2_sql="""select distinct move_id from account_move_line where account_id in (select id from account_account where 
                    code like '%%%s%%' or name like '%%%s%%')
                    and move_id = %s"""%(account_id2,account_id2,res_ids[0],)
                    cr.execute(account_id2_sql)
                    account_id2_id=cr.fetchall()
                    if not account_id2_id:raise osv.except_osv(_('警告!'), _('未找到您需要的凭证记录!'))
                sql_line="""select id from account_move_line where move_id = %s order by  move_id,abs_debit desc"""%(res_ids[0])
        cr.execute(sql_line)
        line_fetch=cr.fetchall()
        line_ids = [one_res[0] for one_res in line_fetch]
        domain=[('id','in',line_ids)]
        if context.get('print',False):
            _name = str(uuid.uuid1())
            _field =[u'凭证字号',u'摘要',u'制单人',u'科目编码',u'科目名称',u'借方',u'贷方', u'会计期间',u'业务伙伴',u'辅助核算项',u'到期日期']  
            _command="""select m.name,line.name,rp1.name,aa.code,aa.name,line.debit,
            line.credit,ap.name,rp.name,aaa.name,line.date_maturity
            from account_move_line line 
            left join account_move m on m.id=line.move_id
            left join account_account aa on aa.id=line.account_id
            left join account_period ap on ap.id=line.period_id
            left join account_analytic_account aaa on aaa.id=line.analytic_account_id
            left join res_users ru on ru.id=m.create_uid
            left join res_partner rp1 on rp1.id=ru.partner_id
            left join res_partner rp on rp.id=line.partner_id where line.id in %s"""%(tuple(line_ids+[0]),)
            _file_name = u'凭证条件查询'
            _Export.do(self, cr, uid, _name, _file_name, _command, _field)
            return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name}
        #if wizard.line_amount:
        return {
            "name": "明细账",
            "type": "ir.actions.act_window",
            "res_model": "account.move.line",
            "view_type": "form",
            "view_mode": "tree,form",
            'views': [(tree_id, 'tree'),(False, 'form')],
            'domain':domain,
            "context": {},
        }
        
        
    def query_line2(self, cr, uid, ids, context=None):
        t_ir_model_date = self.pool.get("ir.model.data")
        tree_id = t_ir_model_date.get_object_reference(cr, uid, "wgmf_account_develop", "view_move_line_querytree")[1]
        wizard = self.browse(cr, uid, ids[0], context=None)
        res_ids=[]
        first=True
        where = ' '
        if wizard.period_id:
            where +='and period_id=%s'%(wizard.period_id.id)
        if wizard.zhaiyao:
            where +="""and name like '%%%s%%'"""%(wizard.zhaiyao)
        if wizard.account_id:
            account_ids=self.pool.get('account.account').search(cr,uid,[('id','child_of',wizard.account_id.id)])
            #where +='and account_id = %s'%(wizard.account_id.id)
            where +='and account_id in %s'%(tuple(account_ids+[0]),)
        if wizard.date_start:
            where +="""and date_maturity >= '%s'"""%(wizard.date_start)
        if wizard.date_end:
            where +="""and date_maturity <= '%s'"""%(wizard.date_end)
        if where:
            move_sql1="""select move_id from account_move_line where 1=1"""+where
            cr.execute(move_sql1)
            myid_1=cr.fetchall()
            res_ids = [one_res[0] for one_res in myid_1]
            first=False
        where2= ''
        if wizard.zhidan:
            where2 +='and create_uid=%s'%(wizard.zhidan.id)
        if wizard.type_id:
            where2 +='and wgmf_type=%s'%(wizard.type_id.id)
        if wizard.pz_name:
            where2 +="""and name like '%%%s%%'"""%(wizard.pz_name)
        if where2:
            move_sql2= """select id from account_move where 1=1"""+where2
            cr.execute(move_sql2)
            myid_2=cr.fetchall()
            res_ids2 = [one_res[0] for one_res in myid_2]
            if first:
                res_ids=res_ids2
                first=False
            else:
                res_ids=[ i for i in res_ids if i in res_ids2 ]
        where3=''
        if wizard.line_amount:
            where3+='and debit=%s or credit=%s'%(wizard.line_amount,wizard.line_amount)
            move_sql3= """select move_id from account_move_line where 1=1"""+where3
            cr.execute(move_sql3)
            myid_3=cr.fetchall()
            res_ids3 = [one_res[0] for one_res in myid_3]
            if first:
                res_ids=res_ids3
                first=False
            else:
                res_ids=[ i for i in res_ids if i in res_ids3 ]
        where4=''
        if wizard.pz_amount:
            where4='and b.amount=%s'%(wizard.pz_amount)
            move_sql4="""select b.move_id from (select move_id,sum(debit) as amount from account_move_line group by move_id) b where 1=1"""+where4
            cr.execute(move_sql4)
            myid_4=cr.fetchall()
            res_ids4 = [one_res[0] for one_res in myid_4]
            if first:
                res_ids=res_ids4
                first=False
            else:
                res_ids=[ i for i in res_ids if i in res_ids4]
        if not res_ids:
            if not first:raise osv.except_osv(_('警告!'), _('未找到您需要的凭证记录!'))
            sql_line="""select id from account_move_line order by move_id,abs_debit desc"""
        else:
            if len(res_ids)!=1:
                if wizard.account_id2 and wizard.account_id:
                    account_id2=str(wizard.account_id2).strip()
                    account_id2_sql="""select distinct move_id from account_move_line where account_id in (select id from account_account where 
                    code like '%%%s%%' or name like '%%%s%%')
                    and move_id in %s"""%(account_id2,account_id2,tuple(res_ids,),)
                    cr.execute(account_id2_sql)
                    account_id2_id=cr.fetchall()
                    if not account_id2_id:raise osv.except_osv(_('警告!'), _('未找到您需要的凭证记录!'))
                    res_ids = [one_res[0] for one_res in account_id2_id]
                    if len(res_ids)!=1:
                        sql_line="""select id from account_move_line where move_id in %s order by move_id,abs_debit desc"""%(tuple(res_ids,),)
                    else:
                        sql_line="""select id from account_move_line where move_id = %s order by move_id,abs_debit desc"""%(res_ids[0])
                else:
                    sql_line="""select id from account_move_line where move_id in %s order by move_id,abs_debit desc"""%(tuple(res_ids,),)
            else:
                if wizard.account_id2 and wizard.account_id:
                    account_id2=str(wizard.account_id2).strip()
                    account_id2_sql="""select distinct move_id from account_move_line where account_id in (select id from account_account where 
                    code like '%%%s%%' or name like '%%%s%%')
                    and move_id = %s"""%(account_id2,account_id2,res_ids[0],)
                    cr.execute(account_id2_sql)
                    account_id2_id=cr.fetchall()
                    if not account_id2_id:raise osv.except_osv(_('警告!'), _('未找到您需要的凭证记录!'))
                sql_line="""select id from account_move_line where move_id = %s order by  move_id,abs_debit desc"""%(res_ids[0])
        cr.execute(sql_line)
        line_fetch=cr.fetchall()
        line_ids = [one_res[0] for one_res in line_fetch]
        domain=[('id','in',line_ids)]
        if context.get('print',False):
            _name = str(uuid.uuid1())
            _field =[u'凭证字号',u'摘要',u'制单人',u'科目编码',u'科目名称',u'借方',u'贷方', u'会计期间',u'业务伙伴',u'辅助核算项',u'到期日期']  
            _command="""select m.name,line.name,rp1.name,aa.code,aa.name,line.debit,
            line.credit,ap.name,rp.name,aaa.name,line.date_maturity
            from account_move_line line 
            left join account_move m on m.id=line.move_id
            left join account_account aa on aa.id=line.account_id
            left join account_period ap on ap.id=line.period_id
            left join account_analytic_account aaa on aaa.id=line.analytic_account_id
            left join res_users ru on ru.id=m.create_uid
            left join res_partner rp1 on rp1.id=ru.partner_id
            left join res_partner rp on rp.id=line.partner_id where line.id in %s"""%(tuple(line_ids+[0]),)
            _file_name = u'凭证条件查询'
            _Export.do(self, cr, uid, _name, _file_name, _command, _field)
            return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name}
        #if wizard.line_amount:
        return {
            "name": "明细账",
            "type": "ir.actions.act_window",
            "res_model": "account.move.line",
            "view_type": "form",
            "view_mode": "tree,form",
            'views': [(tree_id, 'tree'),(False, 'form')],
            'domain':domain,
            "context": {},
        }
account_move_line_query()