# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
import psycopg2
import netsvc
import struct
import xlrd
import base64
import time
from time import localtime
import datetime
from xlrd import open_workbook,xldate_as_tuple

class wgmf_create_fukuan(osv.osv_memory):
    _name = "wgmf.create.fukuan"
    _order = "id desc"
    _columns = {
        'file':  fields.binary('导入文件', filters='*.xls'),
        }
    def create_fukuan(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        mod_obj=self.pool.get('ir.model.data')
        user_obj = self.pool.get('res.users')
        shop_obj = self.pool.get('sale.shop')
        account_obj = self.pool.get('account.account')
        act_obj=self.pool.get('ir.actions.act_window')
        for wiz in self.browse(cr,uid,ids):
            if not wiz.file:
                continue
        excel = xlrd.open_workbook(file_contents=base64.decodestring(wiz.file))
        sh = excel.sheet_by_index(0)
        voucher_obj=self.pool.get('account.voucher')
        dic={}
        newids=[]
        for rx in range(sh.nrows):
            if rx<=1:continue
            rp_name = str(sh.cell(rx, 0).value or '')#客户//A
            date = sh.cell(rx, 1).value or false#生效日期//B
            if date:
                try:
                    date = datetime.date.fromordinal(datetime.date(1899,12,31).toordinal()-1+int(date)).strftime("%Y-%m-%d")
                except Exception, e:
                    date=False
            amount = sh.cell(rx,2).value or 0#付款金额//C
            type = str(sh.cell(rx, 3).value or '')#付款方式//D
            type=type.split('(')[0]
            #price = str(sh.cell(rx, 4).value or '')#制单人//E
            search_dic={'rp_name':rp_name,'date':date,'amount':amount,'type':type,'rx':rx}
            if not date:raise osv.except_osv(_('错误'), _(' rx=%(rx)s 日期(%(rp_name)s) 错误'% (search_dic)))
            sql="""select id from res_partner where (name='%(rp_name)s'  or code='%(rp_name)s') and wgjsdw"""%(search_dic)
            cr.execute(sql)
            rp_id =cr.fetchall()
            if not rp_id:raise osv.except_osv(_('错误'), _(' rx=%(rx)s 客户(%(rp_name)s) 没有查询到,建议使用编码'% (search_dic)))
            if len(rp_id)>1:raise osv.except_osv(_('错误'), _(' rx=%(rx)s 客户(%(rp_name)s) 没有查询到多个,建议使用编码'% (search_dic)))
            rp_id=rp_id[0][0]
            dic.update({'rp_id':rp_id})
            sql="""select id,default_debit_account_id from account_journal where name='%(type)s'"""%(search_dic)
            cr.execute(sql)
            type_id =cr.fetchall()
            if not type_id:raise osv.except_osv(_('错误'), _(' rx=%(rx)s 付款类型(%(type)s) 没有查询到'% (search_dic)))
            if len(type_id)>1:raise osv.except_osv(_('错误'), _(' rx=%(rx)s 付款类型(%(type)s) 没有查询到多个'% (search_dic)))
            type_id,account_id=type_id[0]
            sql="""select id from account_period where special='f' and date_start<='%(date)s'
                   and date_stop>='%(date)s'   and company_id=1"""%(search_dic)
            cr.execute(sql)
            period_id=cr.fetchall()
            if not period_id:raise osv.except_osv(_('错误'), _(' rx=%(rx)s 日期(%(date)s)对应的期间不可用'% (search_dic)))
            if len(period_id)>1:raise osv.except_osv(_('错误'), _(' rx=%(rx)s 日期(%(date)s)对应的期间不可用'% (search_dic)))
            period_id=period_id[0][0]
            dic.update({'type_id':type_id,'account_id':account_id,
                        'payment_rate':1,'type':'receipt',
                        'state':'draft','company_id':1,
                        'amount':amount,'date':date,
                        'period_id':period_id,'payment_rate_currency_id':8,
                        'partner_id':rp_id,'journal_id':type_id,
            })
            newid=voucher_obj.create(cr,uid,dic,context)
            newids.append(newid)
        voucher_obj.approve_submit_nahao(cr,uid,newids,context)
        view=mod_obj.get_object_reference(cr,uid,'account_voucher','action_vendor_receipt')
        view_id=view and view[1] or False
        result=act_obj.read(cr,uid,[view_id])[0]
        result['domain'] = [('id','in',newids)]
        return result
wgmf_create_fukuan()
 
