# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
import psycopg2
import netsvc
import struct
import xlrd
import base64
import time
from time import localtime
import datetime
import uuid
from openerp.addons.web.controllers.main import ExportData2Excel as _Export


class wgmf_data_export(osv.osv_memory):
    _name = "wgmf.data.export"
    _order = "id desc"
    _type = [('hsx','核算项'),('js','结算单位'),('sc','商超'),
    ('zy','职员'),('md','门店'),('srpz','专柜销售凭证'),('wgrk','外购入库'),('wgrk_ng','外购入库退货')]
    _columns = {
        'start_date':  fields.date('开始日期(基础资料的创建时间)'),
        'end_date':  fields.date('结束日期(基础资料的创建时间)'),
        'type': fields.selection(_type, u'基础数据选项'),
        'sql_text': fields.text(u'数据库文本',required=True),
        'head_text': fields.text(u'标题文本',required=True),
        'file_name': fields.char(u'文件名称',size=64,required=True),
        }
    def onchange_sql(self, cr, uid, ids, start_date=False,end_date=False,type=False,context=None):
        if context is None:
            context = {}
        result = {}
        date_sql=''
        date_sc=''
        if start_date:
            start_date+=' 00:00:00'
            start_date = (datetime.datetime.strptime(start_date,'%Y-%m-%d %H:%M:%S') - datetime.timedelta(hours=8)).strftime('%Y-%m-%d %H:%M:%S')
            date_sql+=""" and aa.create_date>='%s'"""%(start_date)
            date_sc+=""" and aa.saledate>='%s'"""%(start_date)
        if end_date:
            end_date+=' 16:00:00'
            date_sql+=""" and aa.create_date<'%s'"""%(end_date)
            date_sc+=""" and aa.saledate<'%s'"""%(end_date)
        dic={'date_sql':date_sql,'date_sc':date_sc,}
        if type=='hsx':
            sql_text="""select aa.code as 编码,aa.name as 名称,
                        case aa.type when 'normal' then '常规科目'
                        when 'view' then '视图'
                        else '其他' end as 类型,
                        ab.name as 上级科目名称,
                        ab.code as 上级编码,
                        to_char(aa.create_date+ interval '8 hours','yyyy/mm/dd') as 创建时间
                        from  account_analytic_account aa
                        left join account_analytic_account ab
                        on ab.id=aa.parent_id
                        where 1=1 %(date_sql)s order by ab.code desc"""%(dic)
            head_text='编码,名称,类型,上级辅助名称,上级辅助编码,创建时间'
            file_name='新增核算项目'
        #结算单位
        if type=='js':
            sql_text="""select aa.code,aa.name,ab.code,ab.name,
                        to_char(aa.create_date+ interval '8 hours','yyyy/mm/dd') as 创建时间
                        from  res_partner aa
                        left join res_partner ab on ab.id=aa.parent_id 
                        where 1=1 %(date_sql)s
                        and aa.shangchao and aa.wgjsdw"""%(dic)
            head_text='编码,名称,所属商超编码,所属商超名称,创建时间'
            file_name='新增结算单位'
        #商超
        if type=='sc':
            sql_text="""select aa.code,aa.name,
                        to_char(aa.create_date+ interval '8 hours','yyyy/mm/dd') as 创建时间
                        from  res_partner aa
                        where 1=1 %(date_sql)s
                        and aa.shangchao and not aa.wgjsdw"""%(dic)
            head_text='编码,名称,创建时间'
            file_name='新增商超'
            result.update({'sql_text':sql_text,'head_text':head_text})
        #职员
        if type=='zy':
            sql_text="""select aa.code,aa.name,ab.code,ab.name,
                        to_char(aa.create_date+ interval '8 hours','yyyy/mm/dd') as 创建时间
                        from  res_partner aa
                        left join res_partner ab on ab.id=aa.parent_id 
                        where 1=1 %(date_sql)s
                        and not aa.shangchao and aa.code is not null"""%(dic)
            head_text='编码,名称,所属办事处编码,所属办事处名称,创建时间'
            file_name='新增职员'
        if type=='md':
            sql_text="""select aa.stores_no,aa.name,aa.k3stock,rp2.code,rp2.name,
                        rp.code,rp.name,or2.name,or3.name,ana.code,ana.name,
                        to_char(aa.create_date+ interval '8 hours','yyyy/mm/dd') as 创建时间
                        from  sale_shop aa
                        left join res_partner rp on rp.id=aa.partner_id 
                        left join res_partner rp2 on rp2.id=aa.stid 
                        left join wgmf_sale_organization or1 on or1.id=aa.sale_organization
                        left join wgmf_sale_organization or2 on or2.id=or1.parent_id
                        left join wgmf_sale_organization or3 on or3.id=or2.parent_id
                        left join account_analytic_account ana on ana.id=aa.project_id
                        where 1=1 %(date_sql)s"""%(dic)
            head_text='编码,名称,K3仓库码,所属结算代码,所属结算名称,所属商超代码,所属商超名称,办事处,大区,辅助核算项编码,辅助核算项名称,创建时间'
            file_name='新增门店'
        if type=='srpz':
            sc="""select id from wgmf_sale_check aa where 1=1 %(date_sc)s"""%(dic)
            dic.update({'sc':sc})
            sql_text="""select aa.name,account.name,aa.debit,ap.name,rp.name,shop.name,
                        rp1.name,rp2.name,or2.name,or3.name,aa.ref,
                        aa.date_maturity as 创建时间
                        from  account_move_line aa
                        left join res_partner rp on rp.id=aa.partner_id
                        left join account_move am on am.id=aa.move_id 
                        left join account_period ap on ap.id=aa.period_id 
                        left join wgmf_sale_check sc on sc.id=am.check_id
                        left join sale_shop shop on shop.id=sc.shop_id
                        left join res_partner rp1 on rp1.id=shop.stid
                        left join res_partner rp2 on rp2.id=shop.partner_id
                        left join account_account account on account.id=aa.account_id
                        left join wgmf_sale_organization or1 on or1.id=shop.sale_organization
                        left join wgmf_sale_organization or2 on or2.id=or1.parent_id
                        left join wgmf_sale_organization or3 on or3.id=or2.parent_id
                        where am.check_id in (%(sc)s) and aa.account_id=524"""%(dic)
            head_text='摘要,科目,借方,期间,业务伙伴,门店,结算单位,商超公司,办事处,大区,关联,到期时间'
            file_name='销售凭证'
        if  type=='wgrk':
            sql_text="""select pt.name,pr.default_code,sum(aa.product_qty)
            from stock_move aa left join product_product pr on pr.id=aa.product_id
            left join product_template pt on pt.id=pr.product_tmpl_id
            where aa.location_dest_id = 89
                    and aa.location_id = 12
            and aa.state='done' 
            and aa.date >= '%s'
            and aa.date <= '%s' group by pt.id,pr.id order by pr.default_code
            """%(start_date,end_date)
            head_text='产品名称,产品编码,数量'
            file_name='外购入库(不含退货)'
        if type=='wgrk_ng':
            sql_text="""select pt.name,pr.default_code,-sum(aa.product_qty)
            from stock_move aa left join product_product pr on pr.id=aa.product_id
            left join product_template pt on pt.id=pr.product_tmpl_id
            where aa.location_dest_id = 12
                    and aa.location_id = 89
            and aa.state='done' 
            and aa.date >= '%s'
            and aa.date <= '%s' group by pt.id,pr.id order by pr.default_code
            """%(start_date,end_date)
            head_text='产品名称,产品编码,数量'
            file_name='外购入库(退货)' 
        if not type:
            sql_text=head_text=file_name=''       
        result.update({'sql_text':sql_text,'head_text':head_text,'file_name':file_name})
        return {'value': result}
    def export_data(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cr, uid, ids[0], context=None)
        head_text,sql_text,file_name=wizard.head_text,wizard.sql_text,wizard.file_name
        limit_action=['update','insert','delete']
        for ac in limit_action:
            if ac in sql_text:
                raise osv.except_osv(_('警告!'), 
                _('您的执行语句中包含限制使用的动作(%s)!')%ac)
        _name = str(uuid.uuid1())
        _field =head_text.split(',')
        _command=sql_text
        _file_name =file_name 
        _Export.do(self, cr, uid, _name, _file_name, _command, _field)
        return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name}
wgmf_data_export()
 
