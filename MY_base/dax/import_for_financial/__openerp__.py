# -*- encoding: utf-8 -*-
{
    'name': "五谷付款导入",
    'description': "付款单导入及财务基础资料导出",
    'version': '0.1',
    'category': 'Generic Modules/report',
    'depends': ['wgmf_account_customize','product'],
    'data': [],
    'update_xml': [
                   'wizard/wgmf_create_fukan_view.xml',
                   'wizard/wgmf_export_data.xml',
                  ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
