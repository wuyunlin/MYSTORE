# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.tools.safe_eval import safe_eval
from openerp import SUPERUSER_ID
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp
import time
import MySQLdb
import calendar
from calendar import monthrange
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import logging
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class product_uom(osv.osv):
    _inherit = 'product.uom'
    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default = default.copy()
        default.update({'name':default.get('name','')+' copy'})
        return super(product_uom, self).copy(cr, uid, id, default, context)
    _columns = {
        'name': fields.char('计量单位', required=True, translate=False),
        }
    def check_active_name(self, cr, uid, ids, context=None):
        for record in self.browse(cr, uid, ids, context=context):
                history=self.pool.get('product.uom').search(cr,uid,[('id','!=',record.id),('name','=',record.name)])
                if history and record.name:
                    raise osv.except_osv(u'警告',u'该单位名称 %s 已存在，请修改!'%(record.name))
        return True
    _constraints = [(check_active_name,'name should be unique!',['name']),]
product_uom()

