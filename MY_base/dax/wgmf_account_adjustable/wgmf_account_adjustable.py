# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.tools.safe_eval import safe_eval
from openerp import SUPERUSER_ID
from tools.translate import _
import datetime
import time
import logging
_logger = logging.getLogger(__name__)

class account_period(osv.osv):
    _inherit = 'account.period'

    def action_draft(self, cr, uid, ids, *args):
        mode = 'draft'
        for period in self.browse(cr, uid, ids):
            if period.fiscalyear_id.state == 'done':
                raise osv.except_osv(_('Warning!'), _('You can not re-open a period which belongs to closed fiscal year'))
        cr.execute('update account_journal_period set state=%s where period_id in %s', (mode, tuple(ids),))
        cr.execute('update account_period set state=%s,biz_closed=False where id in %s', (mode, tuple(ids),))
        return True

    def action_biz_closed(self, cr, uid, ids, *args):
        cr.execute('update account_period set biz_closed=True where id in %s', (tuple(ids),))
        return True

    def _get_group(self, cr, uid, ids,name, unknow_none, context=None):
        src = "0"
        res = dict.fromkeys(ids, False)
        for o in self.browse(cr, uid, ids, context=context):
            group_obj = self.pool.get('res.groups')
            user = self.pool.get('res.users').browse(cr, uid, uid)
            md_user = group_obj.search(cr, uid, [('name', '=', '财务调账')])
            if md_user and o.state == 'draft' and o.biz_closed == False:
                md_user = md_user[0]
                group_u = group_obj.browse(cr, uid, md_user)
                for u1 in group_u.users:
                    if u1.id == uid:
                        src = "1"
        res[o.id] = src
        return res

    _columns = {
        'biz_closed': fields.boolean(u'业务关帐'),
        'user_group': fields.function(_get_group, type='char', string='user group'),
    }

account_period()

class account_period_close(osv.osv_memory):
    _inherit = "account.period.close"

    def data_save(self, cr, uid, ids, context=None):
        period_pool = self.pool.get('account.period')
        account_move_obj = self.pool.get('account.move')
        statement_obj = self.pool.get('pos.statement')
        mode = 'done'
        for form in self.read(cr, uid, ids, context=context):
            if form['sure']:
                for id in context['active_ids']:
                    account_move_ids = account_move_obj.search(cr, uid, [('period_id', '=', id), ('state', '=', "draft")], context=context)
                    if account_move_ids:
                        raise osv.except_osv(_('Invalid Action!'), _('In order to close a period, you must first post related journal entries.'))
                    draft_statement_ids = statement_obj.search(cr, uid, [('period_pz', '=', id), ('approve_stage', 'in', ("draft","approving"))])
                    if draft_statement_ids:
                        raise osv.except_osv(_('非法的动作!'), _('在关闭会计期间之前，请先审批本期间的商超对账单。'))
                    cr.execute('update account_journal_period set state=%s where period_id=%s', (mode, id))
                    cr.execute('update account_period set state=%s,biz_closed=True where id=%s', (mode, id))

        return {'type': 'ir.actions.act_window_close'}

account_period_close()

class stock_move(osv.osv):
    _inherit = 'stock.move'
    def account_adjustable(self, cr, uid, date):
        period_obj = self.pool.get('account.period')
        group_obj = self.pool.get('res.groups')
        groups = group_obj.search(cr, uid, [('name', '=', '财务调账')])
        flag = False
        if groups:
            if len(date) == 10:
                d = datetime.datetime.strptime(date,'%Y-%m-%d').strftime('%m/%Y')
            else:
                d = datetime.datetime.strptime(date,'%Y-%m-%d %H:%M:%S').strftime('%m/%Y')
            period_ids = period_obj.search(cr, uid, [('name', '=', d)])
            if not period_ids:
                flag = True
            if period_ids:
                biz_closed = period_obj.browse(cr, uid, period_ids[0]).biz_closed
                if biz_closed == False:
                    flag = True
                if biz_closed == True:
                    for u1 in group_obj.browse(cr, uid, groups[0]).users:
                        if u1 == uid:
                            flag = True
            if flag == False:
                raise osv.except_osv(_('错误!'), _('财务已关帐,不能操作移库单!'))
        return True

    def write(self, cr, uid, ids, vals, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        if uid != 2:
            frozen_fields = set(['product_qty', 'product_uom', 'product_uos_qty', 'product_uos', 'location_id', 'location_dest_id', 'product_id'])
            for move in self.browse(cr, uid, ids, context=context):
                if move.state == 'done' and move.product_id.type!='7':
                    self.account_adjustable(cr,uid,move.date)
                    if frozen_fields.intersection(vals):
                        raise osv.except_osv(_('Operation Forbidden!'),
                                             _('Quantities, Units of Measure, Products and Locations cannot be modified on stock moves that have already been processed (except by the Administrator).'))
                if move.state != 'done' and vals.get('date',False) and move.product_id.type!='7':
                    self.account_adjustable(cr,uid,vals.get('date',False))
        return  super(stock_move, self).write(cr, uid, ids, vals, context=context)

    def create(self, cr, uid, vals, context=None):
        vals=vals or {}
        product_id=vals.get('product_id',False)
        flag=1
        if product_id:
            sql="""select pt.type from product_product pp
                left join product_template pt on pt.id=pp.product_tmpl_id 
                where pp.id=%s"""%product_id
            cr.execute(sql)
            if cr.fetchall():flag=0
        if vals.get('state',False) == 'done' and vals.get('date',False) and flag:
            self.account_adjustable(cr,uid,vals.get('date',False))
        return super(stock_move, self).create(cr, uid, vals, context)

stock_move()

class account_move_line(osv.osv):
    _inherit = 'account.move.line'
    _columns = {
        "name": fields.char(u"摘要", size=500, required=True),
        }    
    _defaults = {
        'state': 'valid',
        }
    def look_move(self, cr, uid, ids,context=None):
        if isinstance(ids,list):ids=ids[0]
        move_id=self.browse(cr,uid,ids).move_id and self.browse(cr,uid,ids).move_id.id or False
        if not move_id:return True
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.move',
            'view_id': False,
            'res_id': move_id,
            'domain': [('id','=',399923)],
            'type': 'ir.actions.act_window',
            'target':'current',
            'context':context
        }            
    def search(self, cr, uid, args, offset=0, limit=None, order=None,
            context=None, count=False):
        if context is None:
            context = {}
        cr.execute("""select count(*) from res_users where id=%s and company_id=4"""%uid)
        fet=cr.fetchall()
        if fet[0][0]==1 and self._order<>'period_id desc,move_id desc,id':
            self._order='period_id desc,move_id desc,id'
        else:
            if  self._order<>"period_id desc,move_id desc, abs_debit desc, id":
                self._order= "period_id desc,move_id desc, abs_debit desc, id"
        return super(account_move_line, self).search(cr, uid, args, offset, limit,
                order, context=context, count=count)     
    def write(self, cr, uid, ids,vals , context=None,check=True,):
        if context is None:
            context={}
        vals=vals or {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        if uid != 2:
            frozen_fields = set(['state','journal_id','account_id', 'analytic_account_id', 'analytic_lines', 'credit', 'debit', 'move_id', 'name','ref','period_id','partner_id','date','state','statement_id'])
            for line in self.browse(cr, uid, ids, context=context):
                if line.period_id.state == 'done':
                    if frozen_fields.intersection(vals):
                        raise osv.except_osv(_('错误!'), _('财务已关帐,不能修改明细帐!'))
                if vals.get('period_id',0):
                    cr.execute("""select state from  account_period where state = 'done' and id=%s"""%(vals['period_id']))
                    if cr.fetchall():
                        raise osv.except_osv(_('错误!'), _('不能修改明细帐到已经关帐的期间!'))
                if line.move_id and line.move_id.to_check:
                    if frozen_fields.intersection(vals) and self.pool['res.users'].has_group(cr, uid, 'account.group_account_user'):
                        raise osv.except_osv(_('错误!'), _('凭证已审核,请先反审核!'))
        if vals and vals.keys()==['move_id']:
            check=False
        res=super(account_move_line, self).write(cr, uid, ids, vals, context)
        need_fields = set(['period_id','account_id','analytic_account_id', 'analytic_lines', 'credit', 'debit', 'move_id'])
        if need_fields.intersection(vals) and check:
            obj_move_line = self.pool.get('account.move.line')
            obj_move_line.create_analytic_lines(cr, uid, ids, context)
            if vals.get('period_id',False):
                if isinstance(ids,(int,long)):
                    text='id=%s'%int(ids)
                else:
                    new_ids=[int(x) for x in ids]
                    text='id in %s'%(tuple(new_ids+[0]),)
                sql="""update account_analytic_line set period_id=%s
                       where move_id in (select id from account_move_line
                       where move_id in 
                       (select distinct move_id from account_move_line 
                       where  %s))"""%(vals['period_id'],text)
                cr.execute(sql)
        #办事处借款 办事处押金 检查职员信息完整与否 
        if vals.get('account_id',False) in (536,537):
            sql="""select rp1.name,rp1.code,rp2.code from account_move_line line
                   left join res_partner rp1 on rp1.id=line.partner_id
                   left join res_partner rp2 on rp2.id=rp1.parent_id
                   where line.id in %s """%(tuple(ids+[0]),)
            cr.execute(sql)
            res=cr.fetchall()
            for xx in res:
                if not xx[0]:continue
                if not(xx[1] and  xx[2]):
                    raise osv.except_osv(_('错误!'), _('%s 职员信息不完整'%(xx[0])))
        #明细账地方修改 防止不平
        if context.get('balance_check',False) and (vals.get('debit',0) or vals.get('credit',0)):
            for line in self.browse(cr, uid, ids, context=context):
                if float('%.4f' % sum(xx.debit for xx in line.move_id.line_id)) != float('%.4f' % sum(xx.credit for xx in line.move_id.line_id)):
                    raise osv.except_osv(('错误'),_('凭证:%s 借贷不平，不能保存!'%line.move_id.name))
        cr.commit()
        return res
    def create(self, cr, uid, vals, context=None,check=True):
        if vals.get('period_id',0):
            cr.execute("""select state from  account_period where state = 'done' and id=%s"""%(vals['period_id']))
            if cr.fetchall():
                raise osv.except_osv(_('错误!'), _('财务已关帐,不能做新的明细帐!'))
        #办事处借款 办事处押金 检查职员信息完整与否
        res2=super(account_move_line, self).create(cr, uid, vals, context,check=check)
        ids=res2
        if isinstance(ids, (int, long)):
            ids = [int(ids)]
        if vals.get('account_id',False) in (536,537):
            sql="""select rp1.name,rp1.code,rp2.code from account_move_line line
                   left join res_partner rp1 on rp1.id=line.partner_id
                   left join res_partner rp2 on rp2.id=rp1.parent_id
                   where line.id in %s"""%(tuple(ids+[0]),)
            cr.execute(sql)
            res=cr.fetchall()
            for xx in res:
                if not xx[0]:continue
                if not(xx[1] and  xx[2]):
                    raise osv.except_osv(_('错误!'), _('%s 职员信息不完整:'%(xx[0])))
        return res2
account_move_line()

class account_move(osv.osv):
    _inherit = 'account.move'
    def write(self, cr, uid, ids, vals, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        for line in self.browse(cr, uid, ids, context=context):
        #没有变化的行不做处理
            if vals.get('line_id',False):
                line_id=[]
                for i in vals.get('line_id',[]):
                    if i[0]==4:continue
                    if i[0]==2:
                        cr.execute("""delete from account_move_line where id=%s"""%i[1])
                        continue
                    line_id.append(i)
                vals['line_id']=line_id            
            if line.to_check:
                if self.pool['res.users'].has_group(cr, uid, 'account.group_account_user'):
                    raise osv.except_osv(_('错误!'), _('凭证已审核,请先反审核!'))
            if line.period_id.state == 'done':
                raise osv.except_osv(_('错误!'), _('财务已关帐,不能修改凭证!'))
            if vals.get('period_id',0):
                cr.execute("""select state from  account_period where state = 'done' and id=%s"""%(vals['period_id']))
                if cr.fetchall():
                    raise osv.except_osv(_('错误!'), _('不能修改凭证到已经关帐的期间!'))
            if vals.get('period_id',0) or vals.get('journal_id',0):
                journal_id=vals.get('journal_id',False) or line.journal_id.id
                journal=self.pool.get('account.journal').browse(cr,uid,journal_id)
                if journal.sequence_id:
                    period = self.pool.get('account.period').browse(cr, uid, vals.get('period_id',False) or line.period_id.id, context)
                    c = {'fiscalyear_id': period.fiscalyear_id.id, 'seq_month':time.strftime('%Y%m',time.strptime(period.date_start,'%Y-%m-%d'))}
                    new_name = self.pool.get('ir.sequence').next_by_id(cr, uid, journal.sequence_id.id, c)
                    vals.update({'name':new_name})
                else:
                    raise osv.except_osv(_('错误!'), _('尚未设置编号规则,请联系管理员!'))
        return  super(account_move, self).write(cr, uid, ids, vals, context=context)
    def create(self, cr, uid, vals, context=None):
        if vals.get('period_id',0):
            cr.execute("""select state from  account_period where state = 'done' and id=%s"""%(vals['period_id']))
            if cr.fetchall():
                raise osv.except_osv(_('错误!'), _('财务已关帐,不能做新的凭证!'))
        return super(account_move, self).create(cr, uid, vals, context)
        
account_move()

class res_partner(osv.osv):
    _inherit = 'res.partner'

    def _check_active(self, cr, uid, ids, context=None):
        for par in self.browse(cr, uid, ids):
            if par.bm_id and par.code and par.active:
                if  self.search(cr, uid, [('bm_id','=',True),('code', '=', par.code),('id', '!=', par.id),('active','=',True)]):
                    raise osv.except_osv(('错误'),_('部门编码必须唯一!'))
            if par.zy_id and par.code and par.active:
                if  self.search(cr, uid, [('zy_id','=',True),('code', '=', par.code),('id', '!=', par.id),('active','=',True)]):
                    raise osv.except_osv(('错误'),_('职员编码必须唯一!'))
            if par.shangchao and par.code and par.active:
                if par.wgjsdw:
                    if  self.search(cr, uid, [('wgjsdw','=',True),('shangchao','=',True),('code', '=', par.code),('id', '!=', par.id),('active','=',True)]):
                        raise osv.except_osv(('错误'),_('结算单位编码必须唯一!'))
                else:
                    if  self.search(cr, uid, [('wgjsdw','=',False),('shangchao','=',True),('code', '=', par.code),('id', '!=', par.id),('active','=',True)]):
                        raise osv.except_osv(('错误'),_('商超编码必须唯一!'))
        return True

#    _columns = {
#        'credit': fields.float(u'应收款合计'),
#        'debit': fields.float(u'应付款合计'),
#    }
    _constraints =[(_check_active,'Warning',['code']),]
    
    def write(self, cr, uid, ids, vals, context=None):
        vals=vals or {}
        if vals.get('parent_id',False):
            sc=vals.get('parent_id')
            if isinstance(ids, (int, long)):
                sql="""update sale_shop set partner_id=%s
                 where stid=%s and partner_id!=%s"""%(sc,ids,sc)
                cr.execute(sql)
            else:
                sql="""update sale_shop set partner_id=%s
                     where stid in %s and partner_id!=%s"""%(sc,tuple(ids+[0]),sc)
                cr.execute(sql)
        return super(res_partner, self).write(cr, uid, ids, vals, context=context)
res_partner()


