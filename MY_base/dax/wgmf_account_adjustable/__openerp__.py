# -*- encoding: utf-8 -*-
{
    'name': "五谷财务二步关帐",
    'description': "wgmf_account_adjustable",
    'version': '0.1',
    'category': 'Generic Modules/report',
    'depends': ['base','wgmf_account_balance','wgmf_sale_check','account_financial_report_webkit'],
    'data': [],
    'update_xml': [
                   'wgmf_account_adjustable_view.xml',
                   'place_sequence.xml',
                  ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
