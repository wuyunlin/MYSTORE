# -*- coding: utf-8 -*-

from openerp import addons
import datetime
from datetime import timedelta
import time
from openerp import tools
from openerp.osv import fields, osv
from tools.translate import _
from openerp import netsvc
import openerp.addons.decimal_precision as dp
import calendar
from calendar import monthrange
from openerp.tools.safe_eval import safe_eval
import calendar
from calendar import monthrange
from openerp.osv import fields, osv, expression
import unicodedata
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import cx_Oracle
import logging
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class account_invoice(osv.osv):
    _inherit = 'account.invoice'

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner', change_default=True, readonly=True, required=True, states={'draft':[('readonly',False)]}, track_visibility='always', domain=[('shangchao', '=', True),('wgjsdw', '=', True)]),
        'statement_id': fields.many2one('pos.statement', 'Statement', required=True, ondelete='cascade', select=True, domain=[('approve_stage', '=', 'approved')]),
    }

    def invoice_pay_customer(self, cr, uid, ids, context=None):
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account_voucher', 'view_vendor_receipt_dialog_form')

        inv = self.browse(cr, uid, ids[0], context=context)
        return {
            'name':_("Pay Invoice"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'account.voucher',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'payment_expected_currency': inv.currency_id.id,
                'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv.partner_id).id,
                'default_amount': inv.type in ('out_refund', 'in_refund') and -inv.residual or inv.residual,
                'default_reference': inv.name,
                'close_after_process': True,
                'invoice_type': inv.type,
                'invoice_id': inv.id,
                'default_type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                'type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                'statement_id': inv.statement_id.id,
            }
        }

    def action_move_create(self, cr, uid, ids, context=None):
        """Creates invoice related analytics and financial move lines"""
        ait_obj = self.pool.get('account.invoice.tax')
        cur_obj = self.pool.get('res.currency')
        period_obj = self.pool.get('account.period')
        payment_term_obj = self.pool.get('account.payment.term')
        journal_obj = self.pool.get('account.journal')
        move_obj = self.pool.get('account.move')
        if context is None:
            context = {}
        for inv in self.browse(cr, uid, ids, context=context):
            if not inv.journal_id.sequence_id:
                raise osv.except_osv(_('Error!'), _('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line:
                raise osv.except_osv(_('No Invoice Lines!'), _('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = context.copy()
            ctx.update({'lang': inv.partner_id.lang})
            if not inv.date_invoice:
                self.write(cr, uid, [inv.id], {'date_invoice': fields.date.context_today(self,cr,uid,context=context)}, context=ctx)
            company_currency = self.pool['res.company'].browse(cr, uid, inv.company_id.id).currency_id.id
            date = inv.date_invoice or time.strftime('%Y-%m-%d')
            # create the analytical lines
            # one move line per invoice line
            iml = self._get_analytic_lines(cr, uid, inv.id, context=ctx)
            # check if taxes are all computed
            compute_taxes = ait_obj.compute(cr, uid, inv.id, context=ctx)
            self.check_tax_lines(cr, uid, inv, compute_taxes, ait_obj)

            # I disabled the check_total feature
            group_check_total_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account', 'group_supplier_inv_check_total')[1]
            group_check_total = self.pool.get('res.groups').browse(cr, uid, group_check_total_id, context=context)
            if group_check_total and uid in [x.id for x in group_check_total.users]:
                if (inv.type in ('in_invoice', 'in_refund') and abs(inv.check_total - inv.amount_total) >= (inv.currency_id.rounding/2.0)):
                    raise osv.except_osv(_('Bad Total!'), _('Please verify the price of the invoice!\nThe encoded total does not match the computed total.'))

            if inv.payment_term:
                total_fixed = total_percent = 0
                for line in inv.payment_term.line_ids:
                    if line.value == 'fixed':
                        total_fixed += line.value_amount
                    if line.value == 'procent':
                        total_percent += line.value_amount
                total_fixed = (total_fixed * 100) / (inv.amount_total or 1.0)
                if (total_fixed + total_percent) > 100:
                    raise osv.except_osv(_('Error!'), _("Cannot create the invoice.\nThe related payment term is probably misconfigured as it gives a computed amount greater than the total invoiced amount. In order to avoid rounding issues, the latest line of your payment term must be of type 'balance'."))

            # one move line per tax line
            iml += ait_obj.move_line_get(cr, uid, inv.id)

            entry_type = ''
            if inv.type in ('in_invoice', 'in_refund'):
                ref = inv.reference
                entry_type = 'journal_pur_voucher'
                if inv.type == 'in_refund':
                    entry_type = 'cont_voucher'
            else:
                ref = self._convert_ref(cr, uid, inv.number)
                entry_type = 'journal_sale_vou'
                if inv.type == 'out_refund':
                    entry_type = 'cont_voucher'

            diff_currency_p = inv.currency_id.id <> company_currency
            # create one move line for the total and possibly adjust the other lines amount
            total = 0
            total_currency = 0
            total, total_currency, iml = self.compute_invoice_totals(cr, uid, inv, company_currency, ref, iml, context=ctx)
            acc_id = inv.account_id.id

            name = inv['name'] or inv['supplier_invoice_number'] or '/'
            totlines = False
            if inv.payment_term:
                totlines = payment_term_obj.compute(cr,
                        uid, inv.payment_term.id, total, inv.date_invoice or False, context=ctx)
            if totlines:
                res_amount_currency = total_currency
                i = 0
                ctx.update({'date': inv.date_invoice})
                for t in totlines:
                    if inv.currency_id.id != company_currency:
                        amount_currency = cur_obj.compute(cr, uid, company_currency, inv.currency_id.id, t[1], context=ctx)
                    else:
                        amount_currency = False

                    # last line add the diff
                    res_amount_currency -= amount_currency or 0
                    i += 1
                    if i == len(totlines):
                        amount_currency += res_amount_currency

                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': acc_id,
                        'date_maturity': date,
                        'amount_currency': diff_currency_p \
                                and amount_currency or False,
                        'currency_id': diff_currency_p \
                                and inv.currency_id.id or False,
                        'ref': ref,
                    })
            else:
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': total,
                    'account_id': acc_id,
                    'date_maturity': date,
                    'amount_currency': diff_currency_p \
                            and total_currency or False,
                    'currency_id': diff_currency_p \
                            and inv.currency_id.id or False,
                    'ref': ref
            })

            part = self.pool.get("res.partner")._find_accounting_partner(inv.partner_id)

            line = map(lambda x:(0,0,self.line_get_convert(cr, uid, x, part.id, date, context=ctx)),iml)

            line = self.group_lines(cr, uid, iml, line, inv)

            journal_id = inv.journal_id.id
            journal = journal_obj.browse(cr, uid, journal_id, context=ctx)
            if journal.centralisation:
                raise osv.except_osv(_('User Error!'),
                        _('You cannot create an invoice on a centralized journal. Uncheck the centralized counterpart box in the related journal from the configuration menu.'))

            line = self.finalize_invoice_move_lines(cr, uid, inv, line)

            move = {
                'ref': inv.reference and inv.reference or inv.name,
                'line_id': line,
                'journal_id': journal_id,
                'date': date,
                'date_maturity' : date,
                'narration': inv.comment,
                'company_id': inv.company_id.id,
                'name': '/',
            }
            period_id = inv.period_id and inv.period_id.id or False
            ctx.update(company_id=inv.company_id.id,
                       account_period_prefer_normal=True)
            if not period_id:
                period_ids = period_obj.find(cr, uid, inv.date_invoice, context=ctx)
                period_id = period_ids and period_ids[0] or False
            if period_id:
                move['period_id'] = period_id
                for i in line:
                    i[2]['period_id'] = period_id

            ctx.update(invoice=inv)
            move_id = move_obj.create(cr, uid, move, context=ctx)
            new_move_name = move_obj.browse(cr, uid, move_id, context=ctx).name
            # make the invoice point to that move
            self.write(cr, uid, [inv.id], {'move_id': move_id,'period_id':period_id, 'move_name':new_move_name}, context=ctx)
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a cancelled one:
            move_obj.post(cr, uid, [move_id], context=ctx)
        self._log_event(cr, uid, ids)
        return True

    def onchange_statement_id(self, cr, uid, ids, statement_id=False, context=None):
        if context is None:
            context = {}
        result = {}
        statement_obj = self.pool.get('pos.statement')
        if statement_id != False:
            statement = statement_obj.browse(cr,uid,statement_id)
            if statement and statement.shop_id and statement.shop_id.stid:
                result['partner_id'] = statement.shop_id.stid.id
            else:
                raise osv.except_osv(_('错误!'), _('请配置门店所属的结算单位！'))
        return {'value': result}

account_invoice()

class pos_superorder(osv.osv):
    _name = "pos.superorder"
    _order = "id desc"
    _description = u"商超销售单"
    _inherit = ['mail.thread', 'approve.base']

    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_total': 0.0,
            }
            val = 0.0
            for line in order.superorder_line:
                val += line.price_subtotal
            res[order.id]['amount_total'] = val
        return res

    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('pos.superorder.line').browse(cr, uid, ids, context=context):
            result[line.superorder_id.id] = True
        return result.keys()

    def _get_organization_id(self, cr, uid, ids, fields_name, arg, context=None):
        res = {}.fromkeys(ids, '')
        for inventory in self.browse(cr, uid, ids, context=context):
            one_res = {'area_id': '', 'office_id': '','wguser_id': ''}
            manager_id = inventory.shop_id and inventory.shop_id.sale_organization
            if manager_id:
                office_id = manager_id.parent_id
                if office_id:
                    one_res['office_id'] = office_id.name
                    area_id = office_id.parent_id and office_id.parent_id.name or False
                    one_res['area_id'] = area_id
                    one_res['wguser_id'] = manager_id.name
            res[inventory.id] = one_res
        return res

    def _organization_id_search(self, cr, uid, obj, name, args, context):
        shop_ids = []
        org_ids = []
        cr.execute("select id from wgmf_sale_organization where name" + " " + args[0][1] + " '%" + args[0][2] + "%'")
        res = cr.fetchall()
        if not res:
            return [('id', '=', 0)]
        for org_id in res:
            organization_ids = self.pool.get('wgmf.sale.organization').search(cr, uid, [('id', 'child_of', org_id[0])])
            if len(organization_ids) > 0:
                for organization_id in organization_ids:
                    org_ids.append(organization_id)
        if len(org_ids) == 0:
            return [('id', '=', 0)]
        shop_ids1 = self.pool.get('sale.shop').search(cr, uid, [('sale_organization', 'in', tuple(org_ids))])
        s_ids = self.search(cr, uid, [('shop_id', 'in', tuple(shop_ids1))])
        if len(s_ids) > 0:
            for s_id in s_ids:
                shop_ids.append(s_id)
        if len(shop_ids) == 0:
            return [('id', '=', 0)]
        ids = [('id', 'in', tuple(shop_ids))]
        return ids

    def _check_order_th(self, cr, uid, ids, fields_name, arg, context=None):
        res = dict.fromkeys(ids, 0)
        th = 1
        for order in self.browse(cr, uid, ids, context=context):
            if order.type != '2':
                cr.execute("""select case when l3.qty is null then l.qty else l.qty + l3.qty end as r_qty from (select l.product_id,l.qty from pos_superorder_line l, pos_superorder s
                     where l.superorder_id = s.id
                     and s.name = '%s') l
                        left join (select l2.product_id,l2.qty from pos_superorder_line l2
                            where l2.superorder_id in (select id from pos_superorder s2
                                where s2.superorder_id = '%s')) l3
                     on l.product_id = l3.product_id""" % (order.name,order.name))
                return_orline = cr.fetchall()
                sum = 0.0
                for l in return_orline:
                    sum += l[0]
                if sum > 0.0:
                    th = 0
            res[order.id] = th
        return res

    _columns = {
        'name': fields.char(u'销售单号', size=128,select=True),
        'number': fields.char(u'会员号', size=20,select=True),
        'shop_id' : fields.many2one('sale.shop', u'门店', domain=[('wgmf_usable', '=', True)], required=True,select=True),
        'sale_date' : fields.datetime(u'销售日期', required=True, readonly=True,select=True),
        'update_date' : fields.datetime(u'上传日期', readonly=True),
        'sale_id': fields.many2one('res.users', u'销售员'),
        'type': fields.selection([('1', '销售'),('2', '退货'),('3', '团购')], u'销售类型', required=True,select=True),
        'pur_reason': fields.char(u'购买原因'),
        #'pur_reason': fields.selection([('1', '试吃'),('2', '调理身体'),('3', '送礼'),('4', '小孩辅食'),('5', '营养补充'),('6', '营养早餐'),('7', '中晚餐')], u'购买原因',select=True),
        #'age': fields.selection([('1', '儿童0-6岁'),('2', '少年7-17岁'),('3', '青年 18-40岁'),('4', '中年40-65岁'),('5', '老年 65以上')], u'年龄段',select=True),
        'age': fields.selection([('1', '大叔'),('2', '阿姨'),('3', '大哥'),('4', '大姐'),('5', '帅哥'),('6', '美女'),('7', '小弟'),('8', '小妹')], u'年龄段',select=True),
        'mdbc': fields.selection([('Z', '早班'),('W', '晚班')], u'班次'),
        'code': fields.selection([('2', 'POS门店模式'),('3', 'POS督导模式'),('4', 'APP门店模式'),('5', 'APP督导模式')], u'模式', required=True),
        'origin': fields.selection([('0', '手持POS创建'),('1', '手工创建'),('2', '退货')], u'单据来源', select=True),
        'oe_wd': fields.boolean(u'是否网店'),
        'oe_cx': fields.boolean(u'已添加促销/无促销'),
        'oe_zp': fields.boolean(u'有赠品'),
        'oe_tb_oracle': fields.boolean(u'已积分'),
        'oe_th':fields.function(_check_order_th, type='boolean', string=u'是否退货'),
        'superorder_line': fields.one2many('pos.superorder.line', 'superorder_id', 'Superorder Lines'),
        'superorder_id' : fields.char(u'退货原单据', size=128,select=True),
        'state': fields.selection([('draft', u'未提交'), ('approved', u'完成'), ('cancel', u'已取消')], '状态', required=True,select=True),
        #'picking_id' : fields.many2one('stock.picking', u'出库单'),
        'picking_id': fields.many2many('stock.picking', 'superorder_pickings', 'superorder_pickids', 'picking_id', u'移库单'),
        'move_id' : fields.many2one('account.move', u'会计凭证'),
        'amount_total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string=u'总计',
            store={
                'pos.superorder': (lambda self, cr, uid, ids, c={}: ids, ['superorder_line'], 10),
                'pos.superorder.line': (_get_order, ['price', 'qty'], 10),
            },multi='sums', help="The total amount.", track_visibility='always'),
        #'amount_total': fields.float('总价', digits_compute=dp.get_precision('Product Price')),
        'office_id':fields.function(_get_organization_id, fnct_search=_organization_id_search, type='char', string=u'办事处', multi='get_parent_id'),
        'area_id':fields.function(_get_organization_id, fnct_search=_organization_id_search, type='char', string=u'大区', multi='get_parent_id'),
        'wguser_id': fields.function(_get_organization_id, fnct_search=_organization_id_search, type='char', string=u'负责人', multi='get_parent_id'),
    }

    def onchange_shop_id(self, cr, uid, ids, shop_id,context=None):
        if context is None:
            context = {}
        res = {'value': {'oe_wd' :False}}
        if not shop_id:
            return res
        shop_obj = self.pool.get('sale.shop')
        shop = shop_obj.browse(cr, uid, shop_id, context=context)
        if not shop_id:
            res['warning'] = {'title': _('错误!'), 'message': _('没有选择门店！')}
        if shop_id:
            if shop.yymode == 'WSD':
                res['value'].update({'oe_wd': True})
        return res

    def default_get(self, cr, uid, fields, context=None):
        res = super(pos_superorder, self).default_get(cr, uid, fields, context=context)
        t = time.strftime('%Y-%m-%d %H:%M:%S')
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        if user.pos_config:
            shop_id = user.pos_config.shop_id.id
        else:
            shop_id = False
        if context.get('oe_origin','0'):
            origin = context.get('oe_origin','0')
        else:
            origin = '0'
        res.update({
            'name': '/',
            'shop_id': shop_id,
            'sale_date': t,
            'update_date': t,
            'sale_id': uid,
            'origin': origin,
            'type': '1',
            'code': '2',
            'state': 'draft',
        })
        return res

    _sql_constraints = [
        ('uniq_name', 'unique(name)', '单号必须唯一!'),
    ]

    def _wgmf_prepare_order_picking(self, cr, uid, number, origin, type, date,context):
        context=context or {}
        if type != '2':
            pick_name = self.pool.get('ir.sequence').get(cr, uid, 'stock.picking.out')
            type='out'
            #state='done'
        else:
            pick_name = self.pool.get('ir.sequence').get(cr, uid, 'stock.picking.in')
            #state='done'
            type='internal'
        company_id = self.pool.get('res.users').browse(cr,uid,uid).company_id.id
        #date = time.strftime("%Y-%m-%d %H:%M:%S")
        if number != False:
            orig = origin + ":" + number
        else:
            orig = origin
        return {
            'name': pick_name,
            'origin': orig,
            'date': date,
            'type': type,
            'state': 'done',
            'invoice_state': 'none',
            'company_id': company_id,
        }

    def _wgmf_prepare_order_line_move(self, cr, uid, type, date, picking_id, shop, product_name, product_id, uom_id, qty, price):
        if float(price) == 0.0:
            if type != '2':
                location_id = shop.warehouse_id.lot_stock_id.id
                output_id = 81
            else:
                location_id = 81
                output_id = shop.warehouse_id.lot_stock_id.id
        else:
            if type != '2':
                location_id = shop.warehouse_id.lot_stock_id.id
                output_id = shop.warehouse_id.partner_id.property_stock_customer.id
            else:
                location_id = shop.warehouse_id.partner_id.property_stock_customer.id
                output_id = shop.warehouse_id.lot_stock_id.id
        company_id = self.pool.get('res.users').browse(cr,uid,uid).company_id.id
        return {
            'name': product_name,
            'picking_id': picking_id,
            'product_id': product_id,
            'date': date,
            'date_expected': date,
            'product_qty': qty,
            'product_uom': uom_id,
            'product_uos_qty': qty,
            'product_uos': uom_id,
            'location_id': location_id,
            'location_dest_id': output_id,
            'tracking_id': False,
            'state': 'done',
            'company_id': company_id,
            'price_unit': price,
        }

    def create_picking_order_out(self, cr, uid, type, bom_id, date, lot_stock_id, company_id, factor, product, picking_id, context):
        partial_data = {}
        user_obj = self.pool.get('res.users')
        bom_obj = self.pool.get('mrp.bom')
        stock_move = self.pool.get('stock.move')
        stock_picking = self.pool.get('stock.picking')
        product_obj = self.pool.get('product.product')
        wf_service = netsvc.LocalService("workflow")
        bom_point = bom_obj.browse(cr, uid, bom_id, context=context)
        res = bom_obj._bom_explode(cr, uid, bom_point, factor, [])
        if not picking_id:
            picking_id = stock_picking.create(cr,uid,{
                 'name': self.pool.get('ir.sequence').get(cr, uid, 'stock.picking.in'),
                 'date': date,
                 'min_date': date,
                 'date_done': date,
                 'invoice_state': 'none',
                 'company_id': company_id,
                 'origin': "ECJG",
                 'type': 'internal',
                 'move_lines' : [],
                 'state': 'done',
            })
        for line in res[0]:
            product = product_obj.browse(cr, uid, line['product_id'])
            if not product.property_stock_production:
                raise osv.except_osv(_('库位错误'), _('请设置生产库位!'))
            if type != '2':
                location_id = lot_stock_id
                location_dest_id = product.property_stock_production.id
            else:
                location_id = product.property_stock_production.id
                location_dest_id = lot_stock_id
            if product.supply_method == 'produce':
                self.find_bom(cr, uid, type, lot_stock_id, product.id, line['product_qty'], date, picking_id, context)
            else:
                move_line = stock_move.create(cr,uid,{
                     'name': line['name'],
                     'product_id': line['product_id'],
                     'product_qty': line['product_qty'],
                     'product_uos_qty': line['product_qty'],
                     'product_uom': line['product_uom'],
                     'product_uos': line['product_uom'],
                     'date': date,
                     'date_expected': date,
                     'location_id': location_id,
                     'location_dest_id': location_dest_id,
                     'picking_id': picking_id,
                     'state': 'done',
                     'type':'internal',
                     'company_id': company_id,
                })
        return picking_id

    def create_picking_order_in(self, cr, uid, type, product_id, factor, bom_id, date, company_id, lot_stock_id, context):
        partial_data = {}
        user_obj = self.pool.get('res.users')
        product_obj = self.pool.get('product.product')
        stock_move = self.pool.get('stock.move')
        stock_picking = self.pool.get('stock.picking')
        wf_service = netsvc.LocalService("workflow")
        product = product_obj.browse(cr, uid, product_id)
        if not product.property_stock_production:
            raise osv.except_osv(_('库位错误'), _('请设置生产库位!'))
        if type != '2':
            location_id = product.property_stock_production.id
            location_dest_id = lot_stock_id
        else:
            location_id = lot_stock_id
            location_dest_id = product.property_stock_production.id
        if not product.default_code:
            name = product.name
        else:
            name = "[" + product.default_code + "]" + " " + product.name
        pic_id = stock_picking.create(cr,uid,{
             'name': self.pool.get('ir.sequence').get(cr, uid, 'stock.picking.in'),
             'date': date,
             'min_date': date,
             'date_done': date,
             'invoice_state': 'none',
             'company_id': company_id,
             'origin': "ECJG",
             'type': 'internal',
             'state': 'done',
             'move_lines' : [],
        })
        mv_line = stock_move.create(cr,uid,{
             'name': name,
             'product_id': product.id,
             'product_qty': factor,
             'product_uos_qty': factor,
             'product_uom': product.uom_id.id,
             'product_uos': product.uom_id.id,
             'date': date,
             'date_expected': date,
             'location_id': location_id,
             'location_dest_id': location_dest_id,
             'picking_id': pic_id,
             'state': 'done',
             'type':'internal',
             'company_id': company_id,
        })
        return pic_id

    def find_bom(self, cr, uid, type, lot_stock_id, product_id, factor, date, picking_id, context):
        user_obj = self.pool.get('res.users')
        bom_obj = self.pool.get('mrp.bom')
        product_obj = self.pool.get('product.product')
        product = product_obj.browse(cr, uid, product_id)
        company_id = user_obj.browse(cr, uid, uid).company_id.id
        bom_id = bom_obj._bom_find(cr, uid, product.id, product.uom_id and product.uom_id.id, [])
        if bom_id:
            picking = self.create_picking_order_out(cr, uid, type, bom_id, date, lot_stock_id, company_id, factor, product, picking_id, context=context)
            return bom_id
        return False

    def pos_superpicking(self,cr,uid,vals,context=None):
        partial_data = {}
        picking_ids = []
        product_context = {}
        mod_obj = self.pool.get('ir.model.data')
        result = mod_obj.get_object_reference(cr, uid, 'stock', 'location_production')
        product_location_id = result and result[1] or False
        picking_obj = self.pool.get('stock.picking')
        move_obj = self.pool.get('stock.move')
        prod_tmp_obj = self.pool.get('product.template')
        shop_obj = self.pool.get('sale.shop')
        user_obj = self.pool.get('res.users')
        shop = shop_obj.browse(cr, uid, vals.get('shop_id',False))
        lot_stock_id = shop.warehouse_id.lot_stock_id.id
        company_id = user_obj.read(cr, uid, uid, ['company_id'], context=context)['company_id']
        if vals.get('type','1') != '2':
            pick_name = self.pool.get('ir.sequence').get(cr, uid, 'stock.picking.out')
            type='out'
            location_id = shop.warehouse_id.lot_stock_id.id
            output_id = shop.warehouse_id.partner_id.property_stock_customer.id
        else:
            pick_name = self.pool.get('ir.sequence').get(cr, uid, 'stock.picking.in')
            type='internal'
            location_id = shop.warehouse_id.partner_id.property_stock_customer.id
            output_id = shop.warehouse_id.lot_stock_id.id
        if vals.get('number','') != False:
            orig = vals.get('name','/') + ":" + vals.get('number','')
        else:
            orig = vals.get('name','/')
        picking_id = picking_obj.create(cr, uid, self._wgmf_prepare_order_picking(cr, uid, vals.get('number',''), vals.get('name','/'), vals.get('type','1'),vals.get('sale_date',False),context))
        picking_ids.append(picking_id)
        out_lines = False
        for line in vals.get('superorder_line',False):
            if line[0] == 0 and line[2].get('product_id',False) != 1597:
                out_lines = True
                if vals.get('type','1') != '2':
                    qty = line[2].get('qty',0.0)
                else:
                    qty = line[2].get('qty',0.0) * -1
                uom_id = prod_tmp_obj.read(cr, uid, line[2].get('product_id',False), ['uom_id'], context=context)['uom_id']
                move_line = move_obj.create(cr, uid, self._wgmf_prepare_order_line_move(cr, uid, vals.get('type','1'), vals.get('sale_date',False), picking_id, shop, vals.get('name','/'), line[2].get('product_id',False), uom_id[0], qty, line[2].get('price',0.0)))
                cr.execute("insert into wgmf_show_jf_log (member_no,log_date,log_action,product_id,product_qty,shop_id) values (%s,%s,%s,%s,%s,%s)", (vals.get('number',''),vals.get('sale_date',False),'gm',line[2].get('product_id',False),qty,vals.get('shop_id',False),))
                cr.execute("select m.id from mrp_bom m left join product_product p on m.product_id = p.id where m.bom_id is NULL and m.product_id = %s",(line[2].get('product_id',False),))# and p.product_rehearsals ='t'
                bom_id = cr.fetchone()
                if bom_id and bom_id[0] is not None:
                    bom_ids = self.find_bom(cr, uid, vals.get('type','1'), lot_stock_id, line[2].get('product_id',False), qty, vals.get('sale_date',False), picking_id=False, context=context)
                    if bom_ids:
                        pic = self.create_picking_order_in(cr, uid, vals.get('type','1'), line[2].get('product_id',False), qty, bom_ids, vals.get('sale_date',False), company_id[0], lot_stock_id, context=context)
                        picking_ids.append(int(pic))
        if out_lines != True:
            cr.execute("delete from stock_picking where id =%s",(picking_id,))
        return picking_ids

    def oe_pos_superpicking(self,cr,uid,vals,context=None):
        partial_data = {}
        picking_ids = []
        product_context = {}
        mod_obj = self.pool.get('ir.model.data')
        result = mod_obj.get_object_reference(cr, uid, 'stock', 'location_production')
        product_location_id = result and result[1] or False
        picking_obj = self.pool.get('stock.picking')
        move_obj = self.pool.get('stock.move')
        prod_tmp_obj = self.pool.get('product.template')
        shop_obj = self.pool.get('sale.shop')
        user_obj = self.pool.get('res.users')
        shop = shop_obj.browse(cr, uid, vals.get('shop_id',False))
        lot_stock_id = shop.warehouse_id.lot_stock_id.id
        company_id = user_obj.read(cr, uid, uid, ['company_id'], context=context)['company_id']
        if vals.get('type','1') != '2':
            pick_name = self.pool.get('ir.sequence').get(cr, uid, 'stock.picking.out')
            type='out'
            location_id = shop.warehouse_id.lot_stock_id.id
            output_id = shop.warehouse_id.partner_id.property_stock_customer.id
        else:
            pick_name = self.pool.get('ir.sequence').get(cr, uid, 'stock.picking.in')
            type='internal'
            location_id = shop.warehouse_id.partner_id.property_stock_customer.id
            output_id = shop.warehouse_id.lot_stock_id.id
        if vals.get('number','') != False:
            orig = vals.get('name','/') + ":" + vals.get('number','')
        else:
            orig = vals.get('name','/')
        picking_id = picking_obj.create(cr, uid, self._wgmf_prepare_order_picking(cr, uid, vals.get('number',''), vals.get('name','/'), vals.get('type','1'),vals.get('sale_date',False),context))
        picking_ids.append(picking_id)
        out_lines = False
        for line in vals.get('superorder_line',False):
            if line.get('product_id',False) != 1597:
                out_lines = True
                if vals.get('type','1') != '2':
                    qty = line.get('qty',0.0)
                else:
                    qty = line.get('qty',0.0) * -1
                uom_id = line.get('product_uom',False)
                move_line = move_obj.create(cr, uid, self._wgmf_prepare_order_line_move(cr, uid, vals.get('type','1'), vals.get('sale_date',False), picking_id, shop, vals.get('name','/'), line.get('product_id',False), uom_id, qty, line.get('price',0.0)))
                cr.execute("insert into wgmf_show_jf_log (member_no,log_date,log_action,product_id,product_qty,shop_id) values (%s,%s,%s,%s,%s,%s)", (vals.get('number',''),vals.get('sale_date',False),'gm',line.get('product_id',False),qty,vals.get('shop_id',False),))
                cr.execute("select m.id from mrp_bom m left join product_product p on m.product_id = p.id where m.bom_id is NULL and m.product_id = %s",(line.get('product_id',False),))# and p.product_rehearsals ='t'
                bom_id = cr.fetchone()
                if bom_id and bom_id[0] is not None:
                    bom_ids = self.find_bom(cr, uid, vals.get('type','1'), lot_stock_id, line.get('product_id',False), qty, vals.get('sale_date',False), picking_id=False, context=context)
                    if bom_ids:
                        pic = self.create_picking_order_in(cr, uid, vals.get('type','1'), line.get('product_id',False), qty, bom_ids, vals.get('sale_date',False), company_id[0], lot_stock_id, context=context)
                        picking_ids.append(int(pic))
        if out_lines != True:
            cr.execute("delete from stock_picking where id =%s",(picking_id,))
        return picking_ids

    def get_price(self, cr, uid, shop_id, product_id):
        cr.execute("""select price from wgmf_product_directory_line dir_l
           left join (select dir.id,s.id as shop_id from wgmf_product_directory dir left join sale_shop s on (s.product_directory = dir.id)) dir2 
               on (dir_l.directory_id = dir2.id)
           where dir2.shop_id =%s
           and dir_l.product_id = %s
        """ % (shop_id, product_id))
        dir_price = cr.fetchone()
        if dir_price and dir_price[0]:
            new_price = dir_price[0]
        else:
            new_price = 0.0
        return new_price

    def is_number(self, cr, uid, uchar):
        """判断一个unicode是否是数字"""
        if uchar >= u'\u0030' and uchar<=u'\u0039':
                return True
        else:
                return False

    def Q2B(self, cr, uid, uchar):
        """全角转半角"""
        inside_code=ord(uchar)
        if inside_code==0x3000:
                inside_code=0x0020
        else:
                inside_code-=0xfee0
        if inside_code<0x0020 or inside_code>0x7e:
                return uchar
        return unichr(inside_code)

    def stringQ2B(self, cr, uid, ustring):
        """把字符串全角转半角"""
        return "".join([self.Q2B(cr, uid, uchar) for uchar in ustring])

    def uniform(self, cr, uid, ustring):
        """格式化字符串，完成全角转半角，大写转小写的工作"""
        return self.stringQ2B(cr, uid, ustring).lower()

    def check_number(self, cr, uid, number):
        ustring = self.uniform(cr, uid, number)
        for uchar in ustring:
            """判断是否非汉字，数字和英文字符"""
            if not self.is_number(cr, uid, uchar):
                raise osv.except_osv(_('会员号错误'), _('请输入数字类型的会员号!'))
        return ustring

    def return_cx_msg(self, cr, uid, ids, context=None):
        for order in self.browse(cr, uid, ids, context):
            for line in order.superorder_line:
                if line.is_cx == True:
                    cr.execute("delete from pos_superorder_line where id =%s",(line.id,))
                    cr.execute("update pos_superorder set oe_cx = False where id =%s",(order.id,))
        return True

    def get_cx_msg(self,cr,uid,vals,context=None):
        promotions = []
        cx_total = 0.0
        promotion_obj = self.pool.get('wgmf.shop.promotion')
        lod_promotion = promotion_obj.load_shop_promotion(cr, uid, vals.get('shop_id',False), vals.get('sale_date',False), context=None)
        for pro in lod_promotion:
            if pro.get('mode',False) != False and (pro.get('mode',False) == 'ZLCX' or pro.get('mode',False) == 'ZLZK_JE' or pro.get('mode',False) == 'ZLZK_BL'):
                if vals.get('amount_total',False) >= pro.get('total',0.0):
                    new_qty = int(float(vals.get('amount_total',False))/float(pro.get('total',0.0)))
                    pro.update({'shop_id': vals.get('shop_id',False),'qty': new_qty})
                    promotions.append(pro)
            if pro.get('mode',False) != False and len(pro.get('buy',[])) > 0 and (pro.get('mode',False) == 'MZHPZK_JE' or pro.get('mode',False) == 'MZCCXX' or pro.get('mode',False) == 'MZHPZK_BL'):
                qtys = []
                cx_total = 0.0
                for i in vals.get('superorder_line',False):
                    for j in pro.get('buy',False):
                        if i.get('product_id',False) == j.get('product_id',False):
                            qtys.append(int(i.get('qty',0.0)/j.get('qty',1)))
                            new_price = self.get_price(cr, uid, vals.get('shop_id',False), i.get('product_id',False))
                            cx_total += new_price
                if len(qtys) > 0 and len(qtys) >= len(pro.get('buy',[])) and min(qtys) > 0:
                    pro.update({'shop_id': vals.get('shop_id',False),'qty': min(qtys),'price': pro.get('price',0.0), 'cx_total': cx_total})
                    promotions.append(pro)
        return promotions

    def order_confirm(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        wf_service = netsvc.LocalService("workflow")
        vals = {}
        superorder_line = []
        if len(ids) == 1:
            w = 'where o.id = %s' % ids[0]
        if len(ids) > 1:
            w = 'where o.id in %s' % ids
        if len(ids) < 1:
            raise osv.except_osv(_('单据错误'), _('未找到符合条件的商超销售单!'))
        cr.execute("""select o.id,o.name,o.shop_id,o.type,o.number,o.sale_date,l.id,l.product_id,l.code,l.qty,l.product_uom,l.price,l.price_subtotal,l.is_cx,o.amount_total,o.oe_cx
            from pos_superorder_line l
            left join pos_superorder o on o.id = l.superorder_id
            %s""" % w)
        order = cr.fetchall()
        if len(order) > 0:
            for o in order:
                number = ''
                oe_wd = False
                if o[4] is not None:
                    number = o[4]
                wd = self.pool.get('sale.shop').read(cr, uid, [o[2]], ['yymode'])[0]['yymode']
                if wd == 'WSD':
                    oe_wd = True
                if o[0] not in vals:
                    vals.update({o[0]:{'name': o[1],'shop_id': o[2],'type': o[3],'number': number,'sale_date': o[5],'amount_total':o[14],'oe_cx':o[15],'oe_wd':oe_wd,'superorder_line':[{'id':o[6],'product_id': o[7],'code':o[8],'qty':o[9],'product_uom':o[10],'price':o[11],'price_subtotal':o[12],'is_cx':o[13]}]}})
                else:
                    vals[o[0]]['superorder_line'].append({'id':o[6],'product_id': o[7],'code':o[8],'qty':o[9],'product_uom':o[10],'price':o[11],'price_subtotal':o[12],'is_cx':o[13]})
            for k,v in vals.items():
                cx_msg = self.get_cx_msg(cr,uid,v,context=context)
                if v.get('oe_wd',False) == True:
                    if len(cx_msg) <= 0 or v.get('oe_cx',False) == True:
                        self.write(cr,uid,[k],{'state':'approved','oe_cx':True})
                        return True
                    else:
                        context.update({
                            'active_model': 'pos.superorder',
                            'active_ids': [k],
                            'active_id': k or False,
                            'cx_msg': cx_msg,
                            'wg_value': v,
                            'wg_state': 'confirm',
                            'oe_wd': True,
                        })
                        return {
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'wgmf.get.cx',
                            'type': 'ir.actions.act_window',
                            'target': 'new',
                            'context': context,
                            'nodestroy': True,
                        }
                else:
                    if len(cx_msg) <= 0 or v.get('oe_cx',False) == True:
                        picking_ids = self.oe_pos_superpicking(cr,uid, v,context=context)
                        self.write(cr,uid,[k],{'state':'approved'})
                        return True
                    else:
                        context.update({
                            'active_model': 'pos.superorder',
                            'active_ids': [k],
                            'active_id': k or False,
                            'cx_msg': cx_msg,
                            'wg_value': v,
                            'wg_state': 'confirm',
                            'oe_wd': False,
                        })
                        return {
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'wgmf.get.cx',
                            'type': 'ir.actions.act_window',
                            'target': 'new',
                            'context': context,
                            'nodestroy': True,
                        }

    def order_add_cx(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        wf_service = netsvc.LocalService("workflow")
        vals = {}
        superorder_line = []
        if len(ids) == 1:
            w = 'where o.id = %s' % ids[0]
        if len(ids) > 1:
            w = 'where o.id in %s' % ids
        if len(ids) < 1:
            raise osv.except_osv(_('单据错误'), _('未找到符合条件的商超销售单!'))
        cr.execute("""select o.id,o.name,o.shop_id,o.type,o.number,o.sale_date,l.id,l.product_id,l.code,l.qty,l.product_uom,l.price,l.price_subtotal,l.is_cx,o.amount_total,o.oe_cx
            from pos_superorder_line l
            left join pos_superorder o on o.id = l.superorder_id
            %s""" % w)
        order = cr.fetchall()
        if len(order) > 0:
            for o in order:
                number = ''
                oe_wd = False
                if o[4] is not None:
                    number = o[4]
                wd = self.pool.get('sale.shop').read(cr, uid, [o[2]], ['yymode'])[0]['yymode']
                if wd == 'WSD':
                    oe_wd = True
                if o[4] is not None:
                    number = o[4]
                if o[0] not in vals:
                    vals.update({o[0]:{'name': o[1],'shop_id': o[2],'type': o[3],'number': number,'sale_date': o[5],'amount_total':o[14],'oe_cx':o[15],'oe_wd':oe_wd,'superorder_line':[{'id':o[6],'product_id': o[7],'code':o[8],'qty':o[9],'product_uom':o[10],'price':o[11],'price_subtotal':o[12],'is_cx':o[13]}]}})
                else:
                    vals[o[0]]['superorder_line'].append({'id':o[6],'product_id': o[7],'code':o[8],'qty':o[9],'product_uom':o[10],'price':o[11],'price_subtotal':o[12],'is_cx':o[13]})
            for k,v in vals.items():
                cx_msg = self.get_cx_msg(cr,uid,v,context=context)
                if v.get('oe_wd',False) == True:
                    if len(cx_msg) <= 0 or v.get('oe_cx',False) == True:
                        self.write(cr,uid,[k],{'oe_cx':True})
                        return True
                    else:
                        context.update({
                            'active_model': 'pos.superorder',
                            'active_ids': [k],
                            'active_id': k or False,
                            'cx_msg': cx_msg,
                            'wg_value': v,
                            'wg_state': 'draft',
                            'oe_wd': True,
                        })
                        return {
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'wgmf.get.cx',
                            'type': 'ir.actions.act_window',
                            'target': 'new',
                            'context': context,
                            'nodestroy': True,
                        }
                else:
                    if len(cx_msg) <= 0 or v.get('oe_cx',False) == True:
                        picking_ids = self.oe_pos_superpicking(cr,uid, v,context=context)
                        #self.write(cr,uid,ids,{'state':'approved'})
                        return True
                    else:
                        context.update({
                            'active_model': 'pos.superorder',
                            'active_ids': [k],
                            'active_id': k or False,
                            'cx_msg': cx_msg,
                            'wg_value': v,
                            'wg_state': 'draft',
                            'oe_wd': False,
                        })
                        return {
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'wgmf.get.cx',
                            'type': 'ir.actions.act_window',
                            'target': 'new',
                            'context': context,
                            'nodestroy': True,
                        }

    def create(self, cr, uid, vals, context=None):
        shop_obj = self.pool.get('sale.shop')
        inventory_obj = self.pool.get('stock.inventory')
        total_amount = 0.0
        sale_date = vals.get('sale_date',False)
        date = (datetime.datetime.strptime(sale_date,'%Y-%m-%d %H:%M:%S')).strftime('%Y-%m-%d')# - datetime.timedelta(hours=8)
        date_time = (datetime.datetime.strptime(sale_date,'%Y-%m-%d %H:%M:%S') + datetime.timedelta(hours=8)).strftime('%Y-%m-%d')
        t = (datetime.datetime.strptime(time.strftime('%Y-%m-%d %H:%M:%S'),'%Y-%m-%d %H:%M:%S') + datetime.timedelta(hours=8)).strftime('%Y-%m-%d')
        if context.get('oe_origin',False) == '1':
            first_date = date + " 15:59:59"
            last2_date = date + " 23:59:59"
            if vals.get('sale_date',False) > first_date and vals.get('sale_date',False) <= last2_date:
                sale_date = first_date
                date = (datetime.datetime.strptime(sale_date,'%Y-%m-%d %H:%M:%S')).strftime('%Y-%m-%d')# - datetime.timedelta(hours=8)
                vals.update({'sale_date': first_date})
                date_time = (datetime.datetime.strptime(first_date,'%Y-%m-%d %H:%M:%S') + datetime.timedelta(hours=8)).strftime('%Y-%m-%d')
        if date > t:
            raise osv.except_osv(_('日期错误'), _('销售日期不能选择未来时间!'))
        if vals.get('shop_id',False) != False:
            wd = shop_obj.read(cr, uid, [vals.get('shop_id',False)], ['yymode'])[0]['yymode']
            if wd == 'WSD':
                vals['oe_wd'] = True
            cr.execute("select id from wgmf_sale_check where yn_hd = '1' and date_trunc('day',saledate)::date = \'%s\' and shop_id = %s" % (date_time,vals.get('shop_id',False)))
            check_id = cr.fetchall()
            if (len(check_id) > 0) and (uid!=16755):
                raise osv.except_osv(_('错误!'),_('不能创建已审核的商超销售单.'))
        if not vals.get('superorder_line',False):
            raise osv.except_osv(_('错误!'),_('不能确认没有明细行的商超销售单.'))
        sign = inventory_obj.get_closed_shop(cr, uid, vals.get('sale_date',False), vals.get('shop_id',False), context)
        if sign == True:
            shop_name = shop_obj.read(cr, uid, [vals.get('shop_id',False)], ['name'])[0]['name']
            raise osv.except_osv(_('错误'), _('该门店(%s)在日期(%s)所在月份已盘点，不能在做销售!' % (shop_name,vals.get('sale_date',False))))
        if vals.get('superorder_line',False) and vals.get('type',False) == '2':
            for line in vals.get('superorder_line',False):
                if type(line[2]) is not list:
                    if line and line[2].get('qty',0.0) >= 0.0:
                        raise osv.except_osv(_('错误!'),_('退货数量必须为负数.'))
                    if line and line[2].get('shop_id',False):
                        if line[2].get('shop_id',False) != vals.get('shop_id',False):
                            line[2].update({'shop_id': vals.get('shop_id',False)})
                    #total_amount += line[2].get('price_subtotal',0.0)
                    if line[2].get('shop_id',False) and line[2].get('product_id',False):
                        if line[2].get('is_cx',False) == True or line[2].get('product_id',False) == 1597:
                            new_price = line[2].get('price',0.0)
                            product = self.pool.get('product.product').browse(cr, uid, line[2].get('product_id',False))
                            code = product.default_code
                            uom_id = product.uom_id.id
                            price_subtotal = new_price * line[2].get('qty',0.0)
                            total_amount += price_subtotal
                            if line[2].get('product_id',False) == 1597:
                                line[2].update({'code': code,'product_uom': uom_id,'price': new_price,'price_subtotal': price_subtotal,'is_cx': True})
                            else:
                                line[2].update({'code': code,'product_uom': uom_id,'price': new_price,'price_subtotal': price_subtotal})
                        else:
                            new_price = self.get_price(cr, uid, line[2].get('shop_id',False), line[2].get('product_id',False))
                            product = self.pool.get('product.product').browse(cr, uid, line[2].get('product_id',False))
                            code = product.default_code
                            uom_id = product.uom_id.id
                            price_subtotal = new_price * line[2].get('qty',0.0)
                            total_amount += price_subtotal
                            line[2].update({'code': code,'product_uom': uom_id,'price': new_price,'price_subtotal': price_subtotal})
                    else:
                        product = self.pool.get('product.product').browse(cr, uid, line[2].get('product_id',False))
                        code = product.default_code
                        uom_id = product.uom_id.id
                        price_subtotal = line[2].get('price',False) * line[2].get('qty',0.0)
                        total_amount += price_subtotal
                        if line[2].get('product_id',False) == 1597:
                            line[2].update({'code': code,'product_uom': uom_id,'price': vals.get('price',False),'price_subtotal': price_subtotal,'is_cx': True})
                        else:
                            line[2].update({'code': code,'product_uom': uom_id,'price': vals.get('price',False),'price_subtotal': price_subtotal})
                    
        if vals.get('superorder_line',False) and vals.get('type',False) == '1':
            for line in vals.get('superorder_line',False):
                if type(line[2]) is not list:
                    if line and line[2].get('qty',0.0) <= 0.0:
                        raise osv.except_osv(_('错误!'),_('销售数量不能为负数.'))
                    if line and line[2].get('shop_id',False):
                        if line[2].get('shop_id',False) != vals.get('shop_id',False):
                            line[2].update({'shop_id': vals.get('shop_id',False)})
                    if line[2].get('shop_id',False) and line[2].get('product_id',False):
                        if line[2].get('is_cx',False) == True or line[2].get('product_id',False) == 1597:
                            new_price = line[2].get('price',0.0)
                            product = self.pool.get('product.product').browse(cr, uid, line[2].get('product_id',False))
                            code = product.default_code
                            uom_id = product.uom_id.id
                            price_subtotal = new_price * line[2].get('qty',0.0)
                            total_amount += price_subtotal
                            if line[2].get('product_id',False) == 1597:
                                line[2].update({'code': code,'product_uom': uom_id,'price': new_price,'price_subtotal': price_subtotal,'is_cx': True})
                            else:
                                line[2].update({'code': code,'product_uom': uom_id,'price': new_price,'price_subtotal': price_subtotal})
                        else:
                            new_price = self.get_price(cr, uid, line[2].get('shop_id',False), line[2].get('product_id',False))
                            product = self.pool.get('product.product').browse(cr, uid, line[2].get('product_id',False))
                            code = product.default_code
                            uom_id = product.uom_id.id
                            price_subtotal = new_price * line[2].get('qty',0.0)
                            total_amount += price_subtotal
                            line[2].update({'code': code,'product_uom': uom_id,'price': new_price,'price_subtotal': price_subtotal})
                    else:
                        product = self.pool.get('product.product').browse(cr, uid, line[2].get('product_id',False))
                        code = product.default_code
                        uom_id = product.uom_id.id
                        price_subtotal = line[2].get('price',False) * line[2].get('qty',0.0)
                        total_amount += price_subtotal
                        if line[2].get('product_id',False) == 1597:
                            line[2].update({'code': code,'product_uom': uom_id,'price': vals.get('price',False),'price_subtotal': price_subtotal,'is_cx': True})
                        else:
                            line[2].update({'code': code,'product_uom': uom_id,'price': vals.get('price',False),'price_subtotal': price_subtotal})
        if vals.get('superorder_id',False):
            cr.execute("select id from pos_superorder where name = \'%s\'" % vals.get('superorder_id',False))
            check_retorder = cr.fetchall()
            if len(check_retorder) <= 0:
                raise osv.except_osv(_('错误!'),_('未找到名为(%s)的退货原单据.' % vals.get('superorder_id',False)))
        if vals.get('name','/')=='/':
            vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'pos.superorder') or '/'
        if vals.get('number',False):
            vals['number'] = self.check_number(cr, uid, vals.get('number',""))
        if context.get('oe_origin','0') in ('0','2'):
            vals['state'] = 'approved'
            picking_ids = self.pos_superpicking(cr,uid, vals,context=context)
        else:
            vals['state'] = 'draft'
        vals['amount_total'] = total_amount
        return super(pos_superorder, self).create(cr, uid, vals, context=context)
        
        
    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        default.update({
            'name': self.pool.get('ir.sequence').get(cr, uid, 'pos.superorder'),
        })
        default['approve_logs'] = []
        return super(pos_superorder, self).copy(cr, uid, id, default, context)

    def superorder_cancel(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        for o in self.browse(cr, uid, ids):
            context.update({
                'active_model': o._name,
                'active_ids': [o.id],
                'active_id': len([o.id]) and o.id or False,
            })
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wgmf.sale.return',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': context,
            'nodestroy': True,
        }

    def approve_submit(self, cr, uid, ids, context=None):
        context = context or {}
        todo = []
        picking_obj = self.pool.get('stock.picking')
        move_obj = self.pool.get('stock.move')
        for sup in self.browse(cr, uid, ids):
            partial_data = {}
            if not sup.superorder_line:
                raise osv.except_osv(_('错误!'),_('不能确认没有明细行的商超销售单.'))
            picking_id = picking_obj.create(cr, uid, self._wgmf_prepare_order_picking(cr, uid, sup.number, sup.name,context))
            for line in sup.superorder_line:
                move_line = move_obj.create(cr, uid, self._wgmf_prepare_order_line_move(cr, uid, picking_id, sup.shop_id, line.product_id, line.qty, line.price))
                partial_data['move%s' % (move_line)] = {
                    'product_id': line.product_id.id,
                    'product_qty': line.qty,
                    'product_uom': line.product_id.uom_id.id,
                }
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, 'stock.picking', picking_id, 'button_confirm', cr)
            picking_obj.force_assign(cr, uid, [picking_id])
            picking_obj.do_partial(cr, uid, [picking_id], partial_data)
            self.write(cr,uid,sup.id, {'state':'approved'})
        return True

pos_superorder()

class pos_superorder_line(osv.osv):
    _name = "pos.superorder.line"
    _order = "id desc"

    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = line.price * line.qty
        return res

    def _default_shop_id(self, cr, uid, context=None):
        if context.get('shop_id',False):
            shop_id = context.get('shop_id',False)
        else:
            shop_id = False
        return shop_id

    _columns = {
        'superorder_id': fields.many2one('pos.superorder', 'Superorder', required=True, ondelete='cascade', select=True),
        'product_id': fields.many2one('product.product', u'产品', domain=[('sale_ok', '=', True),('type', '!=', '7')], change_default=True, select=True),
        'categ_id': fields.many2one('product.category',u'分类', change_default=True, domain="[('type','=','normal')]"),
        'name' : fields.char(u'销售单号', size=128),
        'code' : fields.char(u'产品编号', size=128, select=True),
        'bhmode' : fields.char(u'产品角色', size=128),
        'qty': fields.float(u'销售数量', digits_compute= dp.get_precision('Product Unit of Measure')),
        'product_uom': fields.many2one('product.uom', '产品单位'),
        'shop_id' : fields.many2one('sale.shop', u'门店', domain=[('wgmf_usable', '=', True)], select=True),
        'price': fields.float('售价', digits_compute=dp.get_precision('Product Price')),
        #'price_subtotal': fields.function(_amount_line, string=u'小计', digits_compute= dp.get_precision('Account')),
        'price_subtotal': fields.float('小计', digits_compute=dp.get_precision('Account')),
        'is_cx': fields.boolean(u'是否促销品'),
    }

    _defaults = {
        'qty': 1,
        'shop_id': _default_shop_id,
    }

    def _check_product_uom_group(self, cr, uid, context=None):
        group_uom = self.pool.get('ir.model.data').get_object(cr, uid, 'product', 'group_uom')
        res = [user for user in group_uom.users if user.id == uid]
        return len(res) and True or False

    def get_price(self, cr, uid, shop_id, product_id):
        cr.execute("""select price from wgmf_product_directory_line dir_l
           left join (select dir.id,s.id as shop_id from wgmf_product_directory dir left join sale_shop s on (s.product_directory = dir.id)) dir2 
               on (dir_l.directory_id = dir2.id)
           where dir2.shop_id =%s
           and dir_l.product_id = %s
        """ % (shop_id, product_id))
        dir_price = cr.fetchone()
        if dir_price and dir_price[0]:
            new_price = dir_price[0]
        else:
            new_price = 0.0
        return new_price

    def onchange_product_id(self, cr, uid, ids, product_id,uom_id,shop_id,qty,price,context=None):
        if context is None:
            context = {}
        res = {'value': {'product_uom' :False}}
        if not product_id:
            return res
        product_product = self.pool.get('product.product')
        shop_obj = self.pool.get('sale.shop')
        shop = shop_obj.browse(cr, uid, shop_id, context=context)
        if not shop_id:
            res['warning'] = {'title': _('Warning!'), 'message': _('没有选择门店！')}
        if shop_id:
            standard_price = self.get_price(cr, uid, shop_id, product_id)
            #price_subtotal = float(standard_price) * float(qty)
            #res['value'].update({})#,'price_subtotal': price_subtotal
            product_uom = self.pool.get('product.uom')
            product = product_product.browse(cr, uid, product_id, context=context)
            categ_id = product.categ_id.id
            res['value'].update({'code': product.default_code, 'bhmode': product.bhmode,'price': standard_price,'price_subtotal':standard_price * qty,'categ_id':categ_id})
            res['domain'] = {'product_uom': [('category_id','=',product.uom_id.category_id.id)]}
            product_uom_po_id = product.uom_id.id
            if not uom_id:
                uom_id = product_uom_po_id
            if product.uom_id.category_id.id != product_uom.browse(cr, uid, uom_id, context=context).category_id.id:
                if self._check_product_uom_group(cr, uid, context=context):
                    res['warning'] = {'title': _('Warning!'), 'message': _('Selected Unit of Measure does not belong to the same category as the product Unit of Measure.')}
                uom_id = product_uom_po_id
            res['value'].update({'product_uom': uom_id})
        return res

    def create(self, cr, uid, vals, context=None):
        product_obj = self.pool.get('product.product')
        if vals.get('product_id',False) and vals.get('is_cx',False) != True:
            product = product_obj.browse(cr, uid, vals.get('product_id',False), context=context)
            categ_id = product.categ_id.id
            new_price = self.get_price(cr, uid, vals.get('shop_id',False), vals.get('product_id',False))
            vals.update({'code': product.default_code, 'bhmode': product.bhmode,'categ_id':categ_id,'price_subtotal': vals.get('qty',0.0) * new_price, 'price': new_price,'product_uom': product.uom_id.id})
        return super(pos_superorder_line, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        product_obj = self.pool.get('product.product')
        for line in self.browse(cr, uid, ids, context=context):
            if vals.get('is_cx',False):
                is_cx = vals.get('is_cx',False)
            else:
                is_cx = line.is_cx
            if vals.get('shop_id',False):
                shop_id = vals.get('shop_id',False)
            else:
                shop_id = line.shop_id.id
            if vals.get('qty',0.0):
                qty = vals.get('qty',0.0)
            else:
                qty = line.qty
            if vals.get('product_id',False) and is_cx != True:
                product_id = vals.get('product_id',False)
                product = product_obj.browse(cr, uid, vals.get('product_id',False), context=context)
                categ_id = product.categ_id.id
                new_price = self.get_price(cr, uid, shop_id, product_id)
                vals.update({'code': product.default_code, 'bhmode': product.bhmode,'categ_id':categ_id,'price_subtotal': qty * new_price, 'price': new_price,'product_uom': product.uom_id.id})
            if not vals.get('product_id',False) and is_cx != True:
                new_price = self.get_price(cr, uid, shop_id, line.product_id.id)
                vals.update({'price_subtotal': qty * new_price, 'price': new_price})
        return super(pos_superorder_line, self).write(cr, uid, ids, vals, context=context)

pos_superorder_line()

class pos_statement(osv.osv):
    _name = "pos.statement"
    _description = u"商超对账单"
    _order = "id desc"
    _inherit = ['mail.thread', 'approve.base']

    def _get_period(self, cr, uid, context=None):
        ctx = dict(context or {}, account_period_prefer_normal=True)
        period_ids = self.pool.get('account.period').find(cr, uid, context=ctx)
        return period_ids and period_ids[0] or False

    def _get_posnet(self, cr, uid, context=None):
        ctx = dict(context or {}, account_period_prefer_normal=True)
        period_ids = self.pool.get('account.period').find(cr, uid, context=ctx)
        period = self.pool.get('account.period').browse(cr, uid, period_ids, context=None)
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        if user.pos_config and user.pos_config.shop_id:
            if user.pos_config.shop_id.stid:
                partner_id = user.pos_config.shop_id.stid.id
            else:
                return 0
            shop_id = user.pos_config.shop_id.id
        else:
            return 0
        d = period[0].name.split('/')
        year = d[1]
        month = d[0]
        day = calendar.monthrange(int(d[1]),int(d[0]))[1]
        day1 = (datetime.datetime.strptime((str(d[1]) + "-" +'%.2i' % int(d[0]) + "-01 00:00:00"),'%Y-%m-%d %H:%M:%S') - datetime.timedelta(hours=8)).strftime('%Y%m%d %H:%M:%S')#str(d[1]) + "-" +str(d[0]) + "-01 00:00:00"
        day2 = str(d[1]) + "-" +str(d[0]) + "-" + '%.2i' % int(day) + " 15:59:59"
        cr.execute("select sum(xtnet) from wgmf_sale_check where yn_hd = '1' and saledate >= %s and saledate <= %s and shop_id = %s",(day1,day2,shop_id))
        src = cr.fetchone()
        posnet = src and src[0] or 0
        return posnet

    def _get_shop_id(self, cr, uid, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        if  user.pos_config:
            shop_id = user.pos_config.shop_id.id
        else:
            shop_id = False
        return shop_id

    def _get_partner_id(self, cr, uid, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        if  user.pos_config:
            shop = user.pos_config.shop_id
            if shop.stid:
                partner_id = shop.stid.id
            else:
                partner_id  = False
        else:
            partner_id = False
        return partner_id

    _columns = {
        'name': fields.char(u'对账单号', size=128),
        'shop_id' : fields.many2one('sale.shop', u'门店', required=True),
        'partner_id' : fields.many2one('res.partner', u'结算单位', domain=[('shangchao', '=', True)]),
        'dzny': fields.many2one('account.period', u'账期', required=True),
        'k3stor_id': fields.char(u'K3编码', size=20, required=True),
        'dzqj': fields.char(u'对账期间', size=20),
        'zkpnet': fields.float(u'开票总金额', digits=(10,2)),
        'ynpnet': fields.float(u'已回款金额', digits=(10,2)),
        'scnet': fields.float(u'商场销量', digits=(10,2)),
        'cwnet': fields.float(u'财务销量(自然月)', digits=(10,2)),
        'flnet': fields.float(u'返利', digits=(10,2)),
        'sdfnet': fields.float(u'水电费', digits=(10,2)),
        'dzcnet': fields.float(u'电子秤费', digits=(10,2)),
        'hcnet': fields.float(u'耗材费', digits=(10,2)),
        'jqnet': fields.float(u'节庆费', digits=(10,2)),
        'cxglnet': fields.float(u'促销员管理费', digits=(10,2)),
        'jcnet': fields.float(u'进场费', digits=(10,2)),
        'bdfknet': fields.float(u'保底罚款', digits=(10,2)),
        'fkglnet': fields.float(u'管理罚款', digits=(10,2)),
        'othernet': fields.float(u'商场其他费用', digits=(10,2)),
        'otherma': fields.char(u'商场其他费用备注', size=1000),
        'rate': fields.selection([('13', '13%'),('17', '17%'),('1', '13%&17%'),('0', '0')], u'税率(百分数)'),
        'k3net': fields.float(u'K3盘点销量', digits=(10,2)),
        'posnet': fields.float(u'POS手工量', digits=(10,2)),
        'remark': fields.char(u'备注', size=1000),
        'addperson': fields.many2one('res.users','建立人'),
        'adddate': fields.date(u'建立日期'),
        'modperson': fields.many2one('res.users','修改人'),
        'moddate': fields.date(u'修改日期'),
        'gznet': fields.float(u'工资销量', digits=(10,2)),
        'scrate': fields.float(u'扣点(百分数)', digits=(3,2)),
        'gzny': fields.char(u'工资年月', size=6),
        'statement_payday': fields.integer(u'回款天数'),
        'wgmf_date': fields.date(u'交票日期'),
        'wgmf_date2': fields.date(u'开票日期'),
        'chayi_allowed': fields.boolean(u'差异允许'),
        'period_pz': fields.many2one('account.period', u'会计期间(凭证)', domain=['|',('biz_closed', '!=', True),('special', '!=', True)]),
    }

    _defaults = {
        'name': '/',
        'wgmf_date' : time.strftime('%Y-%m-%d'),
        'dzny': _get_period,
        'posnet':_get_posnet,
        'shop_id': _get_shop_id,
        'partner_id': _get_partner_id,
    }

    _sql_constraints = [
        ('uniq_name', 'unique(name)', '单号必须唯一!'),
    ]



    def approve_refuse(self, cr, uid, ids, context=None):
        if context is None:context={}
        l = 0
        for statement in self.browse(cr, uid, ids, context):
            for line in statement.move_lines:
                if line.state == 'posted':
                    self.pool.get('account.move').button_cancel(cr,uid, [line.id], context)
                    self.pool.get('account.move').unlink(cr, uid, [line.id],context)
                else:
                    self.pool.get('account.move').unlink(cr, uid, [line.id],context)
        return super(pos_statement, self).approve_refuse(cr, uid, ids, context)

    def approve_done(self, cr, uid, ids, context=None):
        order=self.browse(cr,uid,ids)
        accmove_line_obj = self.pool.get('account.move.line')
        journal_obj = self.pool.get('account.journal')
        accmove_obj = self.pool.get('account.move')
        s = order.shop_id.name + time.strftime('%Y%m%d')
        date = time.strftime('%Y-%m-%d')
        date1 = (datetime.datetime.strptime(order.wgmf_date,'%Y-%m-%d') + datetime.timedelta(days=order.statement_payday)).strftime('%Y-%m-%d')
        journal_id = journal_obj.search(cr, uid, [('name', '=', '记销五')])
        if not journal_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到名为(记销五)的凭证账簿!"))
        if journal_id:
            journal_id = journal_id[0]
        ys_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '1122.01')])
        if not ys_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(1122.01)的会计科目!"))
        if ys_account_id:
            ys_account_id = ys_account_id[0]
        zg_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '1122.11')])
        if not zg_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(1122.11)的会计科目!"))
        if zg_account_id:
            zg_account_id = zg_account_id[0]
        fl_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '6601.03.01')])
        if not fl_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(6601.03.01)的会计科目!"))
        if fl_account_id:
            fl_account_id = fl_account_id[0]
        sdf_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '6601.03.09')])
        if not sdf_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(6601.03.09)的会计科目!"))
        if sdf_account_id:
            sdf_account_id = sdf_account_id[0]
        dzc_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '6601.03.05')])
        if not dzc_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(6601.03.05)的会计科目!"))
        if dzc_account_id:
            dzc_account_id = dzc_account_id[0]
        qt_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '6601.03.08')])
        if not qt_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(6601.03.08)的会计科目!"))
        if qt_account_id:
            qt_account_id = qt_account_id[0]
        jq_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '6601.03.02')])
        if not jq_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(6601.03.02)的会计科目!"))
        if jq_account_id:
            jq_account_id = jq_account_id[0]
        cxgl_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '6601.03.03')])
        if not cxgl_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(6601.03.03)的会计科目!"))
        if cxgl_account_id:
            cxgl_account_id = cxgl_account_id[0]
        jc_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '6601.03.04')])
        if not jc_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(6601.03.04)的会计科目!"))
        if jc_account_id:
            jc_account_id = jc_account_id[0]
        bdfk_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '6601.03.06')])
        if not bdfk_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(6601.03.06)的会计科目!"))
        if bdfk_account_id:
            bdfk_account_id = bdfk_account_id[0]
        fkgl_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '6601.03.07')])
        if not fkgl_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(6601.03.07)的会计科目!"))
        if fkgl_account_id:
            fkgl_account_id = fkgl_account_id[0]
        kd_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '6601.07')])
        if not kd_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(6601.07)的会计科目!"))
        if kd_account_id:
            kd_account_id = kd_account_id[0]
        cy_account_id = self.pool.get('account.account').search(cr, uid, [('code', '=', '6001.02')])
        if not cy_account_id:
            raise osv.except_osv(
                    _('错误!'),
                    _("未找到编码为(6001.02)的会计科目!"))
        if cy_account_id:
            cy_account_id = cy_account_id[0]
        #会计期间
        period_obj=self.pool.get('account.period')
        date=order.wgmf_date
        period_id=period_obj.search(cr,uid,[('date_start','<=',date),('date_stop','>=',date),('special', '!=', True)])
        if not period_id:raise osv.except_osv( _('错误!'),_("未找到开票日期 %s 对应的会计期间")%date)
        period_id=period_id[0]
        accmove_id = accmove_obj.create(cr, uid, {
            'name': '/',
            'journal_id': journal_id,
            'ref': s,
            'period_id': order.period_pz and order.period_pz.id or order.dzny.id, #period_id,#order.dzny.id,
            'date': date,
            'date_maturity':date1,
            'statement_ids': order.id,
            'to_check': False,
        })
        accmove = accmove_obj.browse(cr, uid, accmove_id)
        if order.zkpnet != 0:
            accmove_line_obj.create(cr, uid, {
                'name': "应收",
                'centralisation': 'debit',
                'partner_id': order.shop_id.stid.id,
                'account_id': ys_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': float(order.zkpnet),
            })
        if order.flnet != 0.0:
            accmove_line_obj.create(cr, uid, {
                'name': "返利",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': fl_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': order.flnet,
            })
        if order.sdfnet != 0.0:
            accmove_line_obj.create(cr, uid, {
                'name': "水电费",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': sdf_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': order.sdfnet,
            })
        if order.dzcnet != 0.0:
            accmove_line_obj.create(cr, uid, {
                'name': "电子秤费",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': dzc_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': order.dzcnet,
            })
        if order.hcnet != 0.0:
            accmove_line_obj.create(cr, uid, {
                'name': "耗材费",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': qt_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': order.hcnet,
            })
        if order.jqnet != 0.0:
            accmove_line_obj.create(cr, uid, {
                'name': "节庆费",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': jq_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': order.jqnet,
            })
        if order.cxglnet != 0.0:
            accmove_line_obj.create(cr, uid, {
                'name': "促销员管理费",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': cxgl_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': order.cxglnet,
            })
        if order.jcnet != 0.0:
            accmove_line_obj.create(cr, uid, {
                'name': "进场费",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': jc_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': order.jcnet,
            })
        if order.bdfknet != 0.0:
            accmove_line_obj.create(cr, uid, {
                'name': "保底罚款",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': bdfk_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': order.bdfknet,
            })
        if order.fkglnet != 0.0:
            accmove_line_obj.create(cr, uid, {
                'name': "管理罚款",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': fkgl_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': order.fkglnet,
            })
        if order.othernet != 0.0:
            accmove_line_obj.create(cr, uid, {
                'name': "其他",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': qt_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': order.othernet,
            })
        kd = float('%.2f' % (float(order.scnet) * (float(order.scrate) / 100)))
        if kd != 0.00:
            accmove_line_obj.create(cr, uid, {
                'name': "销售扣点",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': kd_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': 0.0,
                'debit': kd,
            })
        cy = float('%.2f' % (float(order.scnet) - float(order.posnet)))
        if cy != 0:
            accmove_line_obj.create(cr, uid, {
                'name': "销售差异",
                'centralisation': 'debit',
                'partner_id': False,
                'account_id': cy_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'analytic_account_id': order.shop_id.project_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': cy,
                'debit': 0,
            })
        if order.posnet:
            yszg_id = accmove_line_obj.create(cr, uid, {
                'name': "应收-暂估",
                'centralisation': 'credit',
                'partner_id': order.shop_id.stid.id,
                'account_id': zg_account_id,
                'move_id': accmove_id,
                'journal_id': accmove.journal_id.id,
                'period_id': accmove.period_id.id,
                'date': accmove.period_id.date_stop,
                'date_maturity':date1,
                'credit': order.posnet,
                'debit': 0.0,
            })
            sql="""select sum(debit-credit) from account_move_line
                   where move_id in (select id from account_move where check_id
                   in (select id from wgmf_sale_check where shop_id=%(shop_id)s) )
                   and period_id =%(period_id)s 
                   and account_id=%(account_id)s
                   and partner_id =%(partner_id)s"""
            field_dict={'shop_id':order.shop_id.id,'period_id':order.dzny.id,
                        'account_id':524,'partner_id':order.partner_id.id}      
            cr.execute(sql % field_dict)      
            pos = cr.fetchall()
            if pos[0][0] == order.posnet:
                sql_id="""select id from account_move_line
                       where move_id in (select id from account_move where check_id
                       in (select id from wgmf_sale_check where shop_id=%(shop_id)s) )
                       and period_id =%(period_id)s  and account_id=%(account_id)s
                       and partner_id =%(partner_id)s"""
                cr.execute(sql_id % field_dict)
                line_id = cr.fetchall()
                if not line_id:return True
                yszg_ids=[line[0] for line in line_id]
                yszg_ids.append(int(yszg_id))
                sql_up="""update account_move_line set reconcile_id=null,reconcile_partial_id=null where id"""
                where=' in %s'%(tuple(yszg_ids),)
                cr.execute(sql_up+where)
                accmove_line_obj.reconcile(cr, uid, yszg_ids, 'manual', zg_account_id,accmove.period_id.id, accmove.journal_id.id, context=context)
            else:
                if order.chayi_allowed:return True
                raise osv.except_osv(_('销量异常提示!'), _('您当前的手工量(%s)与系统的日审核手工量(%s)有差异,不能正常核销,如果您允许该差异,请将"允许差异"选中后再试!')%(order.posnet,pos[0][0]))
        return True

    def _get_other_posnet(self, cr, uid, fist_date=False, last_date=False, shop_id=False, context=None):
        if context is None:
            context = {}
        if not shop_id:
            raise osv.except_osv(_('没有默认门店!'), _('修改账期前请先选择门店.'))
        fist_day=(datetime.datetime.strptime(fist_date,'%Y-%m-%d') - timedelta(hours=8)).strftime('%Y-%m-%d %H:%M:%S')
        last_day=(datetime.datetime.strptime(last_date,'%Y-%m-%d') + timedelta(hours=16)).strftime('%Y-%m-%d %H:%M:%S')
        sql="""select sum(xtnet) from wgmf_sale_check where yn_hd = '1' and saledate >= '%s' and saledate < '%s' and shop_id = %s"""%(fist_day,last_day,shop_id)
        cr.execute(sql)
        src = cr.fetchone()
        posnet = src and src[0] or 0
        return posnet

    def onchange_scnet(self, cr, uid, ids, dzqj=False, dzny=False, shop_id=False, scnet=0,zkpnet=0,flnet=0,sdfnet=0,dzcnet=0,hcnet=0,jqnet=0,cxglnet=0,jcnet=0,bdfknet=0,fkglnet=0,othernet=0,scrate=0, context=None):
        if context is None:
            context = {}
        res = {}
        scnet = (float(zkpnet) + float(flnet) + float(sdfnet) + float(dzcnet) + float(hcnet) + float(jqnet) + float(cxglnet) + float(jcnet) + float(bdfknet) + float(fkglnet) + float(othernet))/float(1 - float(scrate)/100.00)
        s = '%.2f' % float(scnet)
        res['scnet'] = float(s)
        if dzqj == False:
            res['cwnet'] = float(s)
        else:
            if dzqj.find('-') == -1:
                raise osv.except_osv(_('格式不正确!'), _("正确格式类型为：月.日-月.日。例如：12.01-12.31!"))
            period = self.pool.get('account.period').browse(cr, uid, dzny, context=None)
            d = period.name.split('/')
            year = int(d[1])
            date = dzqj.split('-')
            d1 = date[0].strip()#开始月-日
            d2 = date[1].strip()#结束月-日
            if d1.find('.') == -1 or d2.find('.') == -1:
                raise osv.except_osv(_('格式不正确!'), _("正确格式类型为：月.日-月.日。例如：12.01-12.31!"))
            fist_m = d1.split('.')[0]
            fist_d = d1.split('.')[1]
            last_m = d2.split('.')[0]
            last_d = d2.split('.')[1]
            fist_d2 = calendar.monthrange(int(year),int(fist_m))[1]
            last_d2 = calendar.monthrange(int(year),int(last_m))[1]
            #fist_day = str(year) + "-" +str(fist_m) + "-" + str(fist_d)
            #last_day = str(year) + "-" +str(last_m) + "-" + str(last_d2)
            #if int(fist_m)-int(last_m) not in (-1,0,11):raise osv.except_osv(_('格式不正确!'), _("请检查对账期间格式!"))
            year_last=year
            if int(fist_m)==12 and int(last_m)==1:
                year_last=int(year)-1
            #1.1-1.15;1.01-2.28
            if int(fist_m)==int(last_m):
                res['cwnet']=0
                return {'value': res}
            if int(fist_m)==int(d[0]):
                pos_add_startdate=str(year_last) + "-" +str(fist_m) + "-" + '01'
                if int(fist_d)!=1:
                    pos_add_enddate=str(year_last) + "-" +str(fist_m) + "-" + str(int(fist_d)-1)
                    fist_posnet = self._get_other_posnet(cr,uid,pos_add_startdate,pos_add_enddate,shop_id,context)
                else:
                    fist_posnet=0
                pos_remove_startdate = str(year) + "-" +str(last_m) + "-" + '01'
                pos_remove_enddate = str(year) + "-" +str(last_m) + "-" + str(last_d)
                last_posnet = self._get_other_posnet(cr,uid,pos_remove_startdate,pos_remove_enddate,shop_id,context)
            elif int(last_m)==int(d[0]):
                if int(last_d)+1<int(last_d2):
                    pos_add_startdate=str(year) + "-" +str(last_m) + "-" + str(int(last_d)+1)
                    pos_add_enddate=str(year) + "-" +str(last_m) + "-" + str(last_d2)
                    fist_posnet = self._get_other_posnet(cr,uid,pos_add_startdate,pos_add_enddate,shop_id,context)
                else:
                    fist_posnet=0
                pos_remove_startdate = str(year_last) + "-" +str(fist_m) + "-" + str(fist_d)
                pos_remove_enddate = str(year_last) + "-" +str(fist_m) + "-" + str(fist_d2)
                last_posnet = self._get_other_posnet(cr,uid,pos_remove_startdate,pos_remove_enddate,shop_id,context)
            else:
                raise osv.except_osv(_('格式不正确!'), _("请检查对账期间格式!"))
            res['cwnet'] = float('%.2f' % (float(s) - float(last_posnet) + float(fist_posnet)))
        return {'value': res}

    def onchange_shop_id(self, cr, uid, ids, dzny=False, dzqj=False, shop_id=False, context=None):
        if context is None:
            context = {}
        result = {}
        warning = {}
        dzny_id = self.search(cr, uid, [('dzny', '=', dzny),('approve_stage', '!=', 'cancel'),('shop_id', '=', shop_id)])
        if dzny_id:
            warning = {
                'title': _('警告!'),
                'message' : _('同一门店在一个账期中已有生成商超对账单.')
            }
        if not shop_id:
            return {}
        shop = self.pool.get('sale.shop').browse(cr,uid,shop_id)
        if shop and shop.stores_no:
            result['k3stor_id'] = shop.stores_no
        if shop and shop.scrate:
            result['scrate'] = shop.scrate
        if shop and shop.stid:
            result['partner_id'] = shop.stid.id
            result['statement_payday'] = shop.stid.statement_payday
        period = self.pool.get('account.period').browse(cr, uid, dzny, context=None)
        d_z = period.name.split('/')
        year_z = d_z[1]
        month_z = d_z[0]
        day_z = calendar.monthrange(int(d_z[1]),int(d_z[0]))[1]
        fist_day = str(d_z[1]) + "-" +str('%.2i' % int(d_z[0])) + "-" + '01'
        last_day = str(d_z[1]) + "-" +str('%.2i' % int(d_z[0])) + "-" + str(day_z)
        if dzqj:
            date = dzqj.split('-')
            d1 = date[0].strip()
            d2 = date[1].strip()
            if d1.find('.') == -1 or d2.find('.') == -1:
                raise osv.except_osv(_('格式不正确!'), _("正确格式类型为：月.日-月.日。例如：12.01-12.31!"))
            fist_m = d1.split('.')[0]
            fist_d = d1.split('.')[1]
            last_m = d2.split('.')[0]
            last_d = d2.split('.')[1]
            fist_day = str(year_z) + "-" +str('%.2i' % int(fist_m)) + "-" + str(fist_d)
            if int(fist_m)==12 and int(last_m)==1:
                year_old=int(year_z)-1
                fist_day = str(year_old) + "-" +str('%.2i' % int(fist_m)) + "-" + str(fist_d)
            last_day = str(year_z) + "-" +str('%.2i' % int(last_m)) + "-" + str(last_d)
        fist_day=(datetime.datetime.strptime(fist_day,'%Y-%m-%d')- timedelta(hours=8)).strftime('%Y-%m-%d %H:%M:%S')
        last_day=(datetime.datetime.strptime(last_day,'%Y-%m-%d') + timedelta(hours=16)).strftime('%Y-%m-%d %H:%M:%S')
        sql="""select sum(xtnet) from wgmf_sale_check where yn_hd = '1' and saledate >= '%s' and saledate < '%s' and shop_id = %s"""%(fist_day,last_day,shop_id)
        cr.execute(sql)
        src = cr.fetchone()
        posnet = src and src[0] or 0
        result['posnet'] = posnet
        return {'warning': warning, 'value': result}

    def create(self, cr, uid, vals, context=None):
        #dzny = self.search(cr, uid, [('dzny', '=', vals.get('dzny',False)),('approve_stage', '!=', 'cancel'),('shop_id', '=', vals.get('shop_id',False))])
        #if dzny:
        #    raise osv.except_osv(_('错误!'), _('同一门店在一个账期中不能重复添加多张商超对账单！'))
        if vals.get('name','/')=='/':
            vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'pos.statement') or '/'
        t = time.strftime('%Y-%m-%d')
        partner = False
        if vals.get('shop_id',False) != False:
            shop = self.pool.get('sale.shop').browse(cr,uid,vals.get('shop_id',False))
            if shop and shop.partner_id:
                partner = shop.stid.id
        vals.update({'addperson': uid,'adddate': t,'partner_id': partner})
        return super(pos_statement, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        if not vals:
            vals = {}
        t = time.strftime('%Y-%m-%d')
        if vals.get('shop_id',False):
            shop = self.pool.get('sale.shop').browse(cr,uid,vals.get('shop_id',False))
            if shop and shop.stid:
                vals.update({'partner_id': shop.stid.id})
        vals.update({'modperson': uid,'moddate': t})
        return super(pos_statement, self).write(cr, uid, ids, vals, context=context)

    def copy(self, cr, uid, id, default=None, context=None):
        default = {} if default is None else default.copy()
        approve_logs = []
        stat = self.browse(cr, uid, id, context=context)
        for l in stat.approve_logs:
            approve_logs.append(l.id)
        default.update({
            'approve_stage':'draft',
            'name': self.pool.get('ir.sequence').get(cr, uid, 'pos.statement'),
            'partner_id': False,
            'addperson': uid,
            'addate': time.strftime('%Y-%m-%d'),
            'modperson': False,
            'moddate': False,
            'wgmf_date': False,
            'move_lines': [],
            'approve_logs': [(6,0,approve_logs)],
        })
        return super(pos_statement, self).copy(cr, uid, id, default, context=context)

pos_statement()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: