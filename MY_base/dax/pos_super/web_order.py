# -*- coding: utf-8 -*-

from openerp import addons
import datetime
from datetime import timedelta
import time
from openerp import tools
from openerp.osv import fields, osv
from tools.translate import _
from openerp import netsvc
import logging
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class wgmf_web_order(osv.osv):
    _name = "wgmf.web.order"
    _order = "sale_date desc"
    _description = u"店商销售单"
    _inherit = ['mail.thread', 'approve.base']

    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_total': 0.0,
            }
            val = 0.0
            for line in order.weborder_line:
                val += line.price_subtotal
            res[order.id]['amount_total'] = val
        return res

    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('wgmf.web.order.line').browse(cr, uid, ids, context=context):
            result[line.weborder_id.id] = True
        return result.keys()

    def _get_organization_id(self, cr, uid, ids, fields_name, arg, context=None):
        res = {}.fromkeys(ids, '')
        for inventory in self.browse(cr, uid, ids, context=context):
            one_res = {'area_id': '', 'office_id': '','wguser_id': ''}
            manager_id = inventory.shop_id and inventory.shop_id.sale_organization
            if manager_id:
                office_id = manager_id.parent_id
                if office_id:
                    one_res['office_id'] = office_id.name
                    area_id = office_id.parent_id and office_id.parent_id.name or False
                    one_res['area_id'] = area_id
                    one_res['wguser_id'] = manager_id.name
            res[inventory.id] = one_res
        return res

    def _organization_id_search(self, cr, uid, obj, name, args, context):
        shop_ids = []
        org_ids = []
        cr.execute("select id from wgmf_sale_organization where name" + " " + args[0][1] + " '%" + args[0][2] + "%'")
        res = cr.fetchall()
        if not res:
            return [('id', '=', 0)]
        for org_id in res:
            organization_ids = self.pool.get('wgmf.sale.organization').search(cr, uid, [('id', 'child_of', org_id[0])])
            if len(organization_ids) > 0:
                for organization_id in organization_ids:
                    org_ids.append(organization_id)
        if len(org_ids) == 0:
            return [('id', '=', 0)]
        shop_ids1 = self.pool.get('sale.shop').search(cr, uid, [('sale_organization', 'in', tuple(org_ids))])
        s_ids = self.search(cr, uid, [('shop_id', 'in', tuple(shop_ids1))])
        if len(s_ids) > 0:
            for s_id in s_ids:
                shop_ids.append(s_id)
        if len(shop_ids) == 0:
            return [('id', '=', 0)]
        ids = [('id', 'in', tuple(shop_ids))]
        return ids

    _columns = {
        'name': fields.char(u'销售单号', size=128,select=True),
        'number': fields.char(u'会员号', size=20,select=True),
        'shop_id' : fields.many2one('sale.shop', u'门店', domain=[('wgmf_usable', '=', True),('yymode', '=', 'WSD')], required=True,select=True),
        'sale_date' : fields.date(u'销售日期', required=True, readonly=True,select=True),
        'sale_id': fields.many2one('res.users', u'销售员'),
        'mdbc': fields.selection([('Z', '早班'),('W', '晚班')], u'班次'),
        'weborder_line': fields.one2many('wgmf.web.order.line', 'weborder_id', 'WebOrder Lines'),
        'state': fields.selection([('draft', u'未提交'), ('done', u'完成'), ('cancel', u'已取消')], 'Status', required=True,select=True),
        #'amount_total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string=u'总计',
        #    store={
        #        'wgmf.web.order': (lambda self, cr, uid, ids, c={}: ids, ['weborder_line'], 10),
        #        'wgmf.web.order.line': (_get_order, ['price', 'qty'], 10),
        #    },multi='sums', help="The total amount.", track_visibility='always'),
        'amount_total': fields.float('实销金额', digits_compute=dp.get_precision('Product Price')),
        'office_id':fields.function(_get_organization_id, fnct_search=_organization_id_search, type='char', string=u'办事处', multi='get_parent_id'),
        'area_id':fields.function(_get_organization_id, fnct_search=_organization_id_search, type='char', string=u'大区', multi='get_parent_id'),
        'wguser_id': fields.function(_get_organization_id, fnct_search=_organization_id_search, type='char', string=u'负责人', multi='get_parent_id'),
    }

    def default_get(self, cr, uid, fields, context=None):
        res = super(wgmf_web_order, self).default_get(cr, uid, fields, context=context)
        t = time.strftime('%Y-%m-%d %H:%M:%S')
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        if user.pos_config:
            shop_id = user.pos_config.shop_id.id
        else:
            shop_id = False
        if context.get('oe_origin','0'):
            origin = context.get('oe_origin','0')
        else:
            origin = '0'
        res.update({
            'name': '/',
            'shop_id': shop_id,
            'sale_date': t,
            'sale_id': uid,
            'state': 'draft',
            'mdbc': 'Z',
        })
        return res

    _sql_constraints = [
        ('uniq_name', 'unique(name)', '单号必须唯一!'),
    ]

    def create(self, cr, uid, vals, context=None):
        shop_obj = self.pool.get('sale.shop')
        inventory_obj = self.pool.get('stock.inventory')
        total_amount = 0.0
        sale_date = vals.get('sale_date',False)
        shop_name = shop_obj.read(cr, uid, [vals.get('shop_id',False)], ['name'])[0]['name']
        t = (datetime.datetime.strptime(time.strftime('%Y-%m-%d %H:%M:%S'),'%Y-%m-%d %H:%M:%S') + datetime.timedelta(hours=8)).strftime('%Y-%m-%d')
        if sale_date > t:
            raise osv.except_osv(_('日期错误'), _('销售日期不能选择未来时间!'))
        if vals.get('shop_id',False) != False:
            cr.execute("select id from wgmf_dssale_check where yn_hd = '1' and date_trunc('day',saledate)::date = \'%s\' and shop_id = %s" % (sale_date,vals.get('shop_id',False)))
            check_id = cr.fetchall()
            if (len(check_id) > 0) and (uid!=16755):
                raise osv.except_osv(_('错误!'),_('门店(%s)在(%s)已经审核,不能在创建商超销售单.' % (shop_name,sale_date)))
        if not vals.get('weborder_line',False):
            raise osv.except_osv(_('错误!'),_('不能确认没有明细行的商超销售单.'))
        sign = inventory_obj.get_closed_shop(cr, uid, sale_date, vals.get('shop_id',False), context)
        if sign == True:
            raise osv.except_osv(_('错误'), _('该门店(%s)在日期(%s)所在月份已盘点，不能在做销售!' % (shop_name,sale_date)))
        if vals.get('weborder_line',False):
            for line in vals.get('weborder_line',False):
                if type(line[2]) is not list:
                    total_amount += line[2].get('price_subtotal',False)
                    if line and line[2].get('shop_id',False):
                        if line[2].get('shop_id',False) != vals.get('shop_id',False):
                            line[2].update({'shop_id': vals.get('shop_id',False)})
        if vals.get('name','/')=='/':
            vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'wgmf.web.order') or '/'
        vals['state'] = 'done'
        vals['sale_id'] = uid
        vals['amount_total'] = total_amount
        return super(wgmf_web_order, self).create(cr, uid, vals, context=context)
        
    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        default.update({
            'name': self.pool.get('ir.sequence').get(cr, uid, 'wgmf.web.order'),
        })
        return super(wgmf_web_order, self).copy(cr, uid, id, default, context)

    def approve_submit(self, cr, uid, ids, context=None):
        context = context or {}
        for sup in self.browse(cr, uid, ids):
            partial_data = {}
            if not sup.weborder_line:
                raise osv.except_osv(_('错误!'),_('不能确认没有明细行的商超销售单.'))
            self.write(cr,uid,sup.id, {'state':'done'})
        return True

wgmf_web_order()

class wgmf_web_order_line(osv.osv):
    _name = "wgmf.web.order.line"
    _order = "id desc"

    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = line.price * line.qty
        return res

    def _default_shop_id(self, cr, uid, context=None):
        if context.get('shop_id',False):
            shop_id = context.get('shop_id',False)
        else:
            shop_id = False
        return shop_id

    _columns = {
        'weborder_id': fields.many2one('wgmf.web.order', 'WebOrder', required=True, ondelete='cascade', select=True),
        'product_id': fields.many2one('product.product', u'产品', domain=[('sale_ok', '=', True),('type', '!=', '7')], change_default=True, select=True),
        'name' : fields.char(u'销售单号', size=128),
        'code' : fields.char(u'产品编号', size=128, select=True),
        'qty': fields.float(u'销售数量', digits_compute= dp.get_precision('Product Unit of Measure')),
        'product_uom': fields.many2one('product.uom', '产品单位'),
        'shop_id' : fields.many2one('sale.shop', u'门店', domain=[('wgmf_usable', '=', True)], select=True),
        'price': fields.float('实销价', digits_compute=dp.get_precision('Product Price')),
        #'price_subtotal': fields.function(_amount_line, string=u'小计', digits_compute= dp.get_precision('Account')),
        'price_subtotal': fields.float('实销金额', digits_compute=dp.get_precision('Account')),
    }

    _defaults = {
        'qty': 1,
        'shop_id': _default_shop_id,
    }

    def _check_product_uom_group(self, cr, uid, context=None):
        group_uom = self.pool.get('ir.model.data').get_object(cr, uid, 'product', 'group_uom')
        res = [user for user in group_uom.users if user.id == uid]
        return len(res) and True or False

    def onchange_product_id(self, cr, uid, ids, product_id,uom_id,shop_id,qty,price,context=None):
        if context is None:
            context = {}
        res = {'value': {'product_uom' :False}}
        if not product_id:
            return res
        product_product = self.pool.get('product.product')
        shop_obj = self.pool.get('sale.shop')
        shop = shop_obj.browse(cr, uid, shop_id, context=context)
        if not shop_id:
            res['warning'] = {'title': _('Warning!'), 'message': _('没有选择门店！')}
        product_uom = self.pool.get('product.uom')
        product = product_product.browse(cr, uid, product_id, context=context)
        res['value'].update({'code': product.default_code,'price_subtotal': price * qty})
        res['domain'] = {'product_uom': [('category_id','=',product.uom_id.category_id.id)]}
        product_uom_po_id = product.uom_po_id.id
        if not uom_id:
            uom_id = product_uom_po_id
        if product.uom_id.category_id.id != product_uom.browse(cr, uid, uom_id, context=context).category_id.id:
            if self._check_product_uom_group(cr, uid, context=context):
                res['warning'] = {'title': _('Warning!'), 'message': _('Selected Unit of Measure does not belong to the same category as the product Unit of Measure.')}
            uom_id = product_uom_po_id
        res['value'].update({'product_uom': uom_id})
        return res

    def create(self, cr, uid, vals, context=None):
        product_obj = self.pool.get('product.product')
        if vals.get('product_id',False):
            product = product_obj.browse(cr, uid, vals.get('product_id',False), context=context)
            vals.update({'code': product.default_code,'product_uom': product.uom_po_id.id})
        return super(wgmf_web_order_line, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        product_obj = self.pool.get('product.product')
        if vals.get('product_id',False):
            product = product_obj.browse(cr, uid, vals.get('product_id',False), context=context)
            vals.update({'code': product.default_code,'product_uom': product.uom_po_id.id})
        return super(wgmf_web_order_line, self).write(cr, uid, ids, vals, context=context)

wgmf_web_order_line()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: