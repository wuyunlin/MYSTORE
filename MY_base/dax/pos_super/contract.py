# -*- coding: utf-8 -*-

from openerp import addons
import datetime
from datetime import timedelta
import time
from openerp import tools
from openerp.osv import fields, osv
from tools.translate import _
from openerp import netsvc
import logging
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class sc_contract(osv.osv):
    _name = "sc.contract"
    _order = "id desc"
    _description = u"商超合同"

    _columns = {
        'dq': fields.char(u'大区', size=20,select=True,readonly=True),
        'bsc': fields.char(u'办事处', size=40,select=True,readonly=True),
        'js': fields.char(u'系统全称', size=80,select=True,readonly=True),
        'sc': fields.char(u'系统简称', size=80,select=True,readonly=True),
        'dz_day': fields.integer(u'对账天数', select=True,readonly=True),
        'hk_day': fields.integer(u'回款天数', select=True,readonly=True),
        'shop_id' : fields.many2one('sale.shop', u'门店', required=True,select=True),
        'state': fields.selection([('ok', '正常'),('overdue', '过期')], u'合同状态',size=20),
        'stage': fields.selection([('draft', '草稿'),('done', '生效')], u'单据状态',size=20),
        'first_date' : fields.date(u'首次合约日期', required=True, select=True),
        'start_date' : fields.date(u'合同开始日期', required=True, select=True),
        'end_date' : fields.date(u'合同结束日期', required=True, select=True),
        'timelimit' : fields.float(u'合同期限', readonly=True, select=True,digits=(16, 1) ),
        'oa_no': fields.char(u'合同OA号', size=80,select=True,),
        'gys_no': fields.char(u'供应商号', size=80,select=True,),
        'kd': fields.char(u'扣点', size=800,select=True,),
        'kd_remark': fields.char(u'扣点说明', size=800,select=True,),
        'fl' : fields.char(u'返利',size=800 ),
        'fl_type': fields.char(u'返利方式', size=800,select=True,),        
        'fl_remark': fields.char(u'返利说明', size=800,select=True,),        
        'rate': fields.selection([('13', '13%'),('17', '17%'),('1', '13%&17%'),('0', '0')], u'税率(百分数)'),
        'bd': fields.char(u'保底', size=200,select=True,),        
        'bd_remark': fields.char(u'保底说明', size=1000,select=True,),       
        'jqf' : fields.float(u'节庆费金额',  select=True,digits=(16, 2) ),
        'jqf_remark': fields.char(u'节庆费说明', size=1000,select=True,),   
        'bzj' : fields.float(u'保证金金额',  select=True,digits=(16, 2) ),
        'bzj_remark': fields.char(u'保证金说明', size=1000,select=True,),                        
        'jcf' : fields.float(u'进场费金额',  select=True,digits=(16, 2) ),
        'jcf_remark': fields.char(u'进场费说明', size=1000,select=True,),         
        'dzcf' : fields.float(u'电子称费金额',  select=True,digits=(16, 2) ),
        'dzcf_remark': fields.char(u'电子称费说明', size=1000,select=True,),           
        'sf' : fields.float(u'水电费金额',  select=True,digits=(16, 2) ),
        'sf_remark': fields.char(u'水费说明', size=1000,select=True,),                 
        'qt1': fields.char(u'其他1', size=1000,select=True,),                 
        'qt2': fields.char(u'其他2', size=1000,select=True,),                 
        'qt3': fields.char(u'其他3', size=1000,select=True,),                 
        'qt4': fields.char(u'其他4', size=1000,select=True,),                 
        'qt5': fields.char(u'其他5', size=1000,select=True,),                 
        'outtime_remark': fields.char(u'合同盖章寄出时间说明', size=1000,select=True,),                 
        'intime_remark': fields.char(u'合同原件收回时间', size=1000,select=True,),                 
        'remark': fields.char(u'备注', size=2000,select=True,),                 
        'create_uid' : fields.many2one('res.users', u'制单人',),
        'create_date' : fields.date( u'制单日期',select=True),
    }
    _defaults = {
        'state': 'draft',
    }    
    
    def onchange_shop_id(self, cr, uid, ids, shop_id,context=None):
        if context is None:
            context = {}
        res = {'value': {'dq' :'','bsc':'','js':'','sc':'','dz_day':0,'hk_day':0}}
        if not shop_id:
            return res
        shop_obj = self.pool.get('sale.shop')
        shop = shop_obj.browse(cr, uid, shop_id, context=context)
        if shop_id:
            res['value']={'dq':shop.area_id   or '',
                                    'bsc':shop.office_id  or '',
                                    'js':shop.stid and shop.stid.name or '',
                                    'sc':shop.stid and shop.stid.parent_id and shop.stid.parent_id.name or '',
                                    'dz_day':shop.stid and shop.stid.statement_pay2 or 0,
                                    'hk_day':shop.stid and shop.stid.statement_payday or 0,
                }
        return res    
        
    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        if not vals:
            vals = {}
        t = time.strftime('%Y-%m-%d')
        if vals.get('shop_id',False) or  vals.get('start_date'):
            shop_id=vals.get('shop_id')  or self.browse(cr,uid,ids[0]).shop_id.id 
            start_date=vals.get('start_date')  or self.browse(cr,uid,ids[0]).start_date 
            cr.execute("""select count(*) from sc_contract where shop_id=%s and start_date='%s' and id !=%s"""%(shop_id,start_date,ids[0]))
            if cr.fetchall()[0][0]!=0:
                raise osv.except_osv(_('错误'), _(' 该门店合同开始日期为%s的合同已存在'% (start_date)))        
        if vals.get('shop_id',False):
            shop = self.pool.get('sale.shop').browse(cr,uid,vals.get('shop_id',False))
            vals.update({'dq':shop.area_id or '',
                                    'bsc':shop.office_id  or '',
                                    'js':shop.stid and shop.stid.name or '',
                                    'sc':shop.stid and shop.stid.parent_id and shop.stid.parent_id.name or '',
                                    'dz_day':shop.stid and shop.stid.statement_pay2 or 0,
                                    'hk_day':shop.stid and shop.stid.statement_payday or 0,})
        return super(sc_contract, self).write(cr, uid, ids, vals, context=context)
        
    def create(self, cr, uid,  vals, context=None):
        if context is None:
            context = {}
        if not vals:
            vals = {}
        if vals.get('shop_id',False) and vals.get('start_date'):
            cr.execute("""select count(*) from sc_contract where shop_id=%s and start_date='%s'"""%(vals['shop_id'],vals['start_date']))
            if cr.fetchall()[0][0]!=0:
                raise osv.except_osv(_('错误'), _(' 该门店合同开始日期为[%s]已存在'% (vals['start_date'])))
        if vals.get('shop_id',False):
            shop = self.pool.get('sale.shop').browse(cr,uid,vals.get('shop_id',False))
            vals.update({'dq':shop.area_id or '',
                                    'bsc':shop.office_id  or '',
                                    'js':shop.stid and shop.stid.name or '',
                                    'sc':shop.stid and shop.stid.parent_id and shop.stid.parent_id.name or '',
                                    'dz_day':shop.stid and shop.stid.statement_pay2 or 0,
                                    'hk_day':shop.stid and shop.stid.statement_payday or 0,})
        return super(sc_contract, self).create(cr, uid,  vals, context=context)
        
                
    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        if context is None:context = {}
        today=time.strftime('%Y-%m-%d')
        cr.execute("""update    sc_contract set state='ok' where start_date<='%s' and end_date>='%s' and state='overdue';
                                 update    sc_contract set state='overdue' where  end_date<'%s' and state='ok';"""%(today,today,today))  
        res = super(sc_contract, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar,submenu=False)
        return res
sc_contract()



