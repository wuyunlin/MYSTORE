# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
import psycopg2
import base64
import datetime
import time
import openerp.addons.decimal_precision as dp
from openerp.addons.web.controllers.main import ExportData2Excel as _Export
import uuid

class wgmf_search_supperorder(osv.osv_memory):
    _name = "wgmf.search.supperorder"

    _columns = {
        'name': fields.char(u'销售单号', size=128,select=True),
        'number': fields.char(u'会员号', size=20,select=True),
        'shop_id' : fields.many2many("sale.shop", string=u"门店"),
        'date_start' : fields.date(u'开始日期'),
        'date_end' : fields.date(u'结束日期'),
        'type': fields.selection([('1', '销售'),('2', '退货'),('3', '团购')], u'销售类型'),
        'code': fields.selection([('2', '固定门店模式'),('3', '督导模式')], u'模式'),
        'state': fields.selection([('draft', u'未提交'), ('approved', u'完成'), ('cancel', u'已取消')], '状态'),
     }

    def default_get(self, cr, uid, fields, context=None):
        res = super(wgmf_search_supperorder, self).default_get(cr, uid, fields, context=context)
        t = time.strftime('%Y-%m-%d %H:%M:%S')
        res.update({
            'date_start': t,
            'date_end': t,
            'state': 'approved',
        })
        return res

    def action_financial_organization(self, cr, uid, ids, context=None):
        context.update({
            'res_model':'wgmf.sale.organization',
            'src_model':'sale.shop',
            'relation_field': 'sale_organization',
            'update_field': 'shop_id',
            'target': 'new'
        })
        return {
            'type': 'ir.actions.client',
            'name': u'门店组织',
            'tag': 'wgmf_customize_query.choose_shop_template',
            'target': 'new',
            'context': context,
        }

    def search_supperorder(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        n = sep = md = d_s = d_n = user_id = ty = pur = ag = c = su_id = st = total = ""
        res = self.read(cr, uid, ids, ['name','number','shop_id','date_start','date_end','type','code','state'])
        name=res[0] and res[0]['name']
        number=res[0] and res[0]['number']
        shop_ids=res[0] and res[0]['shop_id']
        date_start=res[0] and res[0]['date_start']
        date_end=res[0] and res[0]['date_end']
        type=res[0] and res[0]['type']
        code=res[0] and res[0]['code']
        state=res[0] and res[0]['state']
        if name:
            n = "and name = \'%s\'" % name
        if number:
            sep = "and number = \'%s\'" % number
        if shop_ids:
            if len(shop_ids) > 1:
                md = "and shop_id in %s" % str(tuple(shop_ids))
            if len(shop_ids) == 1:
                md = "and shop_id = %s" % str(shop_ids[0])
        if date_start:
            d = (datetime.datetime.strptime(date_start,'%Y-%m-%d') - datetime.timedelta(hours=8)).strftime('%Y-%m-%d')
            d_s = "and sale_date >= \'%s 16:00:00\'" % d
        if date_end:
            d_n = "and sale_date <= \'%s 15:59:59\'" % date_end
        if type:
            ty = "and type = \'%s\'" % type
        if code:
            c = "and code = \'%s\'" % code
        if state:
            st = "and state = \'%s\'" % state
        sql = """select id from pos_superorder where 1=1 %s %s %s %s %s %s %s %s""" % (n, sep, md, d_s, d_n, ty, c, st)
        cr.execute(sql)
        order_ids = cr.fetchall() or []
        result = mod_obj.get_object_reference(cr, uid, 'wgmf_ui_customize', 'pos_superorder_action_tree')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id])[0]
        result['domain'] = [('id','in',order_ids)]
        return result

    def make_sup_order(self, cr, uid, ids, context=None):
        t_ir_model_date = self.pool.get("ir.model.data")
        tree_id = t_ir_model_date.get_object_reference(cr, uid, "pos_super", "pos_superorder_tree_view")[1]
        form_id = t_ir_model_date.get_object_reference(cr, uid, "pos_super", "pos_superorder_form_view")[1]
        return {
            "name": "商超销售单",
            "type": "ir.actions.act_window",
            "res_model": "pos.superorder",
            "view_type": "form",
            "view_mode": "form,tree",
            "target": "inlineview",
            'views': [(form_id, 'form'),(tree_id, 'tree')],
            "context": {'admin_edit':uid!=1,'oe_origin':'1'},
        }

    def print_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        n = sep = md = d_s = d_n = user_id = ty = pur = ag = c = su_id = st = total = ""
        res = self.read(cr, uid, ids, ['name','number','shop_id','date_start','date_end','sale_id','type','pur_reason','age','code','origin','superorder_id','state','amount_total'])
        name=res[0] and res[0]['name']
        number=res[0] and res[0]['number']
        shop_ids=res[0] and res[0]['shop_id']
        date_start=res[0] and res[0]['date_start']
        date_end=res[0] and res[0]['date_end']
        sale_id=res[0] and res[0]['sale_id']
        type=res[0] and res[0]['type']
        pur_reason=res[0] and res[0]['pur_reason']
        age=res[0] and res[0]['age']
        code=res[0] and res[0]['code']
        origin=res[0] and res[0]['origin']
        superorder_id=res[0] and res[0]['superorder_id']
        state=res[0] and res[0]['state']
        amount_total=res[0] and res[0]['amount_total']
        if name:
            n = "and name = \'%s\'" % name
        if number:
            sep = "and number = \'%s\'" % number
        if shop_ids:
            if len(shop_ids) > 1:
                md = "and shop_id in %s" % str(tuple(shop_ids))
            if len(shop_ids) == 1:
                md = "and shop_id = %s" % str(shop_ids[0])
        if date_start:
            d = (datetime.datetime.strptime(date_start,'%Y-%m-%d') - datetime.timedelta(hours=8)).strftime('%Y-%m-%d')
            d_s = "and sale_date >= \'%s 16:00:00\'" % d
        if date_end:
            d_n = "and sale_date <= \'%s 15:59:59\'" % date_end
        if sale_id:
            user_id = "and sale_id = %s" % sale_id
        if type:
            ty = "and type = \'%s\'" % type
        if pur_reason:
            pur = "and pur_reason = \'%s\'" % pur_reason
        if age:
            ag = "and age = \'%s\'" % age
        if code:
            c = "and code = \'%s\'" % code
        if origin:
            o = "and origin = \'%s\'" % origin
        if superorder_id:
            su_id = "and superorder_id = %s" % superorder_id
        if state:
            st = "and state = \'%s\'" % state
        if amount_total:
            total = "and amount_total = %s" % amount_total
        _command = """select case when su.state = 'draft' then '未提交' when su.state = 'approved' then '完成' when su.state = 'cancel' then '已取消' end
            ,su.number,su.name, su.superorder_id,
            o1.name,o2.name,o3.name,s.name,su.amount_total,
            su.sale_date,su.update_date,r.login,
            case when su.type = '1' then '销售' when su.type = '2' then '退货' when su.type = '3' then '团购' end,
            case when su.mdbc = 'Z' then '早班' when su.mdbc = 'W' then '晚班' end,
            case when su.code = '2' then '固定门店模式' when su.code = '3' then '督导模式' end
            from pos_superorder su
            left join sale_shop s on su.shop_id = s.id
            left join res_users r on su.sale_id = r.id
            left join wgmf_sale_organization o1 on s.sale_organization = o1.id
            left join wgmf_sale_organization o2 on o1.parent_id = o2.id
            left join wgmf_sale_organization o3 on o2.parent_id = o3.id
            where 1=1 %s %s %s %s %s %s %s %s %s %s %s %s %s""" % (n, sep, md, d_s, d_n, user_id, ty, pur, ag, c, su_id, st, total)
        _name = str(uuid.uuid1())
        _field = [u'状态',u'会员号',u'销售单号', u'退货原单据', u'大区', u'办事处', u'主管', u'门店', u'总计', u'销售日期', u'上传日期', u'销售员', 
            u'销售类型', u'班次', u'POS编码']
        _file_name = u'商超销售单'
        _Export.do(self, cr, uid, _name, _file_name, _command, _field)
        return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name}

wgmf_search_supperorder()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
