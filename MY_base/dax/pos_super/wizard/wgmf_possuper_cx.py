# -*- coding: utf-8 -*-
import time
from openerp.osv import fields, osv
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.float_utils import float_compare
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from openerp import netsvc
import logging
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class wgmf_possuper_cx_line(osv.osv_memory):
    _name = "wgmf.possuper.cx.line"

    _columns = {
        'cx_id': fields.many2one('wgmf.get.cx', u'促销主档', select=True, required=True, ondelete='cascade'),
        'wg_cx': fields.boolean(u'促销'),
        'comm': fields.char(u'促销方案', size=1024),
        'mode': fields.char(u'主题', size=1024),
        'wg_zp': fields.char(u'赠品列表', size=1024),
        'wg_price_cx': fields.char(u'价格促销明细id', size=1024),
        'quantity': fields.float('数量', digits_compute= dp.get_precision('产品计量单位')),
        'price': fields.float('价格', digits_compute=dp.get_precision('Product Price')),
        'product_id': fields.many2one('product.product', u'产品'),
        'shop_id' : fields.many2one('sale.shop', u'门店', domain=[('wgmf_usable', '=', True)], select=True),
        'total': fields.float('组合品总价', digits_compute=dp.get_precision('Product Price')),
        }

wgmf_possuper_cx_line()

class wgmf_cx_product_line(osv.osv_memory):
    _name = "wgmf.cx.product.line"

    _columns = {
        'cx_product_id': fields.many2one('wgmf.get.cx', u'促销主档', select=True, required=True, ondelete='cascade'),
        'quantity': fields.float('数量', digits_compute= dp.get_precision('产品计量单位')),
        'price': fields.float('价格', digits_compute=dp.get_precision('Product Price')),
        'product_id': fields.many2one('product.product', u'产品', domain=[('sale_ok', '=', True),('type', '!=', '7')], change_default=True, select=True),
        }

wgmf_cx_product_line()

class wgmf_possuper_cx(osv.osv_memory):
    _name = "wgmf.possuper.cx"

    _columns = {
        'cx_line': fields.one2many('wgmf.get.cx.line', 'cx_id', u'促销明细'),
        'cx_product_line': fields.one2many('wgmf.cx.product.line', 'cx_product_id', u'促销明细'),
        'number': fields.char(u'会员号', size=20,select=True),
        'shop_id' : fields.many2one('sale.shop', u'门店', domain=[('wgmf_usable', '=', True)], required=True,select=True),
        'sale_date' : fields.datetime(u'销售日期', required=True,select=True),
        'sale_id': fields.many2one('res.users', u'销售员'),
        'type': fields.selection([('1', '销售'),('2', '退货'),('3', '团购')], u'销售类型', required=True,select=True),
        'pur_reason': fields.selection([('1', '试吃'),('2', '调理身体'),('3', '送礼'),('4', '小孩辅食'),('5', '营养补充'),('6', '营养早餐'),('7', '中晚餐')], u'购买原因',select=True),
        'age': fields.selection([('1', '儿童0-6岁'),('2', '少年7-17岁'),('3', '青年 18-40岁'),('4', '中年40-65岁'),('5', '老年 65以上')], u'年龄段',select=True),
        'superorder_id' : fields.char(u'退货原单据', size=128,select=True),
        'oe_wd': fields.boolean(u'是否网店'),
     }

    def default_get(self, cr, uid, fields, context=None):
        res = super(wgmf_possuper_cx, self).default_get(cr, uid, fields, context=context)
        t = time.strftime('%Y-%m-%d %H:%M:%S')
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        if user.pos_config:
            shop_id = user.pos_config.shop_id.id
        else:
            shop_id = False
        res.update({
            'shop_id': shop_id,
            'sale_date': t,
            'sale_id': uid,
            'type': '1',
        })
        return res

    #def default_get(self, cr, uid, fields, context=None):
    #    res = super(wgmf_possuper_cx, self).default_get(cr, uid, fields, context=context)
    #    moves = [self._partial_move_for(cr, uid, m, context)  for m in context.get('cx_msg',[])]
    #    res.update(cx_line=moves)
    #    return res

    #def _partial_move_for(self, cr, uid, m, context):
    #    partial_move = {
    #        'comm' : m.get('name',''),
    #        'mode' : m.get('mode',''),
    #        'product_id': m.get('product_id',False),
    #        'quantity': m.get('qty',0.0),
    #        'price': m.get('price',0.0),
    #        'shop_id': m.get('shop_id',False),
    #        'wg_zp': str(m.get('promotion','')),
    #        'total': m.get('cx_total',0.0),
    #        'wg_price_cx': str(m.get('line_ids','')),
    #    }
    #    return partial_move

    def add_sup_cx(self, cr, uid, ids, context=None):
        
        return True

    def make_sup_return(self, cr, uid, ids, context=None):
        line_obj = self.pool.get('wgmf.get.cx.line')
        super_line_obj = self.pool.get('pos.superorder.line')
        super_obj = self.pool.get('pos.superorder')
        product_obj = self.pool.get('product.product')
        res = self.read(cr, uid, ids, ['cx_line'])
        cx_line = res[0] and res[0]['cx_line']
        new_value = context.get('wg_value',False)
        i = 0
        for line in line_obj.browse(cr, uid, cx_line):
            if line.wg_cx == True:
                i +=1
        if i > 1:
            raise osv.except_osv(_('促销选择错误!'), _('每张销售单只能选择一种促销方案！'))
        if i <= 0 and context.get('wg_state',False) == 'confirm' and context.get('oe_wd',False) != True:
            picking_ids = super_obj.oe_pos_superpicking(cr,uid, new_value,context=context)
            super_obj.write(cr,uid,[context.get('active_id',False)],{'state':'approved','oe_cx': True})
            return True
        if i <= 0 and context.get('wg_state',False) == 'confirm' and context.get('oe_wd',False) == True:
            super_obj.write(cr,uid,[context.get('active_id',False)],{'state':'approved','oe_cx': True})
            return True
        if i <= 0 and context.get('wg_state',False) != 'confirm':
            super_obj.write(cr,uid,[context.get('active_id',False)],{'oe_cx': True})
            return True
        is_exists_discount_product = super_line_obj.search(cr, uid, [("superorder_id", "=", context.get('active_id',False)), ("product_id", "=", 1597)])
        for line in line_obj.browse(cr, uid, cx_line):
            if line.wg_cx == True and (line.mode == 'MZHPZK_JE' or line.mode == 'MZCCXX' or line.mode == 'MZHPZK_BL'):
                if line.wg_zp != '':
                    for l in eval(line.wg_zp):
                        product = product_obj.browse(cr, uid, l.get('product_id',False))
                        line_id = super_line_obj.create(cr, uid, {
                                'superorder_id': context.get('active_id',False),
                                'product_id': product.id,
                                'code': product.default_code,
                                'qty': l.get('qty',0.0) * line.quantity,
                                'product_uom': product.uom_id.id,
                                'bhmode': 'P',
                                'shop_id': line.shop_id.id,
                                'price': 0,
                                'price_subtotal': 0,
                                'is_cx': True,
                            })
                        if context.get('wg_state',False) == 'confirm' and context.get('oe_wd',False) != True:
                            new_value['superorder_line'].append({
                                'id':line_id,
                                'product_id': product.id,
                                'code': product.default_code,
                                'qty': l.get('qty',0.0) * line.quantity,
                                'product_uom': product.uom_id.id,
                                'price': 0,
                                'price_subtotal': 0,
                                'is_cx':True})

                if line.mode == 'MZHPZK_JE' or line.mode == 'MZHPZK_BL':
                    if line.mode == 'MZHPZK_JE':
                        new_price = line.price
                    if line.mode == 'MZHPZK_BL':
                        new_price = new_value.get('amount_total',0.0) * (1 - line.price/10)
                        line.quantity = 1
                    if not is_exists_discount_product:
                        line_id = super_line_obj.create(cr, uid, {
                                    'superorder_id': context.get('active_id',False),
                                    'product_id': 1597,
                                    'code': '9.8888',
                                    'qty': line.quantity,
                                    'product_uom': 1,
                                    'bhmode': 'P',
                                    'shop_id': line.shop_id.id,
                                    'price': new_price * -1,
                                    'price_subtotal': line.quantity * new_price * -1,
                                    'is_cx': True,
                                })
                        if context.get('wg_state',False) == 'confirm' and context.get('oe_wd',False) != True:
                            new_value['superorder_line'].append({
                                'id':line_id,
                                'product_id': 1597,
                                'code': '9.8888',
                                'qty':line.quantity,
                                'product_uom': 1,
                                'price': new_price * -1,
                                'price_subtotal': line.quantity * new_price * -1,
                                'is_cx':True})
                if context.get('wg_state',False) != 'confirm':
                    super_obj.write(cr,uid,[context.get('active_id',False)],{'oe_cx': True})
                if context.get('wg_state',False) == 'confirm' and context.get('oe_wd',False) == True:
                    super_obj.write(cr,uid,[context.get('active_id',False)],{'state':'approved','oe_cx': True})
                if context.get('wg_state',False) == 'confirm' and context.get('oe_wd',False) != True:
                    picking_ids = super_obj.oe_pos_superpicking(cr,uid, new_value,context=context)
                    super_obj.write(cr,uid,[context.get('active_id',False)],{'state':'approved','oe_cx': True})
            if line.wg_cx == True and (line.mode == 'ZLZK_JE' or line.mode == 'ZLZK_BL'):
                if line.mode == 'ZLZK_JE':
                    new_price = line.price
                if line.mode == 'ZLZK_BL':
                    new_price = new_value.get('amount_total',0.0) * (1 - line.price/10)
                #促销产品ID 1597
                if not is_exists_discount_product:
                    line_id = super_line_obj.create(cr, uid, {
                                'superorder_id': context.get('active_id',False),
                                'product_id': 1597,
                                'code': '9.8888',
                                'qty': 1,
                                'product_uom': 1,
                                'bhmode': 'P',
                                'shop_id': line.shop_id.id,
                                'price': new_price * -1,
                                'price_subtotal': new_price * -1,
                                'is_cx': True,
                            })
                if context.get('wg_state',False) != 'confirm':
                    super_obj.write(cr,uid,[context.get('active_id',False)],{'oe_cx': True})
                if context.get('wg_state',False) == 'confirm' and context.get('oe_wd',False) == True:
                    super_obj.write(cr,uid,[context.get('active_id',False)],{'state':'approved','oe_cx': True})
                if context.get('wg_state',False) == 'confirm' and context.get('oe_wd',False) != True:
                    if not is_exists_discount_product:
                        new_value['superorder_line'].append({
                            'id':line_id,
                            'product_id': 1597,
                            'code': '9.8888',
                            'qty':1,
                            'product_uom': 1,
                            'price': new_price,
                            'price_subtotal': new_price * line.quantity,
                            'is_cx':True})
                    picking_ids = super_obj.oe_pos_superpicking(cr,uid, new_value,context=context)
                    super_obj.write(cr,uid,[context.get('active_id',False)],{'state':'approved','oe_cx': True})
            if line.wg_cx == True and line.mode == 'ZLCX':
                line_id = super_line_obj.create(cr, uid, {
                            'superorder_id': context.get('active_id',False),
                            'product_id': line.product_id.id,
                            'code': line.product_id.default_code,
                            'qty': line.quantity,
                            'product_uom': line.product_id.uom_id.id,
                            'bhmode': line.product_id.bhmode,
                            'shop_id': line.shop_id.id,
                            'price': 0.0,
                            'price_subtotal': 0.0,
                            'is_cx': True,
                        })
                if context.get('wg_state',False) != 'confirm':
                    super_obj.write(cr,uid,[context.get('active_id',False)],{'oe_cx': True})
                if context.get('wg_state',False) == 'confirm' and context.get('oe_wd',False) == True:
                    super_obj.write(cr,uid,[context.get('active_id',False)],{'state':'approved','oe_cx': True})
                if context.get('wg_state',False) == 'confirm' and context.get('oe_wd',False) != True:
                    new_value['superorder_line'].append({
                        'id':line_id,
                        'product_id': line.product_id.id,
                        'code':line.product_id.default_code,
                        'qty':line.quantity,
                        'product_uom':line.product_id.uom_id.id,
                        'price':0.0,
                        'price_subtotal':0.0,
                        'is_cx':True})
                    picking_ids = super_obj.oe_pos_superpicking(cr,uid, new_value,context=context)
                    super_obj.write(cr,uid,[context.get('active_id',False)],{'state':'approved','oe_cx': True})
        return True
wgmf_possuper_cx()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
