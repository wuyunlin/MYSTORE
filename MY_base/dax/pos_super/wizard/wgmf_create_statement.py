# -*- coding: utf-8 -*-
import time
from openerp.osv import fields, osv
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from osv import osv, fields
from tools.translate import _
import psycopg2
import netsvc
import struct
import xlrd
import base64
import time
import datetime
from xlrd import open_workbook,xldate_as_tuple
import logging
import calendar
_logger = logging.getLogger(__name__)

class wgmf_create_statement(osv.osv_memory):
    _name = "wgmf.create.statement"

    _columns = {
        'file':  fields.binary('导入文件', filters='*.xls',required=True),
        }
    def get_pos(self, cr, uid, para):
        if para==0:return 0
        sql="""select sum(xtnet)  from wgmf_sale_check where shop_id=%s and saledate>='%s'  and saledate-interval'1 days'<='%s'  and yn_hd = '1' """%(para[0],para[1],para[2])
        cr.execute(sql)
        return cr.fetchall()[0][0] or 0
    def import_statement(self, cr, uid, ids, context=None):
        context=context or {}
        file= self.browse(cr,uid,ids[0]).file
        excel = xlrd.open_workbook(file_contents=base64.decodestring(file))
        sh = excel.sheet_by_index(0)
        sqlsql=''
        for rx in range(sh.nrows):
            if rx<2:continue
            date_list=[]
            dic={}
            data=[str(sh.cell(rx, i).value or '') for i in range(23) ]
            if not data[0]:continue#A列 门店
            shop=data[0]
            sql="""select id from sale_shop where name='%s' or stores_no='%s'"""%(shop,shop)
            cr.execute(sql)
            fet=cr.fetchall()
            if not fet:raise osv.except_osv(_('错误'), _(' 第%s行门店名称[%s]没有找到正确的门店'% (rx+1,shop)))
            shop_id=fet[0][0]
            date_list.append(shop_id)
            dic['shop_id']=shop_id
            if not data[1]:#B列 账期
                raise osv.except_osv(_('错误'), _(' 第%s账期[信息缺失'% (rx+1)))
            dzny=data[1].split('/')[1]+'/'+data[1].split('/')[0]
            year,month=data[1].split('/')
            sql="""select id from account_period where code='%s' and company_id=1 and special='f'"""%(dzny)
            cr.execute(sql)
            fet=cr.fetchall()
            if not fet:raise osv.except_osv(_('错误'), _(' 第%s行账期%s信息缺失'% (rx+1,dzny)))
            dzny=fet[0][0]
            date_list.append(dzny)
            dic['dzny']=dzny
            dzqj=(data[2]) or ''#C列 对账期间
            date_list.append(dzqj)
            dic['dzqj']=dzqj
            kp=float(data[3] or 0) or 0#D列 开票金额
            dic['kp']=round(kp,2)
            date_list.append(kp)
            fl=float(data[4] or 0) #E列 返利
            dic['fl']=round(fl,2)
            date_list.append(fl)
            sdf=float(data[5] or 0) or 0#F列 水电费
            dic['sdf']=round(sdf,2)
            date_list.append(sdf)
            dzcf=float(data[6] or 0) #G列 电子称费
            dic['dzcf']=round(dzcf,2)
            date_list.append(dzcf)
            hcf=float(data[7] or 0) or 0#H列 耗材费
            dic['hcf']=round(hcf,2)
            date_list.append(hcf)
            jqf=float(data[8] or 0) or 0#I列 节庆费
            dic['jqf']=round(jqf,2)
            date_list.append(jqf)
            cgf=float(data[9] or 0) or 0#J列 促销员管理费
            dic['cgf']=round(cgf,2)
            date_list.append(cgf)
            jcf=float(data[10]  or 0) or 0#K列 进场费
            dic['jcf']=round(jcf,2)
            date_list.append(jcf)
            bdf=float(data[11]  or 0) or 0#L列 保底费
            dic['bdf']=round(bdf,2)
            date_list.append(bdf)
            glf=float(data[12] or 0) or 0#M列 管理费
            dic['glf']=round(glf,2)
            date_list.append(glf)
            qtf=float(data[13]  or 0) or 0#N列 其他费
            dic['qtf']=round(qtf,2)
            date_list.append(qtf)
            bz=data[14] or ''#O列 其他费用备注
            dic['bz']=bz
            date_list.append(bz)
            if not data[15]:#P列 制单人
                raise osv.except_osv(_('错误'), _(' 第%s行制单人信息缺失'% (rx+1)))
            sql="""select id from res_users where login='%s'"""%(data[15])
            cr.execute(sql)
            fet=cr.fetchall()
            if not fet:raise osv.except_osv(_('错误'), _(' 第%s行制单人[%s]没有找到'% (rx+1,data[15])))            
            zdr=fet[0][0]
            dic['zdr']=zdr
            date_list.append(zdr)
            jp_date=data[16]#Q列 交票日期
            #日期格式的
            error=False
            if jp_date:
                try:
                    jp_date=float(jp_date)
                    jp_date = datetime.date.fromordinal(datetime.date(1899,12,31).toordinal()-1+int(jp_date)).strftime("%Y-%m-%d")
                except Exception, e:
                    error='日期转换发生错误,请检查'
            if error:
                try:
                   jp_date=jp_date.replace('/','-')
                   datetime.datetime.strptime(jp_date, "%Y-%m-%d")
                   error=False
                except Exception, e:
                    error='日期格式错误,请输入样式为2015/12/01 格式为日期或者文本'
            if error:
                raise osv.except_osv(_('错误'), _(' 第%s行交票日期[%s]没有找到'% (rx+1,data[16])))            
            date_list.append(jp_date)
            dic['jp_date']=jp_date
            kp_date=data[17]#R列 开票日期
            #日期格式的
            error=False
            if kp_date:
                try:
                    kp_date=float(kp_date)
                    kp_date = datetime.date.fromordinal(datetime.date(1899,12,31).toordinal()-1+int(kp_date)).strftime("%Y-%m-%d")
                except Exception, e:
                    error='日期转换发生错误,请检查'
            if error:
                try:
                   kp_date=data[17].replace('/','-')
                   datetime.datetime.strptime(kp_date, "%Y-%m-%d")
                   error=False
                except Exception, e:
                    error='日期格式错误,请输入样式为2015/12/01 格式为日期或者文本'
            if error:
                raise osv.except_osv(_('错误'), _(' 第%s行开票日期[%s]没有找到'% (rx+1,data[17])))
            date_list.append(kp_date)
            dic['kp_date']=kp_date
            if not data[18]:#S列 凭证期间
                raise osv.except_osv(_('错误'), _(' 第%s凭证期间[信息缺失'% (rx+1)))
            period_pz=data[18].split('/')[1]+'/'+data[18].split('/')[0]
            sql="""select id from account_period where code='%s' and company_id=1 and special='f'"""%(period_pz)
            cr.execute(sql)
            fet=cr.fetchall()
            if not fet:raise osv.except_osv(_('错误'), _(' 第%s行凭证期间[%s]错误'% (rx+1,period_pz)))
            period_pz=fet[0][0]
            date_list.append(period_pz)
            dic['period_pz']=period_pz
            rate=(data[19]) or 0#E列 税率
            if rate=='13&17':rate='1'
            if not rate:rate='0'
            rate=str(   int( float(rate) )   )
            dic['rate']=rate
            gzny=(data[20]) or ''#E列 工资年月
            if len(gzny.split('.')[0])>=6:
                gzny=gzny.split('.')[0]
            dic['gzny']=gzny
            gznet=float(data[21] or 0) or 0#E列 工资销量
            dic['gznet']=round(gznet,2)
            
            #延伸字段 结算/回款天数/扣点/pos手工量
            sql="""with data as (select b.id as partner_id,b.statement_payday as statement_payday,a.scrate as scrate,k3stock from sale_shop a
                         left join res_partner b on b.id=a.stid  
                         where a.id=%s)
                         select now(),k3stock,partner_id,b.statement_payday,b.scrate, sum(xtnet) from wgmf_sale_check a
                         full join data b  on 1=1
                         where saledate<=(select date_stop+interval'1 days'  from account_period where id=%s)
                         and saledate>=(select date_start from account_period where id=%s)
                         and shop_id=%s and yn_hd = '1' 
                         group by b.statement_payday,b.scrate,b.partner_id,k3stock
                         """%(shop_id,dzny,dzny,shop_id)
            cr.execute(sql)
            fet=cr.fetchall()
            if not fet:raise osv.except_osv(_('错误'), _(' 第%s行门店[%s]所属获取手工量/结算信息报错'% (rx+1,shop)))
            tm,k3stock,partner_id,statement_payday,scrate,posnet=fet[0]
            dic['k3stock']=k3stock.split('.')[1]+k3stock.split('.')[2]
            dic['partner_id']=partner_id
            dic['statement_payday']=statement_payday
            if statement_payday is None or (not statement_payday):raise osv.except_osv(_('错误'), _(' 第%s行门店[%s]所属结算尚未设置对账天数'% (rx+1,shop)))
            dic['scrate']=scrate
            dic['posnet']=round(posnet,2)
            cr.execute("""select current_date""")
            dic['time']=cr.fetchall()[0][0]
            #='2015-01-01'
            data22=str(sh.cell(rx, 22).value)
            if data22:
                scrate=round(float(data22),2)
                dic['scrate']=scrate
            if scrate is None or (not scrate):raise osv.except_osv(_('错误'), _(' 第%s行门店[%s]尚未设置扣点'% (rx+1,shop)))
            data23=str(sh.cell(rx, 23).value)#备注
            if not data23:
                raise osv.except_osv(_('错误'), _(' 第%s行门店[%s]未填写差异备注'% (rx+1,shop)))
            dic['remark']=data23
            #商场销量=(开票+费用)/(1-扣点)
            scnet=sum(round(date_list[i],2) for i in range(3,14))/(1-scrate*0.01)
            dic['scnet']=round(scnet,2)
            
            #财务销量=商场销量+上月+本月
            if dzqj:
                if not len(dzqj.split('-'))==2:raise osv.except_osv(_('错误'), _(' 第%s行对账期间[%s]错误'% (rx+1,dzqj)))
                date = dzqj.split('-')
                d1 = date[0].strip()#开始月-日
                d2 = date[1].strip()#结束月-日
                if d1.find('.') == -1 or d2.find('.') == -1:
                    raise osv.except_osv(_('格式不正确!'), _("第%s行 对账期间格式不对;正确格式类型为：月.日-月.日。例如：12.01-12.31!"%(rx+1)))     
                month1,day1=d1.split('.')
                month2,day2=d2.split('.')
                year=int(year)
                month=int(month)
                month1=int(month1)
                month2=int(month2)
                day1=int(day1)
                day2=int(day2)
                year_start=year
                year_end=year
                if int(month)==1:
                    if ( int(month1) not in (1,2,12) ) or (int(month2) not in (1,2,12) ) or  ( month1==12 and month2==2):
                        raise osv.except_osv(_('错误'), _(' 第%s行对账期间[%s]的月份需要为1,2,12且不能对2个月的账'% (rx+1,dzqj)))
                    if int(month1)==12:
                        year_start=year-1
                if month==12 :
                    if ( int(month1) not in (1,11,12) ) or (int(month2) not in (1,11,12) ) or   ( month1==11 and month2==1):
                        raise osv.except_osv(_('错误'), _(' 第%s行对账期间[%s]的月份需要为1,12,11且不能对2个月的账'% (rx+1,dzqj)))
                    if int(month2)==1:
                        year_end=year+1
                start_day="""%4d-%02d-%02d"""%(year_start,month1,day1)
                end_day="""%4d-%02d-%02d"""%(year_end,month2,day2)                 
                maxday=calendar.monthrange(year,month)[1]#本月最后一天
                pos11=pos2=pos1=0
                if month1==month and month2!=month:
                    #待+
                    pos1=[shop_id,"""%4d-%02d-01"""%(year,month),"""%4d-%02d-%02d"""%(year,month,day1-1)]
                    if day1==1:
                        pos1=0                    
                    #待-
                    pos2=[shop_id,"""%4d-%02d-01"""%(year_end,month2),end_day]
                if month2==month and month1!=month:
                    #待-
                    maxday=calendar.monthrange(year_start,month1)[1]#本月最后一天
                    pos2=[shop_id,start_day,"""%4d-%02d-%02d"""%(year_start,month1,maxday)]
                    #待+
                    maxday=calendar.monthrange(year_end,month2)[1]#本月最后一天
                    pos1=[shop_id,"""%4d-%02d-%02d"""%(year_end,month2,day2+1),"""%4d-%02d-%02d"""%(year_end,month2,maxday)]
                    if day2==maxday:
                        pos1=0
                if month1!=month and month2!=month:
                    raise osv.except_osv(_('错误'), _(' 第%s行对账期间[%s]与账期没有交集'% (rx+1,dzqj)))                    
                if month1==month2:
                    pos1=ps2=pos11=0
#                    #待+  #注意start_day=1
#                    pos1=[shop_id,"""%4d-%02d-%01d"""%(year_start,month1,day1-1) ]
#                    if day1==1:
#                        pos1=0
#                    #注意等于最大天数
#                    pos11=[shop_id,"""%4d-%02d-%02d"""%(year_end,month2,day1+1),"""%4d-%02d-%02d"""%(year_end,month2,maxday)]
#                    if day1==maxday:
#                        pos11=0
                    
                pos=self.get_pos(cr,uid,pos1) +self.get_pos(cr,uid,pos11) -self.get_pos(cr,uid,pos2) 
                cwnet=scnet+pos
            else:
                cwnet=scnet    
            dic['cwnet']=round(cwnet,2)
            data21=str(sh.cell(rx, 21).value)
            
            if data21:
                xx=round(float(data21 or 0) ,2)
            else:
                xx=round(cwnet,2)
            dic['gznet']=xx 
            dic['name']='abcd%s'%rx
            sqlsql+="""insert into pos_statement (name,shop_id,partner_id,dzny,k3stor_id,dzqj,statement_payday,zkpnet,rate,posnet,scnet,cwnet,flnet,scrate,gznet,sdfnet,
                            gzny,dzcnet,addperson,hcnet,adddate,jqnet,modperson,cxglnet,moddate,jcnet,wgmf_date,bdfknet,wgmf_date2,fkglnet,period_pz,othernet,otherma,approve_stage,remark)
                            values('%(name)s',%(shop_id)s,%(partner_id)s,'%(dzny)s', '%(k3stock)s','%(dzqj)s',%(statement_payday)s ,%(kp)s,'%(rate)s',%(posnet)s,%(scnet)s,%(cwnet)s,
                            %(fl)s,%(scrate)s,%(gznet)s,%(sdf)s,%(gzny)s,%(dzcf)s,%(zdr)s,%(hcf)s,'%(time)s',%(jqf)s,%(zdr)s ,%(cgf)s,'%(time)s', %(jcf)s,'%(jp_date)s',%(bdf)s,'%(kp_date)s',
                            %(glf)s,%(period_pz)s,%(qtf)s,'%(bz)s' ,'draft' ,'%(remark)s' );
                            """%(dic)
        new_ids=[]
        if sqlsql:
            cr.execute(sqlsql) 
            cr.execute("""select id from  pos_statement  where name like 'abcd%%'  """) 
            new_ids= [x[0] for x in cr.fetchall()]               
            cr.execute("""update pos_statement set name='DR'||id where name like 'abcd%%'  """)  
        mod_obj=self.pool.get('ir.model.data')
        act_obj=self.pool.get('ir.actions.act_window')            
        view=mod_obj.get_object_reference(cr,uid,'wgmf_ui_customize','pos_statement_action_tree')
        view_id=view and view[1] or False
        result=act_obj.read(cr,uid,[view_id])[0]
        result['domain'] = [('id','in',new_ids)] 
        return result
        
        
    def fix_pz(self, cr, uid, ids, context=None):
        if uid!=1:return True
        sql="""select a.id,analytic_account_id,coalesce(b.parent_id,0) from account_move_line a 
                    left join account_analytic_account b on b.id=a.analytic_account_id
                    where  move_id in (
                    select id from account_move where period_id=41 and ref like '%%进销存%%')
                and account_id=873 and analytic_account_id  in (select project_id from sale_shop where project_id is not null) """
        cr.execute(sql)
        fet=cr.fetchall()
        line_obj=self.pool.get('account.move.line')
        for line in fet:
            sql="""select id from account_analytic_account where parent_id=%s and name like '%%事处%%' and type='normal' """%(line[2])
            cr.execute(sql)
            x=cr.fetchall()
            if len(x)!=1:continue         
            line_obj.write(cr,uid,line[0],{'analytic_account_id':x[0][0] or False})
wgmf_create_statement()

