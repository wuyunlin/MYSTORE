# -*- coding: utf-8 -*-
import time
from openerp.osv import fields, osv
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from osv import osv, fields
from tools.translate import _
import psycopg2
import netsvc
import struct
import xlrd
import base64
import time
import datetime
from xlrd import open_workbook,xldate_as_tuple
import logging
import calendar
_logger = logging.getLogger(__name__)

class wgmf_create_contract(osv.osv_memory):
    _name = "wgmf.create.contract"

    _columns = {
        'file':  fields.binary('导入文件', filters='*.xls',required=True),
        }

    def import_statement(self, cr, uid, ids, context=None):
        context=context or {}
        file= self.browse(cr,uid,ids[0]).file
        excel = xlrd.open_workbook(file_contents=base64.decodestring(file))
        sh = excel.sheet_by_index(0)
        shop_obj=self.pool.get('sale.shop')
        sqlsql=''
        for rx in range(sh.nrows):
            if rx<2:continue
            dic={}
            data=[str(sh.cell(rx, i).value or '') for i in range(40) ]
            if not data[2]:continue#c列 门店
            shop=data[2]
            sql="""select id from sale_shop where name='%s' or stores_no='%s'"""%(shop,shop)
            cr.execute(sql)
            fet=cr.fetchall()
            if not fet:raise osv.except_osv(_('错误'), _(' 第%s行门店名称[%s]没有找到正确的门店'% (rx+1,shop)))
            shop_id=fet[0][0]
            dic['shop_id']=shop_id
            shop=shop_obj.browse(cr,uid,shop_id)
            if data[0] and shop.area_id!=data[0]:raise osv.except_osv(_('错误'), _(' 第%s行门店名称[%s]在系统中大区[%s]与excel大区[%s] 不一致'% (rx+1,data[2],shop.area_id,data[0])))
            if data[1] and shop.office_id!=data[1]:raise osv.except_osv(_('错误'), _(' 第%s行门店名称[%s]在系统中办事处[%s]与excel办事处[%s] 不一致'% (rx+1,data[2],shop.office_id,data[1])))
            if data[3] and (shop.stid and shop.stid.name or '')!=data[3]:raise osv.except_osv(_('错误'), _(' 第%s行门店名称[%s]在系统中系统全称[%s]与excel系统全称[%s] 不一致'% (rx+1,data[2],shop.stid and shop.stid.name or '',data[3])))
            if data[4] and (shop.stid and shop.stid.parent_id and shop.stid.parent_id.name or '')!=data[4]:raise osv.except_osv(_('错误'), _(' 第%s行门店名称[%s]在系统中系统简称[%s]与excel系统简称[%s] 不一致'% (rx+1,data[2],shop.stid and shop.stid.parent_id and shop.stid.parent_id.name or '',data[4])))
            if data[5] and (shop.stid and shop.stid.statement_pay2 or 0)!=float(data[5]):raise osv.except_osv(_('错误'), _(' 第%s行门店名称[%s]在系统中对账天数[%s]与excel对账天数[%s] 不一致'% (rx+1,data[2],shop.stid and shop.stid.statement_pay2 or 0,data[5])))
            if data[6] and (shop.stid and shop.stid.statement_payday or 0)!=float(data[6]):raise osv.except_osv(_('错误'), _(' 第%s行门店名称[%s]在系统中回账天数[%s]与excel回账天数[%s] 不一致'% (rx+1,data[2],shop.stid and shop.stid.statement_payday or 0,data[6])))
            dic['dq']=shop.area_id
            dic['bsc']=shop.office_id
            dic['js']=shop.stid and shop.stid.name or ''
            dic['sc']=shop.stid and shop.stid.parent_id and shop.stid.parent_id.name or ''
            dic['dz_day']=shop.stid and shop.stid.statement_pay2 or 0
            dic['hk_day']=shop.stid and shop.stid.statement_payday or 0
            dic['state']=(not data[7] or data[7]=='正常' ) and 'ok' or 'overdue'
            dic['stage']='draft'
            dic['first_date']=data[8]
            if len(dic['first_date'].split('-'))==1:
                try:
                    dic['first_date']= datetime.date.fromordinal(datetime.date(1899,12,31).toordinal()-1+int(float(dic['first_date']))).strftime("%Y-%m-%d")
                except Exception, e:
                    dic['first_date']=False             
            dic['start_date']=data[9]
            if len(dic['start_date'].split('-'))==1:
                try:
                    dic['start_date']= datetime.date.fromordinal(datetime.date(1899,12,31).toordinal()-1+int(float(dic['start_date']))).strftime("%Y-%m-%d")
                except Exception, e:
                    dic['start_date']=False                         
            dic['end_date']=data[10]
            if len(dic['end_date'].split('-'))==1:
                try:
                    dic['end_date']= datetime.date.fromordinal(datetime.date(1899,12,31).toordinal()-1+int(float(dic['end_date']))).strftime("%Y-%m-%d")
                except Exception, e:
                    dic['end_date']=False              
            if not  dic['first_date'] or  not dic['start_date'] or not dic['end_date']:raise osv.except_osv(_('错误'), _(' 第%s行 日期格式有误 请检查数据及格式'% (rx+1)))
            dic['timelimit']=data[11] or 0
            dic['oa_no']=str(data[12]).split('.')[0]
            dic['gys_no']=str(data[13]).split('.')[0]
            x=data[14] or ''
            if '%'  in x:
                dic['kd']=x
            else:    
                dic['kd']=str(round(float(data[14] or 0),4)*100)+'%'
            dic['kd_remark']=data[15]
            dic['fl']=str(round(float(data[16] or 0),4)*100)+'%'
            dic['fl_type']=data[17]
            dic['fl_remark']=data[18]
            dic['rate']=(data[19]  in ('13%','0.13') and '13') or (data[19] in ('17%','0.17') and '17') or (data[19]=='13%&17%' and '1') or '0'
            dic['bd']=data[20] or ''
            dic['bd_remark']=data[21]
            dic['jqf']=round(float(data[22] or 0),2)
            dic['jqf_remark']=data[23]
            dic['bzj']=round(float(data[24] or 0),2)
            dic['bzj_remark']=data[25]
            dic['jcf']=round(float(data[26] or 0),2)
            dic['jcf_remark']=data[27]
            dic['dzcf']=data[28] or 0
            dic['dzcf_remark']=data[29]
            dic['sf']=round(float(data[30] or 0),2)
            dic['sf_remark']=data[31]
            dic['qt1']=data[32]
            dic['qt2']=data[33]
            dic['qt3']=data[34]
            dic['qt4']=data[35]
            dic['qt5']=data[36]
            dic['outtime_remark']=data[37]
            dic['intime_remark']=data[38]
            dic['remark']=data[39]
            dic['create_uid']=uid
            dic['write_uid']=uid
            dic['write_uid']=uid
            dic['write_date']=time.strftime('%Y-%m-%d')
            dic['create_date']=time.strftime('%Y-%m-%d')
            cr.execute("""select count(*) from sc_contract where shop_id=%s and start_date='%s'"""%(dic['shop_id'],dic['start_date']))
            if cr.fetchall()[0][0]!=0:
                raise osv.except_osv(_('错误'), _(' 第%s行门店名称[%s]对应的合同开始日期%s的合同已存在'% (rx+1,data[2],dic['start_date'])))
            sqlsql+="""insert into sc_contract (dq,bsc,shop_id,js,sc,dz_day,hk_day,state,stage,first_date,start_date,end_date, timelimit,oa_no,gys_no,
            kd,kd_remark,fl,fl_type,fl_remark,rate,bd,bd_remark,jqf,jqf_remark,bzj,bzj_remark,jcf,jcf_remark,dzcf,dzcf_remark,sf,sf_remark,qt1,qt2,qt3,
            qt4,qt5,outtime_remark,intime_remark,remark,create_uid,write_uid,create_date,write_date)
            values ('%(dq)s','%(bsc)s',%(shop_id)s,'%(js)s','%(sc)s',%(dz_day)s,%(hk_day)s,'%(state)s','%(stage)s','%(first_date)s','%(start_date)s','%(end_date)s',
            '%(timelimit)s','%(oa_no)s','%(gys_no)s','%(kd)s','%(kd_remark)s','%(fl)s','%(fl_type)s','%(fl_remark)s','%(rate)s','%(bd)s','%(bd_remark)s',%(jqf)s,'%(jqf_remark)s',
            %(bzj)s,'%(bzj_remark)s',%(jcf)s,'%(jcf_remark)s',%(dzcf)s,'%(dzcf_remark)s',%(sf)s,'%(sf_remark)s','%(qt1)s','%(qt2)s','%(qt3)s','%(qt4)s','%(qt5)s','%(outtime_remark)s',
            '%(intime_remark)s','%(remark)s',%(create_uid)s,%(write_uid)s,'%(create_date)s','%(create_date)s' );"""%(dic)
        if  sqlsql:
            cr.execute('select coalesce(max(id),0) from sc_contract where create_uid=%s'%(uid))
            maxid=cr.fetchall()[0][0]
            cr.execute(sqlsql)
            mod_obj=self.pool.get('ir.model.data')
            act_obj=self.pool.get('ir.actions.act_window')            
            view=mod_obj.get_object_reference(cr,uid,'pos_super','action_sc_contract')
            view_id=view and view[1] or False
            result=act_obj.read(cr,uid,[view_id])[0]
            result['domain'] = [('id','>',maxid),('create_uid','=',uid)] 
            return result
           

            
            


wgmf_create_contract()

