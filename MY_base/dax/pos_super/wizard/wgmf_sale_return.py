# -*- coding: utf-8 -*-
import time
from openerp.osv import fields, osv
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.float_utils import float_compare
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from openerp import netsvc
import logging
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class wgmf_sale_return(osv.osv_memory):
    _name = "wgmf.sale.return"

    _columns = {
        'ret_date': fields.datetime('退货日期'),
     }

    """def default_get(self, cr, uid, fields, context=None):
        res = super(wgmf_sale_return, self).default_get(cr, uid, fields, context=context)
        t = time.strftime('%Y-%m-%d %H:%M:%S')
        res.update({
            'ret_date': t,
        })
        return res"""

    def make_sup_return(self, cr, uid, ids, context=None):
        line_ids = []
        new_ids = []
        inventory_obj = self.pool.get('stock.inventory')
        shop_obj = self.pool.get('sale.shop')
        order = self.pool.get('pos.superorder').browse(cr,uid,context.get('active_id',False))
        res = self.read(cr, uid, ids, ['ret_date'])
        ret_date = res[0] and res[0]['ret_date']
        if ret_date > time.strftime('%Y-%m-%d %H:%M:%S'):
            raise osv.except_osv(_('日期错误'), _('销售日期不能选择未来时间!'))
        sign = inventory_obj.get_closed_shop(cr, uid, ret_date, order.shop_id.id, context)
        if sign == True:
            raise osv.except_osv(_('错误'), _('该门店(%s)在日期(%s)所在月份已盘点，不能在做退货操作!' % (order.shop_id.name,ret_date)))
        result = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'wgmf_ui_customize', 'pos_superorder_action_tree')
        id = result and result[1] or False
        result = self.pool.get('ir.actions.act_window').read(cr, uid, [id])[0]
        cr.execute("""select l.product_id,l.categ_id,l.code,l.bhmode,case when l3.qty is null then l.qty else l.qty + l3.qty end as r_qty,l.product_uom,l.price,l.is_cx from 
             (select l.product_id,l.categ_id,l.code,l.bhmode,l.product_uom,l.price,l.qty,l.is_cx from pos_superorder_line l, pos_superorder s
             where l.superorder_id = s.id
             and s.name = '%s') l
                left join (select l2.product_id,l2.qty from pos_superorder_line l2
                where l2.superorder_id in (select id from pos_superorder s2
                    where s2.superorder_id = '%s')) l3
             on l.product_id = l3.product_id""" % (order.name,order.name))
        return_orline = cr.fetchall()
        if len(return_orline) > 0.0:
            for l in return_orline:
                if l[4] != 0.0:
                    line_ids.append([0, False, {
                        'product_id': l[0],
                        'categ_id': l[1],
                        'code' : l[2],
                        'bhmode' : l[3],
                        'qty': l[4] * -1,
                        'product_uom': l[5],
                        'shop_id': order.shop_id.id,
                        'price': l[6],
                        'is_cx':l[7],
                        'price_subtotal': l[4] * -1 * l[6],
                    }])
            type='2'
            if order.type=='2':
                type='1'
            if len(line_ids) > 0.0:
                new_order_id = self.pool.get('pos.superorder').create(cr, uid, {
                    'oe_tb_oracle': False,
                    'type':type,
                    'shop_id': order.shop_id.id,
                    'sale_id': uid,
                    'code': order.code,
                    'number': order.number,
                    'sale_date':ret_date,
                    'update_date': False,
                    'superorder_id': order.name,
                    'superorder_line': line_ids,
                },{'oe_origin': '2'})
                new_ids.append(new_order_id)
            result['domain'] = [('id','in',new_ids)]
        return result
wgmf_sale_return()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
