# -*- coding: utf-8 -*- #
import time
from osv import osv, fields
import logging
import netsvc
from tools.translate import _
import uuid
from openerp.addons.web.controllers.main import ExportData2Excel as _Export
logger = logging.getLogger(__name__)

class wgmf_fk_query(osv.osv_memory):
    _name = "wgmf.fk.query"
    def get_period(self, cr, uid, context):
        sql="""select min(id)  from account_period where company_id=1 and state!='done'  and special!='t' """
        cr.execute(sql)
        return cr.fetchall()[0][0] or False
    _columns = {
        'period_id':  fields.many2one('account.period',u'期间',domain="[('company_id','=',1)]"),
        'partner_name':  fields.char(u'业务伙伴(可填写部分模糊查找)',size=64,help=u'可填写部分,如沃尔玛'),
        'name':fields.char('name',size=64)
        }
    _defaults   ={'period_id':get_period,'name':u'付款查询'}
    def query(self, cr, uid, ids, context=None):
        context=context or {}
        partner_name=context.get('partner_name',False)
        period_id=context.get('period_id',False)
        context['period_id_where']='1=1'
        context['partner_name']='1=1'
        if period_id:context['period_id_where']='b.period_id=%s'%period_id
        if partner_name:context['partner_name']="""c.name like '%%%s%%'"""%partner_name
        if context.get('print',False):
#            sql="""with a as (
#                        select a.date as 日期,b.name as 凭证字号,c.name as 客户名称,a.amount as 银行回单,a.move_id,
#                        coalesce(sum(d.credit-d.debit),0) as 金额
#                       from account_voucher a
#                       left join account_move b on a.move_id=b.id
#                       left join res_partner c on c.id=a.partner_id
#                      left join account_move_line d on d.move_id=a.move_id and d.account_id=514
#                       where %(period_id_where)s and %(partner_name)s group by a.id,b.name,c.name)
#                       
#                        select 日期,凭证字号,客户名称,银行回单 ,金额 ,coalesce(sum(e.debit-e.credit),0) as 账扣费用金额,金额-银行回单-coalesce(sum(e.debit-e.credit),0) as 差异
#                        from a
#                        left join account_move_line e on e.move_id=a.move_id and e.account_id =any(select id from account_account where user_type=9)
#                        group by 日期,凭证字号,客户名称,银行回单,金额
#                        order by 客户名称"""%(context)
                        
            sql="""select a.date as 回款日期,b.name as 凭证字号,c.name as 客户名称,d.name as 费用摘要, 
                        e.name as 门店,f.name as 科目,d.amount as 费用金额
                       from account_voucher a
                       left join account_move b on a.move_id=b.id
                       left join res_partner c on c.id=a.partner_id
                      left join account_voucher_other_line d on d.voucher_ids=a.id
                       left join account_analytic_account e on e.id=d.analytic_account_id
                       left join account_account f on f.id=d.account_id
                       where d.id is not null and %(period_id_where)s and %(partner_name)s   """%(context)                        
            _file_name = u'付款导出'
            _name = str(uuid.uuid1())
            _field=[u'回款日期',u'凭证字号',u'客户名称',u'费用摘要' ,u'门店' ,u'科目',u'费用金额']
            _Export.do(self, cr, uid, _name, _file_name, sql, _field)
            return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name} 
        if isinstance(ids,list):ids=ids[0]
        ids=int(ids)
        context.update({'create_uid':uid,'write_uid':uid,'line_id':ids})
        sql="""delete from wgmf_fk_report where create_uid=%(create_uid)s or line_id=%(line_id)s;
                        insert into wgmf_fk_report (line_id,date,name,partner_name,fy_name,shop_name,km_name,fy_amount,create_uid,write_uid,create_date,write_date)
                        select %(line_id)s,a.date as 回款日期,b.name as 凭证字号,c.name as 客户名称,d.name as 费用摘要, 
                        e.name as 门店,f.name as 科目,d.amount as 费用金额,%(create_uid)s,%(write_uid)s,now()-interval'8 hours',now()-interval'8 hours' 
                       from account_voucher a
                       left join account_move b on a.move_id=b.id
                       left join res_partner c on c.id=a.partner_id
                      left join account_voucher_other_line d on d.voucher_ids=a.id
                       left join account_analytic_account e on e.id=d.analytic_account_id
                       left join account_account f on f.id=d.account_id
                       where d.id is not null and %(period_id_where)s and %(partner_name)s      """%(context)                          
        cr.execute(sql)
        return {
            "name": "付款查询",
            "type": "ir.actions.act_window",
            "res_model": "wgmf.fk.report",
            "view_type": "form",
            "view_mode": "tree",
            'views': [(False, 'tree')],
            'domain':[('line_id','=',ids)],
            "context": {},
        }
wgmf_fk_query()
##line_id,date,name,partner_name,bank_amout,dz_amount,fy_amount
class wgmf_fk_report(osv.osv_memory):
    _name = "wgmf.fk.report"
    _order = "id desc"
    _columns = {
        "line_id": fields.integer(u"辅助明细id"),
        "date": fields.date(u"回款日期",),
        "name": fields.char(u"凭证字号",size=40),
        "fy_name": fields.char(u"费用摘要",size=120),
        "km_name": fields.char(u"科目",size=120),
        "shop_name": fields.char(u"门店",size=120),
        "partner_name": fields.char(u"客户名称",size=1000),
        "bank_amount": fields.float(u"银行回单"),
        "dz_amount": fields.float(u"金额"),
        "fy_amount": fields.float(u"账扣费用金额"),
        "cy_amount": fields.float(u"差异(金额-银行回单-费用)"),
    }
wgmf_fk_report()


