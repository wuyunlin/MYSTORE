# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
import psycopg2
import netsvc
import struct
import xlrd
import base64
import time
from time import localtime
import datetime
from xlrd import open_workbook,xldate_as_tuple
import openerp.addons.decimal_precision as dp

class wgmf_import_web_order(osv.osv_memory):
    _name = "wgmf.import.web.order"

    _columns = {
        'file':  fields.binary('导入文件', filters='*.xls'),
        'date_from' : fields.date(u'开始日期', required=True),
        'date_to' : fields.date(u'结束日期', required=True),
        'date_start' : fields.date(u'开始日期', required=True),
        'date_end' : fields.date(u'结束日期', required=True),
        "shop_id": fields.many2many("sale.shop", string=u"门店"),
        'number': fields.char(u'会员号', size=20,select=True),
        'sale_id': fields.many2one('res.users', u'销售员'),
        'amount_total': fields.float('实销金额', digits_compute=dp.get_precision('Product Price')),
        }

    def action_financial_organization(self, cr, uid, ids, context=None):
        context.update({
            'res_model':'wgmf.sale.organization',
            'src_model':'sale.shop',
            'relation_field': 'sale_organization',
            'update_field': 'shop_id',
            'target': 'new'
        })
        return {
            'type': 'ir.actions.client',
            'name': u'门店组织',
            'tag': 'wgmf_customize_query.choose_shop_template',
            'target': 'new',
            'context': context,
        }

    def default_get(self, cr, uid, fields, context=None):
        res = super(wgmf_import_web_order, self).default_get(cr, uid, fields, context=context)
        t = time.strftime('%Y-%m-%d')
        res.update({
            'date_from': t,
            'date_to': t,
            'date_start': t,
            'date_end': t,
        })
        return res

    def search_order(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        res = self.read(cr, uid, ids, ['shop_id', 'date_start', 'date_end', 'sale_id', 'number', 'amount_total'], context=context)
        date_start = res[0] and res[0]['date_start']
        date_end = res[0] and res[0]['date_end']
        sale_id = res and res[0]['sale_id']
        number = res and res[0]['number']
        amount_total = res and res[0]['amount_total']
        if res and res[0]["shop_id"]:
            shop_id = res[0]["shop_id"]
            shop = "and shop_id in (" + ",".join(map(lambda x: str(x), shop_id)) + ")"
        else:
            shop = ""
        if sale_id:
            u = "and sale_id = %s" % sale_id[0]
        else:
            u = ""
        if number:
            num = "and number = \'%s\'" % number
        else:
            num = ""
        if amount_total:
            total = "and amount_total = %s" % amount_total
        else:
            total = ""
        cr.execute('select id from wgmf_web_order where sale_date >= \'%s\' and sale_date <= \'%s\' %s %s %s %s' % (date_start,date_end,shop,u,num,total))
        order_ids = cr.fetchall()
        if len(order_ids) > 0:
            web_ids = [a for (a,) in order_ids]
        else:
            raise osv.except_osv(_('警告!'), _('所选条件下无符合要求的网店销售单.'))
        result = mod_obj.get_object_reference(cr, uid, 'wgmf_ui_customize', 'wgmf_web_order_action_tree')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id])[0]
        result['domain'] = [('id','in',web_ids)]
        return result

    def import_order(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        web_obj = self.pool.get('wgmf.web.order')
        webline_obj = self.pool.get('wgmf.web.order.line')
        product_obj = self.pool.get('product.product')
        shop_obj = self.pool.get('sale.shop')
        order_ids = []
        res = self.read(cr, uid, ids, ['date_from','date_to'])
        date_from = res[0] and res[0]['date_from']
        date_to = res[0] and res[0]['date_to']
        cr.execute("delete from wgmf_web_order where sale_date >= %s and sale_date <= %s",(date_from,date_to))
        for wiz in self.browse(cr,uid,ids):
            if not wiz.file:
                continue
        excel = xlrd.open_workbook(file_contents=base64.decodestring(wiz.file))
        sh = excel.sheet_by_index(0)
        code = []
        for rx in range(sh.nrows):
            line_ids = []
            if sh.cell(rx, 1).value == '':
                break
            if str(sh.cell(rx, 0).value or '').split('.')[0] == '门店':
                continue
            shop_code = str(sh.cell(rx, 1).value or '').split('.')[0]#门店编码
            if not shop_code:
                raise osv.except_osv(_('错误'), _('门店编码不能为空(行%s)!'% (rx)))
            default_code = str(sh.cell(rx, 3).value or '')#产品编码
            if not default_code:
                raise osv.except_osv(_('错误'), _('产品编码不能为空(行%s)!'% (rx)))
            qty = sh.cell(rx, 4).value or 1.0
            number = str(sh.cell(rx, 5).value or '').split('.')[0]#会员号
            sale_date = sh.cell(rx, 6).value or False#销售日期
            if sale_date:
                try:
                    date = datetime.date.fromordinal(datetime.date(1899,12,31).toordinal()-1+int(sale_date)).strftime("%Y-%m-%d")
                except Exception, e:
                    date=False
            if not sale_date or date == False:
                raise osv.except_osv(_('错误'), _('销售日期不能为空(行%s)!'% (rx)))
            if date < date_from or date > date_to:
                raise osv.except_osv(_('错误'), _('%s行的销售日期不再开始期间%s和结束日期%s之间!'% ((int(rx) + 1),date_from,date_to)))
            total_price = sh.cell(rx, 7).value or 0.0
            for k in code:
                v = str(shop_code) + str(sale_date)
                if v == k:
                    raise osv.except_osv(_('错误!'),_('网店销售单门店重复行(%s).' % (rx)))
            code.append(str(shop_code) + str(sale_date))
            product_id = product_obj.search(cr, uid, [('default_code', '=', default_code)])
            if not product_id:
                raise osv.except_osv(_('错误'), _('未找到编码为%s的产品!'% (default_code)))
            else:
                product = product_obj.browse(cr, uid, product_id[0])
            shop_id = shop_obj.search(cr, uid, [('stores_no', '=', shop_code)])
            if not shop_id:
                raise osv.except_osv(_('错误'), _('未找到编码为%s的门店!'% (shop_code)))
            name = self.pool.get('ir.sequence').get(cr, uid, 'wgmf.web.order') or '/'
            line_ids.append([0, False, {
                'product_id': product_id[0],
                'name': name,
                'code': default_code,
                'qty': qty,
                'product_uom': product.uom_po_id.id,
                'shop_id': shop_id[0],
                'price_subtotal': total_price,
            }])
            web_id = web_obj.create(cr, uid, {
                'name': name,
                'number': number,
                'shop_id': shop_id[0],
                'sale_date': date,
                'sale_id': uid,
                'weborder_line': line_ids,
                'state': 'done',
            })
            order_ids.append(web_id)
        result = mod_obj.get_object_reference(cr, uid, 'wgmf_ui_customize', 'wgmf_web_order_action_tree')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id])[0]
        result['domain'] = [('id','in',order_ids)]
        return result
wgmf_import_web_order()
