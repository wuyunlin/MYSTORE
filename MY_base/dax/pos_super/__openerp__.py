# -*- coding: utf-8 -*-
{
    'name': '商超销售模块',
    'version': '0.1',
    'category': 'Generic Modules/report',
    'description': """
    【业务背景】
    为了支持商超销售，系统需要提供日销量上报和商超对账两个基本功能。
    日销量上报：需要通过短信、微信，或3G网络上报日销量；
    商超对账：上传商超对账单，系统核对同期营业员上报的销量，生成差异报表。
    """,
    'author': 'OSCG',
    'website': 'http://www.oscg.com.hk',
    'images': [
        'static/src/img/default_image.png',
    ],
    'depends': ['base','point_of_sale','oscg_approve','sale'],
    'web_depends': [],
    'init_xml': [],
    'update_xml': [
                   'pos_super_view.xml',
                   'pos_super_seq.xml',
                   'web_order_view.xml',
                   'contract_view.xml',
                   'wizard/wgmf_sale_return_view.xml',
                   'wizard/wgmf_get_cx_view.xml',
                   'wizard/wgmf_import_web_order_view.xml',
                   'wizard/wgmf_search_supperorder_view.xml',
                   'wizard/wgmf_create_statement.xml',
                   'wizard/wgmf_fk_query_view.xml',
                   'wizard/wgmf_create_contract.xml',
                  ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': False,
}
