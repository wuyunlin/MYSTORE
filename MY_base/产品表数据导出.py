select a.default_code,b.name,c.name as 库存单位,a.often_switch as 常用转换,d.name as 常用单位,
case when a.jg_type='deep' then '深加工'
else '初加工' end as 加工类型,
f.name,g.name,e.safestoreqty 
from product_product a
left join product_template b on b.id=a.product_tmpl_id
left join product_uom c on c.id=b.uom_id
left join product_uom d on d.id=a.uom_id_often
left join bs_material_corp e on e.product_id=a.id
left join bs_wcenter f on f.id=e.bs_workcentre_id
left join bs_publictype_d g on g.id=a.v_bigcat_id
--where b.name like '%深%' and g.name!='劳保用品'
where a.active='t'
order by a.default_code