# -*- coding:utf-8 -*-
# import pyodbc
import psycopg2
import time
import os

# os.chdir("/home/lxyraul")
#os.chdir("F:\\0_Python\\Python_code")

# Auther: Toney
# CreateTime: 2016-06-17
# 检查PG两个数据库字段的差异和大小

# PG--PG_TEST 表名
# key: PG表名
# value: PG_TEST 对应的表名
# select tablename from pg_tables where tableowner='openerp' and schemaname='public' order by tablename  # 查询数据库表名
# select pg_size_pretty(pg_relation_size('fm_pm_invoice'));  # 查询表的大小

# TABLE_NAME = {
# 	'product_product':'product_product',
# }

SERVER_T = {
	'host': ’127.0.0.1’,
	# 'database': "svm_odoo10”,
	'database':'svm_odoo10',
} 
SERVER = {
	'host': '127.0.0.1',
	'database': ‘svm’,
}


# 获取数据库表名
def get_tabname(cr_t, cr):
	tab_t = []
	tab = []
	vals = {}
	sql = """ select tablename from pg_tables where tableowner='openerp' and schemaname='public' """
	cr_t.execute(sql)
	tabname_t = cr_t.fetchall()
	cr.execute(sql)
	tabname = cr.fetchall()
	print len(tabname_t), len(tabname)
	for i in tabname_t:
		tab_t.append(str(i[0]))
	for j in tabname:
		tab.append(str(j[0]))
	for n in tab_t:
		if n in tab:
			vals[n] = n
	return vals

# 连接数据库
def connPG():
	# 连接Postgresql测试环境
	conn_t = psycopg2.connect(database=SERVER_T['database'], user='openerp', password='postgres', host=SERVER_T['host'], port='5432')
	cr_t = conn_t.cursor()
	# 连接Postgresql正式环境
	conn = psycopg2.connect(database=SERVER['database'], user='openerp', password='postgres', host=SERVER['host'], port='5432')
	cr = conn.cursor()
	return cr_t, cr

# 获取PG表中的字段
def check_pg_columns(table_name):
	columns_list_t = []   # 测试数据库的字段
	columns_list = []     # 正式数据库的字段
	# cr_t = connPG_TEST()
	cr_t, cr = connPG()
	# 查询测试环境字段
	sql = """Select column_name from information_schema.columns where table_name='{}' """.format(table_name)
	cr_t.execute(sql)
	for i in cr_t.fetchall():
		columns_list_t.append(i[0])

	# 查询表大小
	sql_size = """ select pg_size_pretty(pg_relation_size('{}')) """.format(table_name)
	cr_t.execute(sql_size)
	tab_size_t = cr_t.fetchall()

	# 查询正式环境字段
	cr.execute(sql)
	for j in cr.fetchall():
		columns_list.append(j[0])

	# 查询表大小
	sql_size = """ select pg_size_pretty(pg_relation_size('{}')) """.format(table_name)
	cr.execute(sql_size)
	tab_size = cr.fetchall()

	return columns_list_t, columns_list, tab_size_t, tab_size

def main_check(table_name):
	f = open('tabname_check_pg.txt', 'w+')
	f_t = open('tabname_check_pg_t.txt', 'w+')
	for k in table_name.keys():
		if k<>'ir_ui_menu':continue       
		pg_col_t, pg_col, tab_size_t, tab_size = check_pg_columns(k)
		print"===pg_col_t===========" ,pg_col_t     
		print"===pg_col===========" ,pg_col     
		# 	print "表 {} :在数据库 {} 的大小为 {} ---- 在数据库 {} 中的大小为 {}".format(k, SERVER_T['host'], tab_size_t, SERVER['host'], tab_size)
		# else:
		# 	pass
		# 检查PG中的字段是否在PG_TEST中创建
		for i in pg_col:
			if i not in pg_col_t:
				# pass
				print "{} 上 {} 表中的 {} 字段没有在 {} 中创建！".format(SERVER['host'], k, i, SERVER_T['host'])
				# f_t.write("{} 上 {} 表中的 {} 字段没有在 {} 中创建！\n".format(SERVER['host'], k, i, SERVER_T['host']))
			else:
				pass
		# 检查PG_TEST中的字段是否存在PG中
		for j in pg_col_t:
			if j not in pg_col:
				print "{} 上 {} 表中的 {} 字段没有存在 {} 中！".format(SERVER_T['host'], table_name[k], j, SERVER['host'])
				# f.write(" {} 上 {} 表中的 {} 字段没有存在 {} 中！\n".format(SERVER_T['host'], table_name[k], j, SERVER['host']))
			else:
				pass
	print '===========check done!============='
	f.close()
	f_t.close()
	return True

if __name__ == '__main__':
	cr_t, cr = connPG()
	vals = get_tabname(cr_t, cr)
	main_check(vals)
