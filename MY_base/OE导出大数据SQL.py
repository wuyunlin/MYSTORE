--会员兑换记录导出
select area.name as 大区,office.name as 办事处,wguser.name as 负责人,s.name as 门店,s.stores_no as 门店编码,l.member_no as 会员号,pr.name as 产品名称, 
   pr.default_code as 产品编码,to_char((l.log_date + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 兑换日期,l.product_qty as 兑换数量,
   l.log_integral as 兑换积分,l.kc_integral as 扣除积分,l.total_integral as 现有积分 from wgmf_show_jf_log l
   left join (select t.name,p.default_code,p.id from product_product p left join product_template t on p.product_tmpl_id = t.id) pr on l.product_id = pr.id 
   left join sale_shop s on l.shop_id = s.id
   left join (select o.name,s.id from sale_shop s left join wgmf_sale_organization o on s.sale_organization = o.id) wguser on wguser.id = l.shop_id
   left join (select org.name, org2.shop_id from wgmf_sale_organization org 
                left join (select s.id as shop_id ,o.parent_id from sale_shop s
		                left join wgmf_sale_organization o on s.sale_organization = o.id) org2 on org.id = org2.parent_id) office on office.shop_id = l.shop_id
   left join (select org6.name, org5.shop_id from wgmf_sale_organization org6
    left join (select org4.parent_id, org3.shop_id from wgmf_sale_organization org4
       left join (select s2.id as shop_id ,o2.parent_id from sale_shop s2
		left join wgmf_sale_organization o2 on s2.sale_organization = o2.id) org3 on org4.id = org3.parent_id) org5 on org6.id = org5.parent_id) area on area.shop_id = l.shop_id
   where l.member_no !='' 
   --动作 兑换(dh)/购买(gm)
   and l.log_action = 'dh'
   --开始日期
   and l.log_date >= '2014-12-31 16:00:00'
   --结束日期
   and l.log_date <= '2015-01-31 15:59:59'
   --产品id
   --and l.product_id = ''
   --扣除积分
   --and l.kc_integral = 
   --兑换积分
   --and l.log_integral = 
   --现有积分
   --and l.total_integral = 
   --门店id
   --and l.shop_id = 
   --按组织过滤，将下面的“上海办”更换为对应的组织即可
   and l.shop_id in (with recursive cte as (  
	    select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id where o.name='专卖店'
	    union all  
	    select b.shop_id,b.name,b.id,b.parent_id from (select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id) b, cte where b.parent_id=cte.id
	    )
	    select shop_id from cte where shop_id is not null and name not in ('积分测试','Your Company'))

--专柜销售单记录导出SQL
select area.name as 大区,office.name as 办事处,wguser.name as 负责人,sup.name as 单号,sup.number as 会员号,sup.sale_id as 销售员,s.name as 门店,s.stores_no as 门店编码,pr.name as 产品名称,pr.default_code as 产品编码,
 sup_l.qty as 数量,u.name as 单位,sup_l.price as 售价, sup_l.qty * sup_l.price as 小计,to_char((sup.sale_date + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 销售日期,
 to_char((sup.update_date + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 上传日期 from pos_superorder_line sup_l
 left join pos_superorder sup on sup_l.superorder_id = sup.id
 left join (select t.name,p.default_code,p.id from product_product p left join product_template t on p.product_tmpl_id = t.id) pr on sup_l.product_id = pr.id 
 left join sale_shop s on sup_l.shop_id = s.id
 left join (
    select p.id, uom.name from product_uom uom left join (
       select t.uom_id,p.id from product_product p left join product_template t on p.product_tmpl_id = t.id) p on p.uom_id = uom.id) u on sup_l.product_id = u.id
 left join (select o.name,s.id from sale_shop s left join wgmf_sale_organization o on s.sale_organization = o.id) wguser on wguser.id = sup_l.shop_id
   left join (select org.name, org2.shop_id from wgmf_sale_organization org 
                left join (select s.id as shop_id ,o.parent_id from sale_shop s
		                left join wgmf_sale_organization o on s.sale_organization = o.id) org2 on org.id = org2.parent_id) office on office.shop_id = sup_l.shop_id
   left join (select org6.name, org5.shop_id from wgmf_sale_organization org6
    left join (select org4.parent_id, org3.shop_id from wgmf_sale_organization org4
       left join (select s2.id as shop_id ,o2.parent_id from sale_shop s2
		left join wgmf_sale_organization o2 on s2.sale_organization = o2.id) org3 on org4.id = org3.parent_id) org5 on org6.id = org5.parent_id) area on area.shop_id = sup_l.shop_id
--注意数据库存储的日期是格林威治时间
--销售日期
 --and sup.sale_date >= '2014-12-31 16:00:00'
 --and sup.sale_date <= '2015-01-31 15:59:59'
 --上传日期
 --and sup.update_date <= '2014-12-31 16:00:00'
 --and sup.update_date <= '2015-01-31 15:59:59'
 --按组织过滤，将下面的“上海办”更换为对应的组织即可
   and sup_l.shop_id in (with recursive cte as (  
	    select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id where o.name='专卖店'
	    union all  
	    select b.shop_id,b.name,b.id,b.parent_id from (select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id) b, cte where b.parent_id=cte.id
	    )
	    select shop_id from cte where shop_id is not null and name not in ('积分测试','Your Company'))


 --专卖店销售单记录导出SQL
 select area.name as 大区,office.name as 办事处,wguser.name as 负责人,pos.name as 单号,pos.members_number as 会员号,pos.web_number 网店单号,pos.user_id as 销售员,s.name as 门店,s.stores_no as 门店编码,pr.name as 产品名称,pr.default_code as 产品编码,
 pos_l.qty as 数量,u.name as 单位,pos_l.price_unit as 售价, pos_l.qty * pos_l.price_unit as 小计,to_char((pos.date_order + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 订单日期
 from pos_order_line pos_l
 left join pos_order pos on pos_l.order_id = pos.id
 left join (select t.name,p.default_code,p.id from product_product p left join product_template t on p.product_tmpl_id = t.id) pr on pos_l.product_id = pr.id 
 left join sale_shop s on pos_l.shop_id = s.id
 left join (
    select p.id, uom.name from product_uom uom left join (
       select t.uom_id,p.id from product_product p left join product_template t on p.product_tmpl_id = t.id) p on p.uom_id = uom.id) u on pos_l.product_id = u.id
 left join (select o.name,s.id from sale_shop s left join wgmf_sale_organization o on s.sale_organization = o.id) wguser on wguser.id = pos_l.shop_id
   left join (select org.name, org2.shop_id from wgmf_sale_organization org 
                left join (select s.id as shop_id ,o.parent_id from sale_shop s
		                left join wgmf_sale_organization o on s.sale_organization = o.id) org2 on org.id = org2.parent_id) office on office.shop_id = pos_l.shop_id
   left join (select org6.name, org5.shop_id from wgmf_sale_organization org6
    left join (select org4.parent_id, org3.shop_id from wgmf_sale_organization org4
       left join (select s2.id as shop_id ,o2.parent_id from sale_shop s2
		left join wgmf_sale_organization o2 on s2.sale_organization = o2.id) org3 on org4.id = org3.parent_id) org5 on org6.id = org5.parent_id) area on area.shop_id = pos_l.shop_id
--注意数据库存储的日期是格林威治时间
--销售日期
 --and pos.sale_date >= '2014-12-31 16:00:00'
 --and pos.sale_date <= '2015-01-31 15:59:59'
 --上传日期
 --and pos.update_date <= '2014-12-31 16:00:00'
 --and pos.update_date <= '2015-01-31 15:59:59'
  --按组织过滤，将下面的“上海办”更换为对应的组织即可
   and pos_l.shop_id in (with recursive cte as (  
	    select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id where o.name='专卖店'
	    union all  
	    select b.shop_id,b.name,b.id,b.parent_id from (select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id) b, cte where b.parent_id=cte.id
	    )
	    select shop_id from cte where shop_id is not null and name not in ('积分测试','Your Company'))


 --配送产品记录导出SQL
select area.name as 大区,office.name as 办事处,wguser.name as 负责人,sh.name as 单号,sh.wgmf_ps_bill as 配送单,s.name as 门店,s.stores_no as 门店编码,pr.name as 产品名称,pr.default_code as 产品编码,
 sh_l.tty as 数量,u.name as 单位,sh_l.price as 售价, sh_l.tty * sh_l.price as 小计,to_char((sh.create_date + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 创建日期,sh.oper_lr as 收货人,
 to_char((sh.wdate + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 收货日期,sh.oper_sh as 审核人, to_char((sh.sh_wdate + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 审核日期,
 sh.psbc as 波次号,CASE when sh.ps_type = '1' then '被动配送' when sh.ps_type = '2' then '主动配送' when sh.ps_type = '3' then '配送红冲' when sh.ps_type = '4' then '配送反仓' end as 配送类型 
 from wgmf_shouhuo_order_line sh_l
 left join wgmf_shouhuo_order sh on sh_l.shouhuo_id = sh.id
 left join (select t.name,p.default_code,p.id from product_product p left join product_template t on p.product_tmpl_id = t.id) pr on sh_l.product_id = pr.id 
 left join sale_shop s on sh.shop_id = s.id
 left join (
    select p.id, uom.name from product_uom uom left join (
       select t.uom_id,p.id from product_product p left join product_template t on p.product_tmpl_id = t.id) p on p.uom_id = uom.id) u on sh_l.product_id = u.id
 left join (select o.name,s.id from sale_shop s left join wgmf_sale_organization o on s.sale_organization = o.id) wguser on wguser.id = sh.shop_id
   left join (select org.name, org2.shop_id from wgmf_sale_organization org 
                left join (select s.id as shop_id ,o.parent_id from sale_shop s
		                left join wgmf_sale_organization o on s.sale_organization = o.id) org2 on org.id = org2.parent_id) office on office.shop_id = sh.shop_id
   left join (select org6.name, org5.shop_id from wgmf_sale_organization org6
    left join (select org4.parent_id, org3.shop_id from wgmf_sale_organization org4
       left join (select s2.id as shop_id ,o2.parent_id from sale_shop s2
		left join wgmf_sale_organization o2 on s2.sale_organization = o2.id) org3 on org4.id = org3.parent_id) org5 on org6.id = org5.parent_id) area on area.shop_id = sh.shop_id
--注意数据库存储的日期是格林威治时间
--创建日期
 --and sh.create_date >= '2014-12-31 16:00:00'
 --and sh.create_date <= '2015-01-31 15:59:59'
 --收货日期
 --and sh.wdate <= '2014-12-31 16:00:00'
 --and sh.wdate <= '2015-01-31 15:59:59'
 --审核日期
 --and sh.sh_wdate <= '2014-12-31 16:00:00'
 --and sh.sh_wdate <= '2015-01-31 15:59:59'
 --按组织过滤，将下面的“上海办”更换为对应的组织即可
   where sh.shop_id in (with recursive cte as (  
	    select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id where o.name='专卖店'
	    union all  
	    select b.shop_id,b.name,b.id,b.parent_id from (select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id) b, cte where b.parent_id=cte.id
	    )
	    select shop_id from cte where shop_id is not null and name not in ('积分测试','Your Company'))

 --辅料盘点数据导出SQL
select area.name as 大区,office.name as 办事处,wguser.name as 负责人,ord.name as 单号,s.name as 门店,s.stores_no as 门店编码,pr.name as 产品名称,pr.default_code as 产品编码,
 line.product_qty as 数量,u.name as 单位,line.price_unit as 售价, line.product_qty * line.price_unit as 小计,us1.name as 制单人,
 to_char((ord.date + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 制单日期,us2.name as 审核人, to_char((ord.approved_date + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 审核日期,
 to_char((ord.date_done + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 生效日期,CASE when ord.state = 'draft' then '草稿' when ord.state = 'confirm' then '待审核' when  ord.state = 'done' then '完成' end
  from stock_inventory_line line
 left join stock_inventory ord on line.inventory_id2 = ord.id
 left join (select par.name, us.id from res_users us left join res_partner par on us.partner_id = par.id) us1 on ord.create_user_id = us1.id
 left join (select par.name, us.id from res_users us left join res_partner par on us.partner_id = par.id) us2 on ord.approved_user_id = us2.id
 left join (select t.name,p.default_code,p.id from product_product p left join product_template t on p.product_tmpl_id = t.id) pr on line.product_id = pr.id 
 left join sale_shop s on ord.shop_id = s.id
 left join (
    select p.id, uom.name from product_uom uom left join (
       select t.uom_id,p.id from product_product p left join product_template t on p.product_tmpl_id = t.id) p on p.uom_id = uom.id) u on line.product_id = u.id
 left join (select o.name,s.id from sale_shop s left join wgmf_sale_organization o on s.sale_organization = o.id) wguser on wguser.id = ord.shop_id
   left join (select org.name, org2.shop_id from wgmf_sale_organization org 
                left join (select s.id as shop_id ,o.parent_id from sale_shop s
		                left join wgmf_sale_organization o on s.sale_organization = o.id) org2 on org.id = org2.parent_id) office on office.shop_id = ord.shop_id
   left join (select org6.name, org5.shop_id from wgmf_sale_organization org6
    left join (select org4.parent_id, org3.shop_id from wgmf_sale_organization org4
       left join (select s2.id as shop_id ,o2.parent_id from sale_shop s2
		left join wgmf_sale_organization o2 on s2.sale_organization = o2.id) org3 on org4.id = org3.parent_id) org5 on org6.id = org5.parent_id) area on area.shop_id = ord.shop_id
 where ord.state != 'cancel'
 --注意数据库存储的日期是格林威治时间
 --创建日期
 --and ord.create_date >= '2014-12-31 16:00:00'
 --and ord.create_date <= '2015-01-31 15:59:59'
 --收货日期
 --and ord.wdate <= '2014-12-31 16:00:00'
 --and ord.wdate <= '2015-01-31 15:59:59'
 --审核日期
 --and ord.sh_wdate <= '2014-12-31 16:00:00'
 --and ord.sh_wdate <= '2015-01-31 15:59:59'
 --按组织过滤，将下面的“上海办”更换为对应的组织即可
   and ord.shop_id in (with recursive cte as (  
	    select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id where o.name='专卖店'
	    union all  
	    select b.shop_id,b.name,b.id,b.parent_id from (select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id) b, cte where b.parent_id=cte.id
	    )
	    select shop_id from cte where shop_id is not null and name not in ('积分测试','Your Company'))

 --产品目录盘点数据导出SQL  
 select area.name as 大区,office.name as 办事处,wguser.name as 负责人,ord.name as 单号,s.name as 门店,s.stores_no as 门店编码,pr.name as 产品名称,pr.default_code as 产品编码,
 line.product_qty as 数量,u.name as 单位,line.price_unit as 售价, line.product_qty * line.price_unit as 小计,us1.name as 制单人,
 to_char((ord.date + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 制单日期,us2.name as 审核人, to_char((ord.approved_date + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 审核日期,
 to_char((ord.date_done + interval '8 hours'),'yyyy-mm-dd hh24:mi:ss') as 生效日期,CASE when ord.state = 'draft' then '草稿' when ord.state = 'confirm' then '待审核' when  ord.state = 'done' then '完成' end
  from stock_inventory_line line
 left join stock_inventory ord on line.inventory_id = ord.id
 left join (select par.name, us.id from res_users us left join res_partner par on us.partner_id = par.id) us1 on ord.create_user_id = us1.id
 left join (select par.name, us.id from res_users us left join res_partner par on us.partner_id = par.id) us2 on ord.approved_user_id = us2.id
 left join (select t.name,p.default_code,p.id from product_product p left join product_template t on p.product_tmpl_id = t.id) pr on line.product_id = pr.id 
 left join sale_shop s on ord.shop_id = s.id
 left join (
    select p.id, uom.name from product_uom uom left join (
       select t.uom_id,p.id from product_product p left join product_template t on p.product_tmpl_id = t.id) p on p.uom_id = uom.id) u on line.product_id = u.id
 left join (select o.name,s.id from sale_shop s left join wgmf_sale_organization o on s.sale_organization = o.id) wguser on wguser.id = ord.shop_id
   left join (select org.name, org2.shop_id from wgmf_sale_organization org 
                left join (select s.id as shop_id ,o.parent_id from sale_shop s
		                left join wgmf_sale_organization o on s.sale_organization = o.id) org2 on org.id = org2.parent_id) office on office.shop_id = ord.shop_id
   left join (select org6.name, org5.shop_id from wgmf_sale_organization org6
    left join (select org4.parent_id, org3.shop_id from wgmf_sale_organization org4
       left join (select s2.id as shop_id ,o2.parent_id from sale_shop s2
		left join wgmf_sale_organization o2 on s2.sale_organization = o2.id) org3 on org4.id = org3.parent_id) org5 on org6.id = org5.parent_id) area on area.shop_id = ord.shop_id
 where ord.state != 'cancel'
 --注意数据库存储的日期是格林威治时间
 --创建日期
 --and ord.create_date >= '2014-12-31 16:00:00'
 --and ord.create_date <= '2015-01-31 15:59:59'
 --收货日期
 --and ord.wdate <= '2014-12-31 16:00:00'
 --and ord.wdate <= '2015-01-31 15:59:59'
 --审核日期
 --and ord.sh_wdate <= '2014-12-31 16:00:00'
 --and ord.sh_wdate <= '2015-01-31 15:59:59'
 --按组织过滤，将下面的“上海办”更换为对应的组织即可
   and ord.shop_id in (with recursive cte as (  
	    select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id where o.name='专卖店'
	    union all  
	    select b.shop_id,b.name,b.id,b.parent_id from (select s.id as shop_id,s.name,o.id,o.parent_id from wgmf_sale_organization o left join sale_shop s on s.sale_organization = o.id) b, cte where b.parent_id=cte.id
	    )
	    select shop_id from cte where shop_id is not null and name not in ('积分测试','Your Company'))
        
        
        
        
        
select am.name as 凭证,al.name as 摘要,aa.name as 科目,aa.code as 科目编码,al.debit as 借方,al.credit as 贷方 from account_move_line al 

left join account_move am on am.id=al.move_id

left join account_account aa on aa.id=al.account_id


where al.move_id in (select move_id from account_move_line where period_id in (27,28) and account_id=524 group by move_id) 

order by  al.move_id
        
        
        
        select sl.id from stock_location sl left join stock_warehouse sw  on sw.lot_input_id=sl.id 
                                 left join  sale_shop shop on shop.warehouse_id = sw.id
                                 where shop.wgmf_usable and yymode='DZD'
        
        
select * from (select sum(ps.amount_total) as total ,ps.shop_id as myshop,shop.name as myshopname  from pos_superorder  ps 

        left join sale_shop shop on shop.id=ps.shop_id
        
        where  ps.sale_date<'2015-01-31 23:00:00' and ps.sale_date>='2015-01-31 16:00:00' 
        
        
        group by  ps.shop_id,shop.name) shuju order by total        
        
        
        
        
        
        
        
        
        